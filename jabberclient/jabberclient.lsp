(%%%
	(MODULE-NAME "jabberclient")
	(DECLARE-PUBLIC begin)
	(DECLARE-USED-MODULE "../xml/symb.h")
	(DECLARE-EXTERNAL makeclient)
	(DECLARE-EXTERNAL getconnection)
	(DECLARE-EXTERNAL readmessage)
	(DECLARE-EXTERNAL makeselect)
	(DECLARE-EXTERNAL sendmessage)
	(DECLARE-EXTERNAL printbeauty)
	(DECLARE-EXTERNAL isstring)
	(DECLARE-EXTERNAL getcontent)
	(DECLARE-EXTERNAL getdurtree)
	(DECLARE-EXTERNAL deletetree)
	(DECLARE-EXTERNAL inscont)
	(DECLARE-EXTERNAL newtag)
	(DECLARE-EXTERNAL isxmltag)
	(DECLARE-EXTERNAL xmlattrget)
	(DECLARE-EXTERNAL newattr)
	(DECLARE-EXTERNAL newcontent)
	(DECLARE-EXTERNAL xmlnameget)
	(DECLARE-EXTERNAL newattrlist)
	(DECLARE-EXTERNAL getattrname)
	(DECLARE-EXTERNAL getattrcontent)
	(DECLARE-EXTERNAL insattrcont)
	(DECLARE-EXTERNAL changeclcond)
	(DECLARE-EXTERNAL getclcond)
	(DECLARE-EXTERNAL newtree)
	(DECLARE-EXTERNAL getchipher)
	(DECLARE-EXTERNAL taginstr)
	(DECLARE-EXTERNAL insreadyattr)
	(DECLARE-EXTERNAL concatstr)
	(DECLARE-EXTERNAL strconcatnumb)
	(DECLARE-EXTERNAL numfromstr)
	(DECLARE-EXTERNAL addclinform)
	(DECLARE-EXTERNAL changeclinform)
	(DECLARE-EXTERNAL getclinform)
	(DECLARE-EXTERNAL resetclinform)
	(DECLARE-EXTERNAL generatenum)
	(DECLARE-EXTERNAL dividenum)
        (DECLARE-EXTERNAL printinfilexml)
	(DECLARE-EXTERNAL turnopenfile)
	(DECLARE-EXTERNAL filestream)
	(DECLARE-EXTERNAL getnumberstr)
	(DECLARE-EXTERNAL dividestrsim)
	(DECLARE-EXTERNAL closefilestream)
	(DECLARE-EXTERNAL strfromnumb)
)

;this is start function of jabber-client program
;port - is port number
;ipadr - is ip adress of socket which we want to create
;login - login of jabber-client
;password - password
;jid - jid
(defun begin (port ipadr login password jid)
	(sendhello
		(bindconnectinform (getconnection (makeclient port ipadr)) login password jid)
	)
)

;this function will bind resourses with client
;client - it's client
(defun bindconnectinform(client login password jid)
	(addclinform client "connect_inform"
		(cons (cons "log_file" "log.xml")
			 (cons (cons "login" login) (cons (cons "password" password)(cons (cons "jid" jid) nil))))
	)
	client
)

;___________________________________________________________________
;________________________Work_with_xml-tree_________________________
;___________________________________________________________________

;start to analis tree
(defun startanalis (client changed)
	(start_delete (getdurtree client) client)
	(changetree
		(firststepanalis (getdurtree client) client)
		(deletetree client) changed
	)
)

;after looking throw the tree we make it a little different so we will change it
;client - is created client (here we will work with tree according client's condition)
;tree - is XML-trees
(defun changetree (tree client changed)
	(cond
		((eql (getclcond client) "secondack")
                	(changetree_help tree client changed)
		)
		(t (cons (newtree client tree) (cons changed nil)))
	)
)

(defun changetree_help(tree client changed)
	(cond
		(changed (cons (newtree client tree) (cons t nil)))
		(t (cons (newtree client nil) (cons t nil)))
	)
)

;analis firsttag
;xml - it is getted tag
;client  - it is client
(defun firststepanalis (xml client)
	(analistree (getcontent xml)(newattrlist (newtag (xmlnameget xml))
		(analisattr xml client)) client)
)

;function works with xml-tree
;it finds all unseen tags
;for all tags which are unseen - starts analis-function
;tree - it is our tree
;client -it is client
(defun analistree (tree newone client)
	(cond
		((null tree) newone)
		((isstring tree) (inscont newone tree))
		((isxmltag tree) (inscont newone (analistree (getcontent tree)
			(newattrlist (newtag (xmlnameget tree))(analisattr tree client)) client))
		)
		(t (analistree (cdr tree) (analistree (car tree) newone client) client))
	)
)

;analis xml tag if it has some specific attribute "seen"
;and if there is such attribute functions changes it's meaning
;xml - it is getted xml
;client - it is client
(defun analisattr (xml client)
	(cond
		((findseen (xmlattrget xml))
			(isbuild xml client)
			(changeseen (cdr (xmlattrget xml))(cons (car (xmlattrget xml)) nil))
		)
		(t (xmlattrget xml))
	)
)

;function changes attr seen no yes
;attr - it is list of xml-tag's attr
;newone - it is list of new xml attributes
(defun changeseen (attr newone)
	(cond
		((null attr) newone)
		((eql (getattrname (car attr)) "seen")
			(append(append newone
				(cons (insattrcont (car attr) "yes") nil)) (cdr attr)
			)
		)
		(t (changeseen (cdr attr) (append newone (cons (car attr) nil))))
	)
)

;There are different situations
;these situations depend on unbuild attribute
;xml - it is our message
;client - it is client
(defun isbuild (xml client)
	(cond
		((eql (getattrname (car (xmlattrget xml))) "unbuild")
			(isbuild_help xml client)
		)
		(t (print "error! wrong attrs in isbuild"))
	)
)

(defun isbuild_help (xml client)
	(cond
		((eql (getattrcontent (car (xmlattrget xml))) "no")
			(whichtype xml nil client)
		)
		(t (whichtype xml t client))
	)
)

;function recognises which type of analis is needed
;analis of builded tag or not builded
;xml - it is xml-tag which we analis
;typeb - type of analis
;client - it is client
(defun whichtype (xml typeb client)
	(cond
		(typeb (findcondition client xml))
		(t (findconditioncl client xml))
	)
)

;this function deletes from xml-tree unused xml-tags
;and writes needed in log-file
;tree - xml-tree
;client - it is client
(defun start_delete (tree client)
	(let ((log_file (find_users_data (getclinform client "connect_inform") "log_file")))
		(newtree client (start_delete_messages tree log_file))
	)
)

;this function starts delete-work
;function deletes messages which are old && write them in log
;tree - it's analizing message
(defun start_delete_messages (tree log_file)
	(cond
		((null tree) tree)
		((isstring tree) tree)
		((isxmltag tree) (delete_message tree log_file))
		(t (analise_delete_res
			(start_delete_messages (car tree) log_file)
			(cdr tree) log_file)
		)
        )
)

;this function deletes message from tree and write in file deleted message
;tree - xml-tree
;xml - message which function deletes
;file - file stream in which we will write deleting message
(defun delete_message (tree file)
	(let ((unbuild (getneedattr (xmlattrget tree) "unbuild"))
		(seen (getneedattr (xmlattrget tree) "seen")))
	(cond
		((or (null unbuild) (null seen))
			(print "error!:delete_message no needed attributes in conning message")
			tree
                )
		((or (or (eql unbuild "yes") (eql seen "no")) (and (not (eql "message" (xmlnameget tree)))
			(not (eql "presence" (xmlnameget tree)))))
			(newcontent tree (start_delete_messages (getcontent tree) file))
		)	
		(t (printinfilexml tree file "a") t)
	))
)

(defun analise_delete_res (res tree log_file)
	(cond
		((eql res t) (start_delete_messages tree log_file))
		(t (cons res (start_delete_messages tree log_file)))
        )
)

;___________________________________________________________________
;______________________Connection_to_server_________________________
;___________________________________________________________________

;here we will send server our hello informationinformation
;serv - it's client
(defun sendhello (client)
	(sendmessage client
		"<?xml version='1.0'?>
		<stream to='jabber.ru' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' xml:l='ru' version='1.0'>"
	)
	(waitforanswer client nil)
)

;it's a cycle were we are waiting for acknowlege from serv
;client - it is client
(defun waitforanswer (client changed)
	(endedwork (readmessage (makeselect client)) changed)
)

;it's function were we can learn that work is ended
;work is ended when we get empty message - eof
(defun endedwork (pair changed)
	(cond
		((eql (car pair) -1) (deletetree (cadr pair)))
		(t (waitforanswer (divideansserv (startanalis (cadr pair) changed))))
	)
)

;here we will divide answer for changing tree and client
;pair - is ( tree client ) - it is list which contains two elements
(defun divideansserv (pair)
	(waitforanswer (car pair) (cadr pair))
)

;here we will find client condition
;We use client's conditions to understand in which condition is connection
;when connection would be finished client's condition wouldn't change
;client - it is client
;xml - it is xml-tree
(defun findcondition (client xml)
	(switchcond (getclcond client) client xml)
)

(defun findconditioncl (client xml)
	(switchcondcl (getclcond client) client xml)
)

;we have client's condition and this function finds correct function for it
;here is part for unbuilded xml-tags
;cl_condition - it is string which shows condition of connection
;client - it is client
;xml - it is our xml
(defun switchcond (cl_condition client xml)
	(cond
		((eql cl_condition "mainwork")    (mainwork client xml))
		((eql cl_condition "waitforcon")  (waitforcon client xml))
		((eql cl_condition "firstack")    (firstack client xml))
		((eql cl_condition "authoris")    (authoris client xml))
		((eql cl_condition "givepas")     (givepas client xml))
		((eql cl_condition "allright")    (allright client xml))
		((eql cl_condition "secondack")   (secondack client xml))
		((eql cl_condition "askresource") (askresource client xml))
		((eql cl_condition "statussended")(statussended client xml))
		((eql cl_condition "getlist")     (getlist client xml))
		(t (errorcond xml))
	)
)

;this function finds correct function for client's condition
;it's called, when xml-tag is closed
;cl_condition - it is string which shows condition of connection
;client - it is client
;xml - it is our xml
(defun switchcondcl (cl_condition client xml)
	(cond
		((eql cl_condition "mainwork")    (mainworkcl client xml))
                ((eql cl_condition "firstack")    (firstackcl client xml))
                ((eql cl_condition "authoris")    (authoriscl client xml))
                ((eql cl_condition "givepas")     (givepascl client xml))
		((eql cl_condition "allrigth")    (allrightcl client xml))
		((eql cl_condition "secondack")   (secondackcl client xml))
		((eql cl_condition "askresource") (askresourcecl client xml))
		((eql cl_condition "statussended")(statussendedcl client xml))
		((eql cl_condition "getlist")     (getlistcl client xml))
                (t (errorcond xml))
        )
)

;here is condidtion of error
;something wrong is with our connection, if we are in such condition
;i think that client must be close here
(defun errorcond (xml)
	(print "error! we got error condition of client")
)

;jabbet-client must meet tag with name stream:stream and then
;if it's not close - change on client's condition on "fisrtack"
;client - it is client
;xml - it is xml-tree
(defun waitforcon (client xml)
	(cond
		((eql (xmlnameget xml) "stream:stream") (changeclcond 1 client))
		(t client)
	)
)

;jabber-client must get tag <stream:features>
;(after first ask jabber-client gets features of connection)
;client - it is client
;xml - it is xml-tree
(defun firstack (client xml)
	(cond
		((eql (xmlnameget xml) "stream:features") client)
		(t client)
	)
)

;jabber-client gets stream features
;(in this program author uses DIGEST-MD5 algorithm
;autentification executes according to this algorithm)
;client - is created client
;xml - it is xml-tag
(defun firstackcl (client xml)
	(cond
		((eql (xmlnameget xml) "stream:features")
			(cond
				((findcon xml "DIGEST-MD5") (makeauthr client))
				(t (print "there is no needed mecanism for auth"))
		))
		(t client)
	)
)

;this function sends authorisation-message and changes client's condidtion
;it is part of autentification algorithm (DIGEST-MD5)
;client - it is client
(defun makeauthr (client)
	(sendmessage client "<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='DIGEST-MD5'/>")
	(changeclcond 2 client)
)

;this function returns true if there is needed string somewhere in xml's content
;this function is used to find needed mecanism of autentification (in our case DIGEST-MD5)
;xml - it is xml-tree
;str - it is needed string
;return value - t - if there is needed string somewhere in xml's content
(defun findcon (xml str)
	(cond
		((null xml) nil)
		((isstring xml)(eql xml str))
		((isxmltag xml)(findcon (getcontent xml) str))
		(t (cond
			((findcon (car xml) str) t)
			(t (findcon (cdr xml) str))
		))
	)
)

;this function waits for xml-tag <challenge>
;if it doesn't get it - that means - error
;client - it is client
;xml - it is message which jabber-client got
(defun authoris (client xml)
	(cond
		((eql (xmlnameget xml) "challenge") client)
		(t (print "error! jabber-client didn't get tag <challenge>"))
	)
)

;this function sends answer on authorisation and change condition
;client - it is client
;xml - it is xml-tree
(defun authoriscl (client xml)
	(let ((login (dividestrsim (find_users_data (getclinform client "connect_inform") "login") #\@))
		(password (find_users_data (getclinform client "connect_inform") "password")))
	(cond
		((eql (xmlnameget xml) "challenge") (sendmessage client (getchipher (car (getcontent xml))
			(car login) (cadr login) "2313e069649daa0ca2b76363525059ebd" "xmpp/jabber.ru" password))(changeclcond 3 client))
		(t client)
	))
)

;this funtion waits for tag <challenge>
;client - it is client
;xml - it is message which jabber-client got
(defun givepas (client xml)
	(cond
		((eql (xmlnameget xml) "challenge") client)
		(t (print "error!jabber-client didn't get tag <challenge> (the second time)"))
	)
)

;this function works with built tag<challenge>
; then changes condition and send response again
;if everything is allright
;client - it is client
;xml - it is message
(defun givepascl (client xml)
	(cond
		((eql (xmlnameget xml) "challenge") 
			(sendmessage client "<response xmlns='urn:ietf:params:xml:ns:xmpp-sasl'/>")(changeclcond 4 client))
		(t client)
	)
)

;this function waits for tag <succes>
;if it gets- authentithication is passed
;if function gets success from client - that means that everything is all right
;and login and password were accepted
;client - it is client
;xml - it is message
(defun allright (client xml)
	(cond
		((eql (xmlnameget xml) "success") (print "we get acess") client)
		(t (print "smth wrong - didn't get success") client)
	)
)

;funcion works with built tag <sucess>
;=> prorgam can start it's main work
;now jabber-client will send tag - stream:stream - and some other tags to start corret work
;client - it is client
;xml - it is message
(defun allrightcl (client xml)
	(cond
		((eql (xmlnameget xml) "success")
			(sendmessage client "<?xml version='1.0' encoding='UTF-8'?>
      				<stream:stream to='jabber.ru'
           			xmlns='jabber:client'
           			xmlns:stream='http://etherx.jabber.org/streams'
           			xml:l='ru' version='1.0'>"
			)
			(changeclcond 5 client)
		)
		(t (print "smth wrong! didn't get success") client)
	)
)

;function waits for tag <stream:stream>
;and asks for needed resourses
;it is second acknowlage
;client - it is client
;xml - it is message
(defun secondack (client xml)
	(cond
		((eql (xmlnameget xml) "stream:stream") client)
		(t client)
	)
)

;this function waits for tag <stream:features>
;function sends tag <iq> to bind JID-resource
(defun secondackcl (client xml)
	(cond
		((eql (xmlnameget xml) "stream:features")
			(sendmessage client (askresource_jid client))(changeclcond 6 client))
		(t client)
	)
)

;function asks to bind us JID-resource
;this function constructs correct message to server
(defun askresource_jid (client)
	(let ((jid (dividestrsim (find_users_data (getclinform client "connect_inform") "jid") #\/)))
        (taginstr (insreadyattr(insreadyattr (inscont (newtag "iq")
                (insreadyattr (inscont (newtag "bind") (inscont (newtag "resource") (cdr jid)))
                (newattr "xmlns" "urn:ietf:params:xml:ns:xmpp-bind")))
        (newattr "type" "set")) (newattr "id" "bind_1")))
	)
)

;function waits for answer on inquiry
;client - it is client
;xml - it is message
(defun askresource (client xml)
	(cond
		((eql (xmlnameget xml) "iq") client)
		((eql (xmlnameget xml) "bind") client)
		((eql (xmlnameget xml) "jid") client)
		(t (print "error: we didn't get correct tag on our resourse inquiry"))
	)
)

;this function analises server's answer on inquiry
;and checks that resource was bound
;client - it is client
;xml - it is message
(defun askresourcecl (client xml)
	(cond
		((eql (xmlnameget xml) "iq")
			(isitbind (getcontent xml) (isrosterlist client (getneedattr (xmlattrget xml) "id") "bind_1"))
		)
		((eql (xmlnameget xml) "bind") client)
		((eql (xmlnameget xml) "jid") client)
		((eql (xmlnameget xml) "register") client)
		(t (print "error: smth wrong in answer for our inquiry"))
	)
)

;content must be bind
;an we must have our id
;client - it is client
;xml - it is message
(defun isitbind (xml client)
	(cond
		((eql (xmlnameget (car xml)) "bind")(searchjid (getcontent (car xml)) client))
		(t (print "error:we didn't get bind"))
	)
)

;function searches for needed included tags and strings
;this function will help  to undersand is it our tag or not
;client - it is client
;xml - it is message
(defun searchjid (xml client)
	(cond
		((null xml) (print "error! we didn't get jid"))
		((eql "jid" (xmlnameget (car xml))) (isourjid (getcontent (car xml)) client))
		(t (searchjid (cdr xml) client))
	)
)

;function tests is it our jid or not
;client - it is client
;xml - it is message
(defun isourjid (xml client)
	(let ((jid (find_users_data (getclinform client "connect_inform") "jid")))
	(cond
		((and (isstring (car xml)) (eql (car xml) jid))
				(sendmessage client
					"<iq type='set' id='session_id'>
						<session xmlns='urn:ietf:params:xml:ns:xmpp-session'/>
					</iq>"
				)
				(changeclcond 7 client)
		)
		(t (print "error:smth wrong in jid"))
	))
)

;this function waits for session answer
;serv - it is created socket
;xml - it is message
(defun statussended (client xml)
	client
)

;this function gets complete tag of iq
;and asks for roster-list
;client - it is client
;xml - it is message
(defun statussendedcl (client xml)
	(cond
		((eql (xmlnameget xml) "iq")
			(changeclcond 8
				(sendmessage
					(isrosterlist client (getneedattr (xmlattrget xml) "id") "session_id") 
					(generate_iq client)
				)
			)
		)
		((eql (xmlnameget xml) "bind") client)
                ((eql (xmlnameget xml) "jid") client)
		((eql (xmlnameget xml) "register") client)
		(t (print "error!:jabber-client didn't get roster list") client)
	)
)

;this function generates and sends <iq> message to server
;client - it is client
(defun generate_iq (client)
	(let ((jid (find_users_data (getclinform client "connect_inform") "jid")))
	(taginstr 
		(inscont (insreadyattr (insreadyattr (insreadyattr
			(newtag "iq")
		(newattr "from" jid)) (newattr "type" "get")) (newattr "id" "roster_1"))
		(insreadyattr (newtag "query") (newattr "xmlns" "jabber:iq:roster")))
	)
	)
)

;function waits for roster-list
;client - it is client
;xml - it is message
(defun getlist (client xml)
	client
)

;this function works with roster-list
;After getting roster-list function sends presence
;after getting last connection's information - you can add client's information
;client - it is client
;xml - it gotten message
(defun getlistcl (client xml)
	(cond
		((eql (xmlnameget xml) "iq")(isrosterlist client (getneedattr (xmlattrget xml) "id") "roster_1")
			(sendpresencemes client)
			(insert_inform_for_cl (changeclcond 9 client))
		)
		(t client)
	)
)

;this function tests is it answer on jabber-client's inquiry
;iq - requestions must have id, which jabber-client sends earlier
;client - it is client
;ans - it is id which client got in answer
;str - it is correct id
(defun isrosterlist (client ans str)
	(cond
		((eql ans str) client)
		(t (print "error!: it is not id in iq-tag") client)
	)
)

;___________________________________________________________________
;______________________Work_after_connection________________________
;___________________________________________________________________

;this is the main work of the cycle
;client - it is client
;xml - it is message
(defun mainwork (client xml)
	client
)

;this is work of main cycle of jabber-client
;client - it is client
;xml - it is message
(defun mainworkcl (client xml)
	(cond
		((eql (xmlnameget xml) "message") (message_function client xml))
		((eql (xmlnameget xml) "presence") (issubscribe client (sendstatus xml client) xml) 
			(presence_function client xml)
		)
		((eql (xmlnameget xml) "iq") (isansonsub client xml (getcontent xml))
			(iq_function xml)
		)
		;here you can add xml-tags which you want to analis
		(t client)
	)
)

;___________________________________________________________________
;___________________Subscribe/unsubscride_funcions__________________
;___________________________________________________________________

;this function learns about iq - is it answer on subscription or not
;client - it is client
;xml - it is <iq>
;ans - it is content of xml
(defun isansonsub (client xml ans)
	(cond
		((null ans) client)
		((eql (xmlnameget (car ans)) "query")
			(isfromourserv client xml (getneedattr (xmlattrget xml) "from"))
		)
		(t client)
	)
)

;this functions learns is it information about subscription from server or not
;and if it is - sends on server special message
;client - it is client
;xml - it is message
;ans - it is server's answer
(defun isfromourserv (client xml ans)
	(let ((login (find_users_data (getclinform client "connect_inform") "login")))
	(cond
		((eql ans login) (sendmessage client (taginstr (formackofsub client xml))))
		(t client)
	))
)

;this function sends on server answer that
;jabber-client got information about subscription
;client - it is client
;xml - it is message
(defun formackofsub (client xml)
	(let ((jid (find_users_data (getclinform client "connect_inform") "jid"))
		(login (find_users_data (getclinform client "connect_inform") "login")))
	(insreadyattr
		(insreadyattr
			(insreadyattr
				(insreadyattr (newtag "iq") (newattr "from" jid))
			(newattr "to" login))
		(newattr "type" "result"))
	(newattr "id" (getneedattr (xmlattrget xml) "id")))
	)
)
;is it message of subscription or not?
;if it is message of subscription - jabber-cliens will answer on it
;client - it is client
;xml - it is our xml
;ans - it is content of xml
(defun issubscribe (client ans xml)
	(cond
		((null ans) client)
		(t (sendmessage client (taginstr ans))
			(addcontact client xml (findrosterlist (getdurtree client)))
			(sendsubscription client xml)
		)
	)
)

;function sends  subscription
;client - it is client
;xml - it is our xml
(defun sendsubscription (client xml)
	(let ((login (find_users_data (getclinform client "connect_inform") "login")))
	(sendmessage client (taginstr (insreadyattr 
		(insreadyattr 
			(insreadyattr (newtag "presence") (newattr "from" login))
		(newattr "to" (getneedattr (xmlattrget xml) "from")))
	(newattr "type" "subscribe"))))
	)
)

;function works with roster-list
;and sends message of subscription if it's needed (may be user is already in list)
;client - it is client
;xml - it is our xml
;rlist - it is our roster-list
(defun addcontact (client xml rlist)
	(cond
		((null rlist)(print "error!:no roster-list in during tree"))
		(t (needtoadd client (finduserinrlist rlist (getneedattr (xmlattrget xml) "from"))))
	)
)

;function finds needed user in roster_list
;rlist - it is our roster-list
;name - it is user name
(defun finduserinrlist (rlist name)
	(cond
		((null rlist) name)
		((isxmltag rlist) (finduserinrlist_testtag rlist name))
		((isstring rlist) name)
		(t (finduserinrlist_help rlist name))
	)
)

(defun finduserinrlist_testtag(rlist name)
	(cond
		((eql (xmlnameget rlist) "item")
			(isneededname (getneedattr (xmlattrget rlist) "jid") name)
		)
		(t (finduserinrlist (getcontent rlist) name))
	)
)

(defun finduserinrlist_help(rlist name)
	(let ((res (finduserinrlist (car rlist) name)))
	(cond
		((null res) nil)
		(t (finduserinrlist (cdr rlist) name))
	))
)


;this function tests - is it needed user's name or not
;name - it is user's name
;ans - jid of user in roster lisr
(defun isneededname (ans name)
	(cond
		((eql name ans) nil)
		(t name)
	)
)

;function sends message to add contact in list
;client - it is client
;ans - need to add user or not
(defun needtoadd (client ans)
	(cond
		(ans (sendmessage client (taginstr (fromaddinform ans client))))
		(t client)
	)
)

;function forms correct answer to add a contact
;ans - name of user
;client - it is client
(defun fromaddinform (ans client)
	(let ((login (find_users_data (getclinform client "connect_inform") "login")))
	(inscont
		(insreadyattr 
			(insreadyattr 
				(insreadyattr (newtag "iq") (newattr "from" login))
			(newattr "type" "set")) 
		(newattr "id" "add_1"))
		(inscont
			(insreadyattr (newtag "query") (newattr "xmlns" "jabber:iq:roster"))
			(inscont
				(insreadyattr 
					(insreadyattr (newtag "item") (newattr "jid" ans)) 
				(newattr "name" ans))
			(inscont (newtag "group") "main"))
		)
	))
)

;function finds roster list in during tree
;tree -it is ur xml-tree
(defun findrosterlist (tree)
	(cond
		((null tree) nil)
		((isxmltag tree)
			(cond
				((eql (xmlnameget tree) "query") tree)
				(t (findrosterlist (getcontent tree)))
			)
		)
		((isstring tree) nil)
		(t (isrosterlisthead (findrosterlist (car tree)) (cdr tree)))
	)
)

;function tests incoming tag on being roster list
;ans - it is xml-tag which function must test
;tail - it is tail of xml-tags which program hasn't tested yet
(defun isrosterlisthead (ans tail)
	(cond
		(ans (isitneededquery ans tail (getneedattr (xmlattrget ans) "xmlns")))
		(t (findrosterlist tail))
	)
)

;is it needed tag query
;xml - it is xml-tag which we must test
;tail - it is tail of xml-tags which we haven't tested yet
;it is value of xmlns attr
(defun isitneededquery (xml tail ans)
	(cond
		((eql ans "jabber:iq:roster") xml)
		(t (findrosterlist tail))
	)
)

;functions sends status for all subcsribes
;xml - it is message
;client - it is client
(defun sendstatus (xml client)
	(let ((login (find_users_data (getclinform client "connect_inform") "login")))
	(insstatusattr (insreadyattr (newtag "presence") (newattr "from" login)) xml)
	)
)

;function finds sender
;newone - it is xml-tag which will be sent back
;xml - it is message
(defun insstatusattr (newone xml)
	(hastype (insreadyattr newone (newattr "to" (getneedattr (xmlattrget xml) "from"))) xml
	(getneedattr (xmlattrget xml) "type"))
)

;function learns has presence type or not
;if presence has type - that means - user want to subscribe/unsubscribe us
;newone - it is tag - which we will send back
;ans - it is our answer
;xml - it is our message
(defun hastype (newone xml ans)
	(cond
		((null ans) nil)
		((eql ans "subscribe") (insreadyattr newone (newattr "type" "subscribed")))
		((eql ans "unsubscribe") (insreadyattr newone (newattr "type" "unsubscribed")))
	)
)

;___________________________________________________________________
;________________________Message_functions__________________________
;___________________________________________________________________

;this function sends message to game
;serv - it is created socket
;name -it is name of user which will get our message
;our_name it is our name
;str - it is name of message
;result - we will return serv
(defun send_message_to_serv (serv name our_name str)
	(sendmessage serv (taginstr
		(insreadyattr (insreadyattr (insreadyattr (inscont (newtag "message")
			(inscont (newtag "body") str))
		(newattr "to" name))
		(newattr "from" our_name))
		(newattr "type" "chat")))
	)
)

;this funcion sends server presense messages
(defun sendpresencemes (client)
	(let ((login (find_users_data (getclinform client "connect_inform") "login")))
	(sendmessage client
		(taginstr (inscont(insreadyattr (newtag "presence")
			(newattr "from" login)) (newtag "show")
		))
	)
	(send_presence_message client)
	)
)

;___________________________________________________________________
;__________________________Find_functions___________________________
;___________________________________________________________________

;this function will find client's information according to key
;stat - it is list of pairs ( key . information )
;retun value - needed data or nil if we don't find user
(defun find_users_data (data key)
        (cond
                ((null data) nil)
                ((eql key (caar data)) (cdar data))
                (t (find_users_data (cdr data) key))
        )
)

;function finds unseen attr
;attr - list of attrubutes
;return value - t - if attr seen='no'
;else - nil - seen='yes'
(defun findseen (attr)
	(cond
		((null attr) nil)
		((eql (getattrname (car attr)) "seen")
			(cond
				((eql (getattrcontent (car attr)) "no") T)
				(t nil)
			)
		)
		(t (findseen (cdr attr)))
	)
)

;function finds needed tag in list
;tag - it is tag
;name - name of needed tags
;result list of needed tags
(defun getneedtag (tag name)
        (cond
                ((null tag) nil)
                ((isxmltag (car tag)) (iscorrecttag (car tag) name (cdr tag)))
		(t (getneedtag (cdr tag) name))
	)
)

;function compears name of tag with needed name
;this is help function to function getneeded tag
;tag - it is tag
;name - name of needed tags
;tail - tags which we haven't analised yet
(defun iscorrecttag (tag name tail)
	(cond
		((eql name (xmlnameget tag)) tag)
		(t (getneedtag tail name))
	)
)

;function finds attr with needed name
;attr - list of attrs
;name - name of needed attr
;result - content of needed attr or nil
(defun getneedattr (attr name)
	(cond
		((null attr) nil)
		((eql name (getattrname (car attr))) (getattrcontent (car attr)))
		(t (getneedattr (cdr attr) name))
        )
)

;___________________________________________________________________
;_____________________HERE_WILL_BE_YOUR_CODE________________________
;___________________________________________________________________

;in this function you can insert needed status
;if you need you can save status as one of special client's parameters
;if you want - you can ignore this function
;client - it's client
(defun send_presence_message(client)
	(sendmessage client
		(taginstr (inscont (newtag "presence")
			(inscont (newtag "status") "Your status")
		))
	)
)

;function which works with tags <message>
;client -it is client
;xml - it is message-tag
(defun message_function (client xml)
	;here you can insert your code
	(print "Bot got a message-tag")
)

;function which works with tags <presence>
;client -it is client
;xml - it is presence-tag
(defun presence_function(client xml)
	;here you can insert your code
	(print "Bot got a presence-tag")
)

;function which works with tags <iq>
;client -it is client
;xml - it is iq-tag
(defun iq_function(client xml)
	;here you can insert your code
	(print "Bot got iq-tag")
)

;in this function you can insert needed information
;which will be stored in client
;client - it's client
;return values must be your client
(defun insert_inform_for_cl (client)
        ;for example
        ;(addclinform serv "my_special_file" "my_secial_file's_name")
        client
)
