READ-ME file for jabber-client.

Files from directory jabberclient realize jabber-client.

You can build jabber-client by using command:
make jabberclient

You can run jabber-client by using command:
./jabberclient param1 param2 param3
param1 - login (user's JID-node@user's JID-domain  annie1993@jabber.ru) 
param2 - password
param3 - login with JID-resource (annie1993@jabber.ru/example)

File jabberclient.lsp is divided on 7 code sections. 
These sections are listed below with relevant description for the most important functions.

1. Work_with_xml-tree
There are functions, which work with xml-tree.
Xml-tree is built during jabber-client's work.

2. Connection_to_server
There are functions, which provide connection to server.

3. Work_after_connection
There are functions, which provide work with xml-tags after connection to server.

mainworkcl - this function gets completely built xml-tags.
Here you can use your own functions to process them,
and you can add tags, which you want to process.

4. Subscribe/unsubscribe_funcions
There are functions, which subscribe/unsubscribe users.

5. Message_functions
There are functions, which send messages.

send_message_to_serv - sends message to server.
Here is xml-tag which will be sent to server:
<message to=name from=our_name type="chat">
    <body>
        str
    </body>
</message>

sendpresencemes - sends status-message to server.

6. Find_functions
There are functions, which find needed xml-tags and xml-attributes.

7. HERE_WILL_BE_YOUR_CODE

You can insert your own functions and complete existing functions in this section.
There are already some functions here (you can complete them):

send_presence_message - sends status-message

presence_function - processes xml-tag <presence>

iq_function - processes xml-tag <iq>

message_function - processes xml-tag <message>

insert_inform_for_cl - this function adds specific for your jabber-client information/preferences.
For example, (addclinform serv "my_special_file" "my_secial_file's_name").
"my_special_file" is a key of inserted information.
After adding you can get needed information like this:
(getclinform client "my_special_file")
