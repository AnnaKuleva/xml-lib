#include <intelib/sexpress/sexpress.hpp>
#include <intelib/sexpress/iexcept.hpp>
#include <intelib/sexpress/sstring.hpp>
#include <intelib/tools/sstream.hpp>
#include <intelib/lisp/lisp.hpp>
#include <intelib/lisp/lsymbol.hpp>
#include <intelib/lfun_std.hpp>

#include "../xml/servforlisp.h"
#include "../xml/lispint.h"
#include "../xslt/xsltlib.h"
#include "jabberclient.hxx"
#include "../xml/mistake.h"

//LSymbol XSLTTRANSFORMATION("XSLTTRANSFORMATION");

int main(int argc, char *argv[]){
	//XSLTTRANSFORMATION->SetFunction(xslt_transfomration_xml);
	define_xml_functions();
	define_attr_functions();
	define_string_functions();
	define_number_functions();
	define_file_functions();
	define_print_functions();
	define_client_functions();
	LListConstructor L;
	int port=5222;
	if(argc!=4){
		printf("Error!:wrong number of arguments %d\n", argc);
		return 0;
	}
        try{
                printf("here will start jabber's client work\n");
                LispInit_jabberclient();
                SStreamStdout lstdout;
                //(L|BEGIN, 5222, "77.88.57.178").Evaluate();
		(L|BEGIN, 5222, "95.108.194.209", argv[1], argv[2], argv[3]).Evaluate();
		//(L|BEGIN, 5222, "87.250.251.79").Evaluate();
                printf("work is ended\n");
        }
        catch(IntelibX &x) {
                printf("\nCaught IntelibX: %s\n", x.Description() );
                if(x.Parameter().GetPtr()) {
                        printf("%s\n", x.Parameter()->TextRepresentation().c_str());
                }
                if(x.Stack().GetPtr()) {
                        printf("%s\n", x.Stack()->TextRepresentation().c_str());
                }
        }
        catch(DontEnd &b){
                b.print();
        }
        catch(TurnMistake &b){
                b.sendmes(port);
                b.print();
        }
        catch(BadSimbol &b){
                b.sendmes(port);
                b.print();
        }
        catch(ErrorInForming &b){
                b.sendmes(port);
                b.print();
        }
        catch(EmptyListOfLex &b){
                b.sendmes(port);
                b.print();
        }
        catch(Disbalance &b){
                b.sendmes(port);
                b.print();
        }
        catch(ServPartErrors &b){
                b.print();
                b.print();
        }
        catch(const char *s){
                printf("%s\n", s);
        }
        catch(...) {
                printf("Something strange caught\n");
        }
        return 0;
}

