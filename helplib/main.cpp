#include <stdio.h>
#include "mylist.h"

int main(){
	int arr[]={1, 2, 3, 4, 5};
	char arr1[]={'a' ,'b', 'c', 'd', 'e'};
	struct mylist<int> *cur=NULL, *p;
	struct mylist<char> *cur1=NULL, *p1;
	for(int i=0; i<5; i++){
		cur=insert_end<int>(cur, arr[i]);
	}
	p=cur;
	for(int i=0; i<5; i++){
		printf("%d ", cur->value);
		cur=cur->next;
	}
	printf("\n");
	clean_list<int>(p);
	for(int i=0; i<5; i++){
                cur1=insert_end<char>(cur1, arr1[i]);
        }
	p1=cur1;
        for(int i=0; i<5; i++){
                printf("%c ", cur1->value);
                cur1=cur1->next;
        }
	char *str=getstrlistmy(p1);
	printf("string [%s]\n", str);
	return 0;
}
