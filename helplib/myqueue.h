#ifndef _KULEVA_A_S_MY_LIB_QUEUE_
#define _KULEVA_A_S_MY_LIB_QUEUE_

template <class Type>
class MyQueue{
	struct my_queue{
		Type value;
		my_queue *next;
	};
	my_queue *queue;
public:
	MyQueue():queue(NULL){};
	~MyQueue();
	void push(Type ins);
	Type pop();
	Type backpop();
	bool isempty(){return queue==NULL;}
};

template <class Type>
void MyQueue<Type>::push(Type ins){
	struct my_queue *q, *p;
	q=new my_queue;
	q->value=ins;
	q->next=NULL;
	if(queue==NULL){
		queue=q;
		return;
	}
	p=queue;
	while(p->next!=NULL)
		p=p->next;
	p->next=q;
}

template <class Type>
Type MyQueue<Type>::pop(){
	struct my_queue *q;
	q=queue;
	queue=queue->next;
	q->next=NULL;
	Type res=q->value;
	delete q;
	return res;
}

template <class Type>
Type MyQueue<Type>::backpop(){
	struct my_queue *q, *p;
	q=queue;
	if(queue==NULL)
		return NULL;
	while(q->next!=NULL)
		q=q->next;
	if(q==queue)
		queue=NULL;
	else{
		p=queue;
		while(p->next->next!=NULL)
			p=p->next;
		p->next=NULL;
	}
	Type res=q->value;
	delete q;
	return res;
}
	

template <class Type>
MyQueue<Type>::~MyQueue(){
	struct my_queue *m;
	while(queue!=NULL){
		m=queue;
		queue=queue->next;
		delete m;
	}
}

#endif
