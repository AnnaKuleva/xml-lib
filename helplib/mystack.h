#ifndef _KULEVA_A_S_MY_STACK_
#define _KULEVA_A_S_MY_STACK_

#include <stdio.h>

template<class Type>
class MyStack{
	struct my_stack{
	       	Type value;
        	struct my_stack *next;
	};
	my_stack *stack;
public:
	MyStack():stack(NULL){}
	~MyStack();
	void push(Type ins);
	Type pop();
	bool isempty(){return stack==NULL;}
	void print(){}
};

template <class Type>
void MyStack<Type>::push(Type ins){
	struct my_stack *q;
	q=new my_stack;
	q->value=ins;
	q->next=stack;
	stack=q;
}

template <class Type>
Type MyStack<Type>::pop(){
	if(stack==NULL)
		throw "StackError!:empty stack\n";
	struct my_stack *q;
	q=stack;
	stack=stack->next;
	q->next=NULL;
	Type res=q->value;
	delete q;
	return res;
}

template <class Type>
MyStack<Type>::~MyStack(){
	struct my_stack *n;
	while(stack!=NULL){
		n=stack;
		stack=stack->next;
		delete n;
	}
}
	
#endif
