#ifndef _MYtype_LISType_H_KULEVA_A_S_
#define _MYtype_LISType_H_KULEVA_A_S_

#include <stdio.h>

template <class Type>
struct mylist{
	Type value;
	struct mylist *next;
};

template <class Type, class Ytype>
struct mylist_2{
        Type value;
	Ytype value1;
        struct mylist_2 *next;
};

template <class Type>
struct mylist<Type> *insert_head(struct mylist<Type> *cur, Type val){
        struct mylist<Type> *p;
        p=new mylist<Type>;
        p->value=val;
        p->next=cur;
        return p;
}

template <class Type, class Ytype>
struct mylist_2<Type, Ytype> *insert_head(struct mylist_2<Type, Ytype> *cur, Type val, Ytype val1){
        struct mylist_2<Type, Ytype> *p;
        p=new mylist_2<Type, Ytype>;
        p->value=val;
	p->value1=val1;
        p->next=cur;
        return p;
}

template <class Type>
struct mylist<Type> *insert_end(struct mylist<Type> *cur, Type val){
        struct mylist<Type> *p, *q;
        p=new mylist<Type>;
        p->value=val;
        p->next=NULL;
        q=cur;
        if(q==NULL)
                return p;
        while(q->next!=NULL)
                q=q->next;
        q->next=p;
        return cur;
}

template <class Type>
struct mylist<Type> *insert_end(struct mylist<Type> *cur, struct mylist<Type> *ins){
	struct mylist<Type> *q;
	q=cur;
	if(q==NULL)
		return ins;
	while(q->next!=NULL)
		q=q->next;
	q->next=ins;
	return cur;
}

template <class Type, class Ytype>
struct mylist_2<Type, Ytype> *insert_end(struct mylist_2<Type, Ytype> *cur, Type val, Ytype val1){
        struct mylist_2<Type, Ytype> *p, *q;
        p=new mylist_2<Type, Ytype>;
        p->value=val;
	p->value1=val1;
        p->next=NULL;
        q=cur;
        if(q==NULL)
                return p;
        while(q->next!=NULL)
                q=q->next;
        q->next=p;
        return cur;
}

template <class Type, class Ytype>
struct mylist_2<Type, Ytype> *insert_end(struct mylist_2<Type, Ytype> *cur, struct mylist_2<Type, Ytype> *ins){
	struct mylist_2<Type, Ytype> *q;
	q=cur;
	if(q==NULL)
		return ins;
	while(q->next!=NULL)
		q=q->next;
	q->next=ins;
	return cur;
}

template <class Type>
void clean_list(struct mylist<Type> *cur){
        struct mylist<Type> *q;
        while(cur!=NULL){
                q=cur;
                cur=cur->next;
                delete q;
        }
}

template <class Type, class Ytype>
void clean_list(struct mylist_2<Type, Ytype> *cur){
        struct mylist_2<Type, Ytype> *q;
        while(cur!=NULL){
                q=cur;
                cur=cur->next;
                delete q;
        }
}

char *getstrlistmy(struct mylist<char> *cur);

#endif
