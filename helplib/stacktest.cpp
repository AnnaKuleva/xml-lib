#include <stdio.h>
#include "mystack.h"

int main(){
	MyStack<const char *> *x;
	x=new MyStack<const char *>;
	x->push("hello!");
	x->push("my");
	x->push("dear");
	printf("%s\n", x->pop());
	printf("%s\n", x->pop());
	x->push("world");
	printf("%s\n", x->pop());
	printf("%s\n", x->pop());
	return 0;
}
