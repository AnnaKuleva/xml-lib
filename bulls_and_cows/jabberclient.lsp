(%%%
	(MODULE-NAME "jabberclient")
	(DECLARE-PUBLIC begin)
	(DECLARE-USED-MODULE "../xml/symb.h")
	(DECLARE-EXTERNAL makeclient)
	(DECLARE-EXTERNAL getconnection)
	(DECLARE-EXTERNAL readmessage)
	(DECLARE-EXTERNAL makeselect)
	(DECLARE-EXTERNAL sendmessage)
	(DECLARE-EXTERNAL printbeauty)
	(DECLARE-EXTERNAL isstring)
	(DECLARE-EXTERNAL getcontent)
	(DECLARE-EXTERNAL getdurtree)
	(DECLARE-EXTERNAL deletetree)
	(DECLARE-EXTERNAL inscont)
	(DECLARE-EXTERNAL newtag)
	(DECLARE-EXTERNAL isxmltag)
	(DECLARE-EXTERNAL xmlattrget)
	(DECLARE-EXTERNAL newattr)
	(DECLARE-EXTERNAL newcontent)
	(DECLARE-EXTERNAL xmlnameget)
	(DECLARE-EXTERNAL newattrlist)
	(DECLARE-EXTERNAL getattrname)
	(DECLARE-EXTERNAL getattrcontent)
	(DECLARE-EXTERNAL insattrcont)
	(DECLARE-EXTERNAL changeclcond)
	(DECLARE-EXTERNAL getclcond)
	(DECLARE-EXTERNAL newtree)
	(DECLARE-EXTERNAL getchipher)
	(DECLARE-EXTERNAL taginstr)
	(DECLARE-EXTERNAL insreadyattr)
	(DECLARE-EXTERNAL concatstr)
	(DECLARE-EXTERNAL strconcatnumb)
	(DECLARE-EXTERNAL numfromstr)
	(DECLARE-EXTERNAL addclinform)
	(DECLARE-EXTERNAL changeclinform)
	(DECLARE-EXTERNAL getclinform)
	(DECLARE-EXTERNAL resetclinform)
	(DECLARE-EXTERNAL generatenum)
	(DECLARE-EXTERNAL dividenum)
        (DECLARE-EXTERNAL printinfilexml)
	(DECLARE-EXTERNAL turnopenfile)
	(DECLARE-EXTERNAL filestream)
	(DECLARE-EXTERNAL getnumberstr)
	(DECLARE-EXTERNAL dividestrsim)
	(DECLARE-EXTERNAL closefilestream)
	(DECLARE-EXTERNAL strfromnumb)
)

;this is start function of jabber-client program
;port - is port number
;ipadr - is ip adress of socket which we want to create
;login - login of jabber-client
;password - password
;jid - jid
(defun begin (port ipadr login password jid)
	(sendhello
		(bindconnectinform (getconnection (makeclient port ipadr)) login password jid)
	)
)

;this function will bind resourses with client
(defun bindconnectinform(serv login password jid)
	(addclinform serv "connect_inform"
		(cons (cons "log_file" "log.xml")
			 (cons (cons "login" login) (cons (cons "password" password)(cons (cons "jid" jid) nil))))
	)
	serv
)

;here we will send server our hello informationinformation
;serv - is created socket
(defun sendhello (serv)
	(sendmessage serv"<?xml version='1.0'?><stream to='jabber.ru' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' xml:l='ru' version='1.0'>")
	(waitforanswer serv nil)
)

;it's a cycle were we are waiting for acknowlege from serv
;serv - it is created socket
;changed - 
(defun waitforanswer (serv changed)
	(endedwork (readmessage (makeselect serv)) changed)
)

;it's function were we can learn that work is ended
;work is ended when we get empty message - eof
;pair -
;changed - 
(defun endedwork (pair changed)
	(cond
		((eql (car pair) -1) (deletetree (cadr pair)))
		(t (waitforanswer (divideansserv (startanalis (cadr pair) changed))))
	)
)

;here we will divide answer for changing tree and serv
;pair - is ( tree serv ) - it is list which contains two elements
;tree - is out xml-tree
;serv - is created socket
(defun divideansserv (pair)
	(waitforanswer (car pair) (cadr pair))
)

;start to analis treetree
;serv - created socket
;changed -
(defun startanalis (serv changed)
	(print "startanalis")
	(print (taginstr (getdurtree serv)))
	(start_delete (getdurtree serv) serv)
	(changetree (firststepanalis (getdurtree serv) serv) (deletetree serv) changed)
)

;after looking throw the tree we make it a little different so we will change it
;serv - is created socket (here we will work with tree according serv condition)
;tree - is XML-trees
;changed - is
(defun changetree (tree serv changed)
	(cond
		((eql (getclcond serv) "secondack")
			(cond
				(changed (cons (newtree serv tree) (cons t nil)))
				(t (cons (newtree serv nil) (cons t nil)))
			)
		)
		(t (cons (newtree serv tree) (cons changed nil)))
	)
)

;help-function print our tree
;xml - xml-tree
(defun printxml (xml) (printx xml 0))

;second function for print xmlxml
;xml - xml-tree
(defun printx (xml depth)
	(cond	((null xml) t)
		((isstring xml) (printbeauty xml depth))
		((atom xml) (printbeauty xml depth)
			(printx (getcontent xml) (+ 1 depth)))
		(t (printx (car xml) depth) (printx (cdr xml) depth))
	)
)

;here we will find client condition
;We use client's conditions to understand on which step in connection
;after connection serv-cond wouldn't change
;serv - is created socket
;xml - is xml-tree
(defun findcondition (serv xml)
	(switchcond (getclcond serv) serv xml)
)

(defun findconditioncl (serv xml)
	(switchcondcl (getclcond serv) serv xml)
)

;we have condition of client and we must find correct function for it
;here is part for unbuilded xml-tags
;sost - it is string which shows condition of connection
;serv - it is created socket
;xml - it is our xml
(defun switchcond (sost serv xml)
	(cond
		((eql sost "mainwork")(mainwork serv xml))
		((eql sost "waitforcon")(waitforcon serv xml))
		((eql sost "firstack")(firstack serv xml))
		((eql sost "authoris")(authoris serv xml))
		((eql sost "givepas")(givepas serv xml))
		((eql sost "allright")(allright serv xml))
		((eql sost "secondack")(secondack serv xml))
		((eql sost "askresource")(askresource serv xml))
		((eql sost "statussended")(statussended serv xml))
		((eql sost "getlist")(getlist serv xml))
		(t (errorcond xml))
	)
)

;this function we will call when we will get comlete xml-tag
;sost - it is string which shows condition of connection
;serv - it is created socket
;xml - it is our xml
(defun switchcondcl (sost serv xml)
	(prin1 "switch close ")(print sost)
	(cond
		((eql sost "mainwork")(mainworkcl serv xml))
                ((eql sost "firstack")(firstackcl serv xml))
                ((eql sost "authoris")(authoriscl serv xml))
                ((eql sost "givepas") (givepascl serv xml))
		((eql sost "allrigth") (allrightcl serv xml))
		((eql sost "secondack") (secondackcl serv xml))
		((eql sost "askresource") (askresourcecl serv xml))
		((eql sost "statussended") (statussendedcl serv xml))
		((eql sost "getlist")(getlistcl serv xml))
                (t (errorcond xml))
        )
)

;here is condidtion of error
;something wrong is with our connection, if we are in such condition
;i think that client must be close here
(defun errorcond (xml)
	(print "error! we got error condition of client")
)


;it's fisrt part of our working program
;we must meet here tag with name stream:stream and then
;if it's not close - change on fisrtack condition
;serv - it is created socket
;xml - it is xml-tree
(defun waitforcon (serv xml)
	(print "get open stream change")
	(cond
		((eql (xmlnameget xml) "stream:stream") (changeclcond 1 serv))
		(t serv)
	)
)

;here we must get features
;after first ask we must get features of connection
;serv - it is created socket
;xml - it is xml-tree
(defun firstack (serv xml)
	(cond
		((eql (xmlnameget xml) "stream:features") serv)
		(t serv)
	)
)

;if we have needed mecanism for autentification
;we will do everything according to algorithm
;serv - is created socket
;xml - it is xml-tree
(defun firstackcl (serv xml)
	(print "we will change sost on second")
	(cond
		((eql (xmlnameget xml) "stream:features")
			(cond
				((findcon xml "DIGEST-MD5") (makeauthr serv))
				(t (prin1 "there is no needed mecanism for auth"))
		))
		(t serv)
	)
)

;here we will send message about authirisation and change condidtion
;it is part of autentification algorithm - here we will choose DIGEST-MD5
;serv - it is created socket
(defun makeauthr (serv)
	(print "send message for auth")
	(sendmessage serv "<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='DIGEST-MD5'/>")
	(changeclcond 2 serv)
)

;this function will give true if we have such string somewhere in content
;this function is used to find needed mecanism of autentification (in our case DIGEST-MD5)
;xml - it is xml-tree
;str - it is needed string
;return value - t - if we find needed string somewhere in xml-content
(defun findcon (xml str)
	(cond
		((null xml) nil)
		((isstring xml)(eql xml str))
		((isxmltag xml)(findcon (getcontent xml) str))
		(t (cond
			((findcon (car xml) str) t)
			(t (findcon (cdr xml) str))
		))
	)
)

;here we must get challenge
;if we didn't get it - that means - error
;serv - it is created socket
;xml - it is message which we got
(defun authoris (serv xml)
	(cond
		((eql (xmlnameget xml) "challenge") (prin1 "we get challenge") serv)
		(t (print "error! didn't get challenge"))
	)
)

;here we must send answer on authorisation and change condition
;serv - it is created socket
;xml - it is xml-tree
(defun authoriscl (serv xml)
	(print "challenge closed and we must send answer")
	(let ((login (dividestrsim (find_users_data (getclinform serv "connect_inform") "login") #\@))
		(password (find_users_data (getclinform serv "connect_inform") "password")))
	(cond
		((eql (xmlnameget xml) "challenge") (sendmessage serv (getchipher (car (getcontent xml))
			(car login) "jabber.ru" "2313e069649daa0ca2b76363525059ebd" "xmpp/jabber.ru" password))(changeclcond 3 serv))
		(t serv)
	))
)

;here we must get challenge
;here is the second challenge
;serv - it is created socket
;xml - it is message which we got
(defun givepas (serv xml)
	(cond
		((eql (xmlnameget xml) "challenge") (prin1 "we get challenge again") serv)
		(t (prin1 "error! didn't get challenge"))
	)
)

;here we must look is code from server correct
;and then change sost and send response again
;if everything is allright
;serv - it is created socket
;xml - it is message
(defun givepascl (serv xml)
	(cond
		((eql (xmlnameget xml) "challenge") (prin1 (car (getcontent xml))) 
			(sendmessage serv "<response xmlns='urn:ietf:params:xml:ns:xmpp-sasl'/>")(changeclcond 4 serv))
		(t serv)
	)
)

;un this function we will wait for tag succes
;if we get it - we get authentithication
;here we got success from serv - that means that everything is all right
;and our login and password are accepted
;serv - it is created socket
;xml - it is message
(defun allright (serv xml)
	(print "allrigthf")
	(cond
		((eql (xmlnameget xml) "success") (print "we get acess") serv)
		(t (print "smth wrong - didn't get success") serv)
	)
)

;here we get all tag acces
;=>we can start our main work
;now we must send tag - stream:stream - and some other tags to start corret work
;serv - it is created socket
;xml - it is message
(defun allrightcl (serv xml)
	(print "allrigthtclf")
	(cond
		((eql (xmlnameget xml) "success")
			(print "WE GET SUCCESS!")
			(sendmessage serv "<?xml version='1.0' encoding='UTF-8'?>
      				<stream:stream to='jabber.ru'
           			xmlns='jabber:client'
           			xmlns:stream='http://etherx.jabber.org/streams'
           			xml:l='ru' version='1.0'>"
			)
			(changeclcond 5 serv)
		)
		(t (prin1 "smth wrong! didn't get success") serv)
	)
)

;here we must get opening stream for information
;and ask for needed resourses
;it is our second acknowlage
;serv - it is created socket
;xml - it is message
(defun secondack (serv xml)
	(prin1 "we wait for stream:stream")
	(cond
		((eql (xmlnameget xml) "stream:stream") serv)
		(t serv)
	)
)

;here we got features - and they are comletely
;now we will send tag <iq> to bind JID-resource
(defun secondackcl (serv xml)
	(cond
		((eql (xmlnameget xml) "stream:features")(print "stream:features closed")
			(sendmessage serv (askresource_jid serv))(changeclcond 6 serv))
		(t serv)
	)
)

;here we will ask to bind us JID-resource
;this function construct correct message to server
(defun askresource_jid (serv)
	(let ((jid (dividestrsim (find_users_data (getclinform serv "connect_inform") "jid") #\/)))
        (taginstr (insreadyattr(insreadyattr (inscont (newtag "iq")
                (insreadyattr (inscont (newtag "bind") (inscont (newtag "resource") (cdr jid)))
                (newattr "xmlns" "urn:ietf:params:xml:ns:xmpp-bind")))
        (newattr "type" "set")) (newattr "id" "bind_1")))
	)
)

;here we must get answer on our inquiry
;serv - it is created socket
;xml - it is message
(defun askresource (serv xml)
	(prin1 "askresourse open")
	(cond
		((eql (xmlnameget xml) "iq") serv)
		((eql (xmlnameget xml) "bind") serv)
		((eql (xmlnameget xml) "jid") serv)
		(t (prin1 "error: we didn't get correct tag on our resourse inquiry"))
	)
)

;here we will analis server answer on our inquiry
;and we must check that resource was bound
;serv - it is created socket
;xml - it is message
(defun askresourcecl (serv xml)
	(cond
		((eql (xmlnameget xml) "iq")(isitbind (getcontent xml) (isrosterlist serv (getneedattr (xmlattrget xml) "id") "bind_1")))
		((eql (xmlnameget xml) "bind") serv)
		((eql (xmlnameget xml) "jid") serv)
		(t (prin1 "error: smth wrong in answer for our inquiry"))
	)
)

;content must be bind
;an we must have our id
;serv - it is created socket
;xml - it is message
(defun isitbind (xml serv)
	(cond
		((eql (xmlnameget (car xml)) "bind")(searchjid (getcontent (car xml)) serv))
		(t (prin1 "error:we didn't get bind"))
	)
)

;here we will look for needed included tags and strings
;this function will help us to undersant is it our tag or not
;serv - it is created socket
;xml - it is message
(defun searchjid (xml serv)
	(cond
		((null xml) (prin1 "error! we didn't get jid"))
		((eql "jid" (xmlnameget (car xml))) (isourjid (getcontent (car xml)) serv))
		(t (searchjid (cdr xml) serv))
	)
)

;here we will look is it our jid or not
;serv - it is created socket
;xml - it is message
(defun isourjid (xml serv)
	(print "we get jid")
	(let ((jid (find_users_data (getclinform serv "connect_inform") "jid")))
	(cond
		((and (isstring (car xml)) (eql (car xml) jid))
				(sendmessage serv 
					"<iq type='set' id='session_id'><session xmlns='urn:ietf:params:xml:ns:xmpp-session'/></iq>")(changeclcond 7 serv)
		)
		(t (prin1 "error:smth wrong in jid"))
	))
)

;here we are waiting for session answer
;serv - it is created socket
;xml - it is message
(defun statussended (serv xml)
	(print "open statussended")
	serv
)

;this function get complete tag of iq
;and we will ask roster-list now
;serv - it is created socket
;xml - it is message
(defun statussendedcl (serv xml)
	(print "close statusended")
	(cond
		((eql (xmlnameget xml) "iq") (changeclcond 8
		(sendmessage (isrosterlist serv (getneedattr (xmlattrget xml) "id") "session_1") 
				(generate_iq serv)
			))
		)
		(t (print "error!:we didn't get roster list") serv)
	)
)

;this function will generate and send id message to server
;;serv - it is created socket
(defun generate_iq (serv)
	(let ((jid (find_users_data (getclinform serv "connect_inform") "jid")))
	(taginstr 
		(inscont (insreadyattr (insreadyattr (insreadyattr
			(newtag "iq")
		(newattr "from" jid)) (newattr "type" "get")) (newattr "id" "roster_1"))
		(insreadyattr (newtag "query") (newattr "xmlns" "jabber:iq:roster")))
	)
	)
)

;here we are waiting for roster-list
;serv - it is created socket
;xml - it is message
(defun getlist (serv xml)
	(print "getlist")
	serv
)

;here we must work with our roster-list
;here we got  roster-list and will send presence
;after getting last connection information - we will add client's information
;here i will add files about game files
;where we will save game_information
;serv - it is created socket
;xml - it getted message
(defun getlistcl (serv xml)
	(print "close gentlist")
	(cond
		((eql (xmlnameget xml) "iq")(isrosterlist serv (getneedattr (xmlattrget xml) "id") "roster_1")
			(sendpresencemes serv)
			(addclinform (changeclcond 9 serv) "game_files"
				(cons (cons "help_xslt" "game_data/help_file.txt") (cons (cons "help_xml" "game_data/help_file.xml")
				(cons (cons "rules_file" "game_data/rules.xml") nil)))
			)
		)
		(t serv)
	)
)

;this funcion will send server presense messages
(defun sendpresencemes (serv)
	(let ((login (find_users_data (getclinform serv "connect_inform") "login")))
	(sendmessage serv
		(taginstr (inscont (insreadyattr (newtag "presence") (newattr "from" login)) (newtag "show")))
	)
	(sendmessage serv "<presence><status>Cows and bulls! Let's play!</status></presence>")
	)
)

;here we will learn is it our answer
;iq - requestions must have such id - which we send then earlier
;serv - it is created socket
;ans - it is id which we got in answer
;str - it is our id
(defun isrosterlist (serv ans str)
	(cond
		((eql ans str) serv)
		(t (print "error!: it is not or id in iq-tag")serv)
	)
)

;here is the main work of the cycle
;;serv - it is created socket
;xml - 
(defun mainwork (serv xml)
	serv
)

;this is work of main cycle of our jabber-client
;serv - it is creted serv
;xml - it is message
(defun mainworkcl (serv xml)
	(cond
		;((eql (xmlnameget xml) "message") (sendanswer (insreadyattr(newtag "message")
		;		(newattr "from" "annie1993@jabber.ru/text")) xml serv))
		((eql (xmlnameget xml) "message") (game_cows_and_bulls serv xml (getcontent xml)))
		((eql (xmlnameget xml) "presence") (issubscribe serv (sendstatus xml serv) xml) (start_presence_cows_and_bulls serv xml))
		((eql (xmlnameget xml) "iq") (isansonsub serv xml (getcontent xml)))
		(t serv)
	)
)

;in this function we will learn about iq - is it answer on subscription or not
;serv - it is created socket
;xml - it is our xml
;ans - it is content of xml
(defun isansonsub (serv xml ans)
	(cond
		((null ans) serv)
		((eql (xmlnameget (car ans)) "query") (isfromourserv serv xml (getneedattr (xmlattrget xml) "from")))
		(t serv)
	)
)

;here we will learn is it really information about subscription from server or not
;and if it is - we will send on serv special message
;serv - it is created socket
;xml - it is message
;ans - it is our serv
(defun isfromourserv (serv xml ans)
	(let ((login (find_users_data (getclinform serv "connect_inform") "login")))
	(cond
		((eql ans login) (sendmessage serv (taginstr (formackofsub serv xml))))
		(t serv)
	)
	)
)

;here we will send server answer that we get information about subscription
;serv - it is created socket
;xml - it is message
(defun formackofsub (serv xml)
	(let ((jid (find_users_data (getclinform serv "connect_inform") "jid"))
		(login (find_users_data (getclinform serv "connect_inform") "login")))
	(insreadyattr
		(insreadyattr
			(insreadyattr
				(insreadyattr (newtag "iq") (newattr "from" jid))
			(newattr "to" login))
		(newattr "type" "result"))
	(newattr "id" (getneedattr (xmlattrget xml) "id")))
	)
)

;this function will find client's information according to key
;stat - it is list of pairs ( key . information )
;retun value - needed data or nil if we don't find user
(defun find_users_data (data key)
	(print "find_users_data")
	(print data)
	(print key)
	(cond
		((null data) nil)
		((eql key (caar data)) (print (cdar data)))
		(t (find_users_data (cdr data) key))
	)
)
 

;is it message of subscription or not?
;if it is message of subscription - we will answer on it
;serv - it is created socket
;xml - it is our xml
;ans - it is content of xml
(defun issubscribe (serv ans xml)
	(cond
		((null ans) serv)
		(t (sendmessage serv (taginstr ans))(addcontact serv xml (findrosterlist (getdurtree serv)))
			(sendsubscription serv xml)
		)
	)
)

;here we will send a subscription
;serv - it is created socket
;xml - it is our xml
(defun sendsubscription (serv xml)
	(let ((login (find_users_data (getclinform serv "connect_inform") "login")))
	(sendmessage serv (taginstr (insreadyattr 
		(insreadyattr 
			(insreadyattr (newtag "presence") (newattr "from" login))
		(newattr "to" (getneedattr (xmlattrget xml) "from")))
	(newattr "type" "subscribe"))))
	)
)

;here we will work with roster-list
;and send message of subscription id we need (may be user is already in our list)
;serv - it is created socket
;xml - it is our xml
;rlist - it is our roster-list
(defun addcontact (serv xml rlist)
	(cond
		((null rlist)(print "error!:no roster-list in during tree"))
		(t (needtoadd serv (finduserinrlist rlist (getneedattr (xmlattrget xml) "from"))))
	)
)

;here we will find our user in roster_list
;rlist - it is our roster-list
;name - it is user name
(defun finduserinrlist (rlist name)
	(cond
		((null rlist) name)
		((isxmltag rlist) 
			(cond
				((eql (xmlnameget rlist) "item") 
					(isneededname (getneedattr (xmlattrget rlist) "jid") name)
				)
				(t (finduserinrlist (getcontent rlist) name))
			)
		)
		((isstring rlist) name)
		(t 
			(cond
				((null (finduserinrlist (car rlist) name)) nil)
				(t (finduserinrlist (cdr rlist) name))
			)
		)
	)
)
;this function test - is it needed user's name or not
;name - it is user's name
;ans - jid of user in roster lisr
(defun isneededname (ans name)
	(cond
		((eql name ans) nil)
		(t name)
	)
)

;here we will send message for add contact in list
;serv - it is created socket
;ans - need to add user or not
(defun needtoadd (serv ans)
	(print "we need to add user")
	(cond
		(ans (sendmessage serv (taginstr (fromaddinform ans serv))))
		(t (print "he is inside")serv)
	)
)

;here we will form correct answer for add a contact
;ans - name of user
;serv - it is created socket
(defun fromaddinform (ans serv)
	(print "informaddinform")
	(let ((login (find_users_data (getclinform serv "connect_inform") "login")))
	(inscont
		(insreadyattr 
			(insreadyattr 
				(insreadyattr (newtag "iq") (newattr "from" login))
			(newattr "type" "set")) 
		(newattr "id" "add_1"))
		(inscont
			(insreadyattr (newtag "query") (newattr "xmlns" "jabber:iq:roster"))
			(inscont
				(insreadyattr 
					(insreadyattr (newtag "item") (newattr "jid" ans)) 
				(newattr "name" ans))
			(inscont (newtag "group") "main"))
		)
	))
)

;here we will look for roster list in during tree
;tree -it is ur xml-tree
(defun findrosterlist (tree)
	(cond
		((null tree) nil)
		((isxmltag tree)
			(cond
				((eql (xmlnameget tree) "query") tree)
				(t (findrosterlist (getcontent tree)))
			)
		)
		((isstring tree) nil)
		(t (isrosterlisthead (findrosterlist (car tree)) (cdr tree)))
	)
)

;here we will learn was it in roster list or not
;ans - it is xml-tag which we must test
;tail - it is tail of xml-tags which we haven't tested yet
(defun isrosterlisthead (ans tail)
	(cond
		(ans (isitneededquery ans tail (getneedattr (xmlattrget ans) "xmlns")))
		(t (findrosterlist tail))
	)
)

;is it needed tag query
;xml - it is xml-tag which we must test
;tail - it is tail of xml-tags which we haven't tested yet
;it is value of xmlns attr
(defun isitneededquery (xml tail ans)
	(cond
		((eql ans "jabber:iq:roster") xml)
		(t (findrosterlist tail))
	)
)

;we must send status for all our subcsribes
;xml - it is message
;serv - it is created socket
(defun sendstatus (xml serv)
	(let ((login (find_users_data (getclinform serv "connect_inform") "login")))
	(insstatusattr (insreadyattr (newtag "presence") (newattr "from" login)) xml)
	)
)

;here we will find sender
;newonw - it is xml-tag which we will send back
;xml - it is message
(defun insstatusattr (newone xml)
	(hastype (insreadyattr newone (newattr "to" (getneedattr (xmlattrget xml) "from"))) xml
	(getneedattr (xmlattrget xml) "type"))
)

;here we will learn if we have type of presence
;if presence has type - that means - user want to subscribe/unsubscribe us
;newone - it is tag - which we will send back
;ans - it is our answer
;xml - it is our message
(defun hastype (newone xml ans)
	(cond
		((null ans) nil)
		((eql ans "subscribe") (insreadyattr newone (newattr "type" "subscribed")))
		((eql ans "unsubscribe") (insreadyattr newone (newattr "type" "unsubscribed")))
	)
)

;here we will make correct message for answer
;this function will inseart name of user - he will get our message
;tag - it is tag - which we will send back
;xml - it is getted message
;serv - it is created socket
(defun sendanswer (tag xml serv)
	(formcortypeid (insreadyattr tag (newattr "to" (getneedattr (xmlattrget xml) "from"))) xml serv)
)		

;here we will find another two attributes - id and type
;tag - it is tag - which we will send back
;xml - it is getted message
;serv - it is created socket
(defun formcortypeid (tag xml serv)
	(findbody (insreadyattr (insreadyattr tag (newattr "id" (getneedattr (xmlattrget xml) "id")))
		(newattr "type" (getneedattr (xmlattrget xml) "type"))) xml serv)
)

;here we will find body of message - if we havent it - we will send empty letter
;tag - it is tag - which we will send back
;xml - it is getted message
;serv - it is created socket
(defun findbody (tag xml serv)
	(print "findbody")
	(workwithbody (getneedtag (getcontent xml) "body") tag xml serv)
)

;if body was empty we will send empty tag
;else we will send body - tag body contains all text which user sent to us
;!!! here we will make gteat cycle to game cows && bulls !!!
;newone - it is tag - which we will send back
;xml - it is getted message
;serv - it is created socket
;ans - it is content of tag body
(defun workwithbody (ans newone xml serv)
	(prin1 "answer")
	(cond
		((null ans) (sendmessage serv (taginstr (inscont newone (newtag "body")))))
		(t (sendmessage serv (taginstr (inscont newone (inscont (newtag "body") (dividewords (getcontent ans)))))))
	)
)

;in body we have words and we must divide them
;ans - it is content of tag body
;result - i string
(defun dividewords (ans)
	(insertpr (car ans) (cdr ans))
)

;here we will find needed tag in list
;tag - it is tag
;name - name of needed tags
;result list of needed tags
(defun getneedtag (tag name)
	(cond
		((null tag) nil)
		((isxmltag (car tag)) (iscorrecttag (car tag) name (cdr tag)))
		(t (getneedtag (cdr tag) name))
	)
)

;here we compear name of tag with needed name
;this is help function to function getneeded tag
;tag - it is tag
;name - name of needed tags
;tail - tags which we haven't analised yet
(defun iscorrecttag (tag name tail)
	(cond
		((eql name (xmlnameget tag)) tag)
		(t (getneedtag tail name))
	)
)

;here we will insert ' ' between words
;tail is list of words
;newone - will be result (first of all here we have the first word)
;result - is string - and between words are ' '
(defun insertpr (newone tail)
	(cond
		((null tail) newone)
		(t (insertpr (concatstr (concatstr newone " ") (car tail)) (cdr tail)))
	)
)

;here we will find attr from name which is given
;attr list of attrs
;name - name of needed attr
;result - content of needed attr or nil
(defun getneedattr (attr name)
	(cond
		((null attr) nil)
		((eql name (getattrname (car attr))) (getattrcontent (car attr)))
		(t (getneedattr (cdr attr) name))
	)
)

;analis firsttag
;xml - it is getted tag
;serv - it is created socket
(defun firststepanalis (xml serv)
	(print "firststepanalis")
	(analistree (getcontent xml)(newattrlist (newtag (xmlnameget xml))
		(analisattr xml serv)) serv)
)

;here we will work with tree
;we will find all unseen tags
;for all tags which are unseen - we will start function of analst
;tree - it is our tree
;serv -it is created socket
(defun analistree (tree newone serv)
	(cond
		((null tree) newone)
		((isstring tree) (inscont newone tree))
		((isxmltag tree) (inscont newone (analistree (getcontent tree)
			(newattrlist (newtag (xmlnameget tree))(analisattr tree serv)) serv))
		)
		(t (analistree (cdr tree) (analistree (car tree) newone serv) serv))
	)
)

;analis xml tag if it has somespecifuc attributes "seen"
;and if we find such attribute we will change it's meaning
;xml - it is getted xml
;serv - it is created socket
(defun analisattr (xml serv)
	(cond
		((findseen (xmlattrget xml))
			(isbuild xml serv)(changeseen (cdr (xmlattrget xml))(cons (car (xmlattrget xml)) nil))
		)
		(t (xmlattrget xml))
	)
)

;here we will change attr seen no yoes
;attr - it is list of xml-tag's attr
;new one - it is list of new xml attributes
(defun changeseen (attr newone)
	(cond
		((null attr) newone)
		((eql (getattrname (car attr)) "seen") (append(append newone 
			(cons (insattrcont (car attr) "yes") nil)) (cdr attr))
		)
		(t (changeseen (cdr attr) (append newone (cons (car attr) nil))))
	)
)

;find unseen attr
;attr - list of attrubutes
;return value - t - if attr seen='no'
;else - nil - seen='yes'
(defun findseen (attr)
	(cond
		((null attr) nil)
		((eql (getattrname (car attr)) "seen")
			(cond
				((eql (getattrcontent (car attr)) "no") T)
				(t nil)
			)
		)
		(t (findseen (cdr attr)))
	)
)

;we have different situations
;these situations depend on unbuild attribute
;xml - it is our message
;serv - it is created tag
(defun isbuild (xml serv)
	(cond
		((eql (getattrname (car (xmlattrget xml))) "unbuild")
			(cond
				((eql (getattrcontent (car (xmlattrget xml))) "no") (whichtype xml nil serv))
				(t (whichtype xml t serv))
			)
		)
		(t (prin1 "error! wrong attrs in isbuild"))
	)
)

;here we will recognise which type of analis we need
;analis of builded tag or not builded
;if it's not builded - only it's attributes -> think about it
;xml - it is xml-tag which we analis
;typeb - type of analis
;serv - it is created socket
(defun whichtype (xml typeb serv)
	(cond
		(typeb (findcondition serv xml))
		(t (findconditioncl serv xml))
	)
)

(defun start_delete (tree serv)
	(let ((log_file (find_users_data (getclinform serv "connect_inform") "log_file")))
		(newtree serv (start_delete_messages tree log_file))
	)
)

;this function starts delete-work
;function deletes messages which are old && write them in log
;serv -it's created socket
;tree - it's analizing message
(defun start_delete_messages (tree log_file)
	(cond
		((null tree) tree)
		((isstring tree) tree)
		((isxmltag tree) (delete_message tree log_file))
		(t (analise_delete_res (start_delete_messages (car tree) log_file) (cdr tree) log_file))
	)
)

;this function deletes message from tree and write in file deleted message
;tree - xml-tree
;xml - message which function deletes
;file - file stream in which we will write deleting message
(defun delete_message (tree file)
	(let ((unbuild (getneedattr (xmlattrget tree) "unbuild")) (seen (getneedattr (xmlattrget tree) "seen")))
	(cond
		((or (null unbuild) (null seen))
			(print "error!:delete_message no needed attributes in conning message") tree
		)
                ((or (or (eql unbuild "yes") (eql seen "no")) (and (not (eql "message" (xmlnameget tree)))
			(not (eql "presence" (xmlnameget tree)))))
			(newcontent tree (start_delete_messages (getcontent tree) file))
		)
		(t (printinfilexml tree file "a") t)
	))
)

(defun analise_delete_res (res tree log_file)
	(cond
		((eql res t) (start_delete_messages tree log_file))
		(t (cons res (start_delete_messages tree log_file)))
	)
)

;_________________________________________________________
;_____________here is part for bulls and cows_____________
;_________________________________________________________


;this is the start function of game-logic
;xml - it is content of message which we get
;serv -it is created socket
;l - it is message
(defun game_cows_and_bulls (serv l xml)
	(print "game_cows_and_bulls")
	(cond
		((null xml) serv)
		((and (isxmltag (car xml)) (eql "body" (xmlnameget (car xml))))
			(cows_and_bulls (getcontent (car xml)) l serv)
		)
		(t (game_cows_and_bulls serv l (cdr xml)))
	)
)

;this function will collect create list of numbers
;or from numbers/strings which we got from user
;return value nil - will be if be got not only numbers
(defun collect_number (numb res)
	(let ((num (numfromstr (car numb))))
	(cond
		((null numb) (print res))
		(num (collect_number (cdr numb) (append res num)))
		(t nil)
	))
)

;this function gets text of users message
;according to this text - we will start game, resume game or stop
;text - its is test
;serv -it is created socket
(defun cows_and_bulls (text xml serv)
	(print "cows_and_bulls")
	(let ((names (get_names  xml)) (head (car text)))
	(cond
		((null names) (print "Error!:we didn't game one of names") serv)
		((null text) serv)
		((eql "start" head) (start_cows_and_bulls serv (car names) (cdr names)))
		((eql "help" head) (help_cows_and_bulls serv (car names) (cdr names)))
		((eql "rules" head) (rules_cows_and_bulls serv (car names) (cdr names)))
		((eql "statistics" head) (statistics_cows_and_bulls serv (car (dividestrsim (car names) #\/)) (cdr names)))
		;(t (main_work_cows_and_bulls (parse_num (car (numfromstr text)))  serv (car names) (cdr names)))
		(t (number_check_cows_and_bulls serv (collect_number text nil) (car names) (cdr names)))
	))
)

;this function will start min logic of cows and bulls
;but first of all it will test is getted string number or not
;serv - it is created socket
;user_name - name of user (need it to form back message)
;our_name - our name (need it to form back message)
;user_num - it is users message
(defun number_check_cows_and_bulls (serv user_num user_name our_name)
	(let ((game_data (getclinform serv "game_data")))
	(cond
		((not (find_game_session game_data user_name))
			(send_message_to_serv serv user_name our_name "Sorry! I don't understend your message=( Press help, if you need")
		)
		(user_num (main_work_cows_and_bulls (parse_numbers user_num) serv user_name our_name))
		(t (send_message_to_serv serv user_name our_name "Sorry! I'm waiting for number! Press help, if you need"))
	))
)

;this function will starts game
;first of all we will look for game session for this user
;we need name of our user - we will make cons of name and parameters
;may be later we will save here some other params ( name_of_user . his_params )
;his_params = ( ( number . value ) ( number_of_tyies . value ) )
;xml - message - from this tag - we will get from and to
;serv - it is created socket
;user_name - name of user (need it to form back message)
;our_name - our name (need it to form back message)
(defun start_cows_and_bulls (serv user_name our_name)
	(print "start_cows_and_bulls")
	(print user_name)
	(print our_name)
	(inseart_game_session (getclinform serv "game_data") user_name our_name serv)
)


;this function starts game
;if user had game session we must clean it and generate new number
;if user hadn't game session we will create new
;game_date - it is information about all players and their numbers
;serv - it is created socket
;user_name - name of user (need it to form back message)
;our_name - our name (need it to form back message)
(defun inseart_game_session (game_data user_name our_name serv)
	(print "inseart_game_session")
	(cond
		((find_game_session game_data user_name)
			(send_message_to_serv serv user_name our_name "Hey! You have just restarted game")
			(changeclinform serv "game_data" (rebuild_game_data serv user_name our_name game_data))
			(begin_statistics serv user_name)
			serv
		)
		(t (inseart_new_player serv user_name our_name game_data))
	)
)

;this function will inseart new player in game_data
;this function will calculate begin statistics,because game is started
;game_date - it is information about all players and their numbers
;serv - it is created socket
;user_name - name of user (need it to form back message)
;our_name - our name (need it to form back message)
(defun inseart_new_player (serv user_name our_name game_data)
	(print "inseart_new_player")
	(changeclinform serv "game_data" (cons (cons user_name (reset_param 0 "steps" (reset_param (generate_numb 4) "number" nil))) game_data))
	(send_message_to_serv serv user_name our_name "Game started. I have just think of a number. It' is number, which consists of 4 different characters.")
	(begin_statistics serv user_name)
)


;this function generates numbers
;num - it is number of needed characters
;return value - list of numbers
(defun generate_numb (num)
	(print "generate_numb")
	(let ((res (generate_num num nil)))
	(cond
		((eq 0 (car res)) (reverse res))
		(t res)
	))
)

(defun generate_num (num res)
	(print "generate_num")
	(let ((generated (generatenum 0 9)))
	(cond
		((eql num 0) res)
		((inside generated res) (generate_num num res))
		(t (generate_num (- num 1) (cons generated res)))
	))
)

;this function will return true if value is inside list
(defun inside (value l)
	(cond
		((null l) nil)
		((eql value (car l)) t)
		(t (inside value (cdr l)))
	)
)

;this function returns true if user is already inside
;game_date - it is information about all players and their numbers
;user_name - name of user (need it to form back message)
(defun find_game_session (game_data user_name)
	(print "find_game_session")
	(cond
		((null game_data) nil)
		((eql (caar game_data) user_name) t)
		(t (find_game_session (cdr game_data) user_name))
	)
)

;this function will rebuild parameters
;this function will calculate begin statistics, because we restarl game
;game_date - it is information about all players and their numbers
;serv - it is created socket
;user_name - name of user (need it to form back message)
;our_name - our name (need it to form back message)
(defun rebuild_game_data (serv user_name our_name game_data)
	(cond
		((eql user_name (caar game_data))
			(send_message_to_serv serv user_name our_name "So. I have new number for you! Good luck!")
			(cons (cons user_name (reset_param 0 "steps" (reset_param (generate_numb 4) "number" (cdar game_data)))) (cdr game_data))
		)
		(t (cons (car game_data) (rebuild_game_data serv user_name our_name (cdr game_data))))
	)
)

;this function will rebuild users parameters
;new_val - it is new value of paramter
;key - it is key of parameter
;params - analising params
(defun reset_param (new_val key params)
	(print "reset_param")
	(print params)
	(print key)
	(print new_val)
	(cond
		((null params) (cons (cons key new_val) nil))
		((eql key (caar params)) (cons (cons key new_val) (cdr params)))
		(t (cons (car params) (reset_param new_val key (cdr params))))
	)
)
		
;this function will give to user information about game's commands
;serv - it is created socket
;user_name - it is name of user
;our_name - it is our name
(defun help_cows_and_bulls (serv user_name our_name)
	(let ((game_data (getclinform serv "game_files")))
        (cond
                ((null game_data)(send_message_to_serv serv user_name our_name "Sorry! Something has gone wrong with me=( Please, try later"))
                (t (sendmessage serv (taginstr (insreadyattr (insreadyattr
                        (car (turnopenfile (work_with_param "help_xml" game_data)))
                        (newattr "to" user_name)) (newattr "from" our_name))))
                )
        ))
)

;this function will send user rules of the game
(defun rules_cows_and_bulls (serv user_name our_name)
	(print "rules_cows_and_bulls")
	(let ((game_data (getclinform serv "game_files")))
	(cond
		((null game_data)(send_message_to_serv serv user_name our_name "Sorry! Something has gone wrong with me=( Please, try later"))
		(t (sendmessage serv (taginstr (insreadyattr (insreadyattr
			(car (turnopenfile (work_with_param "rules_file" game_data)))
			(newattr "to" user_name)) (newattr "from" our_name))))
		)
	))
)

;this function will show statistics to user
(defun statistics_cows_and_bulls (serv user_name our_name)
	(print "statistics_cows_and_bulls")
	(let ((stat (getclinform serv "statistics")))
	(print stat)
	(cond
		((null stat) (send_message_to_serv serv user_name our_name "Sorry! Something has gone wrong with me=( Please, try later"))
		(t (send_message_to_serv serv user_name our_name (form_statistics_message (find_users_data stat user_name))))
	))
)

;this function will construct message to user
(defun form_statistics_message (stat)
	(let ((numb (work_with_param "best" stat))
	(str
	(concatstr (concatstr
	(concatstr "total number of played games : " (strfromnumb (work_with_param "total_games" stat)))
	(concatstr "\ntotal number of won games : " (strfromnumb (work_with_param "won_games" stat))))
	"\nbest score : ")
	))
	(cond
		((isstring numb) (concatstr str numb))
		(t (concatstr str (strfromnumb numb)))
	))
)
;this function will send to user help information
;serv - it is created socket
;user_name - it is name of user
;our_name - it is our name
;h_xml - it is name of help_xml
;h_xslt - it is name of help_xslt
(defun send_help_message (serv user_name our_name h_xml h_xslt)
;	(sendmessage serv 
;		(taginstr (insreadyattr (insreadyattr (insreadyattr
;			(xslttransformation h_xml h_xslt)
;
;		(newattr "to" user_name)) (newattr "from" our_name)) (newattr "type" "chat")))
;	)
	(send_message_to_serv serv user_name our_name "help information")
)
;user_num - we have already converted number from string
;but we still can haven't separate numbers
;we will go here only if we hage game session for this user
(defun main_work_cows_and_bulls (user_num serv user_name our_name)
	(print "main_work_cows_and_bulls")
	(print user_num)
	(cond
		((null user_num) (send_message_to_serv serv user_name our_name "Sorry I can't understand you message. Press help"))
		(t (find_number_cows_and_bulls user_name (getclinform serv "game_data") user_num our_name serv))
	)
)

;(defun parse_numbers(l1)
;	(reverse (parse_num l1))
;)

;this function will parse numbers
;result - list of separated numbers
(defun parse_numbers (l1)
	(print "parse_numbers")
	(print l1)
	(cond
		((null l1) nil)
		(t (append (dividenum (car l1)) (parse_numbers (cdr l1))))
	)
)

;this function will find attributes, from which we will learn who sent as
;the message, and where he sent his message
;result - (user_name . our_name)
;result - nil if we didn't get on of needed params
(defun get_names (xml)
	(print "get_names")
	(print xml)
	(let ((user_name (getneedattr (xmlattrget xml) "from")) (our_name (getneedattr (xmlattrget xml) "to")))
	(cond
		((or (null user_name) (null our_name)) nil)
		(t (cons user_name our_name))
	))
)

;this function sends message to game
;serv - it is created socket
;name -it is name of user which will get our message
;our_name it is our name
;str - it is name of message
;result - we will return serv
(defun send_message_to_serv (serv name our_name str)
	(sendmessage serv (taginstr
		(insreadyattr (insreadyattr (insreadyattr (inscont (newtag "message")
			(inscont (newtag "body") str))
		(newattr "to" name))
		(newattr "from" our_name))
		(newattr "type" "chat")))
	)
)

;here we will search in list our user and his number
;name - name of user
;game_data - list of game data - we will modidficate it
;user_num - it is user's number (list)
;our_name - it is our name
;result - serv
(defun find_number_cows_and_bulls (name game_data user_num our_name serv)
	(print "find_number_cows_and_bulls")
	(cond
		;here we must send message to user
		;that we didn't start game
		((null game_data) (send_message_to_serv serv name our_name "You haven't start game yet. Press help, if you need"))
		((eql name (caar game_data)) (correct_params name our_name user_num (work_with_param "number" (cdar game_data)) serv))
		(t (find_number_cows_and_bulls name (cdr game_data) user_num our_name serv))
	)
)

;this function will start main game logic
;or send sorry it we have some problems with parameters
(defun correct_params (name our_name user_num our_num serv)
	(print "correct_params")
	(print user_num)
	(cond
		((null our_num) (send_message_to_serv serv name our_name "Sorry. Something have gone wrong=( Please, start game again"))
		((correct_number serv name our_name user_num our_num)(end_statistics serv name)(delete_win_user serv name))
		(t serv)
	)
)

;this function will find value of needed parameter
;name - name of parameter
;params - list of parameters
;result - value of needed param (nil - if we didn't find param)
(defun work_with_param (name params)
	(print "work_with_param")
	(print params)
	(cond
		((null params) (print "error!: no needed parameter")(print name) nil)
		((eql name (caar params)) (cdar params))
		(t (work_with_param name (cdr params)))
	)
)

;this function will form message to server with answer
;serv - it is created socket
;name - user name
;our_name - it is our name
;res - is if ( bulls . cows )
(defun send_answer_to_serv (serv name our_name res)
	(let ((str (concatstr (strconcatnumb "bulls - " (car res)) (strconcatnumb " cows - " (cdr res)))))
		(send_message_to_serv serv name our_name str)
	)
	(add_steps serv name)
	serv
)

;users number is correct
;if it is - we will start function which will count bulls and cows
;or may be users input correct number so we will send congretulations (and delete his information from list)
;return value - true if user wins (we will calculate end statistics and write it in file)
;return value - nil otherwise
(defun correct_number (serv name our_name user_num our_num)
	(print "correct_number")
	(let ((correct (same_length user_num our_num)))
	(cond
		;here we must delete user's information
		((and correct (same_list user_num our_num))
			(send_message_to_serv serv name our_name "Congratulations! your number is correct")t)
		(correct (send_answer_to_serv serv name our_name (count_bulls_and_cows user_num our_num)) nil)
		(t (send_message_to_serv serv name our_name (strconcatnumb "Wrong length of number! Length should be " (length our_num))) nil)
	))
)

;this function will delete_user from list
;when he wins
;later - we will modificate user's statistic here
(defun delete_win_user (serv user_name)
	(changeclinform serv "game_data" (delete_user (getclinform serv "game_data") user_name))
)

;this fucntion will delete users infromation from list
;game_data - it is list with many users
;user_name - it is name of user whom we will delete
;result - list of users which survived
(defun delete_user (game_data user_name)
	(print game_data)
	(cond
		((null game_data) nil)
		((eql user_name (caar game_data)) (cdr game_data))
		(t (cons (car game_data) (delete_user (cdr game_data) user_name)))
	)
)

;this function returns t if list are the same
(defun same_list(l1 l2)
	(cond
		((and (null l1) (null l2)) t)
		((or (null l1) (null l2)) nil)
		((eql (car l1) (car l2)) (same_list (cdr l1) (cdr l2)))
		(t nil)
	)
)

;this function will count bulls and cows
;result - ( bulls . cows )
;user_num - it is user number
;our_num - it is our number
(defun count_bulls_and_cows (user_num our_num)
	(print "count_bulls_and_cows")
	(print (cons (count_bulls user_num our_num 0) (count_cows user_num our_num 1 0)))
)

;this function will count how many bulls we have (numbers of their places)
;user_num - it is user's number (list)
;our_num - it is our number (list)
;result - bulls
(defun count_bulls (user_num our_num result)
	(print "count_bulls")
	(cond
		((null user_num) result)
		((eql (car user_num) (car our_num)) (count_bulls (cdr user_num) (cdr our_num) (+ 1 result)))
		(t (count_bulls (cdr user_num) (cdr our_num) result))
	)
)

;this function will count how many cows we have
;user_num - it is user's number (list)
;our_num - it is our number (list)
;place - it is currecnt analising place
;result - cows (numbers which are correct but not on their places
(defun count_cows (user_num our_num place result)
	(print "count_cows")
	(cond
		((null user_num) result)
		((is_it_cow (car user_num) our_num place 1) (count_cows (cdr user_num) our_num (+ place 1) (+ result 1)))
		(t (count_cows (cdr user_num) our_num (+ place 1) result))
	)
)

;this is predicate which returns true if it is cow
;and returns false - otherwise
;numb - is it number;
;our_num - it is our number (list)
;place - it is place of user's number
(defun is_it_cow (numb our_num place cur_pl)
	(print "is_it_cow")
	(cond
		((null our_num) nil)
		((and(not (eql place cur_pl))(eql numb (car our_num)))  t)
		(t (is_it_cow numb (cdr our_num) place (+ 1 cur_pl)))
	)
)

;this function will return true if lists are the same length
;l1 - it is the first list
;l2 - it is the secod list
(defun same_length (l1 l2)
	(cond
		((and (null l1) (null l2)) t)
		((null l1) nil)
		((null l2) nil)
		(t (same_length (cdr l1) (cdr l2)))
	)
)

;________________________________________________________________________
;_______________________presence_work____________________________________|
;________________________________________________________________________|


(defun start_presence_cows_and_bulls (serv xml)
	(print "start_presence_cows_and_bulls")
	(print xml)
	(let ((names (get_names  xml)))
	(cond
		((null names) (print "Error!: smth wrong with presence-message!"))
		((eql (car names) (cdr names)) serv)
		(t (presence_work_cows_and_bulls serv (car names) (cdr names)))
	))
)
;this function will look for player information
;serv -it is created socket
;user_name - it is name of our user
;our_name - it is our_name
(defun presence_work_cows_and_bulls (serv user_name our_name)	
	(let ((res (turnopenfile (concatstr "game_data/" (concatstr (car (dividestrsim user_name #\@)) "_statistic.xml")))))
	(cond
		((null res)
			(inseart_new_stat serv user_name our_name (new_player serv user_name our_name))
		)
		(t (old_player serv user_name our_name res))
	))
)

;this function will inseart new statistic about new player in our client's infromation
(defun inseart_new_stat (serv user_name our_name stat)
	(print "inseart_new_stat")
	(cond
		((null stat) (print "Error!:somthing is wrong with statistic of new player"))
		(t
			(changeclinform serv "statistics" (cons (parse_statistics serv stat (cons nil nil))(getclinform serv "statistics")))
			(print (getclinform serv "statistics"))
			;(send_message_to_serv serv user_name our_name
			;	"Hi! You are new! Welcome to game Cows and Bulls! We are glad to see you! Press help to learn about game's commands")

		)
	)
)

;this function will create new player
;and will send to user hello information
;serv - it is created socket
;user_name it is name of our user
;return value - it is created statistics
(defun new_player (serv user_name our_name)
	(print "new_player")
	(create_new_file (create_statistics user_name) user_name)
)

;this function will create new user's file and new xml-tag with statistisc
;user_name - it is name of our user
(defun create_statistics (user_name)
	(inscont (inscont (inscont (inscont (newtag "statistics")
		(inscont (newtag "name") (car (dividestrsim user_name #\/))))
		(inscont (newtag "total_games") "0"))
		(inscont (newtag "won_games") "0"))
		(inscont (newtag "best") "no_played_games"))
)

;this function will write in file user's information
;stat - it is our statistics
;user_name - it is name of our user
;return value is created statistics
(defun create_new_file (stat user_name)
	(print "create_new_file")
	(print stat)
	(print (dividestrsim user_name #\@))
	(let ((name (concatstr "game_data/" (concatstr (car (dividestrsim user_name #\@)) "_statistic.xml"))))
		(printinfilexml stat name "w")
		stat
	)
)

;this function will parse information of user which is old
;after parsing we will add user's data information to client's data
(defun old_player (serv user_name our_name xml)
	(print "old_player")
	(let (
		(game_data (getclinform serv "statistics"))	
		(params (parse_statistics serv  xml (cons nil nil)))
	 )
	(print params)
	(cond
		((null (car params)) (print "Error!: smth wrong with user's statistic") serv)
		(t (changeclinform serv "statistics" (cons params game_data))
			;(send_message_to_serv serv user_name our_name "Hi! Welcome back to Cows and Bulls! Press help if you need to learn about game's commands")
		) 
	))
)

;this function will work with tags from statisticsstatistics
; paras - it is parameters which we will form
(defun parse_statistics (serv xml params)
	(print "parse_statistics")
	(cond
		((null xml) params)
		((isxmltag xml) (statistics_parse serv xml params))
		(t (parse_statistics serv (car xml) (parse_statistics serv (cdr xml) params)))
	)
)

;this function will parse xml tag for statistics
;return value - it is params of user - statisics params
(defun statistics_parse (serv xml params)
	(print "statistics_parse")
	(let ((name (xmlnameget xml)))
	(print params)
	(print xml)
	(cond
		((null name) (print "Error!: something wrong with tag in user's statistics") params)
		((eql name "statistics") (parse_statistics serv (getcontent xml) params))
		((eql name "name") (cons (car (getcontent xml)) (cdr params)))
		((eql name "total_games") (cons (car params) (cons (cons "total_games" (car (getnumberstr (car (getcontent xml))))) (cdr params))))
		((eql name "won_games") (cons (car params) (cons (cons "won_games" (car (getnumberstr (car (getcontent xml))))) (cdr params))))
		((eql name "best") (statistics_best serv (car (getcontent xml)) params))
	))
)

;this function will parse statistics' data about best score
;return value - correct statistic params of users
(defun statistics_best (serv num params)
	(let ((numb (getnumberstr num)))
	(print num)
	(cond
		((null numb) (cons (car params)(cons (cons "best" "no_played_games") (cdr params))))
		(t (cons (car params) (cons (cons "best" (car numb)) (cdr params))))
	))
)

;_________________________________________________________________________
;___________________work_with_statistic_information_______________________|
;_________________________________________________________________________|


;this function starts statistic modifications which we must make
;in the begining of game session
(defun begin_statistics(serv user_name)
	(print "begin_statistics")
	(add_total_games serv (car (dividestrsim user_name #\/)))
	(print "begin_statistics end")
	serv
)

;this function starts staistic modidfications which we must make
;in the end of game session
;after all modifications we will write the result in players file
(defun end_statistics(serv user_name)
	(print "end_statistics")
	(let((name (concatstr "game_data/" (concatstr (car (dividestrsim user_name #\@)) "_statistic.xml"))) 
		(real_name (car (dividestrsim user_name #\/)))
	)
	(cond 
		((or (null (add_won_games serv real_name)) (null (change_score serv user_name))) serv)
		(t (printinfilexml (form_statistics real_name (find_users_data (getclinform serv "statistics") real_name)) name "w") serv)
	))
)

;this function will write statistic in file
;stat - it is user's statistic
(defun form_statistics (user_name stat)
	(let ((best (work_with_param "best" stat)))
	(inseart_best_param (inscont (inscont (inscont
		(newtag "statistics")
		(inscont (newtag "name") (car (dividestrsim user_name #\/))))
		(inscont (newtag "total_games") (strfromnumb (work_with_param "total_games" stat))))
		(inscont (newtag "won_games") (strfromnumb (work_with_param "won_games" stat))))
		best
	))
)
	
;this function will inseartbest param - it is string or number
(defun inseart_best_param (tag best)
	(let ((param (strfromnumb best)))
	(cond
		((null param) (inscont tag (inscont (newtag "best") best)))
		(t (inscont tag (inscont (newtag "best") param)))
	))
)

;this function will add 1 to total games
;serv - it is created socket
;user_name -it is user's name
(defun add_total_games (serv user_name)
	(print "add_total_games")
	(let ((stat (getclinform serv "statistics")))
	(cond
		((null stat) (print "Error!:something wrong with statistics in add_total_games") nil)
		(t (total_games_params serv user_name (find_users_data stat user_name) stat))
	))
)

;this function will add 1 to won games
;serv it is createt socket
;user_name it is user's name
(defun add_won_games (serv user_name)
	(print "add_won_games")
	(let ((stat (getclinform serv "statistics")))
	(cond
		((null stat) (print "Error!:something wrong with statistics in add_won_games") nil)
		(t (won_games_params serv user_name (find_users_data stat user_name) stat))
	))
)

;this function will add 1 to steps which player has made
(defun add_steps (serv user_name)
	(print "add_steps")
	(let ((game_data (getclinform serv "game_data")) (real_name (car (dividestrsim user_name #\/))))
	(cond
		((null game_data) (print "Error!:something wrong with add_steps") nil)
		(t (add_steps_params serv user_name (find_users_data game_data user_name) game_data))
	))
)

;this function will change best score if we need
(defun change_score (serv user_name)
	(print "change_score")
	(let ((game_stat (getclinform serv "statistics")) (game_data (getclinform serv "game_data"))
		(real_name (car (dividestrsim user_name #\/))))
	(cond
		((or (null game_data) (null game_stat)) (print "Error!:something wrong with dame_data/statistics") nil)
		(t (change_best_params serv real_name (find_users_data game_stat real_name) game_stat
			(find_users_data game_data user_name))
		)
	))
)

;this function will work with user's statistic and find number of user's games
;stat - it is user's statistic
;game_stat - it is total game statistics
(defun total_games_params (serv user_name stat game_stat)
	(print "total_games_params")
	(cond
		((null stat) (print "Error!:no user's statistics") nil)
		(t (add_total_games_end serv user_name stat game_stat (work_with_param "total_games" stat)))
	)
)

;this function will work with user's statistic and find number of user's games
;which he won
;stat - it is user's statistic
;game_stat - it is total game statistics
(defun won_games_params (serv user_name stat game_stat)
        (print "won_games_params")
	(cond
                ((null stat) (print "Error!:no user's statistics") nil)
                (t (add_won_games_end serv user_name stat game_stat (work_with_param "won_games" stat)))
        )
)

;this function will change best score if it is necessary
(defun change_best_params (serv user_name stat game_stat data)
	(print "change_best_params")
	(cond
		((or (null stat) (null data)) (print "Error!: no user's data about best score") nil)
		(t (change_best_end serv user_name stat game_stat (work_with_param "best" stat) (work_with_param "steps" data)))
	)
)

;this function will work with user's game_data and find number of steps
(defun add_steps_params (serv user_name data game_data)
	(print "add_steps_params")
	(cond
		((null data) (print "Error!: no user's data in game_data") nil)
		(t (add_steps_end serv user_name data game_data (work_with_param "steps" data)))
	)
)

;this function will rebuild add 1 to total
;and call rebuild statistic
;stat - it is old value of users statistic
;game_stat - it is old game statistic
(defun add_total_games_end (serv user_name stat game_stat num)
	(print "add_total_games_end")
	(cond
		((null num) (print "Error!:no total_games value in statistic") nil)
		(t (changeclinform serv "statistics"
			(rebuild_params game_stat (cons user_name  (rebuild_params stat (cons "total_games" (+ num 1))))))
		)
	)
)

;this function will rebuild add 1 to won games
;and call rebuild statistic
;stat - it is old value of users statistic
;game_stat - it is old game statistic
(defun add_won_games_end (serv user_name stat game_stat num)
        (print "add_won_games_end")
	(cond
                ((null num) (print "Error!:no won_games value in statistic") nil)
                (t (changeclinform serv "statistics"
                        (rebuild_params game_stat (cons user_name (rebuild_params stat (cons "won_games" (+ num 1))))))
                )
        )
)

;thins function will compare best score and current score
;and if current is better we will change old value on it
(defun change_best_end (serv user_name stat game_stat num num1)
	(print "change_best_end")
	(cond
		((or (null num) (null num1)) (print "Error!: no best/stept in clien't data") nil)
		((or (isstring num) (> num num1))
			(changeclinform serv "statistics"
				(rebuild_params game_stat (cons user_name (rebuild_params stat (cons "best" num1)))))
		)
		(t serv)
	)
)

;this function will add 1 to steps and will rebuild game_data
(defun add_steps_end (serv user_name data game_data num)
	(print "add_steps_end")
	(cond
		((null num) (print "Error!:no steps number in game_data") nil)
		(t (changeclinform serv "game_data"
			(rebuild_params game_data (cons user_name (rebuild_params data (cons "steps" (+ num 1))))))
		)
	)
)

;this function wil rebuld parameters
;params - it is old value of parameters
;new_param - it is cons which we must inseart
(defun rebuild_params (params new_param)
	(print "rebuild_params")
	(cond
		((null params) (print (cons new_param nil)))
		((eql (caar params) (car new_param)) (print (cons new_param (cdr params))))
		(t (cons (car params) (rebuild_params (cdr params) new_param)))
	)
)
