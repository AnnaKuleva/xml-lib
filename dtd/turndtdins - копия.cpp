#include "turndtdins.h"
#include <string.h>
#include "dtdsexpression.h"

static LListConstructor L;

//open given file and create object
DtdTurner::DtdTurner(char *str){
	FILE *f;
	f=fopen(str, "r");
	if(f==NULL)
		throw "DTDError:can't open given file\n";
	myfile=new SExpressionStreamFile(f);
	lex=new DtdLexer();
	sost=nothing;
	get=NULL;
	dur=NULL;
	dtddoc=SListConstructor();
	startform=false;
	stack=new MyStack<char *>;
	savestack=new MyQueue<SReference>;
	strength=new MyStack<SReference>;
}
	
//here is the main cycle for working with dtd-documents
//we feed lexer with simbols
//and get ready lexems
void DtdTurner::maincycle(){
	char c;
	struct mylist_2<char *, const char *> *m;
	DTDDEPRF(("in dtd main cycle\n"));
	while((c=myfile->Getc())!=EOF){
		lex->feedchar(c);
		if(lex->readylexem()){
			m=lex->getlexem();
			DTDDEPRF(("lexem: [%s] type [%s]\n", m->value, m->value1));
			//sometimes we could get more than one lexem
			get=insert_end<char *, const char *>(get, m);
			switch_part();
			DTDDEPRF(("during result is:\n %s\n", dtddoc->TextRepresentation().c_str()));
		}
	}
	lex->feedchar(c);
	has_ready_contentes();
	DTDDEPRF(("result is:\n %s\n", dtddoc->TextRepresentation().c_str()));
}

//this function is to insert ready contents of dtd elem in existing part of a tree
void DtdTurner::has_ready_contentes(){
	if(stack->isempty() && !savestack->isempty()){
                DTDDEPRF(("we will insert ready dtd elem\n"));
                dur->insert_inner(savestack->pop());
                if(!savestack->isempty())
                        throw "DtdError!: something wrong with stacks for saving tag-contents\n";
                dtddoc=dtddoc||(L|dur);
                dur=NULL;
                sost=nothing;
        }
}

//here we will search an algorithm to turn geting lexems in s-expressions
void DtdTurner::switch_part(){
	if(strcmp(get->value1, "dtd_type")==0)
		has_ready_contentes();
	switch (sost){
	case element:
		element_turn();
		break;
	case attlist:
		attlist_turn();
		break;
	case notation:
		notation_turn();
		break;
	case nothing:
		nothing_turn();
		break;
	};
}

//condition "nothing" we can meet only than it's dtdtype
void DtdTurner::nothing_turn(){
	DTDDEPRF(("in nothing_turn\n"));
	if(strcmp(get->value1, "dtd_type")!=0)
		throw "DtdError!:need dtd_type, but didn't get it\n";
	if(strcmp(get->value, "ELEMENT")==0){
		sost=element;
	}
	else if(strcmp(get->value, "ATTLIST")==0){
		sost=attlist;
	}
	else if(strcmp(get->value, "NOTATION")==0){
		sost=notation;
	}
	else
		throw "DtdError!:wrong dtd element. Such doesn't exists\n";
	deleteel();
}

//here we will work with sexpressiondtd
void DtdTurner::element_turn(){
	DTDDEPRF(("in element_turn\n"));
	//if we haven't during dtd elem, got structure includes name of xml-tag
	if(dur==NULL){
		dur=new SExpressionDtd(get->value);
		deleteel();
		return;
	}
	//if we have during elem that means that lexem is part of descripton
	if(strcmp(get->value, "EMPTY")==0 && !startform){
		getempty();
		return;
	}
	if(strcmp(get->value, "ANY")==0 && !startform){
		getany();
		return;
	}
	//we must meet '(' before anithing else
	if(strcmp(get->value, "(")==0){
		startform=true;
		DTDDEPRF(("one braket push\n"));
		stack->push(get->value);
		stack->print();
		deleteel();
		return;
	}
	if(strcmp(get->value, ")")==0){
		stack->print();
		closebracket();
		return;
	}
	if(strcmp(get->value, "*")==0 || strcmp(get->value, "+")==0 || strcmp(get->value, "?")==0){
		//here is case than strength is going after brackets
		if(!savestack->isempty()){
			SReference s=new SExpressionCons(savestack->backpop(), get->value);
			savestack->push(s);
		}
		else
			stack->push(get->value);
		deleteel();
		return;
	}
        //now we will think that in every other case we will have only contentes
        //but we know that we have or
	stack->push(get->value);
	DTDDEPRF(("inner push\n"));
	stack->print();
	deleteel();
}

//this function is for work when we have empty tag
void DtdTurner::getempty(){
	dtddoc=dtddoc||(L|dur);
	dur=NULL;
	sost=nothing;
	deleteel();
}

//this function is for work when we have discription of tag with any contentes
void DtdTurner::getany(){
	SReference s="ANY";
	DTDDEPRF(("s is %s\n", s->TextRepresentation().c_str()));
	dur->insert_inner(s);
	dtddoc=dtddoc||(L|dur);
	dur=NULL;
	sost=nothing;
	deleteel();
}

//here we will work when we meet close braket
void DtdTurner::closebracket(){
	DTDDEPRF(("in closebracket\n"));
	stack->print();
	SReference res=SListConstructor();
	char *str=stack->pop();
	while(strcmp(str, "(")!=0){
		DTDDEPRF(("in pop cycle [%s]\n", str));
		if(strcmp(str, "*")==0 || strcmp(str, "+")==0 || strcmp(str, "?")==0){
			char *str1=stack->pop();
			SReference s=new SExpressionCons(str1, str);
			res=(L|s)||res;
		}
		else
			res=(L|str)||res;
		str=stack->pop();
	}
	//here we must reverse our list
	//after main cycle we must look for that ones which were saved for us in save stack
	while(!savestack->isempty()){
		res=(L|savestack->pop())||res;
	}
	savestack->push(res);
	deleteel();
}

//this function is for cleaning memory
void DtdTurner::deleteel(){
	struct mylist_2<char *, const char *> *n;
	n=get;
	n->next=NULL;
	get=get->next;
	delete n;
}

void DtdTurner::attlist_turn(){
	DTDDEPRF(("in attlist_turn\n"));
}

void DtdTurner::notation_turn(){
	DTDDEPRF(("in notation_turn\n"));
}
