(%%%
        (MODULE-NAME "dtdcompare")
        (DECLARE-PUBLIC dtdcompare)
        (DECLARE-PUBLIC startattrlist)
	(DECLARE-USED-MODULE "../xml/symb.h")
	(DECLARE-EXTERNAL dtdelname)
	(DECLARE-EXTERNAL dtdelcont)
	(DECLARE-EXTERNAL xmlnameget)
	(DECLARE-EXTERNAL getcontent)
	(DECLARE-EXTERNAL isstring)
	(DECLARE-EXTERNAL isxmltag)
	(DECLARE-EXTERNAL getdattrname)
	(DECLARE-EXTERNAL getdattrtype)
	(DECLARE-EXTERNAL getdattrvalue)
	(DECLARE-EXTERNAL getdattrlist)
	(DECLARE-EXTERNAL newattr)
	(DECLARE-EXTERNAL xmlattrget)
	(DECLARE-EXTERNAL newattrlist)
	(DECLARE-EXTERNAL getattrcontent)
	(DECLARE-EXTERNAL getattrname)
)

;here is srart function. it has two params:
;first is xml-sexpression of our main document
;second is dtd-sexpression
(defun dtdcompare (xml dtd)
	(prin1 "dtdcompeare work - lisp")
	(print xml)
	(print dtd)
	(startanalis xml dtd)
	(print "lisp has already finished it's work with dtd discription")
)

;T - all xml-document meets requirements of dtd-document
(defun startanalis (xml dtd)
	(print "startanalis")
	(cond
		((null xml) T)
		((isstring xml) T)
		((isxmltag xml) (meetsreq xml dtd))
		((startanalis (car xml) dtd) (startanalis (cdr xml) dtd))
		(T nil)
	)
)

;T - xml-tag meets requirements of dtd-document
(defun meetsreq (xml dtd)
	(print "meetsreq")
	(cond
		((analizexml xml dtd) (startanalis (getcontent xml) dtd))
		(T nil)
	)
)


(defun analizexml (xml dtd)
	(print "analizxml")
	(let ((l (finddtdcont (xmlnameget xml) dtd)))
	(cond
		(l (analizecont (getcontent xml) l))
		(t nil)
	))
)

;this function has two params
;first - name of xml tag
;second - dtd-document
;description of content of our xml-tag according to dtd-document - will be result
(defun finddtdcont (xml dtd)
	(print "finddtdcont")
	(cond
		((null dtd) (print "ERROR!: there isn't disctiption of such xml-tag in dtd-doc!")
			(print xml) nil
		)
		((eql xml (dtdelname (car dtd))) (dtdelcont (car dtd)))
		(t (finddtdcont xml (cdr dtd)))
	)
)

;this function has two params
;first - content of xml-tag
;second - content of this xml-tag according to dtd-doc
;result T, when contentce meets the requirements of dtd-doc
(defun analizecont (xml dtd)
	(print "here is dtd content fo our xml-tag")
	(print xml)
	(print dtd)
	(cond
		((null dtd) nil)
		(t (resultsanalis xml dtd))
	)
)

;here we will analis results from mainanaliscycle
(defun resultsanalis (xml dtd)
	(print "resultsanalis")
	(let ((l (mainanaliscycle xml dtd)))
	(print l)
	(cond
		((not (null (car l))) (print "Error!: not empty xml list after xml analis") nil)
		((eql (cdr l) t) t)
		(t (print (cdr l)) nil)
	))
) 

;here will be cycle working with "or"
(defun orcycle (xml dtd)
	(print "orcycle")
	(cond
		((null dtd) (cons nil "Error!:or cycle - no xml-tag discribed in dtd-doc in or section"))
		(t (helporfunction xml dtd))
	)
)

(defun helporfunction (xml dtd)
	(print "helporfunction")
	(let ((l (mainanaliscycle xml (cons (car dtd) nil))))
	(print l)
	(cond
		((eql (cdr l) t) l)
		(t (orcycle xml (cdr dtd)))
	))
)

;here is the main cycle for analis contencte of xml-tag
;cycle param - list of dtd's elements
(defun mainanaliscycle (xml dtd)
	(print "mainanaliscycle")
	(print xml)
	(print dtd)
	(let ((head (car dtd)))
	(cond
		((and (null xml) (null dtd)) (cons nil T))
		((null dtd) (cons xml t))
		((null xml) (looktail  dtd))
		((eql head "or") (orcycle xml (cdr dtd)))
		((eql (car head) "EMPTY") (emptyfunc xml dtd))
		((eql (car head) "ANY")
			(cond 
				((null (cdr dtd)) (cons (cdr dtd) T))
				(T (cons "Error!:wrong description of dtd ELEMENT" nil))
			)
		)
		((atom (car head)) (mainhelpatom xml dtd))
		(t (mainhelplist xml dtd))
	))
)

;help function we will analismistakes from chooseatom
(defun mainhelpatom (xml dtd)
	(print "mainhelpatom")
	(let ((l (chooseatom xml (car dtd))))
	(print l)
	(cond
		((eql (cdr l) t) (mainanaliscycle (car l) (cdr dtd)))
		(t  l)
	))
)

;help function we will analismistakes from chooselist
(defun mainhelplist (xml dtd)
	(print "mainhelplist")
	(let ((l (chooselist xml (car dtd))))
	(print "helplist")
	(print l)
	(cond
		((eql (cdr l) t) (mainanaliscycle (car l) (cdr dtd)))
		(t l)
	))
)

;this function will work with xml-tags, which must be empty
(defun emptyfunc (xml dtd)
	(print "emptyfunc")
	(cond
		((and (null xml) (null (cdr dtd))) (cons nil T))
		((not (null xml)) (cons nil "Error!:not empty xml-tag, which according to dtd-doc must be empty"))
		((not (null (cdr dtd))) (cons nil "Error!:wrong description of dtd ELEMENT"))
	)
)

;this function will be used when list of xml is emptyq
;but we still have elements in dtd description
;all dtd elements must be ? or *
(defun looktail (dtd)
	(print "looktail")
	(let ((tail (cdar dtd)))
	(cond
		((null dtd) (cons nil T))
		((eql tail "?") (looktail (cdr dtd)))
		((eql tail "*") (looktail (cdr dtd)))
		(t (cons nil (cons "Error!:we need element" (caar dtd))))
	))
)

;this function will learn how many such xml-tags we need for atoms
(defun chooseatom (xml dtd)
	(print "chooseatom")
	(let ((tail (cdr dtd)) (head (car dtd)))
	(cond
		((eql tail 1) (onlyone1 xml head))
		((eql tail "+") (oneandmore1 xml head))
		((eql tail "?") (oneorzero1 xml head))
		((eql tail "*") (repeatall1 xml head))
		(t (cons nil "ERROR!:we didn't meet correct number of tags"))
	))
)

;this function will learn how many xml-tags we need for lists
(defun chooselist (xml dtd)
	(print "chooselist")
	(print dtd)
	(let ((tail (cdr dtd)) (head (car dtd)))
	(cond
		((eql tail 1) (onlyone2 xml head))
                ((eql tail "+") (oneandmore2 xml head))
                ((eql tail "?") (oneorzero2 xml head))
		((eql tail "*") (repeatall2 xml head))
        	(t (cons nil "ERROR!:we didn't meet correct number of tags"))
	))
)

;___group1 of functions to work with atoms___
;all functions will have two params: first - during xml; second - name of element
;this function will look for 0 or more elements
(defun repeatall1 (xml l)
	(cond
		((null xml) (cons nil t))
		((eql l "#PCDATA") (allstring xml))
		((eql (xmlnameget (car xml)) l) (repeatall1 (cdr xml) l))
		(t (cons xml t))
	)
)

;we must meet text information 0 or more times
(defun allstring (xml)
	(cond
		((isstring (car xml)) (allstring (cdr xml)))
		(t (cons xml t))
	)
)

;this function will look for 0 or 1 elements
(defun oneorzero1 (xml l)
	(cond
		((null xml) (cons nil t))
		((eql l "#PCDATA") (oneorzerostr xml))
		((eql (xmlnameget (car xml)) l) (cons (cdr xml) t))
		(t (cons xml t))
	)
)

;one or zero times we must meet string in content of xml-tag
(defun oneorzerostr (xml)
	(cond
		((isstring (car xml)) (cons (cdr xml) t))
		(t (cons xml t))
	)
)

;this function will look for 1 or more elements
(defun oneandmore1 (xml l)
	(cond
		((null xml)  (cons nil (cons "ERROR!:we didn't get element" l)))
		((eql l "#PCDATA")(onemorestr xml))
		((eql (xmlnameget (car xml)) l) (repeatall1 (cdr xml) l))
		(t (cons nil (cons "ERROR!:we didn't get element" l)))
	)
)

;we must meet string 1 or more times
(defun onemorestr (xml)
	(cond
		((isstring (car xml)) (allstring (cdr xml)))
		(t (cons nil "Error!:we didn't find string in xml-tag's content"))
	)
)

;this function will look for one special element
(defun onlyone1 (xml l)
	(print "onlyone atom")
	(cond
		((null xml) (cons nil (cons "ERROR!:we didn't get element" l)))
		((eql l "#PCDATA") (workstring xml))
		((eql (xmlnameget (car xml)) l) (cons (cdr xml) t))
		(t (cons nil (cons "ERROR!:we didn't get element" l)))
	)
)

;this function will look for string contents in xml-tag
(defun workstring (xml)
	(cond
		((isstring (car xml)) (cons (cdr xml) t))
		(t (cons nil "Error!:we need string in content"))
	)
)

;___group2 of functions to work with lists___
;all functions will have two params: first - during xml; second - name of element
;this function will look for 0 or more elements
(defun repeatall2 (xml l)
	(cond
		((null xml) (cons nil t))
		(t (repeatall21 xml l))
	)
)

;this function will analis for 0 or more elements
;here we have only lists
(defun repeatall21 (xml l)
	(let ((xml1 (helpfunc xml l)))
	(cond
		((eql t (cdr xml1)) (repeatall2 (car xml1) l))
		(t (cons xml t))
	))
)
		

;this is help function here we will analis during xml and part of dtd
;it will return true if defenition of dtd is correct
(defun helpfunc (xml l)
	(let ((xml1 (mainanaliscycle xml l)))
	(print "helpfunc")
	(print xml1)
	(cond
		(xml1 xml1)
		(t nil)
	))
)

;this function will look for 0 or 1 elements
(defun oneorzero2 (xml l)
	(cond
		((null xml) (cons nil t))
		(t (oneorzero21 xml l))
	)
)

;this is help function to recognize 0 or 1 needed element
(defun oneorzero21 (xml l)
	(let ((xml1 (helpfunc xml l)))
	(cond
		((eql t (cdr xml1))  xml1)
		(t (cons xml t))
	))
)

;this is function for analis 1 or more elements
(defun oneandmore2 (xml l)
	(cond
		((null xml) (cons nil (cons  "Error!:we didn't get element" l)))
		(t (oneandmore21 xml l))
	)
)

;this is help function to recognize 1 and more elements
(defun oneandmore21 (xml l)
	(let ((xml1 (helpfunc xml l)))
	(cond
		((eql t (cdr xml1)) (repeatall2 (car xml1) l))
		(t (cons nil (cons "Error!:we didn't get element" l)))
	))
)

;this function will look for 1 (only 1!) element
(defun onlyone2 (xml l)
	(cond
		((null xml) (cons nil (cons "Error!:we didn't get element" l)))
		(t (onlyone21 xml l))
	)
)

(defun onlyone21 (xml l)
	(let ((xml1 (helpfunc xml l)))
	(cond
		((eql t (cdr xml1))  xml1)
		(t (cons nil (cons "Error!:we didn't get element " l)))
	))
)

;here will be section where we will analis all xml
;______________________________________________________
;in every attlist every attribute must be unick

;this function will fund in attlist needed attribute
;will return nil - if we won't find
(defun findattr (name attr)
	(cond
		((null attr) nil)
		((eql name (getattrnamed (car attr))) (car attr))
		(t (findattr name (cdr attr)))
	)
)

;here we will anais attrlist 
;if we find two equal we will return nil
(defun twoequalattr (attr)
	(cond
		((null attr) t)
		((findattr (getattrnamed (car attr)) (cdr attr)) nil)
		(t (twoequalattr (cdr attr)))
	)
)

;here we get xml-tag (always!)
;we will test it's attlist and start test it's content
(defun testattlist (xml)
	(cond
		((twoequalattr (getattrlistd xml)) (testallattrlist (getcontent xml)))
		(t nil)
	)
)

;here we will analis all xml-tree
(defun testallattrlist (xml)
	(cond
		((null xml) t)
		((isstring xml) t)
		((isxmltag xml) (testattlist xml))
		(t 
			(cond
				((testallattrlist (car xml)) (testallattrlist (cdr xml)))
				(t nil)
			)
		)
	)
)

;here will be section where we will work with attributes
;_______________________________________________________
;please don't forget to analis that in all xml tags every attirbute meet only once!!!!

;this function will analis value of our attribute
;it can has four types of vales
;1 - #REQUIRED
;2 - #IMPLIED
;3 - #FIXED + value
;4 - value
;dattr is structure SExpressionDATR
;attr is attribute of xml tag
(defun valueanalis (attr dattr)
	(let ((value (getdattrvalue dattr)))
	(print value)
	(cond
		((atom value) t)
		((eql (cadr value) (getattrcontent attr)) t)
		(t  "Error!:wrong value of fixed attribute!")
	))
)
;here we will analise type of attribute
;dattr - it's type of dtd attr
(defun analistype (xml dattr name)
	(print "analistype")
	(print dattr)
	(print name)
	(let ((typea (getdattrtype dattr)))
	(cond
		((isstring typea) (stranalistype xml dattr name))
		(t (lstanalistype xml dattr name))
	))
)

;if dattr had list in type
;that means - there are fixed values of attribute
(defun lstanalistype (xml dattr name)
	(listtypeattr xml dattr name)
)

;if dattr was a string
;that means that it has standart type
(defun stranalistype (xml dattr name)
	(print "stranalistype")
	(let ((typea (getdattrtype dattr)))
	(cond
		((eql typea "CDATA") (cdataattranalis xml dattr name))
		((eql typea "ID") (idattranalis xml dattr name))
	))
)

;here we will start to work with attributes which have id type
(defun idattranalis (xml datta name)
	(let ((result (idtypeanalis xml datta name nil)))
	(cond
		((eql (cdr result) t) t)
		(t nil)
	)) 
)

;here will be main work with attrlists
(defun startattrlist (xml dtd)
	(print "ATTLIST analise section")
	(attlistanalis xml dtd)
	(print xml)
	(print "end ATTLIST analise section")
)

;here will be main cycle to analise all attlists
(defun attlistanalis (xml dtd)
	(cond
		((null dtd) t)
		((secondattcycle xml (dtdelname (car dtd)) (getdattrlist (car dtd))) (attlistanalis xml (cdr dtd)))
		(t (print "Error!:smth wrong in ATTLIST analis"))
	)
)

;here will be cycle for ATTLIST analis: xml-tree and list of attr definitions
(defun secondattcycle (xml name attr)
	(cond
		((null attr) t)
		(t (helpfuncattsecond xml  name attr))
	)
)

(defun helpfuncattsecond (xml name attr)
	(let ((result (analistype xml (car attr) name)))
	(cond
		((eql result t) (secondattcycle xml name (cdr attr)))
		(t (print result) nil)
	))
)
;here we will analis attributes with type CDATA
;it's simple analis - we only look have we such element or not
(defun cdataattranalis (xml attr xmlname)
	(print "cdataattranalis")
	(print xmlname)
	(cond
		((null xml) t)
		((isstring xml) t)
		((isxmltag xml)
			(cond
				((eql (xmlnameget xml) xmlname) (xmlattvalueanalis xml attr (xmlattrget xml)))
				(t (cdataattranalis (getcontent xml) attr xmlname))
			)
		)
		(t (cdatahelpfunc xml attr xmlname))
	)
)

;here will be help function for cdataattranalis
(defun cdatahelpfunc (xml attr xmlname)
	(print "here")
	(print xml)
	(let ((result (cdataattranalis (car xml) attr xmlname)))
	(print "cdatahelpfunc")
	(print result)
	(cond
		((eql result t) (cdataattranalis (cdr xml) attr xmlname))
		(t result)
	))
)

;here we will analis xml-tag attlist => and it can be transformed
;return t or message of mistake
(defun xmlattvalueanalis (xml attr attrlist)
	(print "xmlattvalueanalis")
	(let ((result (startvalueanalis attrlist attr attrlist)))
	(cond
		;here is mistake
		((isstring (cdr result)) (cdr result))
		(t (newattrlist xml (car result)) t)
	))
)
;here we will startvalue analis
;return (saved attlist . t/message of mistake)
(defun startvalueanalis (attlist dattr saved)
	(print "startvalueanalis")
	(let ((name (getdattrname dattr)))
	(cond
		((null attlist) (maybeinit dattr saved))
		((eql name (getattrname (car attlist))) (cons saved (valueanalis (car attlist) dattr)))
		(t (startvalueanalis  (cdr attlist) dattr saved))
	))
)

;here it's function wich will input attribute if it has initial value
;(new attr list . t/message of mistake)
(defun maybeinit (dattr saved)
	(print "maybeinit")
	(let ((value (getdattrvalue dattr)))
	(cond
		((isstring value)
			(cond
				((eql value "#REQUIRED") (cons nil "Error!:we didn't get required attribute"))
				((eql value "#IMPLIED") (cons saved  t))
				(t (cons (cons (newattr (getdattrname dattr) value) saved) t))
			)
		)
		(t (cons (cons (newattr (getdattrname dattr) (cadr value)) saved) t))
	))
)

;here we will analis attributes which have fixed list of types
;return value t or nil
;xml - xml-tree
;dattr - information from ATTLIST about attribute
;name - it's name of needed attribute
(defun listtypeattr (xml dattr name)
    	(print "lissttypeattr")
	(cond
            ((null xml) t)
            ((isstring xml) t)
            ((isxmltag xml) (secondlisttype xml dattr name))
            (t (helplsttype xml dattr name))
    )
)

;here will be helpfunction to analis xml-tag's attrubutes
(defun helplsttype (xml dattr name)
    	(print "helplsttype")
	(cond
        	((listtypeattr (car xml) dattr name) (listtypeattr (cdr xml) dattr name))
        	(t nil)
	)
)

;here will analis xml-tag - need we to analis is or not?
;xml - xml-tree
;dattr - information from ATTLIST about attribute
;name - it's name of needed attribute;xml - xml-tree
(defun secondlisttype (xml dattr name)
	(print "secondlisttype")
	(let ((n (xmlnameget xml)))
	(cond
		((eql n name) (workwithtype (xmlattrget xml) dattr xml))
    		(t (listtypeattr (getcontent xml) dattr name))
	))
)

;here we will start work with analis type
;we will change list of attributes
;so we must reload list of attr in xml-tag
(defun workwithtype (attr dattr xml)
	(print "workwithtype")
	(let ((result (analisltype attr dattr attr)))
	(cond
        	((eql (cdr result) t) (newattrlist xml (car result)))
        	(t (print (cdr result)) nil)
    	))
)

;here we will analis our attributes from xml-tag
;we must find one with same name
;result (new list of attrs . t/message of mistake)
(defun analisltype (attr dattr saved)
	(print "analisltype")
	(let ((name (getdattrname dattr)))
	(cond
        	((null attr) (maybeinit dattr saved))
        	((eql (getattrname (car attr)) name) (cons saved (findsametype (car attr) (getdattrtype dattr) dattr)))
       		(t (analisltype (cdr attr) dattr saved))
    	))
)

;here we will analis type of attribute
;attr - it's xml-attr
;dattr - it's dtd-attr content
(defun findsametype (attr dattr saved)
	(print "findsametype")
	(print attr)
	(print dattr)
	(let ((cont (getattrcontent attr)))
	(cond
        	((null dattr) "Error!:the value of xml-attribute is different from values from dtd-document")
        	;here we will analise value, because maybe it's fixed
        	((eql (car dattr) cont) (valueanalis attr saved))
        	(t (findsametype attr (cdr dattr) saved))
    	))
)

;here we will analis xml-tree, we will collect all values of
;current attribute because attr's type is "ID"
;all values must be different
;xml - xmt-tree
;dattr - attribute from dtd
;name - name of needed xml-tag
;values - collected values
;return value will be (new list of values . t/nil)
(defun idtypeanalis (xml dattr name value)
	(print "idtypeanalis")
	(print xml)
	(cond
        	((null xml) (cons value t))
        	((isstring xml) (cons value t))
        	((isxmltag xml) (idtypexmlan xml dattr name value))
        	(t (helpidtypean xml dattr name value))
    	)
)

;these is help function it will create cycle
;!attention
;result must be (_ . _)
(defun helpidtypean (xml dattr name value)
	(print "helpidtypean")
	(let ((result (idtypeanalis (car xml) dattr name value)))
    	(print result)
	(cond
        	((eql (cdr result) t) (idtypeanalis (cdr xml) dattr name (car result)))
        	(t (print (car result))(cons nil  nil))
    	))
)

;here we will learn need we such attribute of not
;result - (new value list . t/nil)
(defun idtypexmlan (xml dattr name value)
	(print "idtypexmlan")
	(cond
        	((eql (xmlnameget xml) name) (getindtypes (xmlattrget xml) dattr value xml))
        	(t (idtypeanalis (getcontent xml) dattr name value))
    	)
)

;here we will analis attributes with id type
;result (values . t/nil)
;attr - is list of attributes from xml-tag
(defun getindtypes (attr dattr value xml)
	(print "getindtypes")
	(let ((result (findneedidattr attr dattr attr)) (name (getdattrname dattr)))
	(cond
        	((eql (cdr result) t) (print (onlyonevalue (car result) (car result) name xml value)))
        	(t (cons (cdr result) nil))
    	))
)
 
 ;when we come in this function we have already analised xml-attrlist
 ;and ir's correct
(defun onlyonevalue (attr saved name xml value)
    (cond
        ;situation when attribute is implied
        ((null attr) (newattr xml saved)(cons value t))
        ((eql (getattrname (car attr)) name) (insideval (getattrcontent (car attr)) value xml saved))
        (t (onlyonevalue (cdr attr) saved name xml value))
    )
)

;here we will analis is tihs attr inside current values
;if it is that is mistake
;because such attributes must be identic
(defun insideval (name value xml saved)
    	(print "insideval")
	(print value)
	(cond
        	((isinside name value) (cons "Error!:two same values of attr which must be identic"  nil))
        	(t (newattrlist xml saved) (cons (cons name value) t))
    	)
)

;here we will get true if name is already inside
(defun isinside (name value)
    (cond
        ((null value) nil)
        ((eql name (car value)) t)
        (t (isinside name (cdr value)))
    )
)
 
;here we will work with attribute
(defun findneedidattr (attr dattr saved)
    (let ((name (getdattrname dattr)))
    (cond
        ((null attr) (maybeinit dattr saved))
        ;need to do it because may be it's fixed parametr
        ((eql name (getattrname (car attr))) (cons saved (valueanalis (car attr) dattr)))
        (t (findneedidattr (cdr attr) dattr saved))
    ))
)
