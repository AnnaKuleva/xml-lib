#include "attsexpression.h"
#include <stdio.h>
#include <string.h>

IntelibTypeId SExpressionDAtr::TypeId(&SExpression::TypeId);

static LListConstructor L;

//this is the main constructor of SExpressionDAtr
//all three characteristics of attribute must be!
SExpressionDAtr::SExpressionDAtr(char *str, SReference t, SReference pr):SExpression(TypeId){
	name=new char[strlen(str)+1];
	if(strcpy(name, str)==NULL)
		throw "DtdError!:smth wrong in coping strings in SExpressionDAtr constructor";
	name[strlen(str)]='\0';
	SExpressionString *str1=pr.DynamicCastGetPtr<SExpressionString>();
	if(str1==NULL)
		presence=deletekov(pr);
	else
		presence=workstringdel(str1);	
	str1=t.DynamicCastGetPtr<SExpressionString>();
        if(str1==NULL)
                type=deletekov(t);
        else
                type=workstringdel(str1);
}

//this function will be used to delete kovs from strings
SReference SExpressionDAtr::deletekov(SReference pr1){
	SReference inseart=SListConstructor();
        ATTDEPRF(("here %s\n", pr1->TextRepresentation().c_str()));
        while(!pr1.IsEmptyList()){
                SExpressionString *str=(pr1.Car()).DynamicCastGetPtr<SExpressionString>();
                if(str==NULL)
                        throw "DtdError!:not string in presence";
                SExpressionString *res=workstringdel(str);
		inseart=inseart||(L|res);
                pr1=pr1.Cdr();
                ATTDEPRF(("here %s\n", pr1->TextRepresentation().c_str()));
        }
	return inseart;
}

//here will be delete operation 
SExpressionString *SExpressionDAtr::workstringdel(SExpressionString *str){
	const char *res=str->GetValue();
                if(res[0]=='"'){
                        int l=0;
                        while(res[l]!='\0')
                                l++;
                        char *getnew=new char[l-1];
                        for(int i=1; i<l-1; i++)
                                getnew[i-1]=res[i];
                        SExpressionString *ins=new SExpressionString(getnew);
			return ins;
		}
	return str;
}

#if INTELIB_TEXT_REPRESENTATIONS == 1
SString SExpressionDAtr::TextRepresentation() const{
	SString str1=SString("attribute-name: ");
	SString str2=SString(" attribute-type: ");
	SString str3=SString(" attribute-value: ");
	SString t=type->TextRepresentation();
	SString res;
	SString pr=presence->TextRepresentation();
	res=str1+SString(name)+str2+t+str3+pr;
	return res;
}
#endif

SExpressionDAtr::~SExpressionDAtr(){
	delete []name;
}
