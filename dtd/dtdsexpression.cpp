#include "dtdsexpression.h"
#include <stdio.h>
#include <string.h>

IntelibTypeId SExpressionDtd::TypeId(&SExpression::TypeId);

SExpressionDtd::SExpressionDtd(char *n):SExpression(TypeId){
	int i=strlen(n);
	name=new char[i+1];
	for(int k=0; k<i; k++){
		name[k]=n[k];
	}
	name[i]='\0';
	SEXPRDEPRF(("name of dtd elem is [%s]\n", name));
	inner=SListConstructor();
	attlist=SListConstructor();
}

#if INTELIB_TEXT_REPRESENTATIONS == 1
SString SExpressionDtd::TextRepresentation() const{
	char n[]="name:";
	SString in=inner->TextRepresentation();
	SString att=attlist->TextRepresentation();
	SString res=SString(n)+SString(name)+" contents:"+in+" attlist:"+att;
	SEXPRDEPRF(("result in SExpressionDtd::TextRepresentation  %s\n", res->TextRepresentation().c_str()));
	return res;
}
#endif

static LListConstructor L;

//tins function is for insert name of inner tag and how many tmes we could meet it
void SExpressionDtd::insert_inner(char *inname, char *rep){
	SExpressionCons *pair;
	//we will insert a pair (_ . _) 1 - name of xml-tag; 2 - number of repeats
	pair=new SExpressionCons(inname, rep);
	inner=inner||(L|pair);
	SEXPRDEPRF(("here is inner elements of %s: [%s]\n", name, inner->TextRepresentation().c_str()));
}

//if we want insert ready sreference;
void SExpressionDtd::insert_inner(SReference k){
	inner=k;
	SEXPRDEPRF(("here is inner elements of %s: [%s]\n", name, inner->TextRepresentation().c_str()));
}

//this function gets attribute and insert it in list
void SExpressionDtd::insert_attribute(SExpressionDAtr *atr){
	attlist=attlist||(L|atr);
}

//this functon will make dotted pair from all saved things and in
void SExpressionDtd::insert_whole_num(char *num){
	inner=new SExpressionCons(inner, num);
        SEXPRDEPRF(("here is inner elements of %s: [%s]\n", name, inner->TextRepresentation().c_str()));
}

SExpressionDtd::~SExpressionDtd(){
	delete [] name;
}
