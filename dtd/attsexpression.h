#ifndef _SEXPRESSIONATTLIST_H_KURS_4_
#define _SEXPRESSIONATTLIST_H_KURS_4_

#include "/Users/mac/mylib/mylist.h"
#include <intelib/sexpress/sexpress.hpp>
#include <intelib/sexpress/sstring.hpp>
#include <intelib/lisp/lsymbol.hpp>

#ifdef ATTDEBUG
#define ATTDEPRF(x) printf x
#else
#define ATTDEPRF(x)
#endif

class SExpressionDAtr:public SExpression{
	char *name;
	SReference type;
	SReference presence;

	SReference deletekov(SReference p1);
	SExpressionString *workstringdel(SExpressionString *str);
public:
	SExpressionDAtr(char *str, SReference t, SReference pr);
	static IntelibTypeId TypeId;
        #if INTELIB_TEXT_REPRESENTATIONS == 1
        virtual SString TextRepresentation() const;
        #endif
	SReference gettypeattr(){return type;}
	SReference getpresenceattr(){return presence;}
	char *getnameattr(){return name;}
	void inseartnewtype(SReference ins){type=ins;}
	void inseartnewpresence(SReference ins){presence=ins;}
protected:
	virtual ~SExpressionDAtr();
};

#endif
