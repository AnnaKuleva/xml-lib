#ifndef _TURN_DTD_IN_S_EXPRESSION_KURS_4_
#define _TURN_DTD_IN_S_EXPRESSION_KURS_4_

#include "lexan_dtd.h"
#include "../helplib/mylist.h"
#include "../helplib/mystack.h"
#include "../helplib/myqueue.h"
#include <intelib/lisp/lsymbol.hpp>
#include <stdio.h>
#include <intelib/tools/sstream.hpp>
#include "dtdsexpression.h"
#include "entsexpression.h"
#include "dtdlib.h"
#include "dtdentity.hxx"
#include "notsexpression.h"

#ifdef DTDDEBUG
#define DTDDEPRF(x) printf x
#else
#define DTDDEPRF(x)
#endif

class DtdTurner{
	enum dtdelem{
		element,
		attlist,
		dnotation,
		entity,
		comment,
		nothing
	};
	enum attstage{
		xmltagname,
		attlistname,
		attlisttype,
		attlistvalue
	};
	enum entstage{
		entstart,
		entname,
		parametr,
		entsetvalue,
		systemexternal,
		publicexternal,
		entpublic_id,
		entseturi,
		entunparsed
	};
	enum notstage{
		notstart,
		notname,
		notsystem,
		notpublic,
		notpublicid
	};
	bool startform;
	dtdelem sost;
	attstage attsost;
	entstage entsost;
	notstage notsost;
	DtdLexer *lex;
	SExpressionStreamFile *myfile;
	SExpressionDtd *dur;
	SReference dtdval;
	SReference dtddoc;
	
	MyStack<SReference> *stack;
	char *attname;
	//SReference atttype;
	SReference attvalue;
	SExpressionDtd *insertattr;
	//here will be parametrs which will help us to analis entities
	bool parametric;
	SExpressionExternal *external;
	SExpressionInternal *internal;
	SReference param_ent;
	SReference xml_ent;
	char * nameofent;
	
	SExpressionDNotation *notation;
	SReference dtdnotation;

	struct mylist_2<char *, const char *> *get;
	void element_turn();
	void attlist_turn();
	void notation_turn();
	void nothing_turn();
	void entity_turn();
	void switch_part();
	void deleteel();
	void getempty();
	void getany();
	void closebracket();
	void has_ready_contentes();
	SReference work_with_or(SReference s);
	//section of function which analis attributes
	void attlist_xmlname();
	void attlist_type();
	void attlist_name();
	void attlist_value();
	void formattlist();
	//section of functions which analis entities
	void startanalisentity();
	void getentityname();
	void willbeparametrent();
	void setentityval();
	void systementity();
	void publicentity();
	void getpublic_id();
	void geturientity();
	void unparsedentity();
	void changeparament();
	void getemptyentity();
	void getenyentity();
	void entity_element_turn();
	void entity_has_ready_contentes();
	//section of functions which analis notation
	void startnotation();
	void getnotationname();
	void worksysnotation();
	void workpublnotation();
	void workpubidnotation();
	SExpressionDNotation *getnotation(const char *name);
public:
	DtdTurner(char *str);
	~DtdTurner(){
		myfile->Close();
		delete lex;
	}
	void maincycle();
	SReference getdtd(){return dtddoc;}
	SReference getxmlentity(){return xml_ent;}
};

#endif
