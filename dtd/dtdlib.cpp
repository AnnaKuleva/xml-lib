#include "dtdlib.h"
#include <stdio.h>
#include "declare_functions.h"

#define ASSERT_PARAMC(n) \
    INTELIB_ASSERT(params <= n, IntelibX_too_many_params(params));\
    INTELIB_ASSERT(params >= n, IntelibX_too_few_params(params));

static LListConstructor L;

//this function will help to get name of ELEMENT
SReference getelemname(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionDtd *dtd=param[0].DynamicCastGetPtr<SExpressionDtd>();
	if(dtd==0){
		throw "it's not a Dtd ELEMENT in getelemname()\n";
	}
	return dtd->dtdelemname();
}

//this function will help us to get contentce of ELEMENT
SReference getelemcont(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionDtd *dtd=param[0].DynamicCastGetPtr<SExpressionDtd>();
	if(dtd==0)
		throw "it's not a Dtd ELEMENT in getelemcont()\n";
	return dtd->dtdelemcont();
}


//this function will work with dtd atribute
//this function will return dtd attlist
SReference getattlist(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionDtd *dtd=param[0].DynamicCastGetPtr<SExpressionDtd>();
	if(dtd==0)
		throw "it's not a Dtd ELEMENT in getattlist()\n";
	return dtd->getattlist();
}

//here we will get type of our attribute
SReference gettypeofattribute(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionDAtr *datt=param[0].DynamicCastGetPtr<SExpressionDAtr>();
	if(datt==0)
		throw "it's not a DtdAttr in gettypeofattribute()\n";
	return datt->gettypeattr();
}

//here we will get presence  of our attribute
SReference getpresenceattribute(int params, const SReference *param){
        ASSERT_PARAMC(1);
	printf("%s\n", param[0]->TextRepresentation().c_str());
        SExpressionDAtr *datt=param[0].DynamicCastGetPtr<SExpressionDAtr>();
        if(datt==0)
                throw "it's not a DtdAttr in getpresenceofattribute()\n";
        return datt->getpresenceattr();
}

//here we will get name of our attribute
SReference getnameofattr(int params, const SReference *param){
        ASSERT_PARAMC(1);
        SExpressionDAtr *datt=param[0].DynamicCastGetPtr<SExpressionDAtr>();
        if(datt==0)
                throw "it's not a DtdAttr in getnameofattr()\n";
	SExpressionString *name=new SExpressionString(datt->getnameattr());
	return name;
}

//here we will get name of entity or nil
//if string doesn't include any entities
//this function will get two parametrs
//first - string where we will find entity
//second - will nil if we analis parametric entity
SReference entityinside(int params, const SReference *param){
	ASSERT_PARAMC(2);
	SExpressionString *str=param[0].DynamicCastGetPtr<SExpressionString>();
	if(str==0)
		throw "it's not a string in entityinside()\n";
	int i=0;
	bool entity=false;
	bool readyent=false;
	SString s(str);
	int begin;
	bool parametric=false;
	if(param[1].IsEmptyList())
		parametric=true;
	while(s[i]!='\0'){
		if(parametric && s[i]=='%' && entity)
			throw "in entityinside wrong declaration of entity!";
		if(!parametric && s[i]=='&' && entity)
			throw "in entityinside wrong declaration of entity!";
		if(!parametric && s[i]=='&'){
                        entity=true;
                        begin=i;
                }
		if(parametric && s[i]=='%'){
			entity=true;
			begin=i;
		}
		if(s[i]==';' && entity){
			readyent=true;
			break;
		}
		i++;
	}
	if(!readyent)
		return *PTheEmptyList;
	int length=i-begin;
	begin++;
	char *res=new char[length];
	for(int j=0; j<length-1; j++){
		res[j]=s[begin];
		begin++;
	}
	res[length-1]='\0';
	return new SExpressionString(res);
}

//here we will get name of entity
SReference getdentityname(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionEntity *ent=param[0].DynamicCastGetPtr<SExpressionEntity>();
	if(ent==0)
		throw "it's not an entity in getentityname";
	SExpressionString *res=new SExpressionString(ent->getname());
	return res;
}

//here we will get value of entity
SReference getentityvalue(int params, const SReference *param){
        ASSERT_PARAMC(1);
        SExpressionEntity *ent=param[0].DynamicCastGetPtr<SExpressionEntity>();
        if(ent==0)
                throw "it's not an entity in getentityvalue";
	return ent->getvalue();
}

//here we will inseart new type in attribute
SReference insnewattrtype(int params, const SReference *param){
        ASSERT_PARAMC(2);
	SExpressionDAtr *attr=param[0].DynamicCastGetPtr<SExpressionDAtr>();
	if(attr==0)
		throw "it's not a dtd-attribute in insnewattrtype";
	attr->inseartnewtype(param[1]);
	return attr;
}

//here we will inseart new presence in attribute
SReference insnewattrpresence(int params, const SReference *param){
        ASSERT_PARAMC(2);
        SExpressionDAtr *attr=param[0].DynamicCastGetPtr<SExpressionDAtr>();
        if(attr==0)
                throw "it's not a dtd-attribute in insnewattrtype";
        attr->inseartnewpresence(param[1]);
	return attr;
}

//here we will inseart new presence in attribute
SReference insnewdtdinner(int params, const SReference *param){
        ASSERT_PARAMC(2);
	SExpressionDtd *dtd=param[0].DynamicCastGetPtr<SExpressionDtd>();
	if(dtd==NULL)
		throw "it's not a dtd-element in insnewinner";
	dtd->insnewinner(param[1]);
	return dtd;
}

SReference insnewdtdattr(int params, const SReference *param){
        ASSERT_PARAMC(2);
        SExpressionDtd *dtd=param[0].DynamicCastGetPtr<SExpressionDtd>();
        if(dtd==NULL)
                throw "it's not a dtd-element in insnewdtdattr";
	dtd->insnewattr(param[1]);
	return dtd;
}

//here we will change entity
//first parametr - string which we will rebuild
//second parametr - dtd entity from which we will get value
//third parametr will be nil if we have parametric entity
SReference changedentities(int params, const SReference *param){
	ASSERT_PARAMC(3);
	SEXPRDEPRF(("changedentities\n"));
	SExpressionString *str=param[0].DynamicCastGetPtr<SExpressionString>();
	SExpressionEntity *ent=param[1].DynamicCastGetPtr<SExpressionEntity>();
	if(str==0)
		throw "it's not a string in changeentities";
	if(ent==0)
		throw "it's not entity in hcangeentities";
	SExpressionString *str1=new SExpressionString(ent->getname());
	SString analis(str->GetValue());
	SString find(str1->GetValue());
	bool parametric=false;
	if(param[2].IsEmptyList())
		parametric=true;
	if(parametric)
		find=SString("%")+find+SString(";");
	else
		find=SString("&")+find+SString(";");
	SEXPRDEPRF(("analisstring %s\n", str->GetValue()));
	SEXPRDEPRF(("findstring %s\n", find->GetValue()));
	int length=analis.Length();
	int length1=find.Length();
	int begin=-1;
	for(int i=0; i<length; i++){
		if(analis[i]==find[0]){
			bool flag=true;
			for(int j=0; j<length1; j++){
				if(analis[j+i]!=find[j]){
					flag=false;
					break;
				}
			}
			if(flag)
				begin=i;
		}
		if(begin>0)
			break;
	}
	if(begin==-1)
		return *PTheEmptyList;
	char *first=new char[begin+1];
        for(int i=0; i<begin; i++){
        	first[i]=analis[i];
        }
	first[begin]='\0';
	int l=length-length1-begin+1;
        char *third=new char[l];
        int j=0;
        for(int i=begin+length1; i<length; i++){
                third[j]=analis[i];
                j++;
        }
	third[l-1]='\0';
	SEXPRDEPRF(("first [%s], third [%s]\n", first, third));
	//string will be result
	if(ent->getvalue().DynamicCastGetPtr<SExpressionString>()!=0){
		SString res=SString(first)+ent->getvalue()+SString(third);
		SEXPRDEPRF(("result %s\n", res->GetValue()));
		return res;
	}
	SReference result=SListConstructor();
	//result will be List
	if(begin==0)
		result=ent->getvalue();
	else{
		result=(L|new SExpressionString(first));
		result=result||ent->getvalue();
	}
	if(begin+length1!=length)
		result=result||(L|third);
	SEXPRDEPRF(("result %s\n", result->TextRepresentation().c_str()));
	return result;
}

//this function define functions which help us in working with dtd
void define_dtd_functions(){
	DTDELNAME->SetFunction(getelemname);
	DTDELCONT->SetFunction(getelemcont);
	GETDATTRNAME->SetFunction(getnameofattr);
	GETDATTRTYPE->SetFunction(gettypeofattribute);
	GETDATTRVALUE->SetFunction(getpresenceattribute);
	GETDATTRLIST->SetFunction(getattlist);
}
