(%%%
        (MODULE-NAME "dtdwork")
        (DECLARE-PUBLIC start)
        (DECLARE-USED-MODULE "symb.h")
	(DECLARE-EXTERNAL turnopenfile)
)

(defun start (file)
	(turnopenfile file)
)

