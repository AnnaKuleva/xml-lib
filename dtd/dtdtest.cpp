#include <stdio.h>
#include "turndtdins.h"

int main(int argv, char *argc[]){
	try{
		printf("DTD section:\n");
		if(argv!=2){
			printf("Error! We need only 1 parametr - file name\n");
			return 0;
		}
		DtdTurner *dtddoc=new DtdTurner(argc[1]);
		dtddoc->maincycle();
		printf("End DTD section\n");
	}
	catch(IntelibX &x) {
        	printf("\nCaught IntelibX: %s\n", x.Description() );
        	if(x.Parameter().GetPtr()) {
            		printf("%s\n", x.Parameter()->TextRepresentation().c_str());
        	}
        	if(x.Stack().GetPtr()) {
            		printf("%s\n", x.Stack()->TextRepresentation().c_str());
        	}
	}
	catch(const char *s){
		printf("%s\n", s);
	}
	catch(...){
		printf("Error!: somthing strange in dtd analis\n");
	}
	return 0;
}

