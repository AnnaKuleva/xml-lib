#include "notsexpression.h"
#include <stdio.h>

IntelibTypeId SExpressionDNotation::TypeId(&SExpression::TypeId);

//here will be constructor
SExpressionDNotation::SExpressionDNotation(const char *n):SExpression(TypeId){
        int i=0;
        while(n[i]!='\0')
                i++;
        name=new char[i+1];
        for(int j=0; j<i; j++)
                name[j]=n[j];
        name[i]='\0';
	parser=NULL;
	space=NONE;
}

SExpressionDNotation::~SExpressionDNotation(){
	delete []name;
	if(parser!=NULL)
		delete []parser;
}

#if INTELIB_TEXT_REPRESENTATIONS == 1
SString SExpressionDNotation::TextRepresentation() const{
	SString res("notation: name ");
	res+=SString(name);
	if(space==PUBLIC)
		res+=SString(" PUBLIC ");
	if(space==SYSTEM)
		res+=SString(" SYSTEM ");
	res+=SString(" name of instrument: ") + SString(parser);
	return res;
}
#endif

//here we will inseart value of parser
void SExpressionDNotation::insertparser(const char *p){
	if(parser!=NULL)
		delete [] parser;
	int i=0;
        while(p[i]!='\0')
                i++;
        parser=new char[i+1];
        for(int j=0; j<i; j++)
                parser[j]=p[j];
        parser[i]='\0';
}

//here we will set namespace of notation if we have it
void SExpressionDNotation::settype(bool sys){
	if(sys)
		space=SYSTEM;
	else
		space=PUBLIC;
}
