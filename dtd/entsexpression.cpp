#include "entsexpression.h"
#include <stdio.h>
#include <string.h>

IntelibTypeId SExpressionEntity::TypeId(&SExpression::TypeId);

//here will be constructor
SExpressionEntity::SExpressionEntity(char *n):SExpression(TypeId){
	int i=0;
	while(n[i]!='\0')
		i++;
	name=new char[i+1];
	for(int j=0; j<i; j++)
		name[j]=n[j];
	name[i]='\0';
}

//here will be dectructor
SExpressionEntity::~SExpressionEntity(){
	delete []name;
}

//here will be delete operation 
SExpressionString *SExpressionEntity::reformstring(SExpressionString *str){
        const char *res=str->GetValue();
                if(res[0]=='"'){
                        int l=0;
                        while(res[l]!='\0')
                                l++;
                        char *getnew=new char[l-1];
                        for(int i=1; i<l-1; i++)
                                getnew[i-1]=res[i];
                        SExpressionString *ins=new SExpressionString(getnew);
                        return ins;
                }
        return str;
}

static LListConstructor L;

IntelibTypeId SExpressionInternal::TypeId(&SExpression::TypeId);

IntelibTypeId SExpressionExternal::TypeId(&SExpression::TypeId);

#if INTELIB_TEXT_REPRESENTATIONS == 1
SString SExpressionInternal::TextRepresentation() const{
        char n[]="internal entity name:";
	SString res(n);
	res+=SString(name);
	res+=SString(" value:");
	res+=SString(value->TextRepresentation().c_str());
	return res;
}
#endif

void SExpressionInternal::inseartvalue(SReference val){
	SExpressionString *str=val.DynamicCastGetPtr<SExpressionString>();
	if(str!=0){
		value=reformstring(str);
		return;
	}
	value=val;
}

SExpressionExternal::SExpressionExternal(char *n):SExpressionEntity(n){
	space=NONE;
	unparsed=false;
	unparsedname=NULL;
}

//here we will inseart value of exernal entity
//we will get uri/url
void SExpressionExternal::seturi(char *val){
	int i=0;
        while(val[i]!='\0')
                i++;
        uri=new char[i+1];
        for(int j=0; j<i; j++)
                uri[j]=val[j];
        uri[i]='\0';
	uri=(char *)reformstring(new SExpressionString(uri))->GetValue();
	//here we must work with uri
}

void SExpressionExternal::inseartvalue(SReference val){
	//here we will get infomation from web-site
}

//we will stand type of entity
//it can be public or system - it we get true - it's SYSTEM
void SExpressionExternal::standspace(bool sys){
	if(sys)
		space=SYSTEM;
	else
		space=PUBLIC;
}

SExpressionExternal::~SExpressionExternal(){
}

#if INTELIB_TEXT_REPRESENTATIONS == 1
SString SExpressionExternal::TextRepresentation() const{
	SString res("external entity ");
        res+=SString(name);
        res+=SString(" uri:");
        res+=SString(uri);
	if(space==NONE)
		throw "Error!: in SExpressionExternal::TextRepresentation - not complitely builded entity!";
        if(space==SYSTEM)
                res+=" SYSTEM ";
        else
                res+=" PUBLIC ";
        if(unparsed){
                res+=" unparsed:";
                res+=unparsedname->TextRepresentation();
        }
	ENTPRDEPRF(("result in text representation %s\n", res->GetValue()));
        return res;
}
#endif

//we can have unparsed entity (for example - picture)
//here we will stay name of it
void SExpressionExternal::isunparsed(SExpressionDNotation *val){
	unparsed=true;
        unparsedname=val;

}
