#include "lexan_dtd.h"
#include <stdio.h>

DtdLexer::~DtdLexer(){}

//in this function we will start function which have to be next
void DtdLexer::get_dur_sost(){
	switch(sost){
	case start:
		DEPRF(("going to start\n"));
		startpos();
		break;
	case new_description:
		new_will_come();
		break;
	case get_it:
		wait_for_inform();
		break;
	case name_of_disk:
		type_dtd_tag();
		break;
	case follow:
		cont_of_dtd_tag();
		break;
	case empty:
		empty_part();
		break;
	case end_dtd:
		throw "DtdLexer:: analis has been already ended!\n";
		break;
	};
}

//here we can meet ' ' '\n' '\t', if we meet '<' go to new_description
//in other cases it's mistake
void DtdLexer::startpos(){
	DEPRF(("in startpos\n"));
	if(simbol==' ' || simbol=='\n' || simbol=='\t')
		return;
	if(simbol=='<'){
		sost=new_description;
		return;
	}
	if(simbol==EOF)
		throw "DtdLexer:: early EOF in startpos\n";
	throw "DtdLexer:: wrong simbol in startpos\n";
}

//in this function we can only get '!', other simbols - mistake
//if this fucntion has started, new dtd-tag would be formed
void DtdLexer::new_will_come(){
	DEPRF(("in new_will_come\n"));
	if(simbol=='!')
		sost=get_it;
	else if(simbol==EOF)
		throw "DtdLexer:: early EOF in new_will_come\n";
	else
		throw "DtdLexer:: wrong simbol in new_will_come\n";
}

//here we can meet ' ' '\n' '\t', if big letter - type is started
//in other cases it's mistake
void DtdLexer::wait_for_inform(){
	DEPRF(("in wait_for_inform\n"));
	if(simbol==' ' || simbol=='\n' || simbol=='\t')
		return;
	if(simbol>='A' && simbol<='Z' || simbol=='-'){
		letters=insert_end<char>(letters, simbol);
		sost=name_of_disk;
		return;
	}
	if(simbol==EOF)
		throw "DtdLexer:: early EOF in wait_for_mistakes\n";
	throw "DtdLexer:: wrong simbol in wait_for_mistakes\n";
}

//here we can meet only big letters
//' ' '\n' '\t' - next condition; other simbols - mistake
void DtdLexer::type_dtd_tag(){
	DEPRF(("in type_dtd_tag\n"));
	if(simbol>='A' && simbol<='Z'){
		letters=insert_end<char>(letters, simbol);
		return;
	}
	if(simbol=='-'){
		letters=insert_end<char>(letters, simbol);
		lexem_to_end("dtd_type");
                sost=follow;
                return;
	}
	if(simbol==' ' || simbol=='\n' || simbol=='\t'){
		lexem_to_end("dtd_type");
		sost=follow;
		return;
	}
	if(simbol==EOF)
		throw "DtdLexer:: early EOF in type_dtd_tag\n";
	throw "DtdLexer:: wrong simbol in type_dtd_tag\n";
}

//in this function we will insert new part to the end of lexems list
void DtdLexer::lexem_to_end(const char * str){
	if(letters!=NULL){
		lexqueue=insert_end<char *, const char *>(lexqueue, getstrlistmy(letters), str);
		clean_list(letters);
		letters=NULL;
	}
}

//here we will form contents of dtd tag
//end of this part is simbol '>'
void DtdLexer::cont_of_dtd_tag(){
	DEPRF(("in cont_of_dtd_tag\n"));
	if(simbol==' ' || simbol=='\n' || simbol=='\t'){
		lexem_to_end("dtd_follow");
		return;
	}
	//we want '(' and ')' to be separete lexems
	if(simbol=='(' || simbol==')' || simbol==',' || simbol=='|' ||
			simbol=='*' || simbol=='+' || simbol=='?'){
		lexem_to_end("dtd_follow");
		letters=insert_end<char>(letters, simbol);
		lexem_to_end("dtd_follow");
		return;
	}
	if(simbol=='>'){
		lexem_to_end("dtd_follow");
		sost=empty;
		return;
	}
	if(simbol==EOF)
		throw "DtdLexer:: early EOF in cont_of_dtd_tag\n";
	letters=insert_end<char>(letters, simbol);
}

//here is empty space of dtd document
void DtdLexer::empty_part(){
	DEPRF(("empty_part\n"));
	if(simbol=='<'){
		sost=new_description;
		return;
	}
	if(simbol==EOF){
		printf("Analis of DtD is ended\n");
		sost=end_dtd;
		return;
	}
	if(simbol==' ' || simbol=='\n' || simbol=='\t')
		return;
	throw "DtdLexer:: wrong simbol in empty_part\n";
}

//here we will learn - have we ready lexem or not
bool DtdLexer::readylexem(){
	if(lexqueue==NULL)
		return false;
	return true;
}

//here we feed lexer with new char, and it stats it's work
void DtdLexer::feedchar(char c){
	simbol=c;
	get_dur_sost();
}

struct mylist_2<char *, const char *> *DtdLexer::getlexem(){
	if(lexqueue==NULL)
		throw "DtdLexer:: no lexems to give back\n";
	struct mylist_2<char *, const char *> *m=lexqueue;
	lexqueue=lexqueue->next;
	return m;
}
