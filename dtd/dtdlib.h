#ifndef _DTDLIB_H_KURS_4_
#define _DTDLIB_H_KURS_4_

#include <intelib/sexpress/sexpress.hpp>
#include <intelib/genlisp/lispform.hpp>
#include <intelib/lisp/lisp.hpp>
#include <intelib/lisp/lsymbol.hpp>
#include "dtdsexpression.h"
#include "entsexpression.h"

SReference getelemname(int params, const SReference *param);
SReference getelemcont(int params, const SReference *param);
SReference getattlist(int params, const SReference *param);
SReference gettypeofattribute(int params, const SReference *param);
SReference getpresenceattribute(int params, const SReference *param);
SReference getnameofattr(int params, const SReference *param);
SReference entityinside(int params, const SReference *param);
SReference getdentityname(int params, const SReference *param);
SReference insnewattrtype(int params, const SReference *param);
SReference insnewattrpresence(int params, const SReference *param);
SReference insnewdtdinner(int params, const SReference *param);
SReference insnewdtdattr(int params, const SReference *param);
SReference getentityvalue(int params, const SReference *param);
SReference changedentities(int params, const SReference *param);

void define_dtd_functions();

#endif
