(%%%
	(MODULE-NAME "dtdentity")
	(DECLARE-PUBLIC find_entities)
	(DECLARE-USED-MODULE "dtdsymb.h")
	(DECLARE-EXTERNAL getelemcont)
	(DECLARE-EXTERNAL getattrlist)
	(DECLARE-EXTERNAL getentname)
	(DECLARE-EXTERNAL getentvalue)
	(DECLARE-EXTERNAL getattrtype)
	(DECLARE-EXTERNAL getattrvalue)
	(DECLARE-EXTERNAL insattrtype)
	(DECLARE-EXTERNAL insattrvalue)
	(DECLARE-EXTERNAL insnewcont)
	(DECLARE-EXTERNAL insnewattr)
	(DECLARE-EXTERNAL entityinside)
	(DECLARE-EXTERNAL changedent)
)

;this is start function to analis dtd and parametric entities
(defun find_entities (dtd param notparam)
	(cond
		((null dtd) t)
		((analisdtd (car dtd) param notparam) (find_entities (cdr dtd) param notparam))
		(t nil)
	)
)

;here we will start to analis dtd-element
(defun analisdtd (dtd param notparam)
	(cond
		((analiselement (getelemcont dtd) param dtd notparam) (analisattr (getattrlist dtd) param notparam))
		( t nil)
	)
)

;here we will replace dtd-element contence
(defun analiselement (dtd param saved notparam)
	(let ((result (contanalis dtd param notparam)))
	(print "analis result")
	(print result)
	(cond
		((eql result t) nil)
		(t (insnewcont saved result))
	))
)

;here we will analis contence of dtd element
;attention - t - means mistake!
(defun contanalis (dtd param notparam)
	(print "contanalis")
	(print dtd)
	(cond
		((null dtd) nil)
		((atom dtd) (rebuild dtd param notparam))
		((eql (car dtd) "or") (helporlist (cdr dtd) param notparam))
		((atom (car dtd)) (helpdotted dtd param notparam))
		((atom (cdr dtd)) (helpcontanalis dtd param notparam))
		(t (helplistsit dtd param notparam))
	)
)

;this is help function to analis dotted-pair
(defun helpdotted (dtd param notparam)
	(print "helpdotted")
	(print dtd)
	(let ((result (rebuild (car dtd) param notparam)))
	(print result)
	(cond
		((eql result t) t)
		(t (cons result (cdr dtd)))
	))
)

;this is help function to analis "or" situation
(defun helporlist (dtd param notparam)
	(print "helporlist")
	(print dtd)
	(let ((result (contanalis dtd param notparam)))
	(print result)
	(cond
		((eql result t) t)
		(t (cons "or" result))
	))
)

;here will be help function to list situations
(defun helplistsit (dtd param notparam)
	(print "helplistsit")
	(print dtd)
	(let ((res1 (contanalis (car dtd) param notparam)) (res2 (contanalis (cdr dtd) param notparam)))
	(cond
		((or (eql res1 t) (eql res2 t)) t)
		(t (cons res1 res2))
	))
)

;this is help function which orginize contanalis
(defun helpcontanalis (dtd param notparam)
	(print "helpcontanalis")
	(print dtd)
	(let ((result (contanalis (car dtd) param notparam)))
	(cond
		((null result) t)
		(t (cons result (cdr dtd)))
	))
)

;here we will replace entity if it necessary
(defun rebuild (dtd param notparam)
	(print "rebuild")
	(print dtd)
	(let ((result (entityinside dtd notparam)))
	(cond
		(result (replaceent dtd param notparam))
		(t dtd)
	))
)

;here we will replace entity
(defun replaceent (dtd param notparam)
	(print "replaceent")
	(cond
		((null param) nil)
		(t (helpreplaceent dtd param notparam))
	)
)

;here is comming back changed entity
(defun helpreplaceent (dtd param notparam)
	(print "helpreplaceent")
	(let ((result (changedent dtd (car param) notparam)))
	(cond
		((null result) (replaceent dtd (cdr param) notparam))
		(t result)
	))
)

;here we will analis attrlist of dtd element
(defun analisattr(dtd param notparam)
	(print "analisattr")
	(print dtd)
	(cond
		((null dtd) t)
		((helpfuncattr (car dtd) param notparam) (analisattr (cdr dtd) param notparam))
		(t nil)
	)
)

;this is help function to analis dtd-attr
(defun helpfuncattr (dtd param notparam)
	(print "helpfuncattr")
	(print dtd)
	(and (attrtypechange (getattrtype dtd) param dtd notparam) (attrvaluechange (getattrvalue dtd) param dtd notparam))
)

;this is function to analis type of attribute
(defun attrtypechange (dtd param saved notparam)
	(cond
		((null dtd) t)
		((atom dtd) (analistype dtd param saved notparam))
		((analistype (car dtd) param saved notparam) (attrtypechange (cdr dtd) param saved notparam))
		(t nil)
	)
)

;this is function to analis value of attribute
(defun attrvaluechange (dtd param saved notparam)
	(cond
		((null dtd) t)
		((atom dtd) (analisvalue dtd param saved notparam))
		((analisvalue (car dtd) param saved notparam) (attrvaluechange (cdr dtd) param saved notparam))
		(t nil)
	)
)

;here we will analis strings from attr type
(defun analistype (dtd param saved notparam)
	(let ((result (entityinside dtd notparam)))
	(cond
	        (result (findneedent dtd param saved notparam))
                (t t)
        ))
)

;here we will analis strings from attr value
(defun analisvalue (dtd param saved notparam)
	(let ((result (entityinside dtd notparam)))
	(cond
		(result (findneedentval dtd param saved notparam))
		(t t)
	))
)

;here we will find needed entity
(defun findneedent (str param dtd notparam)
	(cond
		((null param) nil)
		((findneedinlst str (car param) dtd notparam) t)
		(t (findneedent str (cdr param) dtd notparam))
	)
)

;here we will find need entity for value
(defun findneedentval (str param dtd notparam)
	(print "findneedent")
	(cond
		((null param) nil)
		((findneedinlstv str (car param) dtd notparam) t)
		(t (findneedentval str (cdr param) dtd notparam))
	)
)

;here we will find entities in list
(defun findneedinlst (str param dtd notparam)
	(let ((result (changedent str param notparam)))
	(cond
		((null result) nil)
		(t (insattrtype dtd result))
	))
)

;here we will find entities in list and inseart then in dtd-attr-value
(defun findneedinlstv (str param dtd notparam)
	(print "findneedinlstv")
	(print param)
	(print str)
	(let ((result (changedent str param notparam)))
	(cond
		((null result) nil)
		(t (insattrvalue dtd result))
	))
)
