#ifndef _KURS_4_NOTATION_SEXPRESSEION_
#define _KURS_4_NOTATION_SEXPRESSEION_

#include <intelib/sexpress/sexpress.hpp>
#include <intelib/sexpress/sstring.hpp>
#include <intelib/lisp/lsymbol.hpp>

class SExpressionDNotation:public SExpression{
	char *name;
	char *parser;
	enum space_namespace{
		SYSTEM,
		PUBLIC,
		NONE
	};
	space_namespace space;
public:
	SExpressionDNotation(const char *n);
	void insertparser(const char *p);
	void settype(bool sys);
	static IntelibTypeId TypeId;
        #if INTELIB_TEXT_REPRESENTATIONS == 1
        virtual SString TextRepresentation() const;
        #endif
	const char *notationname(){return name;}
	const char *parservalue(){return parser;}
protected:
	virtual ~SExpressionDNotation();
};

#endif
