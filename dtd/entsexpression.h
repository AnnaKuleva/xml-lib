#ifndef _ENTITYSEXPRESSION_H_KURS_4_
#define _ENTITYSEXPRESSION_H_KURS_4_

#include "/Users/mac/mylib/mylist.h"
#include <intelib/sexpress/sexpress.hpp>
#include <intelib/sexpress/sstring.hpp>
#include <intelib/lisp/lsymbol.hpp>
#include "notsexpression.h"

#ifdef ENTPRDEBUG
#define ENTPRDEPRF(x) printf x
#else
#define ENTPRDEPRF(x)
#endif

/*here will be definition of entities from DTD-doc
first will be base class for two types of entities*/
class SExpressionEntity:public SExpression{
protected:
        char *name;
       	SReference value;	
public:
        SExpressionEntity(char *n);
        static IntelibTypeId TypeId;
        #if INTELIB_TEXT_REPRESENTATIONS == 1
        virtual SString TextRepresentation() const=0;
        #endif
        virtual void inseartvalue(SReference val)=0;
	char *getname(){return name;}
	SReference getvalue(){return value;}
protected:
        virtual ~SExpressionEntity();
	SExpressionString *reformstring(SExpressionString *str);
};

/*here will be definition of internal entity
we know it's values from dtd-document*/
class SExpressionInternal:public SExpressionEntity{
public:
	SExpressionInternal(char *n):SExpressionEntity(n){}
	static IntelibTypeId TypeId;
        #if INTELIB_TEXT_REPRESENTATIONS == 1
        virtual SString TextRepresentation() const;
        #endif
	virtual void inseartvalue(SReference val);
protected:
	virtual ~SExpressionInternal(){};
};

/*here will be definition of external entity
we will learn it's value from web-site*/
class SExpressionExternal:public SExpressionEntity{
	char *uri;
	enum space_namespace{
		SYSTEM,
		PUBLIC,
		NONE
	};
	space_namespace space;
	bool unparsed;
	SExpressionDNotation *unparsedname;
public:
        SExpressionExternal(char *n);
        static IntelibTypeId TypeId;
        #if INTELIB_TEXT_REPRESENTATIONS == 1
        virtual SString TextRepresentation() const;
        #endif
	virtual void inseartvalue(SReference val);
	void standspace(bool sys);
	void isunparsed(SExpressionDNotation *val);
	void seturi(char *ur);
protected:
	virtual ~SExpressionExternal();
};

#endif
