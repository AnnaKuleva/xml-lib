#include "turndtdins.h"
#include <string.h>
#include "dtdsexpression.h"
#include "dtdentity.hxx"

static LListConstructor L;

//open given file and create object
DtdTurner::DtdTurner(char *str){
	FILE *f;
	f=fopen(str, "r");
	if(f==NULL)
		throw "DTDError:can't open given file\n";
	myfile=new SExpressionStreamFile(f);
	lex=new DtdLexer();
	sost=nothing;
	attsost=xmltagname;
	entsost=entstart;
	notsost=notstart;
	get=NULL;
	dur=NULL;
	dtddoc=SListConstructor();
	param_ent=SListConstructor();
	xml_ent=SListConstructor();
	dtdnotation=SListConstructor();
	notation=NULL;
	external=NULL;
	internal=NULL;
	parametric=false;
	startform=false;
	stack=new MyStack<SReference>;
	attname=NULL;
        attvalue=NULL;
	dtdval=NULL;
        insertattr=NULL;
}
	
//here is the main cycle for working with dtd-documents
//we feed lexer with simbols
//and get ready lexems
void DtdTurner::maincycle(){
	char c;
	struct mylist_2<char *, const char *> *m;
	DTDDEPRF(("in dtd main cycle\n"));
	while((c=myfile->Getc())!=EOF){
		lex->feedchar(c);
		if(lex->readylexem()){
			m=lex->getlexem();
			DTDDEPRF(("lexem: [%s] type [%s]\n", m->value, m->value1));
			//sometimes we could get more than one lexem
			get=insert_end<char *, const char *>(get, m);
			switch_part();
			//DTDDEPRF(("during result is:\n %s\n", dtddoc->TextRepresentation().c_str()));
		}
	}
	lex->feedchar(c);
	has_ready_contentes();
	if(external!=NULL){
		if(parametric)
			param_ent=param_ent||(L|external);
		else
			xml_ent=xml_ent||(L|external);
	}
	if(internal!=NULL)
		if(parametric)
                        param_ent=param_ent||(L|internal);
		else
			xml_ent=xml_ent||(L|internal);
	DTDDEPRF(("result is:\n %s\n", dtddoc->TextRepresentation().c_str()));
	DTDDEPRF(("parametric entity %s\n", param_ent->TextRepresentation().c_str()));
	DTDDEPRF(("entities from xml-file %s\n", xml_ent->TextRepresentation().c_str()));
	DTDDEPRF(("notations from dtd-doc %s\n", dtdnotation->TextRepresentation().c_str()));
	changeparament();
}

LSymbol GETELEMCONT("GETELEMCONT");
LSymbol GETATTRLIST("GETATTRLIST");
LSymbol GETENTNAME("GETENTNAME");
LSymbol GETENTVALUE("GETENTVALUE");
LSymbol GETATTRTYPE("GETATTRTYPE");
LSymbol GETATTRVALUE("GETATTRVALUE");
LSymbol INSATTRTYPE("INSATTRTYPE");
LSymbol INSATTRVALUE("INSATTRVALUE");
LSymbol INSNEWCONT("INSNEWCONT");
LSymbol INSNEWATTR("INSNEWATTR");
LSymbol ENTITYINSIDE("ENTITYINSDE");
LSymbol CHANGEDENT("CHANGEDENT");

void DtdTurner::changeparament(){
	GETELEMCONT->SetFunction(getelemcont);
	GETATTRLIST->SetFunction(getattlist);
	GETENTNAME->SetFunction(getdentityname);
	GETENTVALUE->SetFunction(getentityvalue);
	GETATTRTYPE->SetFunction(gettypeofattribute);
	GETATTRVALUE->SetFunction(getpresenceattribute);
	INSATTRTYPE->SetFunction(insnewattrtype);
	INSATTRVALUE->SetFunction(insnewattrpresence);
	INSNEWCONT->SetFunction(insnewdtdinner);
	INSNEWATTR->SetFunction(insnewdtdattr);
	ENTITYINSIDE->SetFunction(entityinside);
	CHANGEDENT->SetFunction(changedentities);
	LispInit_dtdentity();
	SReference result;
	result=(L|FIND_ENTITIES, ~dtddoc, ~param_ent, *PTheEmptyList).Evaluate();
	if(result.IsEmptyList())
		throw "DtdTurn Error:! wrong work with parametric entities";
	result=(L|FIND_ENTITIES, ~dtddoc, ~xml_ent, T).Evaluate();
	DTDDEPRF(("this is result after replacing entities\n"));
	DTDDEPRF(("%s\n", dtddoc->TextRepresentation().c_str()));
}

//this function is to insert ready contents of dtd elem in existing part of a tree
void DtdTurner::has_ready_contentes(){
	if(!stack->isempty()){
		SReference s=stack->pop();
		if(!stack->isempty())
			throw "DtdError!: we must have empty stack\n";
		SExpressionString *orstr=s.Car().DynamicCastGetPtr<SExpressionString>();
		if(orstr!=0)
			if(strcmp(orstr->GetValue(), "or")==0){
				SExpressionCons *r=new SExpressionCons(s, 1);
				s=r;
			}
		SExpressionString *str=s.DynamicCastGetPtr<SExpressionString>();
		if(str!=0)
			throw "DtdError!: last elem in stack must be list\n";
                DTDDEPRF(("we will insert ready dtd elem\n"));
		if(!s.Cdr().IsEmptyList()){
			SExpressionString *str=s.Cdr().DynamicCastGetPtr<SExpressionString>();
			SExpressionInt *num=s.Cdr().DynamicCastGetPtr<SExpressionInt>();
			if(str!=0 || (num!=0 && num->GetValue()==1))
				dur->insert_inner((L|s));
			else
				dur->insert_inner(s);
		}
		else{
			DTDDEPRF(("!!!!!!here is not list %s\n", s->TextRepresentation().c_str()));
			dur->insert_inner(s);
		}
                dtddoc=dtddoc||(L|dur);
                dur=NULL;
                sost=nothing;
        }
}

//this function is to insert ready contents of dtd elem in existing part of a tree
void DtdTurner::entity_has_ready_contentes(){
        if(!stack->isempty()){
                SReference s=stack->pop();
                if(!stack->isempty())
                        throw "DtdError!: we must have empty stack\n";
                SExpressionString *orstr=s.Car().DynamicCastGetPtr<SExpressionString>();
                if(orstr!=0)
                        if(strcmp(orstr->GetValue(), "or")==0){
                                SExpressionCons *r=new SExpressionCons(s, 1);
                                s=r;
                        }
                SExpressionString *str=s.DynamicCastGetPtr<SExpressionString>();
                if(str!=0)
                        throw "DtdError!: last elem in stack must be list\n";
                DTDDEPRF(("we will insert value of entity\n"));
		if(internal==NULL)
			throw "DtdError!: no internal entity!\n";
                if(!s.Cdr().IsEmptyList()){
                        SExpressionString *str=s.Cdr().DynamicCastGetPtr<SExpressionString>();
                        SExpressionInt *num=s.Cdr().DynamicCastGetPtr<SExpressionInt>();
                        if(str!=0 || (num!=0 && num->GetValue()==1))
                               internal->inseartvalue((L|s));
                        else
                               internal->inseartvalue(s);
                }
                else{
                        DTDDEPRF(("!!!!!!here is not list %s\n", s->TextRepresentation().c_str()));
                        internal->inseartvalue(s);
                }
                if(parametric)
			param_ent=param_ent||(L|internal);
		else
			xml_ent=xml_ent||(L|internal);
                internal=NULL;
		parametric=false;
                sost=nothing;
		entsost=entstart;
        }
}

//here we will search an algorithm to turn geting lexems in s-expressions
void DtdTurner::switch_part(){
	if(strcmp(get->value1, "dtd_follow")==0 && sost==comment){
                int length=strlen(get->value);
		if(length<2){
			deleteel();
			return;
		}
		if(get->value[length-1]=='-' && get->value[length-2]=='-')
                        sost=nothing;
                deleteel();
                return;
        }
	if(strcmp(get->value1, "dtd_type")==0){
		if(internal!=NULL)
			entity_has_ready_contentes();
		else
			has_ready_contentes();
	}
	switch (sost){
	case element:
		element_turn();
		break;
	case attlist:
		attlist_turn();
		break;
	case dnotation:
		notation_turn();
		break;
	case nothing:
		nothing_turn();
		break;
	case entity:
		entity_turn();
		break;
	case comment:
		break;
	};
}

//condition "nothing" we can meet only than it's dtdtype
void DtdTurner::nothing_turn(){
	DTDDEPRF(("in nothing_turn %s\n", get->value));
	if(strcmp(get->value1, "dtd_type")!=0)
		throw "DtdError!:need dtd_type, but didn't get it\n";
	if(strcmp(get->value, "ELEMENT")==0){
		sost=element;
	}
	else if(strcmp(get->value, "ATTLIST")==0){
		sost=attlist;
	}
	else if(strcmp(get->value, "NOTATION")==0){
		sost=dnotation;
	}
	else if(strcmp(get->value, "ENTITY")==0){
		sost=entity;
	}
	else if(strcmp(get->value, "--")==0){
                sost=comment;
	}
	else
		throw "DtdError!:wrong dtd element. Such doesn't exists\n";
	deleteel();
}

//here we will work with sexpressiondtd
void DtdTurner::element_turn(){
	DTDDEPRF(("in element_turn\n"));
	//if we haven't during dtd elem, got structure includes name of xml-tag
	if(dur==NULL){
		dur=new SExpressionDtd(get->value);
		deleteel();
		return;
	}
	//if we have during elem that means that lexem is part of descripton
	if(strcmp(get->value, "EMPTY")==0){ //&& !startform){
		getempty();
		return;
	}
	if(strcmp(get->value, "ANY")==0){ //&& !startform){
		DTDDEPRF(("will work with any\n"));
		getany();
		return;
	}
	//we must meet '(' before anithing else
	if(strcmp(get->value, "(")==0){
		startform=true;
		DTDDEPRF(("one braket push\n"));
		stack->push(get->value);
		stack->print();
		deleteel();
		return;
	}
	if(strcmp(get->value, ")")==0){
		stack->print();
		closebracket();
		return;
	}
	if(strcmp(get->value, "*")==0 || strcmp(get->value, "+")==0 || strcmp(get->value, "?")==0){
		//here we will work with strength
		SReference s=new SExpressionCons(stack->pop(), get->value);
		stack->push(s);
		deleteel();
		return;
	}
	//that means - it is entity
	if(stack->isempty()){
		if(get->value[0]!='&' && get->value[0]!='%')
			throw "DtdError!: not an entity in elenemt_turn";
		dur->insert_inner(new SExpressionString(get->value));
		deleteel();
		dtddoc=dtddoc||(L|dur);
                dur=NULL;
		sost=nothing;
		return;
	}
        //now we will think that in every other case we will have only contentes
        //but we know that we have or
	stack->push(get->value);
	stack->print();
	deleteel();
}

//here we will work with value of internal entity
void DtdTurner::entity_element_turn(){
        DTDDEPRF(("in entity_element_turn\n"));
        //if we have during elem that means that lexem is part of descripton
        if(strcmp(get->value, "EMPTY")==0){ //&& !startform){
                getempty();
                return;
        }
        if(strcmp(get->value, "ANY")==0){ //&& !startform){
                DTDDEPRF(("will work with any\n"));
                getany();
                return;
        }
        //we must meet '(' before anithing else
        if(strcmp(get->value, "(")==0){
                startform=true;
                DTDDEPRF(("one braket push\n"));
                stack->push(get->value);
                stack->print();
                deleteel();
                return;
        }
        if(strcmp(get->value, ")")==0){
                stack->print();
                closebracket();
                return;
        }
        if(strcmp(get->value, "*")==0 || strcmp(get->value, "+")==0 || strcmp(get->value, "?")==0){
                //here we will work with strength
                SReference s=new SExpressionCons(stack->pop(), get->value);
                stack->push(s);
                deleteel();
                return;
        }
        //now we will think that in every other case we will have only contentes
        //but we know that we have or
        if(stack->isempty()){
		SExpressionString *res=new SExpressionString(get->value);
		internal->inseartvalue(res);
		if(parametric)
			param_ent=param_ent||(L|internal);
		else
			xml_ent=xml_ent||(L|internal);
		parametric=false;
		internal=NULL;
		sost=nothing;
		entsost=entstart;
		deleteel();
		return;
	}
	stack->push(get->value);
        stack->print();
        deleteel();
}

//this function is for work when we have empty tag
void DtdTurner::getempty(){
	SReference s=new SExpressionCons("EMPTY", 1);
	dur->insert_inner((L|s));
	dtddoc=dtddoc||(L|dur);
	dur=NULL;
	sost=nothing;
	deleteel();
}

//this function will inseart value of empty tag in the entity
void DtdTurner::getemptyentity(){
	SReference s=new SExpressionCons("EMPTY", 1);
	if(internal!=NULL)
        	internal->inseartvalue((L|s));
        else
		throw "DtdTurner Error!:no internal entity in getemptyentity";
	deleteel();
	sost=nothing;
        entsost=entstart;
        if(parametric)
                param_ent=param_ent||(L|internal);
        else
                xml_ent=xml_ent||(L|internal);
        internal=NULL;
        parametric=false;
}

//this function is for work when we have discription of tag with any contentes
void DtdTurner::getany(){
	DTDDEPRF(("section ANY\n"));
	SReference s=new SExpressionCons("ANY", 1);
	DTDDEPRF(("s is %s\n", s->TextRepresentation().c_str()));
	dur->insert_inner((L|s));
	dtddoc=dtddoc||(L|dur);
	dur=NULL;
	sost=nothing;
	deleteel();
}

//this function is for work when we have discription of tag with any contents
//that will be saved in entity
void DtdTurner::getenyentity(){
        SReference s=new SExpressionCons("ANY", 1);
        if(internal!=NULL)
                internal->inseartvalue((L|s));
        else
                throw "DtdTurner Error!:no internal entity in getemptyentity";
	deleteel();
        sost=nothing;
        entsost=entstart;
        if(parametric)
                param_ent=param_ent||(L|internal);
        else
                xml_ent=xml_ent||(L|internal);
        internal=NULL;
        parametric=false;
}

//here we will work when we meet close braket
void DtdTurner::closebracket(){
	DTDDEPRF(("in closebracket\n"));
	stack->print();
	SReference res=SListConstructor();
	SReference z=stack->pop();
	SExpressionString *str=z.DynamicCastGetPtr<SExpressionString>();
	while(str==0 || strcmp(str->GetValue(), "(")){
		DTDDEPRF(("in pop cycle [%s]\n", z->TextRepresentation().c_str()));
		if(str!=0)
			if(strcmp(str->GetValue(), "|")==0){
				res=work_with_or(res);
				break;
			}
		if(str!=0 && strcmp(str->GetValue(), ",")==0){
			z=stack->pop();
                	str=z.DynamicCastGetPtr<SExpressionString>();
			continue;
		}
		if(str!=0){
			z=new SExpressionCons(z, 1);
		}
		else{
			str=z.Cdr().DynamicCastGetPtr<SExpressionString>();
			if(str==0)
				z=new SExpressionCons(z, 1);
			else{
				SExpressionString *sor=z.Car().DynamicCastGetPtr<SExpressionString>();
				if(sor!=0 && strcmp(sor->GetValue(), "or")==0)
					z=new SExpressionCons(z, 1);
			}
		}
		if(res.IsEmptyList())
			res=z.MakeCons(*PTheEmptyList);
		else{
			res=z.MakeCons(res);
		}
		z=stack->pop();
		str=z.DynamicCastGetPtr<SExpressionString>();
	}
	stack->push(res);
	deleteel();
}

//this fucntion is for convert 'or'
//all elems which are linked with or must stay in brackets (and only they)
SReference DtdTurner::work_with_or(SReference s){
	SReference res=SListConstructor();
	if(!s.Cdr().IsEmptyList())
		throw "DtdError!:on one level with '|' every second must be '|'\n";
	res=s;
	SReference z;
	SExpressionString *str=new SExpressionString("|");
	while(strcmp(str->GetValue(), "(")!=0){
		DTDDEPRF(("in work_with_or %s\n", str->GetValue()));
		if(strcmp(str->GetValue(), "|")!=0)
			throw "DtdError!:if we have or - every second must be '|'\n";
		z=stack->pop();
		str=z.DynamicCastGetPtr<SExpressionString>();
		if(str!=0)
			z=new SExpressionCons(z, 1);
		else{
			str=z.Cdr().DynamicCastGetPtr<SExpressionString>();
			if(str==0)
				z=new SExpressionCons(z, 1);
		}
		res=z.MakeCons(res);
		z=stack->pop();
		str=z.DynamicCastGetPtr<SExpressionString>();
		DTDDEPRF(("work_with_or %s\n", str->GetValue()));
	}
	z=new SExpressionString("or");
	return z.MakeCons(res);
}

//this function is for cleaning memory
void DtdTurner::deleteel(){
	struct mylist_2<char *, const char *> *n;
	n=get;
	n->next=NULL;
	get=get->next;
	delete n;
}

void DtdTurner::attlist_turn(){
	DTDDEPRF(("in attlist_turn\n"));
	switch(attsost){
	case xmltagname:
		attlist_xmlname();
		break;
	case attlistname:
		attlist_name();
		break;
	case attlisttype:
		attlist_type();
		break;
	case attlistvalue:
		attlist_value();
		break;
	};
}

//this function will work with attlistname
void DtdTurner::attlist_name(){
	DTDDEPRF(("attlist_name %s\n", get->value));
	if(strcmp(get->value, "ELEMENT")==0){
		sost=element;
		attsost=xmltagname;
		insertattr=NULL;
		deleteel();
		return;
	}
	if(strcmp(get->value, "ATTLIST")==0){
		sost=attlist;
		attsost=xmltagname;
		insertattr=NULL;
		deleteel();
		return;
	}
	if(strcmp(get->value, "NOTATION")==0){
		sost=dnotation;
		attsost=xmltagname;
		insertattr=NULL;
		deleteel();
		return;
	}
	if(strcmp(get->value, "ENTITY")==0){
		sost=entity;
		attsost=xmltagname;
		insertattr=NULL;
		deleteel();
		return;
	}
	if(attname==NULL){
		attname=new char(strlen(get->value)+1);
		if(strcpy(attname, get->value)==NULL)
			throw "DtdError!:smth wrong in string copy in DtdTurner::attlist_turn";
		attname[strlen(get->value)]='\0';
		deleteel();
		attsost=attlisttype;
		return;
	}
	else
		throw "DtdError!:somthing is inside attlist name in DtdTurner::attlist_name";
}


//here we will analis of dtd-attlist value
//it can be default value of attribute, #REQUIRED, #IMPLIED, #FIXED [value]
void DtdTurner::attlist_value(){
	DTDDEPRF(("attlis_value %s\n", get->value));
	if(get->value[0]=='"'){
		SExpressionString *str = new SExpressionString(get->value);
		if(attvalue==NULL){
			attvalue=str;
			deleteel();
			formattlist();
			return;
		}
		if(attvalue.IsEmptyList())
			throw "DtdError!:empty list in DtdTurner::attlist_value";
		else{
			SExpressionString *carl=attvalue.Car().DynamicCastGetPtr<SExpressionString>();
			if(carl==NULL)
				throw "DtdError!:met not string in DtdTurner::attlist_value";
			if(strcmp(carl->GetValue(), "#FIXED")!=0)
				throw "DtdError!:didn't meet #FIXED in DtdTurner::attlist_value";
			DTDDEPRF(("fixed value\n"));
			attvalue=attvalue||(L|str);
			deleteel();
			formattlist();
			return;
		}
	}
	if(strcmp(get->value, "#FIXED")==0){
		if(attvalue!=NULL)
			throw "DtdError!:not empty list in Dtdurnet::attlist_value!";
		attvalue=SListConstructor();
		attvalue=attvalue||(L|"#FIXED");
		deleteel();
		return;
	}
	if(strcmp(get->value, "#REQUIRED")==0 || strcmp(get->value, "#IMPLIED")==0){
		SExpressionString *str=new SExpressionString(get->value);
		attvalue=str;
		deleteel();
		formattlist();
		return;
	}
	if(!attvalue.IsEmptyList()){
		SExpressionString *carl=attvalue.Car().DynamicCastGetPtr<SExpressionString>();
                if(carl==NULL)
                	throw "DtdError!:met not string in DtdTurner::attlist_value";
                if(strcmp(carl->GetValue(), "#FIXED")!=0)
                	throw "DtdError!:didn't meet #FIXED in DtdTurner::attlist_value";
		formattlist();
	}
}

//here we will analis dtd-attlist type
//type can be list or senses or only type
void DtdTurner::attlist_type(){
	DTDDEPRF(("attlist_type %s\n", get->value));
        if(strcmp("(", get->value)==0){
                dtdval=SListConstructor();
                deleteel();
		return;
        }
        if(dtdval==NULL){
                SExpressionString *str=new SExpressionString(get->value);
                dtdval=str;
                deleteel();
		attsost=attlistvalue;        
        	return;
	}
	if(strcmp(")", get->value)==0){
		deleteel();
		attsost=attlistvalue;
		return;
	}
	if(strcmp("|", get->value)==0){
		deleteel();
		return;
	}
	SExpressionString *str=new SExpressionString(get->value);
	dtdval=dtdval||(L|str);
	DTDDEPRF(("here is dtdval %s\n", dtdval->TextRepresentation().c_str()));
	deleteel();
}

//this function will look for ELEMENT, because we can't have
//ATTLIST definition without ELEMENT definition
void DtdTurner::attlist_xmlname(){
	DTDDEPRF(("attlist_xmlname %s\n", get->value));
	SReference find=dtddoc;
	if(insertattr!=NULL)
		throw "DtdError!:we met unformed attlist in DtdTurner::attlist_xmlname";
	while(!find.IsEmptyList()){
		SReference head=find.Car();
		SExpressionDtd *work=head.DynamicCastGetPtr<SExpressionDtd>();
		if(work==0)
			throw "DtdError!:we got smth strange in dtddoc list in DtdTurner::findsuchxml";
		char *name=work->dtdelemname();
		if(strcmp(name, get->value)==0){
			insertattr=work;
			attsost=attlistname;
			deleteel();
			return;
		}
		find=find.Cdr();
	}
	throw "DtdError!:we got ATTLIST for element which we haven't met yet";
}

//this function will form attribute
void DtdTurner::formattlist(){
	DTDDEPRF(("in formattlist\n"));
	if(attname==NULL)
		throw "DtdError!:haven't attribute name in DtdTurner::formattlist";
	if(attvalue==NULL)
		throw "DtdError!:haven't attribute type in DtdTurner::formattlist";
	if(dtdval==NULL)
		throw "DtdError!:haven't attribute value in DtdTurner::formattlist";
	if(insertattr==NULL)
		throw "DtdError!:didn't save dtd ELEMENT in which we must insert ATTLIST in DtdTurner::formattlist";
	SExpressionDAtr *datr=new SExpressionDAtr(attname, dtdval, attvalue);
	insertattr->insert_attribute(datr);
	attname=NULL;
	attvalue=NULL;
	dtdval=NULL;
	//insertattr=NULL;
	sost=attlist;
	attsost=attlistname;
	DTDDEPRF(("result in  formattlist %s\n", dtddoc->TextRepresentation().c_str()));
}

void DtdTurner::notation_turn(){
	DTDDEPRF(("in notation_turn\n"));
	switch(notsost){
	case notstart:
		startnotation();
		break;
	case notname:
		getnotationname();
		break;
	case notsystem:
		worksysnotation();
		break;
	case notpublic:
		workpublnotation();
		break;
	case notpublicid:
		workpubidnotation();
		break;
	};
}

//here we will work create new notation
void DtdTurner::startnotation(){
	if(notation!=NULL)
		throw "DtdError!:in startnotation we have umcomplete notation\n";
	notation=new SExpressionDNotation(get->value);
	deleteel();
	notsost=notname;
}

//here we will learn is notation complete or may be it will be public
void DtdTurner::getnotationname(){
	if(notation==NULL)
                throw "DtdError!:in getnotationname we have got notation\n";
	if(strcmp(get->value, "SYSTEM")==0){
		notsost=notsystem;
		deleteel();
		return;
	}
	if(strcmp(get->value, "PUBLIC")==0){
		notsost=notpublic;
		deleteel();
		return;
	}
	notation->insertparser(get->value);
	sost=nothing;
	dtdnotation=dtdnotation||(L|notation);
	notation=NULL;
	deleteel();
	notsost=notstart;
}

//here we will work with system notation
void DtdTurner::worksysnotation(){
	if(notation==NULL)
                throw "DtdError!:in worksysnotation we have got notation\n";
	notation->insertparser(get->value);
        notation->settype(true);
	sost=nothing;
	deleteel();
        dtdnotation=dtdnotation||(L|notation);
        notation=NULL;
        notsost=notstart;
}

//here we will work with public notation
void DtdTurner::workpublnotation(){
	if(notation==NULL)
                throw "DtdError!:in workpublnotation we have got notation\n";
	notation->insertparser(get->value);
	notation->settype(false);
	notsost=notpublicid;
	deleteel();
}

//here we will work with public notation
//maybe we have already ended to create notation
void DtdTurner::workpubidnotation(){
	notsost=notstart;
	dtdnotation=dtdnotation||(L|notation);
	if(strcmp(get->value, "ELEMENT")==0){
                sost=element;
		deleteel();
		notation=NULL;
		return;
        }
        if(strcmp(get->value, "ATTLIST")==0){
        	deleteel();
	        sost=attlist;
		notation=NULL;
		return;
        }
        if(strcmp(get->value, "NOTATION")==0){
                sost=dnotation;
        	deleteel();
		notation=NULL;
		return;
	}
        if(strcmp(get->value, "ENTITY")==0){
                sost=entity;
		deleteel();
		notation=NULL;
		return;
        }
	notation->insertparser(get->value);
	notation=NULL;
	deleteel();
	sost=nothing;
}

void DtdTurner::entity_turn(){
	DTDDEPRF(("in entity_turn\n"));
	DTDDEPRF(("we got %s\n", get->value));
	switch(entsost){
	case entstart:
		startanalisentity();
		break;
	case entname:
		getentityname();
		break;
	case parametr:
		willbeparametrent();
		break;
	case entsetvalue:
		setentityval();
		break;
	case systemexternal:
		systementity();
		break;
	case publicexternal:
		publicentity();
		break;
	case entpublic_id:
		getpublic_id();
		break;
	case entseturi:
		geturientity();
		break;
	case entunparsed:
		unparsedentity();
		break;
	};
}

//here we will get information that we startanalis
void DtdTurner::startanalisentity(){
	DTDDEPRF(("in startanalisentity\n"));
	//that will be parametric entity
	if(strcmp(get->value, "%")==0){
		entsost=parametr;
		parametric=true;
		deleteel();
		return;
	}
	//that will be entity from xml-file
	nameofent=new char[35];
	strcpy(nameofent, get->value);
	deleteel();
	entsost=entname;
}

//here we get name of dtd 
void DtdTurner::getentityname(){
	DTDDEPRF(("getentityname\n"));
	//that is external system entity
	if(strcmp(get->value, "SYSTEM")==0){
		entsost=systemexternal;
		external=new SExpressionExternal(nameofent);
		external->standspace(true);
		delete []nameofent;
		nameofent=NULL;
		deleteel();
		return;
	}
	//that will be external public entity
	if(strcmp(get->value, "PUBLIC")==0){
		entsost=publicexternal;
		external=new SExpressionExternal(nameofent);
		external->standspace(false);
		delete []nameofent;
		nameofent=NULL;
		deleteel();
		return;
	}
	//that will be internal entity
	internal=new SExpressionInternal(nameofent);
	delete []nameofent;
	nameofent=NULL;
	setentityval();
}

//here we will wait for parametric name
void DtdTurner::willbeparametrent(){
	DTDDEPRF(("willbeparametrent\n"));
	nameofent=new char[35];
	strcpy(nameofent, get->value);
	deleteel();
	entsost=entname;
}

//here we will inseart value in internal entity
void DtdTurner::setentityval(){
	DTDDEPRF(("setentityval\n"));
	if(internal==NULL)
		throw "DtdTurner Error!:setentityval() we don't have entity";
	entsost=entsetvalue;
	if(strcmp(get->value, "\"")==0){
		deleteel();
		return;
	}
	entity_element_turn();
}

//here we will work with system entity 
void DtdTurner::systementity(){
	DTDDEPRF(("systementity\n"));
	if(external==NULL)
		throw "DtdTurner Error!:systementity() we don't have entity";
	external->seturi(get->value);
	deleteel();
	entsost=entseturi;
}

//here we will work with
void DtdTurner::publicentity(){
	DTDDEPRF(("publicentity\n"));
	deleteel();
	entsost=entpublic_id;
}

//here we will work with public entity
void DtdTurner::getpublic_id(){
	DTDDEPRF(("getpublic_id\n"));
	if(external==NULL)
		throw "DtdTurner Error!:getpublic_id we don't have entity";
	external->seturi(get->value);
	deleteel();
	entsost=entseturi;
}

//here we will get needed notation
SExpressionDNotation *DtdTurner::getnotation(const char *name){
	SReference analis=dtdnotation;
	DTDDEPRF(("in getnotation %s\n", dtdnotation->TextRepresentation().c_str()));
	while(!analis.IsEmptyList()){
		SReference head=analis.Car();
		SExpressionDNotation *r=head.DynamicCastGetPtr<SExpressionDNotation>();
		if(r==0)
			throw "DtdError!: smth wrong with dtd-notations list in getnotation\n";
		if(strcmp(name, r->notationname())==0)
			return r;
		analis=analis.Cdr();
	}
	throw "DtdError!:we don't have needed entity in getnotation\n";
}

//here we will build ready exturnal entity
void DtdTurner::geturientity(){
	DTDDEPRF(("geturientity\n"));
	if(strcmp("NDATA", get->value)==0){
		entsost=entunparsed;
		deleteel();
		return;
	}
	if(parametric)
		param_ent=param_ent||(L|external);
	else
		xml_ent=xml_ent||(L|external);
	external=NULL;
	parametric=false;
	entsost=entstart;
	if(strcmp(get->value, "ELEMENT")==0){
                sost=element;
                deleteel();
                return;
        }
        if(strcmp(get->value, "ATTLIST")==0){
                sost=attlist;
                deleteel();
                return;
        }
        if(strcmp(get->value, "NOTATION")==0){
                sost=dnotation;
                deleteel();
                return;
        }
        if(strcmp(get->value, "ENTITY")==0){
                sost=entity;
                deleteel();
                return;
        }
	if(strcmp(get->value, "--")==0){
		sost=comment;
		deleteel();
		return;
	}

}

//here we will build unparsed entity
void DtdTurner::unparsedentity(){
	DTDDEPRF(("unparsedentity\n"));
	if(external==NULL)
		throw "DtdTurner Error!:unparsedentity() we don't have entity";
	SExpressionDNotation *r=getnotation(get->value);
	external->isunparsed(r);
	if(parametric)
                param_ent=param_ent||(L|external);
        else
                xml_ent=xml_ent||(L|external);
	external=NULL;
        parametric=false;
        entsost=entstart;
	sost=nothing;
	deleteel();
}
