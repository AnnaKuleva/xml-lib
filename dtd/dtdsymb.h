#ifndef _DTDSYMB_H_KURS_4_
#define _DTDSYMB_H_KURS_4_

#include <intelib/lisp/lisp.hpp>
#include <intelib/lisp/lsymbol.hpp>

extern LSymbol GETELEMCONT;
extern LSymbol GETATTRLIST;
extern LSymbol GETENTNAME;
extern LSymbol GETENTVALUE;
extern LSymbol GETATTRTYPE;
extern LSymbol GETATTRVALUE;
extern LSymbol INSATTRTYPE;
extern LSymbol INSATTRVALUE;
extern LSymbol INSNEWCONT;
extern LSymbol INSNEWATTR;
extern LSymbol ENTITYINSIDE;
extern LSymbol CHANGEDENT;

#endif
