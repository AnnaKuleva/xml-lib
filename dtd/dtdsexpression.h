#ifndef _DTDSEXPRESSION_H_KURS_4_
#define _DTDSEXPRESSION_H_KURS_4_

#include "/Users/mac/mylib/mylist.h"
#include <intelib/sexpress/sexpress.hpp>
#include <intelib/sexpress/sstring.hpp>
#include <intelib/lisp/lsymbol.hpp>
#include "attsexpression.h"

#ifdef SEXPRDEBUG
#define SEXPRDEPRF(x) printf x
#else
#define SEXPRDEPRF(x)
#endif

//definition if ELEMENT
class SExpressionDtd:public SExpression{
	char *name;
	SReference inner;
	SReference attlist;
public:
	SExpressionDtd(char *n);
	static IntelibTypeId TypeId;
	#if INTELIB_TEXT_REPRESENTATIONS == 1
        virtual SString TextRepresentation() const;
        #endif
	void insert_inner(char *inname, char *rep);
	void insert_inner(SReference k);
	void insert_attribute(SExpressionDAtr *atr);
	void insert_whole_num(char *in);
	char *dtdelemname(){return name;}
        SReference dtdelemcont(){return inner;}
	SReference getattlist(){return attlist;}
	void insnewinner(SReference ins){inner=ins;}
	void insnewattr(SReference ins){attlist=ins;}
protected:
	virtual ~SExpressionDtd();
};

#endif
