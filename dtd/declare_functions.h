#ifndef _DECLARE_DTD_FUNCTIONS_KULEVA_
#define _DECLARE_DTD_FUNCTIONS_KULEVA_

//_______________________________________
//___________dtd_functions_______________|
//_______________________________________|

LSymbol DTDELNAME("DTDELNAME");
LSymbol DTDELCONT("DTDELCONT");
LSymbol GETDATTRNAME("GETDATTRNAME");
LSymbol GETDATTRTYPE("GETDATTRTYPE");
LSymbol GETDATTRVALUE("GETDATTRVALUE");
LSymbol GETDATTRLIST("GETDATTRLIST");

#endif
