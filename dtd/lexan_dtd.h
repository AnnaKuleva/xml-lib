#ifndef _LEXAN_H_KURS_4_
#define _LEXAN_H_KURS_4_

#include <stdio.h>
#include "../helplib/mylist.h"

#ifdef DTDDEBUG
#define DTDDEPRF(x) printf x
#else
#define DTDDEPRF(x)
#endif

#ifdef DEBUG
#define DEPRF(x) printf x
#else
#define DEPRF(x)
#endif


enum dtdcond{
	start,
	new_description,
	get_it,
	name_of_disk,
	follow,
	empty,
	end_dtd
};

class DtdLexer{
	dtdcond sost;
	char simbol;
	struct mylist<char> *letters;
	struct mylist_2<char *, const char *> *lexqueue;
public:
	DtdLexer():sost(start), letters(NULL), lexqueue(NULL){}
	~DtdLexer();
	void get_dur_sost();
	void startpos();
	void new_will_come();
	void wait_for_inform();
	void type_dtd_tag();
	void cont_of_dtd_tag();
	void empty_part();
	bool readylexem();
	void feedchar(char c);
	void lexem_to_end(const char *str);
	struct mylist_2<char *, const char *> *getlexem();
};

#endif
