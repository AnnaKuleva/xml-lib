ifneq ($(INTELIB),)
	ILL = $(INTELIB)/build/intelib/ill
	LIBDIR = $(INTELIB)
	INCLUDER=$(INTELIB)/build
	INTELIBFLAGS = -I$(LIBDIR)/build -L$(LIBDIR)/build/intelib
else
	ILL = ill
	INTELIBFLAGS =
endif

CXX=g++

CC=gcc

ifneq ($(DEBUG),yes)
DEBUG=no
endif

TARGETDIR = ./build

ifneq ($(TARGETDIR),$(filter /%,$(TARGETDIR)))
TARGETDIRFP = $(CURDIR)/$(TARGETDIR)/xml_lib
else
TARGETDIRFP = $(TARGETDIR)/xml_lib
endif

$(TARGETDIRFP): 
	mkdir -p $(TARGETDIRFP)

libxmllib.a: FORCE
	cd helplib && $(MAKE) all TARGETDIR=$(TARGETDIRFP)/.. \
				TARGETLIBNAME=$@
	cd dtd && $(MAKE) all_add TARGETDIR=$(TARGETDIRFP)/.. \
				TARGETLIBNAME=$@
	cd xml && $(MAKE) all_add TARGETDIR=$(TARGETDIRFP)/.. \
				TARGETLIBNAME=$@
	cd xslt && $(MAKE) all_add TARGETDIR=$(TARGETDIRFP)/.. \
				TARGETLIBNAME=$@
	cd sexpress_date && $(MAKE) all_add TARGETDIR=$(TARGETDIRFP)/.. \
				TARGETLIBNAME=$@
	
ifeq ($(INSTALLMODE),native)

PREFIX = /usr/local
SYMLINK_PREFIX =
INCLUDEDIR = $(PREFIX)/include/xml_lib
INCLUDEDIR_SYMLINK = no
SHAREDIR = $(PREFIX)/share/xml_lib

else

SYSPREFIX = /usr/local
PREFIX = $(SYSPREFIX)/xml_lib
SYMLINK_PREFIX = $(PREFIX)
INCLUDEDIR = $(PREFIX)/include
INCLUDEDIR_SYMLINK = yes
SHAREDIR = $(PREFIX)/share

endif

BINDIR = $(PREFIX)/bin
LIBDIR = $(PREFIX)/lib


INSTALL = install
INSTALL_DIR = $(INSTALL) -d
INSTALL_HEADERS = $(INSTALL) -m 0644
INSTALL_BIN = $(INSTALL) -m 0755
INSTALL_LIB = $(INSTALL) -m 0644
INSTALL_DATA = $(INSTALL) -m 0644

library: $(TARGETDIRFP) FORCE
	ln -sf $(CURDIR)/helplib $(TARGETDIRFP)
	ln -sf $(CURDIR)/dtd $(TARGETDIRFP)
	ln -sf $(CURDIR)/xml $(TARGETDIRFP)
	ln -sf $(CURDIR)/xslt $(TARGETDIRFP)
	ln -sf $(CURDIR)/sexpress_date $(TARGETDIRFP)
	$(MAKE) libxmllib.a 

install: FORCE
	for D in helplib xml dtd xslt sexpress_date; do \
		$(INSTALL_DIR) $(DESTDIR)$(INCLUDEDIR)/$$D ; \
		$(INSTALL_HEADERS) $$D/*.h  $(DESTDIR)$(INCLUDEDIR)/$$D ; \
	done
	$(INSTALL_DIR) $(DESTDIR)$(LIBDIR) $(DESTDIR)$(BINDIR) \
						$(DESTDIR)$(SHAREDIR)
	$(INSTALL_LIB) $(TARGETDIRFP)/*.a $(DESTDIR)$(LIBDIR)
ifndef DESTDIR
ifeq ($(INCLUDEDIR_SYMLINK),yes)
	cd $(DESTDIR)$(INCLUDEDIR) && ( [ -e xml_lib ] || ln -sfn . xml_lib )
endif
ifneq ($(SYMLINK_PREFIX),)
ifneq ($(SYMLINK_PREFIX),$(PREFIX))
	ln -sfn $(PREFIX) $(SYMLINK_PREFIX)
endif
	$(INSTALL_DIR) $(SYSPREFIX)/bin $(SYSPREFIX)/lib
	ln -sf $(SYMLINK_PREFIX)/lib/libxmllib* $(SYSPREFIX)/lib
	$(INSTALL_DIR) $(SYSPREFIX)/include
	[ -e $(SYSPREFIX)/include/xml_lib ] || \
		ln -sf $(SYMLINK_PREFIX)/include $(SYSPREFIX)/include/xml_lib
endif
endif

#TESTDIRS = jabberclient

#jabberclient: FORCE
#	cd jabberclient && $(MAKE) all TARGETDIR=$(TARGETDIRFP)

FORCE:
