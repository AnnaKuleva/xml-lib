#ifndef _DEFINE_FUNCTIONS_KULEVA_
#define _DEFINE_FUNCTIONS_KULEVA_

//_______________________________________
//___________printf_functions____________|
//_______________________________________|

LSymbol PRINTBEAUTY("PRINTBEAUTY");
LSymbol PRINTSMTH("PRINTSMTH");
LSymbol INFILESTR("INFILESTR");
LSymbol INFILETAG("INFILETAG");
LSymbol INFILEATTR("INFILEATTR");
LSymbol INFILECLOSETAG("INFILECLOSETAG");
LSymbol PRINTEMPTY("PRINTEMPTY");

//_______________________________________
//__________xml-tag_functions____________|
//_______________________________________|

LSymbol NEWTAG("NEWTAG");
LSymbol COPYTAG("COPYTAG");
LSymbol INSCONT("INSCONT");
LSymbol ISXMLTAG("ISXMLTAG");
LSymbol XMLNAMEGET("XMLNAMEGET");
LSymbol GETCONTENT("GETCONTENT");
LSymbol NEWCONTENT("NEWCONTENT");

//________________________________________
//___________xml-attr_functions___________|
//________________________________________|

LSymbol NEWATTR("NEWATTR");
LSymbol COPYATTR("COPYATTR");
LSymbol XMLATTRGET("XMLATTRGET");
LSymbol NEWATTRLIST("NEWATTRLIST");
LSymbol GETATTRCONTENT("GETATTRCONTENT");
LSymbol INSATTRCONT("INSATTRCONT");
LSymbol GETATTRNAME("GETATTRNAME");
LSymbol INSREADYATTR("INSREADYATTR");

//________________________________________
//_____file-stream_&&_file_functions______|
//________________________________________|

LSymbol CLOSEFILESTREAM("CLOSEFILESTREAM");
LSymbol FILESTREAM("FILESTREAM");
LSymbol DIRFILES("DIRFILES");
LSymbol EXISTSDIR("EXISTSDIR");
LSymbol TURNOPENFILE("TURNOPENFILE");
LSymbol PRINTINFILEXML("PRINTINFILEXML");

//________________________________________
//__________string_functions______________|
//________________________________________|

LSymbol ISSTRING("ISSTRING");
LSymbol TRANSFORMSTR("TRANSFORMSTR");
LSymbol PARSTAGS("PARSTAGS");
LSymbol CHANGEENT("CHANGEENT");
LSymbol DELSIMB("DELSIMB");
LSymbol CHANGEAMP("CHANGEAMP");
LSymbol NUMBFROMSTR("NUMBFROMSTR");
LSymbol STRFROMNUMB("STRFROMNUMB");
LSymbol CONCATSTR("CONCATSTR");
LSymbol PARSEXPATH("PARSEXPATH");
LSymbol ONLYNAME("ONLYNAME");
LSymbol STRCONCATNUMB("STRCONCATNUMB");
LSymbol GETNUMBERSTR("GETNUMBERSTR");
LSymbol NUMFROMSTR("NUMFROMSTR");
LSymbol DIVIDESTRSIM("DIVIDESTRSIM");
LSymbol TAGINSTR("TAGINSTR");

//________________________________________
//__________number_functions______________|
//________________________________________|

LSymbol GENERATENUM("GENERATENUM");
LSymbol DIVIDENUM("DIVIDENUM");

#endif
