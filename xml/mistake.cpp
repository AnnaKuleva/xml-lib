#include "mistake.h"
#include <unistd.h>
#include "structures.h"
#include <string.h>

void BadNameEl::sendmes(int port){
	char buf1[]="wrong simbol ";
	char buf2[]=" in name of element\n";
	char buf3[2]={c, '\0'};
	write(port, buf1, sizeof(buf1));
	write(port, buf3, sizeof(buf3));
	write(port, buf2, sizeof(buf2));
	closedis(port);
}

void BadNameAttr::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" in name of attribute\n";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
	closedis(port);
}

void BadInBegin::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" between elements\n";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

void BadBetwSk::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" after <\n";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

void BadInDiv::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" in dividing part";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

void BadAttrCB::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" after = in attr meaning\n";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

void BadEnd::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" in the end of file\n";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

void BadInEmptyTag::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" wrong simbol in empty tag %c\n";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

void BadInHidAn::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" in smth what must be hidden from analiz\n";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

void BadInEndCdata::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" instead ] in CDATA\n";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

void BadInCloseEl::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" instead >\n";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

void BadCdata::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" in part CDATA[";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

void BadComment::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" instead - in Comment\n";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

void EarlyEnd::sendmes(int port){
        char buf1[]="met EOF very early\n";
        write(port, buf1, sizeof(buf1));
        closedis(port);
}

void NotXml::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" instead phrase xml\n";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

void BadInEndProlog::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" instead > in prolog, comment or CDATA\n";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

void BadNumOfEl::sendmes(int port){
        char buf1[]="wrong number of close tags\n";
	write(port, buf1, sizeof(buf1));
        closedis(port);
}

void OnlyOneHead::sendmes(int port){
        char buf1[]="we can have only one head el in correct document\n";
        write(port, buf1, sizeof(buf1));
        closedis(port);
}

void BadProlog::sendmes(int port){
        char buf1[]="prolog must be declared before any elements\n";
        write(port, buf1, sizeof(buf1));
        closedis(port);
}

void TooManyProlog::sendmes(int port){
        char buf1[]="we can have only one prolog, but here are more\n";
        write(port, buf1, sizeof(buf1));
        closedis(port);
}

void NotProlog::sendmes(int port){
        char buf1[]="is wasn't prolog, but was closed how prolog ?>\n";
        write(port, buf1, sizeof(buf1));
        closedis(port);
}

void BadDocPos::sendmes(int port){
        char buf1[]="DOCTYPE section can be only inside head tag\n";
        write(port, buf1, sizeof(buf1));
        closedis(port);
}

void BadDoctype::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" in definition\n";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

void ErrorInForming::sendmes(int port){
        char buf1[]="we want to form lexen in sost %s where we can't do this\n";
        write(port, buf1, sizeof(buf1));
        closedis(port);
}

void EmptyListOfLex::sendmes(int port){
        char buf1[]="we get message that lexan has ready ";
	char buf2[]="lexem, but list of ready lexems is empty\n";
        write(port, buf1, sizeof(buf1));
	write(port, buf2, sizeof(buf2));
        closedis(port);
}

void MoreClose::sendmes(int port){
        char buf1[]="we have more close tags than open tags\n";
        write(port, buf1, sizeof(buf1));
        closedis(port);
}

void MoreOpen::sendmes(int port){
        char buf1[]="we have more open tags than close tags\n";
        write(port, buf1, sizeof(buf1));
        closedis(port);
}

void NoAttrName::sendmes(int port){
        char buf1[]="we get attrcontent, but we haven`t attrname\n";
        write(port, buf1, sizeof(buf1));
        closedis(port);
}

void DifferentNames::sendmes(int port){
        //DEPRF3(("in mistake\n"));
	char buf1[]="different names of open and close tag\n";
        write(port, buf1, sizeof(buf1));
        closedis(port);
}

void BadFClTag::sendmes(int port){
        char buf1[]="wrong simbol ";
        char buf2[]=" in between sost\n";
        char buf3[2]={c, '\0'};
        write(port, buf1, sizeof(buf1));
        write(port, buf3, sizeof(buf3));
        write(port, buf2, sizeof(buf2));
        closedis(port);
}

ServPartErrors::ServPartErrors(const char *s){
	int i=0;
	while(s[i]!='\0'){
		buf[i]=s[i];
		i++;
	}
	buf[i]='\0';
}

void DontEnd::sendmes(int port){
        char buf1[]="tag hadn't been complete\n";
        write(port, buf1, sizeof(buf1));
        closedis(port);
}

void WrongType::sendmes(int port){
        char buf1[]="we get lexem with wrong type\n";
        write(port, buf1, sizeof(buf1));
        closedis(port);
}

NoSuchFile::NoSuchFile(char *s){
	int i=0;
	while(s[i]!='\0' || i<128){
		str[i]=s[i];
		i++;
	}
	str[i]='\0';
	delete []s;
}

DtdErrorClass::DtdErrorClass(char *s){
	str=new char[strlen(s)+1];
	strcpy(str, s);
	str[strlen(s)]='\0';
}

void DtdErrorClass::print(){
	printf("DtdError - mistake in comparison between dtd-document and xml-document\n");
	printf("%s\n", str);
}
