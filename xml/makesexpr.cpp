#include "makesexpr.h"
#include "lexan.h"
#include "xmlsexpress.h"
#include <stdio.h>
#include <string.h>
#include "mistake.h"
#include "lispint.h"
#include "../dtd/dtdcompare.hxx"


MakeXmlSExpression::MakeXmlSExpression(bool type):condition(smthelse),
		wait(NULL),save(NULL), avt(type), nonstop(true){
}

//think about correct destructor
MakeXmlSExpression::~MakeXmlSExpression(){
}

static LListConstructor L;

SReference MakeXmlSExpression::start(bool flag){
	DEPRF1(("in start\n"));
	SReference result=SListConstructor();
	dtddoc=SListConstructor();
	if(flag)
		getprolog();
	getdoctype();
	while(nonstop){
		DEPRF3(("I'm looking for another xml-tags\n"));
		SReference get=turn();
		if(nonstop){
			DEPRF3(("i won't stop now!\n"));
			result=result||get;
		}
		DEPRF3(("result in turn %s\n", result->TextRepresentation().c_str()));
	}
	DEPRF3(("here we got our result\n"));
	//here we will analize dtddoc and xml doc
	printf("start compeare dtd-document and xml-document\n");
	bool correct=dtdvsxml(result);
	if(!correct)
		throw "According to dtd-document there are mistakes in xml-document\n";
	DEPRF3(("here is xml sexpression %s\n", result->TextRepresentation().c_str()));
	return result;
}

bool MakeXmlSExpression::dtdvsxml(SReference xml){
	DEPRF1(("dtdvxml %s\n", dtddoc->TextRepresentation().c_str()));
	if(dtddoc.IsEmptyList())
		return true;
	printf("start compeare dtd-document and xml-document\n");
        LispInit_dtdcompare();
	DEPRF3(("converted\n"));
        SReference got;
	(L|DTDCOMPARE, xml.Car(), ~dtddoc).Evaluate();
        (L|STARTATTRLIST, xml.Car(), ~dtddoc).Evaluate();
	printf("everything is good\n");
	if(got.IsEmptyList())
		return false;
	return true;
}

lexem *MakeXmlSExpression::lexemanaliz(){
        char c;
	struct lexem* caught;
        while((c=getsimbol())!=EOF){
                DEPRF1(("simbol %c\n", c));
                avt.feedchar(c);
                if(avt.lexemisready()){
                        caught=avt.getlexem();
                	DEPRF3(("name = %s, type =%s\n", caught->name, caught->type));
			return caught;
                }
        }
        avt.feedchar(c);
	if(avt.getdepth()!=0)
                        throw MoreOpen();
	if(avt.lexemisready()){
                        caught=avt.getlexem();
                        return caught;
        }
        //if we go out here - we had already read all file - so special lexem
        return caught;
};

//we need this function to work with docktype of our xml-document
void MakeXmlSExpression::getdoctype(){
	mylist<char *> *p=NULL;
	if(wait==NULL){
		//that means that we haven't got any lexams yet
		//(may be we read that when we worked with prolog)
		insertnewlex(lexemanaliz());
	}
	changecondition();
	if(condition!=doctype){
		//that means that we have no doc type
		DEPRF1(("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!here!"));
		return;
	}
	while(condition==doctype){
		DEPRF1(("we are in DOCTYPE cycle\n"));
		if(wait==NULL){
			insertnewlex(lexemanaliz());
			changecondition();
		}
		else{
			DEPRF1(("!%s!\n", wait->name));
		}
		if(condition==doctype){
			//here we must do smth with contentce of DOCTYPE
			int len=strlen(wait->name);
			char *str=new char[len+1];
			strcpy(str, wait->name);
			p=insert_end<char *>(p, str);
			deletelex();
		}
	}
	printf("DTD section:\n");
	DEPRF1(("name of dtdfile %s\n", p->value));
	DtdTurner *dtddoc1=new DtdTurner(p->value);
	dtddoc1->maincycle();
	//DEPRF1(("!!!%s!!!\n", dtddoc1->getdtd()->TextRepresentation().c_str()));
	dtddoc=dtddoc1->getdtd();
	delete dtddoc1;
	//DEPRF1(("[%s]\n", dtddoc->TextRepresentation().c_str()));
	printf("End DTD section\n");
}

void MakeXmlSExpression::getprolog(){
	DEPRF2(("it's doctype and i want get prolog\n"));
	if(wait!=NULL)
		throw "Something wrong in prolog of doctype!\n";
	insertnewlex(lexemanaliz());
	changecondition();
	if(condition!=prolog){
		//turn();
		return;
	}
	deletelex();
	while(true){
		DEPRF3(("cycle in getprolog\n"));
                if(wait==NULL){
                        insertnewlex(lexemanaliz());
                }
                changecondition();
                if(condition==attrname){
                        DEPRF1(("we got name of attr\n"));
                        save=wait;
                        save->next=NULL;
                        wait=wait->next;
                        continue;
                }
                if(condition==attrcontent){
                        if(save==NULL){
                                throw NoAttrName();
                        }
                        if(save->name!=NULL)
                                delete []save->name;
                        delete save;
                        save=NULL;
                        deletelex();
                        continue;
                }
		if(condition==end){
			deletelex();
			break;
		}
		if(condition==smthelse)
                        throw "strange type in MakeXmlSExpression::getprolog\n";
	}
}

SReference MakeXmlSExpression::turn(){
	DEPRF2(("in turn\n"));
	SString *strg;
	bool strstart=false;
	if(wait==NULL){
		insertnewlex(lexemanaliz());
		changecondition();
	}
	SExpressionXmlTag *during;
	if(condition==opentag){
		DEPRF1(("it's opentag\n"));
        	during=new SExpressionXmlTag(wait->name);
		deletelex();
	}
	else{
	if(condition==end){
		nonstop=false;
		deletelex();
		return NULL;
	}
	else{
		throw WrongType();
	}
	}
	while(true){
		DEPRF3(("cycle in turn\n"));
		if(wait==NULL){
			insertnewlex(lexemanaliz());
		}
		changecondition();
		if(condition==attrname){
                	DEPRF1(("we got name of attr\n"));
			save=wait;
			save->next=NULL;
			wait=wait->next;
                	//because all attributes must go in pairs
        		continue;
		}
        	if(condition==attrcontent){
			if(save==NULL){
				throw NoAttrName();
			}
                	SExpressionXmlAttr *a;
			DEPRF2(("save name = %s\n", save->name));
			a=new SExpressionXmlAttr(save->name, wait->name);
			DEPRF2(("save name = %s\n", save->name));
			if(save->name!=NULL)
                                delete []save->name;
                        delete save;
                        save=NULL;
			deletelex();
                	//here we got hole attr, and we must insert it in last xml-tag
                	SReference m=(L|a);
			during->InsertAttr(m);
			continue;
        	}
		if(condition==tagcontent){
			if(strstart){
				DEPRF3(("it's strintg in tur %s\n", strg->c_str()));
				strg->operator+=(' ');
				strg->operator+=(wait->name);
			}
			else{
				strg=new SString(wait->name);
				strstart=true;
			}
			deletelex();
			continue;
		}
		if(condition==opentag){
			if(strstart){
				SReference m=(L|strg->c_str());
                        	during->InsertElem(m);
				delete strg;
				strstart=false;
			}
			during->InsertElem(turn());
			continue;
		}
		if(condition==closetag){
			if(strstart){
                        	SReference m=(L|strg->c_str());
                                during->InsertElem(m);
                                delete strg;
                                strstart=false;
                        }
			if(strcmp(during->GetName(), wait->name)){
				DEPRF2(("during->GetName - [%s]\n", during->GetName()));
				DEPRF2(("wait->name - [%s]\n", wait->name));
				throw DifferentNames();
			}
			deletelex();
			break;
		}
		if(condition==end)
			throw DontEnd();
		if(condition==smthelse)
			throw "strange type in MakeXmlSExpression::turn\n";
	}
	DEPRF3(("want to form sreference\n"));
	SReference m=(L|during);
	DEPRF2(("return from MakeSExpression::turn = %s " , m->TextRepresentation().c_str()));
	DEPRF2(("it's attrs = %s ", during->GetAttr()->TextRepresentation().c_str()));
	DEPRF2(("it's content =%s\n", during->GetContent()->TextRepresentation().c_str()));
	DEPRF3(("return\n"));
	return m;
}

void MakeXmlSExpression::insertnewlex(lexem *p){
	DEPRF1(("insertnewlex\n"));
	p->next=NULL;
	if(wait==NULL){
		wait=p;
		return;
	}
	struct lexem *q;
	q=wait;
	while(q->next!=NULL)
		q=q->next;
	q->next=p;
}

void MakeXmlSExpression::deletelex(){
	struct lexem *p;
	p=wait;
	DEPRF2(("delete p->name - [%s]\n", p->name));
	wait=wait->next;
	if(p->name!=NULL)
		delete []p->name;
	delete p;
}

void MakeXmlSExpression::changecondition(){
        if(!strcmp(wait->type,"tag_name")){
                condition=opentag;
                return;
        }
        if(!strcmp(wait->type, "attribute_name")){
                condition=attrname;
                return;
        }
        if(!strcmp(wait->type, "tag_content")){
                condition=tagcontent;
                return;
        }
        if(!strcmp(wait->type, "attribute_content")){
                condition=attrcontent;
                return;
        }
        if(!strcmp(wait->type, "close_tag_name")){
                condition=closetag;
                return;
        }
	if(!strcmp(wait->type, "end")){
		condition=end;
		return;
	}
	if(!strcmp(wait->type, "prolog")){
		condition=prolog;
		return;
	}
	if(!strcmp(wait->type, "doctype")){
		condition=doctype;
		return;
	}
        else{
                condition=smthelse;
        }
}

XmlStream::XmlStream(bool type, char *s):MakeXmlSExpression(type), serv(s){
	serv.server();
	serv.work();
	DEPRF3(("we have already got our user, so now let's start\n"));
}

char XmlStream::getsimbol(){
	while(!serv.emptybuf()){
		DEPRF3(("buffer in serv is empty\n"));
		serv.work();
	}
	return serv.getsimbol();
}	

StreamWork::StreamWork():avt(true){
	virtbuf=NULL;
	save=NULL;
	saveattr=NULL;
	savetag=NULL;
	tree=SListConstructor();
	condition=prolog;
	letterswork=false;
}

StreamWork::~StreamWork(){
	struct letter *p;
	while(virtbuf!=NULL){
		p=virtbuf;
		virtbuf=virtbuf->next;
		delete p;
	}
	cleansave();
	tree=SListConstructor();
}

void StreamWork::cleansave(){
        struct letter *p;
        while(save!=NULL){
                p=save;
                save=save->next;
                delete p;
        }
}

void StreamWork::makequque(const char *s, int length){
	int i=0;
	while(s[i]!='\0' && i<length){
		insertletter(s[i]);
		i++;
	}
}

void StreamWork::insertletter(char c){
	struct letter *let=new struct letter;
	let->c=c;
	let->next=NULL;
	if(virtbuf==NULL){
		virtbuf=let;
		return;
	}
	struct letter *p=virtbuf;
	while(p->next!=NULL)
		p=p->next;
	p->next=let;
}

void StreamWork::work(){
	struct lexem *get;
	while(!letterswork){
		get=lexemanaliz();
		avt.printready();
		if(!letterswork)
			readyforbuilding(get);
	}
	letterswork=false;
}

lexem *StreamWork::lexemanaliz(){
        char c;
        struct lexem* caught;
	DEPRF3(("we are in StreamWork::lexemanaliz\n"));
	while(true){
		if(virtbuf==NULL){
			DEPRF3(("buffer with letters is empty\n"));
			letterswork=true;
			break;
		}
		else{
			c=virtbuf->c;
			//struct letter *p=virtbuf;
			virtbuf=virtbuf->next;
		}
                DEPRF3(("simbol %c\n", c));
                avt.feedchar(c);
                while(avt.lexemisready()){
                        caught=avt.getlexem();
                        DEPRF3(("name = %s, type =%s\n", caught->name, caught->type));
			cleansave();
                        // here we have ready lexem and (may be) have some letters
			//so we must work with ready lexem and but it in her place in tree
			return caught;
                }
        }
	return NULL;
}

//must we build a tree?
void StreamWork::readyforbuilding(struct lexem *get){
	switch (condition){
		case attrib:
			buildattr(get);
			break;
		case every:
			operationtype(get);
			break;
		case prolog:
			workprolog(get);
			break;
		case attrprolog:
			workwithprattr(get);
			break;
		case conprolog:
			workwithprcon(get);
			break;
		case end:
			workend(get);
			break;
	};
}

void StreamWork::workprolog(struct lexem *get){
	if(strcmp(get->type, "prolog")){
		condition=every;
		operationtype(get);
		return;
	}
	if(get->name!=NULL)
		delete [] get->name;
	delete get;
	condition=attrprolog;
}

void StreamWork::workwithprattr(struct lexem *get){
	DEPRF3(("here is prolog attr\n"));
	if(strcmp(get->type, "attribute_name")){
		condition=end;
		workend(get);
		return;
	}
	if(get->name!=NULL)
		delete []get->name;
	delete get;
	condition=conprolog;
}

void StreamWork::workwithprcon(struct lexem *get){
	DEPRF3(("here is prolog con\n"));
	if(strcmp(get->type, "attribute_content"))
		throw "no attr content HOW???";
        if(get->name!=NULL)
                delete []get->name;
        delete get;
        condition=attrprolog;
}

void StreamWork::workend(struct lexem *get){
	DEPRF3(("prolog is closed\n"));
	if(strcmp(get->type, "end"))
		throw "prolog didn't close";
        if(get->name!=NULL)
                delete []get->name;
        delete get;
        condition=every;
}

void StreamWork::buildattr(struct lexem *get){
	DEPRF3(("i'm in build attr\n"));
	if(!strcmp(get->type, "attribute_content")){
		if(saveattr!=NULL)
			saveattr->NewCont(get->name);
		else
			throw NoAttrName();
	}
	else
		throw "we were waiting for attr_cont, but didn't get it\n";
	if(get->name!=NULL)
		delete [] get->name;
	delete get;
	SReference r=getlastunbuild(tree);
	SExpressionXmlTag *xml=r.DynamicCastGetPtr<SExpressionXmlTag>();
	if(xml==0)
		throw "buidattr - here we get not xmlsexpress from lastbuilded";
	xml->InsertAttr((L|saveattr));
	DEPRF3(("buildattr during tree is %s\n", tree->TextRepresentation().c_str()));
	saveattr=NULL;
	condition=every;
}

//this function will search which func we must execute for during tree
void StreamWork::operationtype(struct lexem *get){
	if(!strcmp(get->type, "tag_name")){
		insertnewtag(get);
		return;
	}
	if(!strcmp(get->type, "attribute_name")){
		insertnewattr(get);
		return;
	}
	if(!strcmp(get->type, "tag_content")){
		insertcontent(get);
		return;
	}
	if(!strcmp(get->type, "close_tag_name")){
                closetag(get);
                return;
        }
}

void StreamWork::closetag(struct lexem *get){
	SReference r=getlastunbuild(tree);
        SExpressionXmlTag *xml=r.DynamicCastGetPtr<SExpressionXmlTag>();
        if(xml==0)
                throw "closetag - here we get not xmlsexpress from lastbuilded";
	if(!strcmp(get->name, xml->GetName()))
		changebuild(xml);
	else
		throw "closetag - different open and close tags";
	DEPRF3(("closetag - during tree %s\n", tree->TextRepresentation().c_str()));
}

void StreamWork::changebuild(SExpressionXmlTag *xml){
	SReference r=xml->GetAttr();
	SExpressionXmlAttr *attr=r.Car().DynamicCastGetPtr<SExpressionXmlAttr>();
	SExpressionXmlAttr *attr1=r.Cdr().Car().DynamicCastGetPtr<SExpressionXmlAttr>();
	if(!strcmp(attr->GetName(), "unbuild"))
		if(!strcmp(attr->GetContent(), "yes"))
			attr->NewCont("no");
		else
			throw "changebuild - it had been build already";
	else
		throw "changebuild - have no unbuild attribute";
	if(!strcmp(attr1->GetName(), "seen"))
		attr1->NewCont("no");
	else
		throw "changebuild - have no seen attribute";
}

void StreamWork::insertcontent(struct lexem *get){
	SReference r=getlastunbuild(tree);
	SExpressionXmlTag *xml=r.DynamicCastGetPtr<SExpressionXmlTag>();
        if(xml==0)
                throw "insertcontent - here we get not xmlsexpress from lastbuilded";
	xml->InsertElem((L|get->name));
	if(get->name!=NULL)
		delete [] get->name;
	delete get;
}

void StreamWork::insertnewattr(struct lexem *get){
	SExpressionXmlAttr *attr=new SExpressionXmlAttr(get->name);
	if(get->name!=NULL)
		delete [] get->name;
	delete get;
	condition=attrib;
	saveattr=attr;
}

void StreamWork::insertnewtag(struct lexem *get){
	SExpressionXmlTag *xml=new SExpressionXmlTag(get->name);
	SExpressionXmlAttr *attr=new SExpressionXmlAttr("unbuild", "yes");
	SExpressionXmlAttr *attr1=new SExpressionXmlAttr("seen", "no");
	xml->InsertAttr((L|attr));
	xml->InsertAttr((L|attr1));
	if(tree.IsEmptyList()){
		tree=xml;
	}
	else{
		SExpressionXmlTag *last;
		SReference r=getlastunbuild(tree);
		DEPRF3(("find tag for insert\n"));
		last=r.DynamicCastGetPtr<SExpressionXmlTag>();
		if(last==0)
			throw "in insertnew tag return from func isn't an xm tag";
		last->InsertElem((L|xml));
	}
	DEPRF3(("during tree %s\n", tree->TextRepresentation().c_str()));
}

SReference StreamWork::getlastunbuild(SReference search){
	savetag=search;
	DEPRF3(("during is %s\n", search->TextRepresentation().c_str()));
	SExpressionXmlTag *dur=search.DynamicCastGetPtr<SExpressionXmlTag>();
	if(dur==0)
		throw "in getlastunbuild isn't a xml tag";
	SReference r=dur->GetAttr();
	DEPRF3(("get attr %s\n", r->TextRepresentation().c_str()));
	r=r.Car();
	SExpressionXmlAttr *attr=r.DynamicCastGetPtr<SExpressionXmlAttr>();
	if(attr==0)
		throw "in getlastunbuild isn't a attr";
	const char *s=attr->GetName();
	if(strcmp(s, "unbuild"))
		throw "in getlastunbuild wrong attr";
	s=attr->GetContent();
	if(!strcmp(s, "yes")){
		savetag=dur;
		r=dur->GetContent();
		DEPRF3(("%s\n", r->TextRepresentation().c_str()));
		bool everyclose=true;
		while(!r.IsEmptyList()){
			DEPRF3(("start cycle\n"));
			if(isbuild(r.Car())){
				DEPRF3(("build one\n"));
			}
			else{
				DEPRF3(("unbuild one\n"));
				savetag=r.Car();
				everyclose=false;
			}
			DEPRF3(("want get content -"));
			r=r.Cdr();
			DEPRF3(("get it\n"));
		}
		if(everyclose)
			return savetag;
		else
			return getlastunbuild(savetag);
	}
	else
		throw "in getlastunbuild - something from mymind=)";
	
}

bool StreamWork::isbuild(SReference r){
	SExpressionString *str=r.DynamicCastGetPtr<SExpressionString>();
	if(str!=0)
		return true;
	SExpressionXmlTag *xml=r.DynamicCastGetPtr<SExpressionXmlTag>();
	if(xml==0)
		throw "smth stange in isbuild";
	SReference r1=xml->GetAttr();
	SExpressionXmlAttr *attr=r1.Car().DynamicCastGetPtr<SExpressionXmlAttr>();
	if(attr==0)
		throw "not attr in list of attributes isbuild";
	if(!strcmp(attr->GetName(), "unbuild"))
		if(!strcmp(attr->GetContent(), "yes"))
			return false;
		else
			return true;
	else
		throw "isbuild - isn't needed attribute";
	return false;
}

SReference StreamWork::gettree(){
	DEPRF3(("here is our tree\n"));
	DEPRF3(("%s\n", tree->TextRepresentation().c_str()));
	return tree;
}

void StreamWork::setnulltree(){
	tree=SListConstructor();
}

void StreamWork::changecondition(streamcondition s){
	if(s==prolog){
		virtbuf=NULL;
        	save=NULL;
        	saveattr=NULL;
        	savetag=NULL;
        	tree=SListConstructor();
        	condition=prolog;
        	letterswork=false;
	}
	else
		throw "StreamWork::changecondition error changing\n";
	DEPRF3(("it's changed\n"));
}
