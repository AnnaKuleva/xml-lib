#ifndef _MISTAKE_H_KURS3_
#define _MISTAKE_H_KURS3_

#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>

class BadSimbol{
public:
	BadSimbol(){}
	void closedis(int disk){shutdown(disk,5), close(disk);}
	virtual void sendmes(int port)=0;
	virtual void print()=0;
	virtual ~BadSimbol(){}
};

class BadNameEl:public BadSimbol{
	char c;
public:
	BadNameEl(char a):BadSimbol(), c(a){}
	virtual void print(){printf("wrong simbol %c in name of element\n", c);}
	virtual void sendmes(int port);
	virtual ~BadNameEl(){}
};

class BadNameAttr:public BadSimbol{
	char c;
public:
	BadNameAttr(char a):BadSimbol(), c(a){}
	virtual void print(){printf("wrong simbol %c in name of attribute\n", c);}
	virtual void sendmes(int port);
	virtual ~BadNameAttr(){}
};

class BadInBegin:public BadSimbol{
	char c;
public:
	BadInBegin(char a):BadSimbol(), c(a){}
	virtual void print(){printf("wrong simbol [%c] between elements\n", c);}
	virtual void sendmes(int port);
	virtual ~BadInBegin(){}
};

class BadBetwSk:public BadSimbol{
	char c;
public:
	BadBetwSk(char a):BadSimbol(), c(a){}
	virtual void print(){printf("wrong simbol %c after <\n", c);}
	virtual ~BadBetwSk(){};
	virtual void sendmes(int port);
};

class BadInDiv:public BadSimbol{
	char c;
public:
	BadInDiv(char a):BadSimbol(), c(a){};
	virtual void print(){printf("wrong simbol %c in dividing part", c);}
	virtual void sendmes(int port);
	virtual ~BadInDiv(){};
};

class BadAttrCB:public BadSimbol{
	char c;
public:
	BadAttrCB(char a):BadSimbol(), c(a){};
	virtual void print(){printf("wrong simbol %c after = in attr meaning\n", c);}
	virtual void sendmes(int port);
	virtual ~BadAttrCB(){};
};

class BadEnd:public BadSimbol{
	char c;
public:
	BadEnd(char a):BadSimbol(), c(a){};
	virtual void print(){printf("wrong simbol %c in the end of file\n", c);}
	virtual ~BadEnd(){};
	virtual void sendmes(int port);
};

class BadInEmptyTag:public BadSimbol{
	char c;
public:
	BadInEmptyTag(char a):BadSimbol(), c(a){}
	virtual void print(){printf("wrong simbol in empty tag %c\n", c);}
	virtual ~BadInEmptyTag(){}
	virtual void sendmes(int port);
};

class BadInHidAn:public BadSimbol{
	char c;
public:
	BadInHidAn(char a):BadSimbol(), c(a){}
	virtual void print(){printf("wrong simbol %c in smth what must be hidden from analiz\n", c);}
	virtual ~BadInHidAn(){}
	virtual void sendmes(int port);
}; 


class BadCdata:public BadSimbol{
	char c;
public:
	BadCdata(char a):BadSimbol(), c(a){}
	virtual void print(){printf("wrong simbol %c in part CDATA[\n", c);}
	virtual ~BadCdata(){}
	virtual void sendmes(int port);
};

class BadInCloseEl:public BadSimbol{
	char c;
public:
	BadInCloseEl(char a):BadSimbol(), c(a){}
	virtual void print(){printf("wrong simbol %c instead >\n", c);}
	virtual ~BadInCloseEl(){}
	virtual void sendmes(int port);
};

class BadComment:public BadSimbol{
	char c;
public:
	BadComment(char a):BadSimbol(), c(a){}
	virtual void print(){printf("wrong simbol %c instead - in Comment\n", c);}
	virtual ~BadComment(){}
	virtual void sendmes(int port);
};

class BadInEndCdata:public BadSimbol{
	char c;
public:
	BadInEndCdata(char a):BadSimbol(), c(a){}
	virtual void print(){printf("wrong simbol %c instead ] in CDATA\n", c);}
	virtual ~BadInEndCdata(){}
	virtual void sendmes(int port);
};

class EarlyEnd:public BadSimbol{
public:
	EarlyEnd():BadSimbol(){}
	virtual void print(){printf("met EOF very early\n");}
	virtual ~EarlyEnd(){}
	virtual void sendmes(int port);
};

class NotXml:public BadSimbol{
	char c;
public:
	NotXml(char a):BadSimbol(), c(a){}
	virtual void print(){printf("wrong simbol %c instead phrase xml\n", c);}
	virtual ~NotXml(){};
	virtual void sendmes(int port);
};

class BadInEndProlog:public BadSimbol{
	char c;
public:
	BadInEndProlog(char a):BadSimbol(), c(a){}
	virtual  void print(){printf("wrong simbol %c instead > in prolog, comment or CDATA\n", c);}
	virtual ~BadInEndProlog(){};
	virtual void sendmes(int port);
};

class BadNumOfEl:public BadSimbol{
public:
	BadNumOfEl():BadSimbol(){}
	virtual void print(){printf("wrong number of close tags\n");}
	virtual ~BadNumOfEl(){};
	virtual void sendmes(int port);
};

class TooManyProlog:public BadSimbol{
public:
	TooManyProlog():BadSimbol(){}
	virtual void print(){printf("we can have only one prolog, but here are more\n");}
	virtual ~TooManyProlog(){}
	virtual void sendmes(int port);
};

class BadProlog:public BadSimbol{
public:
	BadProlog():BadSimbol(){}
	virtual void print(){printf("prolog must be declared before any elements\n");}
	virtual ~BadProlog(){}
	virtual void sendmes(int port);
};

class NotProlog:public BadSimbol{
public:
	NotProlog():BadSimbol(){}
	virtual void print(){printf("is wasn't prolog, but was closed how prolog ?>\n");}
	virtual ~NotProlog(){}
	virtual void sendmes(int port);
};

class OnlyOneHead:public BadSimbol{
public:
	OnlyOneHead():BadSimbol(){}
	virtual void print(){printf("we can have only one head el in correct document\n");}
	virtual ~OnlyOneHead(){}
	virtual void sendmes(int port);
};

class BadDoctype:public BadSimbol{
	char c;
public:
	BadDoctype(char a):BadSimbol(), c(a){}
	virtual void print(){printf("wrong simbol %c in definition\n", c);}
	virtual ~BadDoctype(){}
	virtual void sendmes(int port);
};

class BadDocPos:public BadSimbol{
public:
	BadDocPos():BadSimbol(){}
	virtual void print(){printf("DOCTYPE section can be only inside head tag\n");}
	virtual ~BadDocPos(){}
	virtual void sendmes(int port);
};

class BadFClTag:public BadSimbol{
	char c;
public:
	BadFClTag(char a):BadSimbol(), c(a){}
	virtual void print(){printf("wrong simbol %c in between sost\n", c);}
	virtual ~BadFClTag(){}
	virtual void sendmes(int port);
};

class ErrorInForming{
	char s[20];
public:
	ErrorInForming(const char *str){
		int i=0;
		while(str[i]!='\0'){
			s[i]=str[i];
			i++;
		}
	}
	void sendmes(int port);
	void closedis(int disk){shutdown(disk,5), close(disk);}
	void print(){printf("we want to form lexem in sost %s where we can't do this\n", s);}
};

class EmptyListOfLex{
public:
	void print(){
		printf("we get message that lexan has ready ");
		printf("lexem, but list of ready lexems is empty\n");
	}
	void closedis(int disk){shutdown(disk,5), close(disk);}
	void sendmes(int port);
};

class Disbalance{
public:
	virtual void print()=0;
	virtual ~Disbalance(){};
	void closedis(int disk){shutdown(disk,5), close(disk);}
	virtual void sendmes(int port)=0;
};

class MoreClose:public Disbalance{
public:
	virtual void print(){printf("we have more close tags than open tags\n");}
	virtual ~MoreClose(){}
	virtual void sendmes(int port);
};

class MoreOpen:public Disbalance{
public:
	virtual void print(){printf("we have more open tags than close tags\n");}
	virtual ~MoreOpen(){}
	virtual void sendmes(int port);
};

class TurnMistake{
public:
	void closedis(int disk){shutdown(disk,5), close(disk);}
	virtual void print()=0;
	virtual ~TurnMistake(){}
	virtual void sendmes(int port)=0;
};

class NoAttrName:public TurnMistake{
public:
	virtual void print(){printf("we get attrcontent, but we haven`t attrname\n");}
	virtual ~NoAttrName(){};
	virtual void sendmes(int port);
};

class DontEnd:public TurnMistake{
public:
	virtual void print(){printf("tag hadn't been complete\n");}
	virtual ~DontEnd(){}
	virtual void sendmes(int port);
};

class WrongType:public TurnMistake{
public:
	virtual void print(){printf("we get lexem with wrong type\n");}
	virtual ~WrongType(){}
	virtual void sendmes(int port);
};

class DifferentNames:public TurnMistake{
public:
	virtual void print(){printf("different names of open and close tag\n");}
	virtual ~DifferentNames(){}
	virtual void sendmes(int port);
};

class ServPartErrors{
	char buf[28];
public:
	ServPartErrors(const char *s);
	void print(){perror(buf);}
};

class NoSuchFile{
	char str[30];
public:
	NoSuchFile(char *s);
	void print(){printf("You hadn't load file %s yet\n", str);}
};

class DtdErrorClass{
	char *str;
public:
	DtdErrorClass(char *s);
	void print();
	~DtdErrorClass(){delete [] str;}
};
#endif
