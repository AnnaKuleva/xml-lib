#ifndef _LISPINT_H_KURS3_
#define _LISPINT_H_KURS3_

/*This file includes functions which provide
 work with s-expressions SExpressionXmlTag, 
 SExpressionXmlAttr. Also, there are functions
 which work with strings, numbers, files.*/

#include <intelib/genlisp/lispform.hpp>
#include <intelib/sexpress/sexpress.hpp>
#include <intelib/sexpress/iexcept.hpp>
#include <intelib/sexpress/sstring.hpp>
#include <intelib/tools/sstream.hpp>
#include <intelib/lisp/lisp.hpp>
#include <intelib/lisp/lsymbol.hpp>
#include <intelib/lfun_std.hpp>

#include "parstag.h"

//_______________________________________
//___________printf_functions____________|
//_______________________________________|

//Function prints xml-tag.
SReference print_beauty(int params, const SReference *param);
//Function prints SReference.
SReference printsmth(int params, const SReference *param);
//Function prints string in user's file.
SReference printstrinusersfile(int params, const SReference *param);
//Function prints in user's file tag's open-part.
SReference printopenusersfile(int params, const SReference *param);
//Function prints in user's file xml-attribute.
SReference printattrusersfile(int params, const SReference *param);
//Function prints in file empty tag.
SReference printemptytagfile(int params, const SReference *param);
//Function prints in file close tag.
SReference printcloseusersfile(int params, const SReference *param);

//_______________________________________
//__________xml-tag_functions____________|
//_______________________________________|

//Function creates new xml-tag.
SReference newxmltag(int params, const SReference *param);
//Function copies xml-tag, it returns new one.
SReference copy_xml_tag(int params, const SReference *param);
//Function insearts new element in xml-tag's content.
SReference insertcontent(int params, const SReference *param);
//Function tests it's parameter on being xml-tag.
SReference isxmlatom(int params, const SReference *param);
//Function returns xml-tag's name.
SReference get_xml_name(int params, const SReference *param);
//Function get's xml-tag's content.
SReference getcontent(int params, const SReference *param);
//Function inserts new content in xml-tag.
SReference deletetagfrom(int params, const SReference *param);

//________________________________________
//___________xml-attr_functions___________|
//________________________________________|

//Function creates new xml-attribute.
SReference newxmlattr(int params, const SReference *param);
//Function copies xml-attribute. It returns new one.
SReference copy_xml_attr(int params, const SReference *param);
//Function gets xml-tag's attributes.
SReference getxmltagattr(int params, const SReference *param);
//Function inserts new attributes in xml-tag.
SReference newxmltagattr(int params, const SReference *param);
//Function gets attribute's content.
SReference getattrcont(int params, const SReference *param);
//Function inserts new content of xml-attribute.
SReference attrnewcont(int params, const SReference *param);
//Function returns xml-attribute's name.
SReference get_attr_name(int params, const SReference *param);
//Function inserts attribute in xml tag.
SReference insertreadyattr(int params, const SReference *param);

//________________________________________
//_____file-stream_&&_file_functions______|
//________________________________________|

//Funtion closes file stream.
SReference close_file_stream(int params, const SReference *param);
//Function creates new file stream.
SReference getfilestream(int params, const SReference *param);
//Function returns list of files' names from directory.
SReference getfilesdir(int params, const SReference *param);
//Function checks needed directory, and if it is needed creates new one.
SReference exists_dir(int params, const SReference *param);
//Function opens xml-file and returns it's converted content.
SReference openfilexml(int params, const SReference *param);
//Function opens file; prints tag in file, closes file.
SReference print_tag_in_file(int params, const SReference * param);

//________________________________________
//__________string_functions______________|
//________________________________________|

SReference isstring(int params, const SReference *param);
SReference stringinxhtml(int params, const SReference *param);
SReference parsestringintag(int params, const SReference *param);
SReference changeentities(int params, const SReference *param);
SReference deletesimbol(int params, const SReference *param);
SReference changeampsonentity(int params, const SReference *param);
SReference getnumbfromstr(int params, const SReference *param);
SReference getstrfromnumb(int params, const SReference *param);
SReference concatstr(int params, const SReference *param);
SReference parse_path(int params, const SReference *param);
SReference onlyname(int params, const SReference *param);
SReference string_concat_number(int params, const SReference *param);
SReference number_from_string(int params, const SReference *param);
SReference divide_string_on_num(int params, const SReference *param);
SReference divide_str_simbol(int params, const SReference *param);
SReference turntaginstr(int params, const SReference *param);

//________________________________________
//__________number_functions______________|
//________________________________________|

SReference generate_random_numb(int params, const SReference *param);
SReference divide_numb(int params, const SReference *param);

void define_print_functions();
void define_xml_functions();
void define_attr_functions();
void define_string_functions();
void define_number_functions();
void define_file_functions();

#endif
