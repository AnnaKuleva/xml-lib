#include "xmlsexpress.h"
#include <stdio.h>
#include "structures.h"
#include <string.h>

static LListConstructor L;

IntelibTypeId SExpressionXmlAttr::TypeId(&SExpression::TypeId);

/*This is class' constructor.
 It creates xml-attribute with needed name and
 needed content.*/
SExpressionXmlAttr::SExpressionXmlAttr(const char *n, const char *c):SExpression(TypeId){
    int len=strlen(n);
    name=new char[len+1];
    strcpy(name, n);
    name[len]='\0';
	DEPRF1(("name = (%s)\n", name));
    len=strlen(c);
    content=new char[len+1];
    strcpy(content, c);
    content[len]='\0';
    DEPRF1(("content = (%s)\n", content));
}

/*This is class' constructor.
 It creates xml-attribute with needed name and
 empty content.*/
SExpressionXmlAttr::SExpressionXmlAttr(const char *n):SExpression(TypeId){
    int len=strlen(n);
    name=new char[len+1];
    strcpy(name, n);
    name[len]='\0';
    DEPRF1(("name = (%s)\n", name));
	content=NULL;
}

/*This is class' copy-constructor*/
SExpressionXmlAttr::SExpressionXmlAttr(SExpressionXmlAttr* attr):SExpression(TypeId){
	int len=strlen(attr->GetName());
	name=new char[len+1];
	strcpy(name, attr->GetName());
    name[len]='\0';
	len=strlen(attr->GetContent());
	content=new char[len+1];
	strcpy(content, attr->GetContent());
    content[len]='\0';
}

/*This method inserts new content of xml-attribute.*/
void SExpressionXmlAttr::NewCont(const char *n){
	if(content!=NULL)
		delete []content;
    int len=strlen(n);
    content=new char[len+1];
    strcpy(content, n);
    content[len]='\0';
}

#if INTELIB_TEXT_REPRESENTATIONS == 1
SString SExpressionXmlAttr::TextRepresentation() const{
	int len1=strlen(name);
	int len2=strlen(content);
	char *res=new char[len1+len2+2];
	int k=0;
	while(name[k]!='\0'){
		res[k]=name[k];
		k++;
	}
	res[k++]='=';
	int i=0;
	while(content[i]!='\0'){
		res[k++]=content[i++];
	}
	res[k]='\0';
	return SString(res);
}
#endif

SExpressionXmlAttr::~SExpressionXmlAttr(){
    if(name!=NULL)
        delete [] name;
    if(content!=NULL)
        delete []content;
}

bool SExpressionXmlAttr::SpecificEql(const SExpression* other) const{
	DEPRF3(("specificeql attr\n"));
	INTELIB_ASSERT(TermType() == other->TermType(), IntelibX_bug());
	const SExpressionXmlAttr *attr=static_cast<const SExpressionXmlAttr*>(other);
	if(0!=strcmp(attr->GetName(), name))
		return false;
	if(0!=strcmp(attr->GetContent(), content))
		return false;
	return true;
}

IntelibTypeId SExpressionXmlTag::TypeId(&SExpression::TypeId);

/*This is class' constructor.
 It creates xml-tag with empty name, empty list
 of attributes and empty content.*/
SExpressionXmlTag::SExpressionXmlTag():SExpression(TypeId){
	name=NULL;
	attr=SListConstructor();
	content=SListConstructor();
}

/*This is class' constructor.
 It creates xml-tag with needed name, empty list
 of attributes and empty content.*/
SExpressionXmlTag::SExpressionXmlTag(const char *n):SExpression(TypeId){
	DEPRF3(("here in constructor\n"));
	int len=strlen(n);
	name=new char[len+1];
	strcpy(name, n);
	name[len]='\0';
	DEPRF1(("[%s]\n", name));
	attr=SListConstructor();
	content=SListConstructor();
}

/*This is class' copy constructor.*/
SExpressionXmlTag::SExpressionXmlTag(SExpressionXmlTag* xml):SExpression(TypeId){
	int len=strlen(xml->GetName());
	name=new char[len+1];
	strcpy(name, xml->GetName());
	name[len]='\0';
	attr=SListConstructor();
	content=SListConstructor();
	SReference attr_copy=xml->GetAttr();
	while(!attr_copy.IsEmptyList()){
		SExpressionXmlAttr *a=attr_copy.Car().DynamicCastGetPtr<SExpressionXmlAttr>();
		SExpressionXmlAttr *_attr=new SExpressionXmlAttr(a);
		attr_copy=attr_copy.Cdr();
		attr=attr||(L|_attr);
	}
	SReference xml_copy=xml->GetContent();
	while(!xml_copy.IsEmptyList()){
		SExpressionXmlTag *t=xml_copy.Car().DynamicCastGetPtr<SExpressionXmlTag>();
		if(t!=NULL){
			SExpressionXmlTag *_xml=new SExpressionXmlTag(t);
			content=content||(L| _xml);
		}
		else{
			SExpressionString *str=xml_copy.Car().DynamicCastGetPtr<SExpressionString>();
			SExpressionString *_str=new SExpressionString(str->GetValue());
			content=content||(L| _str);
		}
		xml_copy=xml_copy.Cdr();
	}
}

SExpressionXmlTag::~SExpressionXmlTag(){
	if(name!=NULL)
		delete []name;
}

#if INTELIB_TEXT_REPRESENTATIONS == 1
SString SExpressionXmlTag::TextRepresentation() const{
	SString atrib=attr->TextRepresentation();
	SString cont=content->TextRepresentation();
	DEPRF1(("xml-tag text representation\n"));
	return SString(name)+" atr="+atrib+" [content="+cont+"] ";
}
#endif

bool SExpressionXmlTag::SpecificEql(const SExpression* other) const{
	DEPRF3(("specificeql xml-tag\n"));
	INTELIB_ASSERT(TermType() == other->TermType(), IntelibX_bug());	
	const SExpressionXmlTag *tag=static_cast<const SExpressionXmlTag*>(other);
	if(0!=strcmp(name, tag->GetName()))
		return false;
	SReference thisattr=attr;
	SReference anotherattr=tag->GetAttr();
	while(!thisattr.IsEmptyList() && !anotherattr.IsEmptyList()){
		if(!thisattr.Car().IsEql(anotherattr.Car()))
			return false;
		thisattr=thisattr.Cdr();
		anotherattr=anotherattr.Cdr();
	}
	if(!thisattr.IsEmptyList() || !anotherattr.IsEmptyList())
		return false;
	SReference thiscont=content;
	SReference anothercont=tag->GetContent();
	while(!thiscont.IsEmptyList() && !anothercont.IsEmptyList()){
		if(!thiscont.Car().IsEql(anothercont.Car()))
			return false;
		thiscont=thiscont.Cdr();
		anothercont=anothercont.Cdr();
	}
	if(!thiscont.IsEmptyList() || !anothercont.IsEmptyList())
		return false;
	return true;
}
