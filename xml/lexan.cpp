#include "lexan.h"
#include <stdio.h>
#include "mistake.h"
#include "structures.h"

Automat::Automat(bool type):simbol('\0'), ready(NULL), cdatacount(0), hyphen(0),
	condition(begincond),open(0), close(0), doccount(0), prol(0),
	word(NULL), depth(0), savename(NULL){
		stream=type;
		itwasprolog=false;
}

Automat::~Automat(){
	struct lexem *p;
	if(ready!=NULL){
		p=ready;
		ready=ready->next;
		if(p->name!=NULL)
			delete []p->name;
		delete p;
	}
	struct letter *q;
	if(word!=NULL){
		q=word;
		word=word->next;
		delete q;
	}
}

void Automat::feedchar(char a){
	simbol=a;
	switch (condition){
	case begincond:
		StartCondition();
		break;
	case whatis:
		HelpInformationCondition();
		break;
	case opentag:
		OpenTagCondition();
		break;
	case divider:
		SeparatorCondition();
		break;
	case attrname:
		AttrNameCondition();
		break;
	case quotation:
		QuatationCondition();
		break;
	case attrcontent:
		AttrContentCondition();
		break;
	case elemcontent:
		TagContentCondition();
		break;
	case between:
		BetweenCondition();
		break;
	case closetag:
		CloseTagCondition();
		break;
	case doctype:
		DoctypeCondition();
		break;
	case emptytag:
		EmptyTagCondition();
		break;
	case hidefroman:
		HiddenInformationCondition();
		break;
	case cdata:
		CdataCondition();
		break;
	case comment:
		CommentCondition();
		break;
	case prolog:
		PrologCondition();
		break;
	case endprolog:
		EndPrologCondition();
		break;
	case endcond:
		EndCondition();
		break;
	};
}

void Automat::StartCondition(){
	DEPRF(("in StartCondition() %c\n", simbol));
	if(simbol=='<'){
		condition=whatis;
		return;
	}
	if(simbol==' ' || simbol=='\n' || simbol=='\t' || simbol=='\0' ||simbol=='\r'){
		DEPRF(("StartCondition() emprty\n"));
		return;
	}
	if(simbol==EOF){
		DEPRF3(("i meet EOF\n"));
		condition=endcond;
		struct lexem *s=new struct lexem;
		s->next=NULL;
		int i=0;
		while(end_of_file[i]!='\0'){
               		s->type[i]=end_of_file[i];
                	i++;
        	}	
		s->name=NULL;
		inserttoend(s);
		DEPRF(("in StartCondition(), end  open=%d close=%d\n", open, close));
		if(close!=open)
			throw BadNumOfEl();
		return;
	}
	else{
		DEPRF(("in StartCondition(), open=%d close=%d\n", open, close));
		throw BadInBegin(simbol);
	}
}

void Automat::HelpInformationCondition(){
	DEPRF(("in HelpInformationCondition() %c\n", simbol));
	if((simbol<='z' && simbol>='a') || (simbol<='Z' && simbol>='A')){
		if(!stream)
			if(close==open && open!=0)
                		throw OnlyOneHead();
		DEPRF(("it is tag for elem\n"));
		condition=opentag;
		open++;
		insertletter(simbol);
		DEPRF(("HelpInformationCondition() open=%d\n", open));
		//take one simbol from name of open tag
		return;
	}
	if(simbol=='!'){
		condition=hidefroman;
		return;
	}
	if(simbol=='?'){
		condition=prolog;
		prol++;
		countprolog=1;
		return;
	}
	if(simbol==EOF)
		throw EarlyEnd();
	else{
		DEPRF(("I'm find an error\n"));
		throw BadBetwSk(simbol);
	}
}

void Automat::OpenTagCondition(){
	DEPRF(("in OpenTagCondition() %c\n", simbol));
	if((simbol<='z' && simbol>='a') || (simbol<='Z' && simbol>='A') || simbol=='_'
		||simbol==':' || simbol<='9' && simbol>='0' || simbol=='-' || simbol=='@'
		|| simbol=='.'){
		insertletter(simbol);
		return;
	}
	if(simbol=='\0' || simbol=='\n' || simbol==' ' || simbol=='\t' || simbol=='\r'){
		struct lexem *s=lettertoword();
		inserttoend(s);
		if(savename!=NULL)
                        delete []savename;
                savename=copystr(s->name);
		condition=divider;
		return;
	}
	if(simbol=='>'){
		struct lexem *s=lettertoword();
		inserttoend(s);
		condition=elemcontent;
		return;
	}
	if(simbol=='/'){
		struct lexem *s=lettertoword();
		inserttoend(s);
		savename=copystr(s->name);
		condition=emptytag;
		return;
	}
	if(simbol==EOF)
		throw EarlyEnd();
	else{
		throw BadNameEl(simbol);
	}
}

char *Automat::copystr(char *s2){
	int i=0;
	char *s1;
	while(s2[i++]!='\0'){};
	s1=new char[i];
	i=0;
	while(s2[i]!='\0'){
		s1[i]=s2[i];
		i++;
	}
	s1[i]='\0';
	return s1;
}

void Automat::copytype(struct lexem *s1){
	int i=0;
	while(close_tag[i]!='\0'){
		s1->type[i]=close_tag[i];
		i++;
	}
	s1->type[i]='\0';
}

void Automat::SeparatorCondition(){
	DEPRF(("in SeparatorCondition() %c\n", simbol));
	if((simbol<='z' && simbol>='a') || (simbol<='Z' && simbol>='A')){
		insertletter(simbol);
		condition=attrname;
		return;
	}
	if(simbol=='>'){
		DEPRF(("SeparatorCondition() will condition=inside\n"));
		condition=elemcontent;
		return;
	}
	if(simbol==' ' || simbol=='\n' || simbol=='\t' || simbol=='\0' ||simbol=='\r'){
		return;
	}
	if(simbol=='?'){
		if(open!=0)
			throw NotProlog();
		condition=endprolog;
		itwasprolog=true;
		return;
	}
	if(simbol=='/'){
		condition=emptytag;
		return;
	}
	if(simbol==EOF)
		throw EarlyEnd();
	else{
		throw BadInDiv(simbol);
	}
}

void Automat::EndPrologCondition(){
	DEPRF(("EndPrologCondition() %c\n", simbol));
	if(simbol=='>' && open==close){
		if(itwasprolog){
			struct lexem *s=new struct lexem;
                	s->next=NULL;
                	int i=0;
                	while(end_of_file[i]!='\0'){
                        	s->type[i]=end_of_file[i];
                        	i++;
                	}
			s->type[i]='\0';
                	s->name=NULL;
                	inserttoend(s);
			itwasprolog=false;
		}
		condition=begincond;
		return;
	}
	if(simbol=='>' && open!=close){
		condition=elemcontent;
		return;
	}
	if(simbol==EOF)
		throw EarlyEnd();
	else
		throw BadInEndProlog(simbol);
}

void Automat::AttrNameCondition(){
	DEPRF(("in AttrNameCondition() %c\n", simbol));
	if(simbol!='=' && simbol!=EOF){
		insertletter(simbol);
		return;
        }
	if(simbol=='='){
		struct lexem *s=lettertoword();
		inserttoend(s);
		condition=quotation;
		return;
	}
	if(simbol==EOF)
		throw EarlyEnd();
	else{
		throw BadNameAttr(simbol);
	}
}

void Automat::QuatationCondition(){
	DEPRF(("in QuatationCondition() %c\n", simbol));
	if(simbol=='"' || simbol==39){
		//here we must start to form our content
		condition=attrcontent;
		return;
	}
	if(simbol==EOF)
		throw EarlyEnd();
	else{
		throw BadAttrCB(simbol);
	}
}

void Automat::AttrContentCondition(){
	DEPRF(("in AttrContentCondition() %c\n", simbol));
	if(simbol=='"' || simbol==39){
		struct lexem *s=lettertoword();
		inserttoend(s);
		condition=divider;
		return;
	}
	if(simbol==EOF)
		throw EarlyEnd();
	else{
		insertletter(simbol);
	}
}

void Automat::TagContentCondition(){
	DEPRF(("in TagContentCondition() %c\n", simbol));
	if(simbol=='<'){
		struct lexem *s=lettertoword();
		inserttoend(s);
		condition=between;
		return;
	}
	if(simbol==' ' || simbol=='\n' || simbol=='\t' || simbol=='\0' || simbol=='\r'){
		struct lexem *s=lettertoword();
                inserttoend(s);
		return;
	}
	if(simbol==EOF)
		throw EarlyEnd();
	else{
		insertletter(simbol);
	}
}

void Automat::BetweenCondition(){
	DEPRF(("in BetweenCondition() %c\n", simbol));
	if(simbol=='/'){
		condition=closetag;
		return;
	}
	if(simbol==EOF)
		throw EarlyEnd();
	if(simbol<='Z' && simbol>='A' || simbol<='z' && simbol>='a' || simbol=='.'){
		insertletter(simbol);
		condition=opentag;
		open++;
		return;
	}
	if(simbol=='!'){
		condition=hidefroman;
		return;
	}
	else{
		throw BadFClTag(simbol);
	}
}

void Automat::CloseTagCondition(){
	DEPRF(("in CloseTagCondition() %c\n", simbol));
	if((simbol<='z' && simbol>='a') || (simbol<='Z' && simbol>='A') || simbol=='_'||
		simbol==':' || simbol<='9' && simbol>='0' || simbol=='-' || simbol=='@'
		|| simbol=='.'){
		insertletter(simbol);
		return;
	}
	if(simbol=='>'){
		close++;
		struct lexem *s=lettertoword();
		inserttoend(s);
		if(close==open){
			condition=begincond;
			return;
		}
		else{
			condition=elemcontent;
			return;
		}
	}
	if(simbol==EOF)
		throw EarlyEnd();
	else{
		throw BadNameEl(simbol);
	}
}

void Automat::EndCondition(){
	DEPRF(("in EndCondition()\n"));
	throw BadEnd(simbol);
}

void Automat::EmptyTagCondition(){
	DEPRF(("in EmptyTagCondition() %c\n", simbol));
	if(simbol=='>'){
		close++;
		depth--;
		if(depth<0) throw MoreClose();
		struct lexem *k=new struct lexem;
		k->name=copystr(savename);
		copytype(k);
		k->next=NULL;
		inserttoend(k);
		if(close==open){
			condition=begincond;
			return;
		}
		else{
			condition=elemcontent;
			return;
		}
	}
	if(simbol==EOF)
		throw EarlyEnd();
	else{
		throw BadInEmptyTag(simbol);
	}
}	

void Automat::HiddenInformationCondition(){
	DEPRF(("in HiddenInformationCondition() %c\n", simbol));
	if(simbol=='-'){
		condition=comment;
		hyphen=1;
		return;
	}
	if(simbol=='['){
		condition=cdata;
		cdatacount=1;
		return;
	}
	if(simbol=='D'){
		if(open!=0)
			throw BadDocPos();
		condition=doctype;
		doccount=1;
		return;
	}
	if(simbol==EOF)
		throw EarlyEnd();
	else{
		throw BadInHidAn(simbol);
	}
}

void Automat::DoctypeCondition(){
	DEPRF(("in DoctypeCondition %c\n", simbol));
	if(doccount==1){
		if(simbol!='O')
			throw BadDoctype(simbol);
		doccount++;
		return;
	}
	if(doccount==2){
		if(simbol!='C')
			throw BadDoctype(simbol);
		doccount++;
		return;
	}
	if(doccount==3){
		if(simbol!='T')
			throw BadDoctype(simbol);
		doccount++;
		return;
	}
	if(doccount==4){
		if(simbol!='Y')
			throw BadDoctype(simbol);
		doccount++;
		return;
	}
	if(doccount==5){
		if(simbol!='P')
			throw BadDoctype(simbol);
		doccount++;
		return;
	}
	if(doccount==6){
		if(simbol!='E')
			throw BadDoctype(simbol);
		doccount++;
		return;
	}
	if(doccount==7){
		if(simbol=='>'){
			struct lexem *s=lettertoword();
			inserttoend(s);
			condition=begincond;
			return;
		}
		if(simbol==' ' || simbol=='\n' || simbol=='\t'){
			struct lexem *s=lettertoword();
			inserttoend(s);
		}
		else{
			insertletter(simbol);
		}
	}
	if(simbol==EOF) throw EarlyEnd();
}

void Automat::CdataCondition(){
	DEPRF(("in Cdata %c\n", simbol));
	if(cdatacount==1){
		if(simbol!='C')
			throw BadCdata(simbol);
		else{
			cdatacount++;
			return;
		}
	}
	if(cdatacount==2){
		if(simbol!='D')
                        throw BadCdata(simbol);
                else{
                        cdatacount++;
			return;
		}
        }
	if(cdatacount==3 || cdatacount==5){
		if(simbol!='A')
                        throw BadCdata(simbol);
                else{
                        cdatacount++;
			return;
		}
        }
	if(cdatacount==4){
		if(simbol!='T')
                        throw BadCdata(simbol);
                else{
                        cdatacount++;
			return;
		}
        }
	if(cdatacount==6){
		if(simbol!='[')
                        throw BadCdata(simbol);
                else{
                        cdatacount++;
			return;
		}
        }
	if(cdatacount==7 && simbol==']'){
		cdatacount++;
		return;
	}
	if(cdatacount==8)
		if(simbol==']'){
			condition=endprolog;
			cdatacount=0;
			return;
		}
		else
			throw BadInEndCdata(simbol);
	if(simbol==EOF)
		throw EarlyEnd();
}

void Automat::CommentCondition(){
	DEPRF(("in CommentCondition %c\n", simbol));
	if(hyphen==1)
		if(simbol!='-')
			throw BadComment(simbol);
		else{
			hyphen++;
			return;
		}
	if(hyphen==2)
		if(simbol=='-'){
			hyphen++;
			return;
		}
		else{
			if(simbol==EOF)
				throw EarlyEnd();
		}
	if(hyphen==3)
		if(simbol!='-')
			hyphen--;
		else{
			condition=endprolog;
			hyphen=0;
			return;
		}
	if(simbol==EOF) throw EarlyEnd();
}

void Automat::PrologCondition(){
	if (prol>1) throw TooManyProlog();
	if(open!=0) throw BadProlog();
	DEPRF(("in PrologCondition() %c\n", simbol));
	if(countprolog==1)
		if(simbol!='x')
			throw NotXml(simbol);
		else{
			countprolog++;	
			return;
		}
	if(countprolog==2)
                if(simbol!='m')
                        throw NotXml(simbol);
                else{
                        countprolog++;  
                        return;
                }
	if(countprolog==3)
                if(simbol!='l')
                        throw NotXml(simbol);
                else{  
                        struct lexem *p=new struct lexem;
			p->name=new char[4];
			p->name[0]='x'; p->name[1]='m';
			p->name[2]='l'; p->name[3]='\0';
			const char *t=getcondition();
			int i=0;
			while(t[i]!='\0'){
				p->type[i]=t[i];
				i++;
			}
			p->type[i]='\0';
			p->next=NULL;
			inserttoend(p);
			condition=divider;
			return;
                }
	if(simbol==EOF) throw EarlyEnd();
}

void Automat::insertletter(char s){
	struct letter *q, *t;
	q=new struct letter;
	q->c=s;
	q->next=NULL;
	if(word==NULL){
		word=q;
		return;
	}
	t=word;
	while(t->next!=NULL) t=t->next;
	t->next=q;
}

void Automat::deleteletter(){
	struct letter *q;
	while(word!=NULL){
		q=word;
		word=word->next;
		delete q;
	}
}

lexem *Automat::lettertoword(){
	struct letter *q;
	int i=0, k=0;
	char *s;
	const char *t;
	q=word;
	while(q!=NULL){
		i++;
		q=q->next;
	}
	DEPRF1(("number of letters in word is %d\n", i));
	s=new char[i+1];
	q=word;
	while(q!=NULL){
		s[k++]=q->c;
		q=q->next;
	}
	s[k]='\0';
	deleteletter();
	if(compear(s, "\n") || compear(s, " ") || compear("\t", s)){
		DEPRF1(("empty part in content\n"));
		return NULL;
	}
	DEPRF1(("word is [%s]\n", s));
	t=getcondition();
	DEPRF1(("type is %s\n", t));
	struct lexem *n;
	n=new lexem;
	n->name=s;
	i=0;
	while(t[i]!='\0'){
		n->type[i]=t[i];
		i++;
	}
	n->type[i]='\0';
	n->next=NULL;
	//changes from here
	if(condition==emptytag){
		i=0;
		struct lexem *m=new lexem;
		m->name=s;
		while(tag_name[i]!='\0'){
			m->type[i]=tag_name[i];
			i++;
		}
		m->type[i]='\0';
		m->next=n;
		return m;
	}
	return n;
}

const char *Automat::getcondition(){
	switch (condition){
	case begincond:
		throw ErrorInForming("begincondition");
	case whatis:
		throw ErrorInForming("whatis");
	case opentag:
		DEPRF1(("is name of open tag\n"));
		depth++;
		return tag_name;
	case divider:
		throw ErrorInForming("divider");
	case attrname:
		DEPRF1(("is name of art\n"));
		return attr_name;
	case quotation:
		throw ErrorInForming("quotation");
	case attrcontent:
		DEPRF1(("is attr content\n"));
		return attr_content;
	case elemcontent:
		DEPRF1(("it's tag content\n"));
		return tag_content;
	case between:
		throw ErrorInForming("between");
	case closetag:
		DEPRF1(("it's close tag name\n"));
		depth--;
                if(depth<0) throw MoreClose();
		//here we need some checks for equal open and close tag
		return close_tag;
	case doctype:
		DEPRF1(("it's smth from DOCTYPE\n"));
		return smth_doctype;
	case emptytag:
		DEPRF1(("it's empty tag\n"));
		return close_tag;
		//throw ErrorInForming("emptytag");
	case hidefroman:
		throw ErrorInForming("hidefroman");
	case cdata:
		throw ErrorInForming("cdata");
	case comment:
		throw ErrorInForming("comment");
	case prolog:
		DEPRF1(("it's prolog\n"));
		return prolog_doc;
		//throw ErrorInForming("prolog");
	case endprolog:
		DEPRF1(("we are in prolog\n"));
		return end_of_file;
		//throw ErrorInForming("endprolog");
	case endcond:
		throw ErrorInForming("endcondition");
	};
	return "error";
}

void Automat::inserttoend(lexem *p){
	if(p==NULL) return;
	if(p->name!=NULL){
		if(compear(p->name, "")){
			DEPRF1(("empty word\n"));
			delete []p->name;
			delete p;
			return;
		}
	}
	else{
		if(!compear(p->type, "end")){
			//throw mistake
		}
	}
	if(ready==NULL)
		ready=p;
	else{
		struct lexem *q;
		q=ready;
		while(q->next!=NULL)
			q=q->next;
		q->next=p;
	}
}

bool Automat::compear(const char *s, const char *t){
	int i=0;
	while(s[i]!='\0' && t[i]!='\0'){
		if(s[i]!=t[i])
			return false;
		i++;
	}
	return s[i]=='\0' && t[i]=='\0';
}

void Automat::printready(){
	struct lexem *p;
	p=ready;
	DEPRF1(("here!!!\n"));
	while(p!=NULL){
		DEPRF1(("content=[%s] type=[%s]\n", p->name, p->type));
		p=p->next;
	}
}

struct lexem *Automat::getlexem(){
	struct lexem *p;
	if(ready==NULL) throw EmptyListOfLex();
	p=ready;
	ready=ready->next;
	return p;
}
