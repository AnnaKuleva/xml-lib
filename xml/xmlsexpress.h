#ifndef _XML_SEXPRESSION_KURS3_H_
#define _XML_SEXPRESSION_KURS3_H_

#include <intelib/sexpress/sexpress.hpp>
#include <intelib/sexpress/sstring.hpp>
#include <stdio.h>
#include "structures.h"
#include <intelib/lisp/lsymbol.hpp>

/*This class represents s-expression of xml attribute.
There are fielads for xml-attribute's name and content,
and methods, which provide work with this class.*/
class SExpressionXmlAttr:public SExpression{
        //xml-attribute's name
        char *name;
        //xml-attribute's content
        char *content;
public:
	static IntelibTypeId TypeId;
    /*This is class' constructor.
     It creates xml-attribute with empty name and
     empty content.*/
	SExpressionXmlAttr():SExpression(TypeId){
		name=NULL;
		content=NULL;
	}
    /*This is class' constructor.
     It creates xml-attribute with needed name and
     needed content.*/
	SExpressionXmlAttr(const char *n, const char *c);
    /*This is class' constructor.
     It creates xml-attribute with needed name and
     empty content.*/
	SExpressionXmlAttr(const char *n);
    /*This is class' copy-constructor.*/
	SExpressionXmlAttr(SExpressionXmlAttr *attr);
    /*This method returns name of xml-attribute.*/
	const char * GetName()const{return name;}
    /*This method returns content of xml-attribute.*/
	const char * GetContent()const{return content;}
    /*This method inserts new content of xml-attribute.*/
	void NewCont(const char *n);

	#if INTELIB_TEXT_REPRESENTATIONS == 1
    	virtual SString TextRepresentation() const;
	#endif
protected:
	virtual ~SExpressionXmlAttr();
	virtual bool SpecificEql(const SExpression* other) const;
};

/*This class represents s-expression of xml-tag.
There are fields for xml-tag's name, attributes and content,
and methods, which provide work with this class.*/
class SExpressionXmlTag:public SExpression{
    //xml-tag's name
	char *name;
    //xml-tag's attributes
	SReference attr;
    //xml-tag's content
	SReference content;
public:
	static IntelibTypeId TypeId;
    /*This is class' constructor.
     It creates xml-tag with empty name, empty list
     of attributes and empty content.*/
	SExpressionXmlTag();
    /*This is class' constructor.
     It creates xml-tag with needed name, empty list
     of attributes and empty content.*/
    SExpressionXmlTag(const char *n);
    /*This is class' copy constructor.*/
	SExpressionXmlTag(SExpressionXmlTag* xml);
	const char *GetName()const{
		return name;
	}
    /*This method returns xml-tag's content.*/
	SReference GetContent() const{return content;}
    /*This method returns xml-tag's attributes.*/
	SReference GetAttr() const{return attr;}
    /*This function inserts new part of xml-tag's attributes.*/
	void InsertAttr(SReference r){
		attr=attr||r;
	}
    /*This function inserts new part of xml-tag's content.*/
	void InsertElem(SReference r){
		content=content||r;
	}
    /*This method inserts new content.*/
	void NewContent(SReference r){
		content=r;
	}
    /*This method inserts new list of attributes.*/
	void NewAttributes(SReference r){
		attr=r;
	}
	#if INTELIB_TEXT_REPRESENTATIONS == 1
        virtual SString TextRepresentation() const;
        #endif
protected:
	virtual ~SExpressionXmlTag();
	virtual bool SpecificEql(const SExpression* other) const;	
};

#endif


