#ifndef _STRUCTURES_H_KURS3_
#define _STRUCTURES_H_KURS3_

const char tag_name[]="tag_name";
const char attr_name[]="attribute_name";
const char tag_content[]="tag_content";
const char attr_content[]="attribute_content";
const char smth_doctype[]="doctype";
const char close_tag[]="close_tag_name";
const char end_of_file[]="end"; 
const char prolog_doc[]="prolog";

#ifdef DEBUG
#define DEPRF(x) printf x
#else
#define DEPRF(x)
#endif

#ifdef DEBUG1
#define DEPRF1(x) printf x
#else
#define DEPRF1(x)
#endif

#ifdef DEBUG2
#define DEPRF2(x) printf x
#else
#define DEPRF2(x)
#endif

#ifdef DEBUG3
#define DEPRF3(x) printf x
#else
#define DEPRF3(x)
#endif

enum streamcondition{
	attrib,
	every,
	prolog,
	attrprolog,
	conprolog,
	end
};

struct letter{
	char c;
	letter *next;
};

struct attribute{
	char *name;
	char *content;
	attribute *next;
};

struct lexem{
	char type[18];
	char *name;
	lexem *next;
};

struct partstr{
	char *part;
	int length;
	partstr *next;
};

#endif _STRUCTURES_H_KURS3_
