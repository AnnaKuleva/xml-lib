#ifndef _LEXAN_H_KURS3_
#define _LEXAN_H_KURS3_

#include "structures.h"
#include <stdio.h>

/*This class resperents automat, which converts text-data
into lexems, from which would be built xml-tags*/
class Automat{
	//current simbol
	char simbol;

	//conditions of automat
	enum condOfAutomat{
		begincond,
		prolog,
		whatis,
		opentag,
		emptytag,
		divider,
		attrname,
		quotation,
		hidefroman,
		attrcontent,
		elemcontent,
		closetag,
		between,
		comment,
		cdata,
		doctype,
		endprolog,
		endcond
	};
	
	//ready lexems
	lexem *ready;
	int cdatacount;
	int hyphen;
	int countprolog;
	condOfAutomat condition;
	int open;
	int close;
	int doccount;
	int prol;
	letter *word;
	int depth;
	char *savename;
	bool stream;
	bool itwasprolog;

	//condition functions
	void StartCondition();
	void OpenTagCondition();
	void AttrNameCondition();
	void QuatationCondition();
	void SeparatorCondition();
	void TagContentCondition();
	void CloseTagCondition();
	void HelpInformationCondition();
	void BetweenCondition();
	void AttrContentCondition();
	void EmptyTagCondition();
	void HiddenInformationCondition();
	void EndPrologCondition();
	void CdataCondition();
	void PrologCondition();
	void CommentCondition();
	void DoctypeCondition();
	void EndCondition();

	//function inserts new letter
	void insertletter(char s);
	void deleteletter();
	
	//function inserts new ready lexem in "ready-list"
	lexem *lettertoword();

	char *copystr(char *s2);
	void copytype(struct lexem *s1);
	const char *getcondition();
	void inserttoend(lexem *p);
	bool compear(const char *s, const char *t);
public:
	//this function gives automat simbols
	void feedchar(char a);
	int getdepth(){return depth;}
	//function returns ready lexems
	struct lexem *getlexem();
	//class' constructor
	Automat(bool type);
	//returns true if there are ready lexems
	bool lexemisready(){return ready!=NULL;}
	~Automat();
	void printready();
};

#endif
