#include "parstag.h"
#include "xmlsexpress.h"
#include <string.h>

static LListConstructor L;

//this function constracts content of hidden xml-tag
//inside hidden xml-tag can be another hidden xml-tags
//1 param - string
//2 param - sreference which function parses in content
SReference ParsString::getcontent(SString find, SReference save){
	SReference content=SListConstructor();
	DEPRF3(("in getcontent save = %s\n", save->TextRepresentation().c_str()));
	while(!save.IsEmptyList()){
		SExpressionString *isstr=save.Car().DynamicCastGetPtr<SExpressionString>();
		if(isstr!=0){
			SString str1(isstr->GetValue());
			SReference get=littlestr(find, str1);
			if(!get.IsEmptyList()){
				SExpressionString *sget=get.Car().DynamicCastGetPtr<SExpressionString>();
				SString s(sget);
				DEPRF3(("yes, we find content of tag %s\n", get->TextRepresentation().c_str()));
				int k=s.length()+find.length();
				DEPRF3(("k = %d length = %d\n", k, str1.length()));
				if(k==str1.length()){
					DEPRF3(("content is %s\n", content->TextRepresentation().c_str()));
					if(s.length()!=0)
						content=content||get;
					if(!save.Cdr().IsEmptyList())
						content=(L|content||save.Cdr());
					else
						content=(L|content);
					DEPRF3(("return from getcontent %s\n", content->TextRepresentation().c_str()));
					return content;
				}
				DEPRF3(("not same length\n"));
				SString str2;
				SReference r=SListConstructor();
				while(str1[k]!='\0'){
					str2+=str1[k];
					k++;
				}
				r=r||(L|str2);
				if(s.length()==0)
					if(!save.Cdr().IsEmptyList())
						content=(L|content||r||save.Cdr());
					else
						content=(L|content||r);
				else{
					content=content||get;
					if(!save.Cdr().IsEmptyList())
						content=(L|content||r||save.Cdr());
					else
						content=(L|content||r);
				}
				DEPRF3(("return from getcontent %s\n", content->TextRepresentation().c_str()));
				return content;
			}
			else{
				DEPRF3(("from little string we get mpty list\n"));
				SReference r=SListConstructor();
				r=r||(L|save.Car());
				content=content||r;
				save=save.Cdr();
			}
		}
		else{
			SReference r=SListConstructor();
			r=r||(L|save.Car());
			content=content||r;
			save=save.Cdr();
		}
	}
	SReference res=(L|*PTheEmptyList)||content;
	DEPRF3(("return from getcontent %s\n", res->TextRepresentation().c_str()));
	return res;
}

//this function finds "_" and divides string
//1 param - string
//result - it is list of substrings
SReference ParsString::findattr(SString line){
	int leg=line.length();
	SString save;
	SReference res=SListConstructor();
	for(int i=0; i<leg; i++){
		if(line[i]==' '){
			save+='\0';
			res=res||(L|save);
			save=SString();
		}
		else{
			save+=line[i];
		}
	}
	if(save.length()!=0){
		save+='\0';
		DEPRF3(("res %s\n", save.c_str()));
		res=res||(L|save);
	}
	DEPRF3(("result from parsing name %s\n", res->TextRepresentation().c_str()));
	return res;		
}

//this function creates list of attributes
//1 param - it list of pairs "name_attr=content_attr"
//result - list of attributes
SReference ParsString::getattrs(SReference list){
	DEPRF3(("in getattrs %s\n", list->TextRepresentation().c_str()));
	SReference res=SListConstructor();
	SReference saved=SListConstructor();
	while(!list.IsEmptyList()){
		SExpressionString *s=list.Car().DynamicCastGetPtr<SExpressionString>();
		if(s==0)
			throw "something stange in ParsString::getattrs-not a string\n";
		const char* isnt_part = strchr(s->GetValue(), '=');
		if(isnt_part==NULL){
			SExpressionXmlAttr *attr=saved.DynamicCastGetPtr<SExpressionXmlAttr>();
			if(attr==NULL){
				if(list.Cdr().IsEmptyList())
					throw "Error in ParsString::getattrs - wrong formed attr-list\n";
				SExpressionString *s1=list.Cdr().Car().DynamicCastGetPtr<SExpressionString>();
		                if(s1==0)
                        		throw "something stange in ParsString::getattrs-not a string\n";
				if(s1->GetValue()[0]=='='){
					SString str=s->GetValue();
					str+=s1->GetValue();
					list=list.Cdr();
					s=new SExpressionString(str.c_str());
				}
				else
					throw "Error in ParsString::getattrs - wrong formed attr-list\n";
			}
			else{
				SString cont=attr->GetContent();
				SString str=s->GetValue();
				SString s1;
				int i=0;
				while(str[i]!='\0'){
					if(str[i]!='"' && str[i]!='\'')
                    		    		s1+=str[i];
                			i++;
				}
				s1+='\0';
				cont=cont+s1;
				attr->NewCont(cont.c_str());
				list=list.Cdr();
				continue; 
			}
		} 
		SExpressionXmlAttr *two=divfortwo(s);
		res=res||(L|two);
		saved=two;
		list=list.Cdr();
	}
	DEPRF3(("result in getattrs %s\n", res->TextRepresentation().c_str()));
	return res;
}

//this function convert string "name_attr=content_attr"
//in attribute
//1 param - string
//result - created xml-attribute
SExpressionXmlAttr *ParsString::divfortwo(SString s){
	SString s1;
	SString s2;
	int i=0;
	DEPRF3(("divfortwo %s\n", s.c_str()));
	while(s[i]!='=' && s[i]!='\0'){
		s1+=s[i];
		i++;
	}
	if(s[i]=='\0')
		throw "smth strange in ParsString::divfortwo - not attr\n";
	i++;
	while(s[i]!='\0'){
		if(s[i]!='"' && s[i]!='\'')
			s2+=s[i];
		i++;
	}
	//if(s2.length()==0)
	//	throw "smth strange in PasrString::divfortwo - no content\n";
	s2+='\0';
	SExpressionXmlAttr *attr=new SExpressionXmlAttr(s1.c_str(), s2.c_str());
	return attr;
}


//this function reverts string to fixed position
//1 param - string
//2 param - position
//result - revertd string
SString ParsString::turnsave(SString str1, int k){
	SString res;
	for(int i=str1.length()-1-k; i>=0; i--)
		res+=str1[i];
	res+='\0';
	return res;
}

//this function will search for substring
//1 param - substring
//2 param - string
//return value - nil if there is not substring
//return value - (string before substring)
SReference ParsString::littlestr(SString str1, SString str2){
	int i=0;
	DEPRF3(("littlestr - length of str is %d, find is %s\n", str1.length(), str1.c_str()));
	DEPRF3(("littlestr - where %s\n", str2.c_str()));
	SString emptystr;
	while(str2[i]!='\0'){
		int x=0;
		bool notaclose=false;
		for(int k=i; k<i+str1.length()-1; k++){
			if(str2[x]=='\0' || str1[x]!=str2[k]){
				notaclose=true;
				break;
			}
			x++;
		}
		if(notaclose){
			emptystr+=str2[i];
			i++;
		}
		else{
			emptystr+='\0';
			DEPRF3(("from littlestr %s\n", emptystr.c_str()));
			SReference res=SListConstructor();
			res=res||(L|emptystr);
			return res;
		}
	}
	DEPRF3(("nothing from littlestr\n"));
	return *PTheEmptyList;
}

//this function will search for close part of tag
//1 param - position
//result - name of tag (close part)
SString ParsString::getname(int i){
	SString savestr;
	SString close("&gt;");
	bool start_attr=false;
	char save_s = '\0';
	while(str[i]!='\0'){
		if(i!=0)
			save_s=str[i-1];
		bool isclose=true;
		bool fail=true;
		int x=0;
		char simbol=str[i];
		if(start_attr || (simbol<='z' && simbol>='a') || (simbol<='Z' && simbol>='A') || simbol=='_'
                ||simbol==':' || simbol<='9' && simbol>='0' || simbol=='-' || simbol=='@' || simbol=='\''
                || simbol=='.' || simbol=='&' || simbol==';' || simbol=='=' || simbol=='"' || simbol==' ' || simbol=='/'
		|| simbol=='+' || simbol=='#' || simbol==':' || simbol=='~' || simbol=='(' || simbol==')' || simbol=='%')
			fail=false;
		if((str[i]=='\'' || str[i]=='"') && start_attr)
                        start_attr=false;
		if(save_s=='=' && (str[i]=='"' || str[i]=='\'') && !start_attr)
			start_attr=true;
		if(fail)
			return "";
		for(int k=i; k<i+close.length()-1; k++){
			if(str[k]=='\0' || close[x]!=str[k]){
				isclose=false;
				break;
			}
			x++;
		}
		if(!isclose){
			savestr+=str[i];
			i++;
		}
		else{
			savestr+='\0';
			DEPRF3(("we get close in getname\n"));
			DEPRF3(("function getname %s\n", savestr.c_str()));
			return savestr;
		}
	}
	savestr+='\0';
	DEPRF3(("function getname %s\n", savestr.c_str()));
	return savestr;
}

//constructor
ParsString::ParsString(SExpressionString *s, SReference t):str(s){
	DEPRF3(("it is construntor of parsSting!!!\n"));
	DEPRF3(("in constructor\n"));
	tail=t;
	DEPRF3(("1 param - %s", s->TextRepresentation().c_str()));
	DEPRF3(("2 param - %s", tail->TextRepresentation().c_str()));
	DEPRF3(("go back\n"));
}

//this function puts two parts in one list
//if we find open part but don't find close part -> don't need to make xml-tag
SReference ParsString::didnfindclpart(int i){
	DEPRF3(("find open part but no close part\n"));
	SReference res=SListConstructor();
	SString head;
        for(int m=0; m<i; m++)
                head+=str[m];
        head+='\0';
	SString intail;
        while(str[i]!='\0'){
                intail+=str[i];
                i++;
        }
	SReference r=SListConstructor();
	r=r||(L|intail->GetValue());
        r=r||tail;
	if(head[0]!='\0'){
                res=res||(L|head->GetValue());
                res=res||r;
        }
        else{
                res=res||(L|*PTheEmptyList);
                res=res||r;
        }

	return res;
}


//this function works when we don't find close tag
//1 param - position
SReference ParsString::didnfindcltag(int i){
	DEPRF3(("we didn't find a close tag\n"));
	SString head;
	for(int m=0; m<i; m++)
		head+=str[m];
	head+='\0';
	SString intail;
	while(str[i]!='\0'){
		intail+=str[i];
		i++;
	}
	SReference r=SListConstructor();
	//r=r||(L|intail->GetValue());
	r=r||tail;
	//SReference res=SListConstructor();
	/*if(head[0]!='\0'){
		res=res||(L|head->GetValue());
		res=res||r;
	}
	else{*/
		//res=res||(L|*PTheEmptyList);
		//res=res||r;
	//}
	DEPRF3(("return from Parse::work %s\n", r->TextRepresentation().c_str()));
	return r;
}

//this function concatenates string, xml-tag, tail
//1 param - position
//2 param - geted tail
//3 param - it's xml-tag which was got
SReference ParsString::findevery(int i, SReference get, SExpressionXmlTag *xml){
	SString str1;
	DEPRF3(("i=%d\n", i));
	for(int m=0; m<i; m++)
		str1+=str[m];
	str1+='\0';
	DEPRF3(("head is %s\n", str1.c_str()));
	SReference res=SListConstructor();
	res=res||(L|str1);
	res=res||(L|xml)||get.Cdr();
	DEPRF3(("return from Parse::work %s\n", res->TextRepresentation().c_str()));
	return res;
}

//this function creates list if there weren't any entities
//result = nil, str, tail
SReference ParsString::noentity(){
	DEPRF3(("we didn't find entities\n"));
	SReference res=SListConstructor();
	SReference r=SListConstructor();
	r=r||(L|str);
	r=r||tail;
	res=res||(L|*PTheEmptyList);
	res=res||r;
	DEPRF3(("return from Parse::work %s\n", res->TextRepresentation().c_str()));
	return res;
}

//this function creates list - on the first position stays xml-tag
//result = nil, xml-tag, get.Cdr
//1 param - got result
//2 param - xml-tag 
SReference ParsString::evetywithtail(SReference get, SExpressionXmlTag *xml){
	SReference res=SListConstructor();
	res=res||(L|*PTheEmptyList);
	res=res||(L|xml);
	res=res||get.Cdr();
	DEPRF3(("return from Parse::work %s\n", res->TextRepresentation().c_str()));
	return res;
}


//this is main work of parsing
//this function finds hidden xml-tags
//return value - list of strings and xml-tags (which were hidden)
SReference ParsString::work(){
	DEPRF3(("IN WORK\n"));
	SString save;
	const SString open("&lt;");
	const SString close("&gt;");
	int i=str.length()-1;
	while(i>=0){
		bool isopen=true;
		int x=0;
		for(int k=i; k<i+open.length()-1; k++){
			if(str[k]=='\0' || open[x]!=str[k]){
				isopen=false;
				break;
			}
			x++;
		}
		if(isopen && str[i+open.length()]!='/'){
			DEPRF3(("find open in work\n"));
			SString name=getname(i+open.length());
			if(name.length()==0 ||  str[name.length()-1+i]=='\0'){
				return didnfindclpart(i);
			}
			int l=name.length()+close.length()+open.length();
			SReference div;
			SString s1;
			int y=0;
			SString newname;
			while(name[y]==' ') y++;
			while(name[y]!='\0'){
				newname+=name[y];
				y++;
			}
			newname+='\0';
			name=newname;
			bool emptytag=false;
			if(name[name.length()-1]=='/'){
				DEPRF3(("it's emptytag\n"));
				for(int k=0; k<name.length()-1; k++)
					s1+=name[k];
				s1+='\0';
				emptytag=true;
			}
			SString s2(name);
			SString n;
			y=0;
			while(name[y]==' ') y++;
			while(name[y]!=' ' && name[y]!='\0'){
				char simbol=name[y];
				if((simbol<='z' && simbol>='a') || (simbol<='Z' && simbol>='A') || simbol=='_'
                			||simbol==':' || simbol<='9' && simbol>='0' || simbol=='-' || simbol=='@'
                			|| simbol=='.')
					n+=simbol;
				else
					return didnfindclpart(i);
				y++;
			}
			n+='\0';
			name=n;
			SReference get=SListConstructor();
			get=get||(L|*PTheEmptyList);
			bool dirty_hack=false;
			if(!emptytag){
				SString find=open+"/"+name+close;
				DEPRF3(("find is %s\n", find.c_str()));
				SReference r=SListConstructor();
				save=turnsave(save, l-1);
				r=r||(L|save);
				r=r||tail;
				get=getcontent(find, r);
				if(get.Car().IsEmptyList()){
					//return didnfindcltag(i);
					//not close tag
					emptytag = true;
					//didnfindcltag(i);
					//get.Cdr() = tail;
					get=get||(L|*PTheEmptyList);
					dirty_hack=true;
				}
			}
			DEPRF3(("name %s\n", name->GetValue()));
			SExpressionXmlTag *xml=new SExpressionXmlTag(name->GetValue());
			xml->InsertElem(get.Car());
			if(emptytag && !dirty_hack)
			{
				div=findattr(s1);
				if(!tail.IsEmptyList()){
					get=SListConstructor();
					get=get||tail;
				}
			}
			else
				div=findattr(s2);
			if(!div.IsEmptyList()){
				div=getattrs(div.Cdr());
				if(!div.IsEmptyList())
					xml->InsertAttr(div);
			}
			DEPRF3(("yes, we get tag\n"));
			if(i!=0){
				return findevery(i, get, xml);
			}	
			else{
				DEPRF3(("it's in first posistion\n"));
				return evetywithtail(get, xml);
			}	
		}
		else{
			save+=str[i];
			i--;
		}
	}
	return noentity();
}
