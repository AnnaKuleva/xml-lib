#ifndef _SERVFORLISP_H_KURS3_
#define _SERVFORLISP_H_KUSR3_

#include <intelib/sexpress/sexpress.hpp>
#include <intelib/genlisp/lispform.hpp>
#include <intelib/lisp/lisp.hpp>
#include <intelib/lisp/lsymbol.hpp>
#include <sys/socket.h>
#include <unistd.h>
#include "makesexpr.h"
#include "structures.h"

//Make Socket incapsulates functions and members
//which help to connect create socket
//and realise server-work
class MakeSocket{
	//socet
	int fsoc;
	//port number
	int nport;
	bool stop;
	//is work ended
	bool end;
	//set of users
	fd_set mas;
	//number of users
	int max_d;
	int mypol;
	//number simbols which we got from users
	int get;
	//during simbol which is analised
	int  during;
	//number of free positions for users
	int numbus;
	//max numb of users
	int saven; 
	//list of users' discriptors
	int *listusers;
	//buffer in which text from users is saved
	char buf[128];

	int convert(const char *s);
	int getmessage(int disk, StreamWork *turner);
	bool emptybuf();
	void changeline(int pol);
public:
	MakeSocket(const char *s, const char *t);
	void makeservsocket();
	~MakeSocket();
	bool needcycle();
        void firstpartofcycle();
        bool isheresomepol();
        int secondpartofcycle(int disk, StreamWork *turner);
        bool mustweaccept();
        int thirdpartofcycle();
        void turnfalsestop(){mypol=-1; end=false;}
        char getsimbol();
	SReference arrayofusers();
};

enum clientsost{
	waitforcon,
        firstack,
        authoris,
        givepas,
        allright,
        secondack,
	askresource,
	statussended,
	mainwork,
	getlist,
	myimagin
};

class ClientCon{
	clientsost sost;
	SReference client_information;
	int fsoc;
	int port;
	const char *ipadr;
	char buf[128];
	fd_set mas;
	StreamWork *turner;
	int getmessage();
public:
	ClientCon(int p, const char *s);
	void connecttoserv();
	void selectcycle();
	int servhavetosay();
	void sendmessage(const char *s);
	StreamWork *getturner(){return turner;}
	clientsost getsost(){return sost;} 
	void changesost(clientsost s){sost=s;}
	void changeturner();
	void add_information(const char* key, SReference data);
	void reset_information(SReference data);
	bool change_information(const char* key, SReference data);
	SReference get_information(const char* key);
	~ClientCon();
};

//this class incapsulates one object of MakeSocket
//this class turns MakeSocket in sexpression, so there is a possibility to work
//with MakeSocket in Lisp
class SExpressionServerPart:public SExpression{
	MakeSocket *sock;
public:
	SExpressionServerPart(SReference x, SReference y);
	~SExpressionServerPart();
	static IntelibTypeId TypeId;
	virtual SString TextRepresentation() const;
	MakeSocket *GetSock(){return sock;}
};

//this class incapsulated one object of StreamWork
//this class turns StreamWork in sexpression, so there is a possibility to work
//with StreamWork in Lisp
class SExpressionStreamWork:public SExpression{
	StreamWork *turner;
	int disk;
public:
	SExpressionStreamWork(SReference x);
	~SExpressionStreamWork();
	static IntelibTypeId TypeId;
	virtual SString TextRepresentation() const;
	StreamWork *GetTurner(){return turner;}
};

class SExpressionClientPart:public SExpression{
	ClientCon *client;
public:
	SExpressionClientPart(int x, const char *s);
	~SExpressionClientPart();
	static IntelibTypeId TypeId;
	virtual SString TextRepresentation() const;
	ClientCon *GetClient(){return client;}
};

class CodeDecode{
	char code[205];
	char out[241];
	int copystrmy(char *where, const char *from, int pos);
	void foranswer(const char *str1,const char *str3,const char *str4,const char *str5,int i);
public:
	unsigned int base64_decode(char *out, char *data, unsigned int len);
	char index2byte(char index);
	void formtheanswer(const char *usern, const char *servn, const char *non,
                const char *cnon, const char *digest, const char *password);
	void copystr(const char *str1);
	void base64_encode(char *out, char *data, unsigned int len);
	char *getcode(){return code;}
};

SReference make_serv_sock(int params, const SReference *param);
SReference openlisten(int params, const SReference *param);
SReference isworkcycle(int params, const SReference *param);
SReference readyformessege(int params, const SReference *param);
SReference havetosay(int params, const SReference *param);
SReference getmesfrompol(int params, const SReference *param);
SReference mustaccept(int params, const SReference *param);
SReference acceptusers(int params, const SReference *param);
SReference stopfalse(int params, const SReference *param);
SReference curuserslist(int params, const SReference *param);
SReference buildtree(int params, const SReference *param);
SReference maketurnforuser(int params, const SReference *param);
SReference make_client(int params, const SReference *param);
SReference get_connection(int params, const SReference *param);
SReference send_message(int params, const SReference *param);
SReference make_select(int params, const SReference *param);
SReference read_messages(int params, const SReference *param);
SReference get_tree(int params, const SReference *param);
SReference delete_tree(int params, const SReference *param);
SReference change_sost(int params, const SReference *param);
SReference get_sost_client(int params, const SReference *param);
SReference new_tree(int params, const SReference *param);
SReference get_chipher(int params, const SReference *param);
SReference turntaginstr(int params, const SReference *param);
SReference add_clients_information(int params, const SReference *param);
SReference reset_clients_information(int params, const SReference *param);
SReference change_clients_information(int params, const SReference *param);
SReference get_clients_information(int params, const SReference *param);

void define_serv_functions();
void define_client_functions();

#endif
