#ifndef _SYMB_H_KUSR3_
#define _SYMB_H_KURS3_

#include <intelib/lisp/lisp.hpp>
#include <intelib/lisp/lsymbol.hpp>

extern LSymbol DTDELNAME;
extern LSymbol DTDELCONT;
extern LSymbol GETDATTRNAME;
extern LSymbol GETDATTRTYPE;
extern LSymbol GETDATTRVALUE;
extern LSymbol GETDATTRLIST;

extern LSymbol PRINTBEAUTY;
extern LSymbol PRINTSMTH;
extern LSymbol INFILESTR;
extern LSymbol INFILETAG;
extern LSymbol INFILEATR;
extern LSymbol INFILECLOSETAG;
extern LSymbol PRINTEMPTY;

extern LSymbol NEWTAG;
extern LSymbol COPYTAG;
extern LSymbol INSCONT;
extern LSymbol ISXMLTAG;
extern LSymbol XMLNAMEGET;
extern LSymbol GETCONTENT;
extern LSymbol NEWCONTENT;

extern LSymbol NEWATTR;
extern LSymbol COPYATTR;
extern LSymbol XMLATTRGET;
extern LSymbol NEWATTRLIST;
extern LSymbol GETATTRCONTENT;
extern LSymbol INSATTRCONT;
extern LSymbol GETATTRNAME;
extern LSymbol INSREADYATTR;

extern LSymbol CLOSEFILESTREAM;
extern LSymbol FILESTREAM;
extern LSymbol DIRFILES;
extern LSymbol EXISTSDIR;
extern LSymbol TURNOPENFILE;

extern LSymbol ISSTRING;
extern LSymbol TRANSFORMSTR;
extern LSymbol PARSTAGS;
extern LSymbol CHANGEENT;
extern LSymbol DELSIMB;
extern LSymbol CHANGEAMP;
extern LSymbol NUMBFROMSTR;
extern LSymbol STRFROMNUMB;
extern LSymbol CONCATSTR;
extern LSymbol PARSEXPATH;
extern LSymbol ONLYNAME;
extern LSymbol STRCONCATNUMB;
extern LSymbol GETNUMBERSTR;
extern LSymbol GENERATENUM;
extern LSymbol DIVIDENUM;
extern LSymbol NUMFROMSTR;
extern LSymbol DIVIDESTRSIM;
extern LSymbol TAGINSTR;

extern LSymbol PRINTINFILEXML;

extern LSymbol MAKELISTEN;
extern LSymbol CREATESERVSTRUCT;
extern LSymbol ISWORK;
extern LSymbol PREPEARFORMS;
extern LSymbol USERHASTOSAY;
extern LSymbol GETMESSAGES;
extern LSymbol HAVEAPPLICATIONS;
extern LSymbol ACCEPTUSERS;
extern LSymbol GETREADYFORNEXT;
extern LSymbol ARRAYOFUSERS;
extern LSymbol GETTURNER;

extern LSymbol MAKECLIENT;
extern LSymbol GETCONNECTION;
extern LSymbol READMESSAGE;
extern LSymbol MAKESELECT;
extern LSymbol SENDMESSAGE;
extern LSymbol GETDURTREE;
extern LSymbol DELETETREE;
extern LSymbol GETCLCOND;
extern LSymbol CHANGECLCOND;
extern LSymbol NEWTREE;
extern LSymbol GETCHIPHER;
extern LSymbol ADDCLINFORM;
extern LSymbol CHANGECLINFORM;
extern LSymbol GETCLINFORM;
extern LSymbol RESETCLINFORM;
#endif
