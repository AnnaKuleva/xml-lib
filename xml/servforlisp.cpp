#include "servforlisp.h"
#include <intelib/sexpress/sstring.hpp>
#include <intelib/tools/sstream.hpp>
#include "xmlsexpress.h"
#include "structures.h"
#include <string.h>
#include "mistake.h"
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "md5.h"
#include <stdlib.h>
#include "declare_serv_functions.h"

#ifndef ASSERT_PARAMC
#define ASSERT_PARAMC(n) \
    INTELIB_ASSERT(params <= n, IntelibX_too_many_params(params));\
    INTELIB_ASSERT(params >= n, IntelibX_too_few_params(params));
#endif

static LListConstructor L;

//constructor of MakeSocket
//1 param - port value
//2 param - max number of users
MakeSocket::MakeSocket(const char *s, const char *t){
	nport=convert(s);
	fsoc=0;
	end=false;
	stop=false;
	get=0;
	during=-1;
	max_d=-1;
	mypol=-1;
	numbus=convert(t);
	saven=numbus;
	listusers=new int[numbus];
	//firstly all discriptors will be -1, because we have no users
	for(int i=0; i<numbus; i++)
		listusers[i]=-1;
}

//destructor
MakeSocket::~MakeSocket(){
	close(fsoc);
	delete [] listusers;
}

//this function gets number from string
//1 param - string
int MakeSocket::convert(const char *s){
	int i=0;
	int res=0;
	while(s[i]!='\0'){
		res=res*10+s[i]-'0';
		i++;
	}
	return res;
}

//this function makes socket
void MakeSocket::makeservsocket(){
	struct sockaddr_in a;
	int opt=1;
	if((fsoc=socket(AF_INET, SOCK_STREAM, 0))==-1){
                throw IntelibX("can't execute func socket");
        }
	a.sin_family=AF_INET;
        a.sin_port=htons(nport);
        a.sin_addr.s_addr=INADDR_ANY;
        setsockopt(fsoc, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
	if((bind(fsoc, (struct sockaddr *)&a, sizeof(a)))==-1){
		DEPRF3(("some errors\n"));
                throw IntelibX("can't execute func bind");
        }
	if(-1==listen(fsoc, 5)){
                throw IntelibX("can't execute func listen");
        }
	DEPRF3(("makeservscoket fsoc=%d\n", fsoc));
}

bool MakeSocket::needcycle(){
	DEPRF3(("need we work or not\n"));
	return (!end);
}

//this function realizes the first part of select-cycle
//function calls "select" and look for mistakes in it
void MakeSocket::firstpartofcycle(){
	int mistake;
	max_d=fsoc;
	FD_ZERO(&mas);
	FD_SET(fsoc, &mas);
	if(numbus<saven){
		for(int i=0; i<saven; i++){
			if(listusers[i]!=-1){
				FD_SET(listusers[i], &mas);
				if(listusers[i]>max_d)
					max_d=listusers[i];
			}
		}
	}
	mistake=select(max_d+1, &mas, NULL, NULL, NULL);
	DEPRF3(("mistake=%d\n", mistake));
	if(mistake<1){
		throw IntelibX("select");
	}
	DEPRF3(("yes\n"));
}

//this function looks for users
bool MakeSocket::isheresomepol(){
	DEPRF3(("second question %d\n", numbus<saven));
	return numbus<saven;
}

//this function realizes the second part of select-cycle 
//function reads message if users has something to say
//1 param - user's discriptor
//2 param - it is an object which turns message in xml-tags
int MakeSocket::secondpartofcycle(int disk, StreamWork *turner){
	DEPRF3(("cycle after select\n"));
	if(FD_ISSET(disk, &mas)){
		DEPRF3(("my user has something for me\n"));
		return getmessage(disk, turner);
	}
	DEPRF3(("go back from second part\n"));
	return 0;
}

//this function returns list of current users
//it turns users in SReference list
SReference MakeSocket::arrayofusers(){
	SReference res=SListConstructor();
	for(int i=0; i<saven; i++)
		res=res||(L|listusers[i]);
	DEPRF3(("return list of users SReference %s\n", res->TextRepresentation().c_str()));
	return res;
}

//this function looks for users which must be accepted
bool MakeSocket::mustweaccept(){
	DEPRF3(("third question\n"));
	return FD_ISSET(fsoc, &mas);
}

//this function realizes the thisd part of cycle
//here we will accept our user if we could
int MakeSocket::thirdpartofcycle(){
	char buf1[]="Sorry! Server is busy! Please, try again later\n";
	char buf2[]="Welcome! Let's start our work!\n";
	DEPRF3(("search for users\n"));
	int get=accept(fsoc, NULL, NULL);
	if(get==-1){
		throw IntelibX("we have some problems with function accept");
	}
	if(numbus==0){
		write(get, buf1, sizeof(buf1));
		close(get);
		return -1;
	}
	else{
		listusers[numbus-1]=get;
		write(listusers[numbus-1], buf2, sizeof(buf2));
		numbus--;
	}
	return get;
}

//this function gets message from user
//if we got EOF that means that user had gone => functions deletes him
//ohterwise - gets message from user
//1 param - user's discriptor
//2 param - it is an object which turns text in xml-tags
int MakeSocket::getmessage(int disk, StreamWork *turner){
	DEPRF3(("we are in getmessage\n"));
	get=read(disk, buf, sizeof(buf)-1);
	if(get==0){
		DEPRF3(("we lost user with disk %d\n", disk));
		shutdown(disk, 2);
		close(disk);
		changeline(disk);
		numbus++;
		//when buffer is empty -> everyone had gone
		//so session is ended and we will began new
		if(numbus==saven){
			end=true;
			DEPRF3(("everyone had gone\n"));
		}
		buf[0]=EOF;
		get=1;
		during=-1;
		return -1;
	}
	buf[get]='\0';
	during=-1;
	DEPRF3(("get from user %d buf [%s]\n", disk, buf));
	turner->makequque(buf, 128);
	turner->work();
	SReference tree=turner->gettree();
	DEPRF3(("during tree is %s\n", tree->TextRepresentation().c_str()));
	return 1;
}

//this function deletes users from array
//if one user had gone away function puts him away from array
//1 param - user's discriptor
void MakeSocket::changeline(int pol){
	DEPRF3(("user which gone away is %d\n", pol));
	int i=0;
	while(listusers[i]!=pol) i++;
	int *changearr=new int[saven];
	changearr[0]=-1;
	for(int j=1; j<=i; j++)
		changearr[j]=listusers[j-1];
	for(int j=i+1; j<saven; j++)
		changearr[j]=listusers[j];
	delete [] listusers;
	listusers=changearr;
	DEPRF3(("here new is array of users\n"));
	for(int i=0; i<saven; i++){
                DEPRF3(("%d ", listusers[i]));
        }
	DEPRF3(("\n"));
}

//function returns true if buffer is empty
bool MakeSocket::emptybuf(){
	if(get==0) return false;
	return true;
}

//function gets simbols from buffer
char MakeSocket::getsimbol(){
	during++;
	if(during+1==get)
		get=0;
	return buf[during];
}

//it is constructor of SExpressionServerPart
//1 param - string - port number
//2 param - string - number of users
SExpressionServerPart::SExpressionServerPart(SReference x, SReference y):SExpression(TypeId){
	SExpressionString *str=x.DynamicCastGetPtr<SExpressionString>();
	if(str==0)
		throw IntelibX("1 it's not a string in SExpressionServerPart");
	SExpressionString *str1=y.DynamicCastGetPtr<SExpressionString>();
	if(str1==0)
		throw IntelibX("2 it's not a string in SExpressionnMakeSocket");
	sock=new MakeSocket(str->GetValue(), str1->GetValue());
}

//destructor of SExperssionMakeSocket
SExpressionServerPart::~SExpressionServerPart(){
	delete sock;
}

IntelibTypeId SExpressionServerPart::TypeId(&SExpression::TypeId);

SString SExpressionServerPart::TextRepresentation() const{
	return SString("#<MAKE SERV SOCKET");
}

//this is SExpressionStreamWork's constructor
//1 param - int - discriptor
SExpressionStreamWork::SExpressionStreamWork(SReference x):SExpression(TypeId){
	SExpressionInt *d=x.DynamicCastGetPtr<SExpressionInt>();
	if(d==0)
		throw IntelibX("it's not an integer in SExpressionStreamWork");
	disk=d->GetValue();
	turner=new StreamWork();
}

//this is SExpressionStreamWork's destructor
SExpressionStreamWork::~SExpressionStreamWork(){
	delete turner;
}

IntelibTypeId SExpressionStreamWork::TypeId(&SExpression::TypeId);

SString SExpressionStreamWork::TextRepresentation() const{
	return SString("#<TURNER FOR XML STREAM");
}

//this function makes SExpressionServerPart
//1 param - string - port number
//2 param - string - number of users
//return value - SExpressionServerPart
SReference make_serv_sock(int params, const SReference *param){
	ASSERT_PARAMC(2);
	SExpressionServerPart *sock=new SExpressionServerPart(param[0], param[1]);
	return sock;
}

//this function makes socket && starts listen
//1 param - sExpressionMakeSocket
SReference openlisten(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionServerPart *sock=param[0].DynamicCastGetPtr<SExpressionServerPart>();
	if(sock==0)
		throw IntelibX("it's not a SExpressionServerPart in openlisten");
	MakeSocket *arg=sock->GetSock();
	arg->makeservsocket();
	return sock;
}

//this function returns 1 if it is necessary to start workcycle
//otherwise - returns 0
//1 param - SExpressionServerPart
SReference isworkcycle(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionServerPart *sock=param[0].DynamicCastGetPtr<SExpressionServerPart>();
	if(sock==0)
		throw IntelibX("it's not a SExpressionServerPart in isworkcycle");
	MakeSocket *arg=sock->GetSock();
	if(arg->needcycle())
		return new SExpressionInt(1);
	else
		return new SExpressionInt(0);
}

//this function starts the firstpart of cycle
//calls select
//1 param - SExpressinMakeSocket
SReference readyformessege(int params, const SReference *param){
	ASSERT_PARAMC(1);
        SExpressionServerPart *sock=param[0].DynamicCastGetPtr<SExpressionServerPart>();
        if(sock==0)
                throw IntelibX("it's not a SExpressionServerPart in readyformessage");
        MakeSocket *arg=sock->GetSock();
        arg->firstpartofcycle();
	return sock;
}

//this function returns 1 - if there are users which sent message
//returns 0 - if there aren't such users
//1 param - SExpressionServerPart
SReference havetosay(int params, const SReference *param){
        ASSERT_PARAMC(1);
        SExpressionServerPart *sock=param[0].DynamicCastGetPtr<SExpressionServerPart>();
        if(sock==0)
                throw IntelibX("it's not a SExpressionServerPart in havetosay");
        MakeSocket *arg=sock->GetSock();
        DEPRF3(("here is an answer %d\n", arg->isheresomepol()));
	if(arg->isheresomepol())
                return new SExpressionInt(1);
        else
                return new SExpressionInt(0);
}

//this function starts the second part of cycle
//1 param - SExpressionServerPart
SReference getmesfrompol(int params, const SReference *param){
        ASSERT_PARAMC(3);
        DEPRF3(("in second func\n"));
	SExpressionServerPart *sock=param[0].DynamicCastGetPtr<SExpressionServerPart>();
        if(sock==0)
                throw IntelibX("1 it's not a SExpressionServerPart in getmesforpol");
        SExpressionInt *disk=param[1].DynamicCastGetPtr<SExpressionInt>();
	if(disk==0)
		throw IntelibX("2 it's not a SExpressionInt in getmesforpo");
	SExpressionStreamWork *turn=param[2].DynamicCastGetPtr<SExpressionStreamWork>();
	if(turn==0)
		throw IntelibX("3 it's not a SExpressionStreamWork in getmespol");
	MakeSocket *arg=sock->GetSock();
        int res=arg->secondpartofcycle(disk->GetValue(), turn->GetTurner());
	SExpressionInt *r=new SExpressionInt(res);
	DEPRF3(("go back\n"));
        return (L|r, turn, sock);
}

//this function returns 1 if there is user who must be accepted
//returns 0 - otherwise
//1 param - SExpressionServerPart
SReference mustaccept(int params, const SReference *param){
        DEPRF3(("we im nust accept\n"));
	ASSERT_PARAMC(1);
        SExpressionServerPart *sock=param[0].DynamicCastGetPtr<SExpressionServerPart>();
        if(sock==0)
                throw IntelibX("it's not a SExpressionServerPart in mustaccept");
        MakeSocket *arg=sock->GetSock();
        if(arg->mustweaccept())
                return new SExpressionInt(1);
        else
                return new SExpressionInt(0);
}

//this function accepts users
//1 param - SExpressionServerPart
SReference acceptusers(int params, const SReference *param){
        ASSERT_PARAMC(1);
        SExpressionServerPart *sock=param[0].DynamicCastGetPtr<SExpressionServerPart>();
        if(sock==0)
                throw IntelibX("it's not a SExpressionServerPart in acceptusers");
        MakeSocket *arg=sock->GetSock();
        int res=arg->thirdpartofcycle();
        SExpressionInt *r=new SExpressionInt(res);
	return (L|r, sock);
}

//this function set MakeSocket ready for the next cycle
//1 param - SExpressionMkeSocket
SReference stopfalse(int params, const SReference *param){
        ASSERT_PARAMC(1);
        SExpressionServerPart *sock=param[0].DynamicCastGetPtr<SExpressionServerPart>();
        if(sock==0)
                throw IntelibX("it's not a SExpressionServerPart in stopfalse");
        MakeSocket *arg=sock->GetSock();
        DEPRF3(("we are in stopfalse\n"));
	arg->turnfalsestop();
	DEPRF3(("going back from storfalse\n"));
        return sock;
}

//this function returns list of current users
//it's needed, for example, when we are searching for
//users which have to say something
SReference curuserslist(int params, const SReference *param){
        ASSERT_PARAMC(1);
        SExpressionServerPart *sock=param[0].DynamicCastGetPtr<SExpressionServerPart>();
        if(sock==0)
                throw IntelibX("it's not a SExpressionServerPart in curuserslist");
        MakeSocket *arg=sock->GetSock();
        return arg->arrayofusers();
}

//this function makes an object StreamWork for user
//1 param - int - users discriptor
SReference maketurnforuser(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionStreamWork *res=new SExpressionStreamWork(param[0]);
	return res;
}

ClientCon::ClientCon(int p, const char *s){
	port=p;
	ipadr=s;
	turner=new StreamWork();
	sost=waitforcon;
	client_information=SListConstructor();
}

void ClientCon::changeturner(){
	delete turner;
	turner=new StreamWork();
	DEPRF3(("NEW TREE!!!!\n"));
	DEPRF3(("%s\n", turner->gettree()->TextRepresentation().c_str()));
}


//here we will add a cons in client's information
void ClientCon::add_information(const char* key, SReference data){
	SExpressionString *k=new SExpressionString(key);
	SReference pair=k;
	SReference p=pair.MakeCons(data);
	client_information=client_information||(L|p);
	DEPRF3(("add_information %s\n", client_information->TextRepresentation().c_str()));
}

//here we will inseart client's data
//if data isn't correct - it will become nill
void ClientCon::reset_information(SReference data){
	client_information=data;
	while(!data.IsEmptyList()){
		SReference head=data.Car().Car();
		SExpressionString *str=head.DynamicCastGetPtr<SExpressionString>();
		if(str==NULL)
		{
			client_information=SListConstructor();
			DEPRF3(("reset_information %s\n", client_information->TextRepresentation().c_str()));
			return;
		}
		data=data.Cdr();
	}
	DEPRF3(("reset_information %s\n", client_information->TextRepresentation().c_str()));
}

//this function will rebuild cons with given key
bool ClientCon::change_information(const char* key, SReference data){
	SReference rebuild=SListConstructor();
	DEPRF3(("change_information %s\n", data->TextRepresentation().c_str()));
	while(!client_information.IsEmptyList()){
		printf("change_information\n");
		SReference head=client_information.Car().Car();
		SExpressionString* str=head.DynamicCastGetPtr<SExpressionString>();
		if(str==NULL)
			throw IntelibX("Error!:bad information in ClientCon\n");
		if(0==strcmp(str->GetValue() ,key)){
			SReference pair=str;
			SReference p=pair.MakeCons(data);
			rebuild=rebuild||(L|p);
			rebuild=rebuild||client_information.Cdr();
			client_information=rebuild;
			DEPRF3(("change_information sec  %s\n", client_information->TextRepresentation().c_str()));
			return true;
		}
		rebuild=rebuild||(L|client_information.Car());
		client_information=client_information.Cdr();
	}
	client_information=rebuild;
	SExpressionString *k=new SExpressionString(key);
	SReference pair=k;
	SReference p=pair.MakeCons(data);
	client_information=client_information||(L|p);
	DEPRF3(("change_information %s\n", client_information->TextRepresentation().c_str()));
	return true;
}

//here we will find information according to given tag
SReference ClientCon::get_information(const char* key){
	DEPRF3(("get_information %s\n", client_information->TextRepresentation().c_str()));
	SReference find=client_information;
	while(!find.IsEmptyList()){
		DEPRF3(("get_information\n"));
		SReference head=find.Car().Car();
                SExpressionString* str=head.DynamicCastGetPtr<SExpressionString>();
                if(str==NULL)
                        throw IntelibX("Error!:bad information in ClientCon\n");
		if(0==strcmp(str->GetValue(), key))
		{
			DEPRF3(("get_information %s\n", find.Car().Cdr()->TextRepresentation().c_str()));
			return find.Car().Cdr();
		}
		find=find.Cdr();
	}
	DEPRF3(("get_infromation empty\n"));
	return *PTheEmptyList;
}

void ClientCon::connecttoserv(){
	struct sockaddr_in addr;
	DEPRF3(("connecttoserv\n"));
        if((fsoc=socket(AF_INET, SOCK_STREAM, 0))==-1){
                throw ServPartErrors("socket");
        }
        DEPRF3(("connecttoserv fsoc=%d\n", fsoc));
        addr.sin_family=AF_INET;
        addr.sin_port=htons(port);
        if(!inet_aton(ipadr, &(addr.sin_addr))){
                throw ServPartErrors("inet_aton");
        }
	DEPRF3(("try to connect\n"));
        if(0!=connect(fsoc, (struct sockaddr *)&addr, sizeof(addr))){
                DEPRF3(("errors int connection\n"));
		throw ServPartErrors("connect");
        }
	DEPRF3(("yeah! we have connection\n"));
}

void ClientCon::selectcycle(){
	FD_ZERO(&mas);
	FD_SET(fsoc, &mas);
	int mistake=select(fsoc+1, &mas, NULL, NULL, NULL);
	if(mistake<1){
		throw ServPartErrors("select");
	}
}

int ClientCon::servhavetosay(){
	if(FD_ISSET(fsoc, &mas)){
		return getmessage();
	}
	return 1;
}

int ClientCon::getmessage(){
        DEPRF3(("we are in getmessage\n"));
        int get=read(fsoc, buf, sizeof(buf)-1);
        if(get==0){
                DEPRF3(("we lost connection with serv\n"));
		//shutdown(fsoc, 2);
                //close(fsoc);
                buf[0]=EOF;
                get=1;
                return -1;
        }
        buf[get]='\0';
        DEPRF3(("get from server [%s]\n", buf));
	turner->makequque(buf, 128);
        turner->work();
        SReference tree=turner->gettree();
        DEPRF3(("during tree is %s\n", tree->TextRepresentation().c_str()));
        return 1;
}

void ClientCon::sendmessage(const char *str){
	int k=0;
	while(str[k++]!='\0'){}
	k--;
	DEPRF(("want to send message\n"));
	int i=write(fsoc, str, k);
	DEPRF3(("in sendmessage. message is sended %d\n", i));
}

ClientCon::~ClientCon(){
	delete turner;
}

IntelibTypeId SExpressionClientPart::TypeId(&SExpression::TypeId);

SString SExpressionClientPart::TextRepresentation() const{
        return SString("#<CLIENT PART");
}

SExpressionClientPart::SExpressionClientPart(int x, const char *s):SExpression(TypeId){
	client=new ClientCon(x, s);
}

SExpressionClientPart::~SExpressionClientPart(){
        delete client;
}

SReference make_client(int params, const SReference *param){
	ASSERT_PARAMC(2);
	SExpressionInt *p=param[0].DynamicCastGetPtr<SExpressionInt>();
	if(p==0)
		throw IntelibX("it's not an SExpressionInt in make_client");
	SExpressionString *str=param[1].DynamicCastGetPtr<SExpressionString>();
	if(str==0)
		throw IntelibX("it's not an SExpressionString int make_client");
	SExpressionClientPart *res=new SExpressionClientPart(p->GetValue(), str->GetValue());
	return res;
}

SReference get_connection(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionClientPart *cl=param[0].DynamicCastGetPtr<SExpressionClientPart>();
	if(cl==0)
		throw IntelibX("it's not an SExpressionClientPart in get_connection");
	cl->GetClient()->connecttoserv();
	return cl;
}

SReference send_message(int params, const SReference *param){
	ASSERT_PARAMC(2);
	SExpressionClientPart *cl=param[0].DynamicCastGetPtr<SExpressionClientPart>();
	if(cl==0)
		throw IntelibX("it's not an SExpressionClientPart in send_message");
	SExpressionString *str=param[1].DynamicCastGetPtr<SExpressionString>();
	if(str==0)
		throw IntelibX("it's not an SExpressionString in send_message");
	DEPRF3(("is sended string [%s]\n", str->GetValue()));
	cl->GetClient()->sendmessage(str->GetValue());
	return cl;
}

SReference make_select(int params, const SReference *param){
	ASSERT_PARAMC(1)
	SExpressionClientPart *cl=param[0].DynamicCastGetPtr<SExpressionClientPart>();
	if(cl==0)
		throw IntelibX("it's not an SExpressionClientPart in make_select");
	cl->GetClient()->selectcycle();
	return cl;
}

SReference read_messages(int params, const SReference *param){
        ASSERT_PARAMC(1);
        SExpressionClientPart *cl=param[0].DynamicCastGetPtr<SExpressionClientPart>();
	if(cl==0)
		throw IntelibX("it's not an SExpressionClientPart in read_messages");
	int res=cl->GetClient()->servhavetosay();
	return (L|res, cl);
}

SReference get_tree(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionClientPart *cl=param[0].DynamicCastGetPtr<SExpressionClientPart>();
	if(cl==0)
		throw IntelibX("it's not an SExpressionClientPart in get_tree");
	StreamWork *res=cl->GetClient()->getturner();
	return res->gettree();
}

SReference delete_tree(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionClientPart *cl=param[0].DynamicCastGetPtr<SExpressionClientPart>();
	if(cl==0)
		throw IntelibX("it's not an SExpressionClientPart in delete_tree");
	StreamWork *res=cl->GetClient()->getturner();
	res->setnulltree();
	return cl;
}

SReference get_sost_client(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionClientPart *cl=param[0].DynamicCastGetPtr<SExpressionClientPart>();
        if(cl==0)
                throw IntelibX("it's not an SExpressionClientPart in get_sost_client");
	clientsost sost=cl->GetClient()->getsost();
	switch (sost){
		case mainwork: return "mainwork";
		case waitforcon: return "waitforcon";
		case firstack: return "firstack";
		case authoris: return "authoris";
		case givepas: return "givepas";
		case allright: return "allrigth";
		case secondack: return "secondack";
		case askresource: return "askresource";
		case statussended: return "statussended";
		case getlist: return "getlist";
		case myimagin: return "myimagin";
	};
	return "myimagin";
}

SReference change_sost(int params, const SReference *param){
	ASSERT_PARAMC(2);
        SExpressionClientPart *cl=param[1].DynamicCastGetPtr<SExpressionClientPart>();
        if(cl==0)
                throw IntelibX("it's not an SExpressionClientPart in change_sost");
	SExpressionInt *numb=param[0].DynamicCastGetPtr<SExpressionInt>();
	if(numb==0)
		throw IntelibX("it's not an SExpressionInt in change_sost");
	int i=numb->GetValue();
	switch(i){
		case 0:
			cl->GetClient()->changesost(waitforcon);
			break;
		case 1:
			cl->GetClient()->changesost(firstack);
			break;
		case 2:
			cl->GetClient()->changesost(authoris);
			break;
		case 3:
			cl->GetClient()->changesost(givepas);
			break;
		case 4:
			cl->GetClient()->changesost(allright);
			break;
		case 5:
			cl->GetClient()->changesost(secondack);
			break;
		case 6:
			cl->GetClient()->changesost(askresource);
			break;
		case 7:
			cl->GetClient()->changesost(statussended);
			break;
		case 8:
			cl->GetClient()->changesost(getlist);
			break;
		case 9:
			cl->GetClient()->changesost(mainwork);
			break;
		default:
			cl->GetClient()->changesost(myimagin);
	};
	return cl;
}

//this function changes current tree in xml-turner
//1 param - client information
//2 param - new tree
//it's very dangerous function
SReference new_tree(int params, const SReference *param){
	ASSERT_PARAMC(2);
        SExpressionClientPart *cl=param[0].DynamicCastGetPtr<SExpressionClientPart>();
        if(cl==0)
                throw IntelibX("it's not an SExpressionClientPart in new_tree");
	if(param[1].IsEmptyList()){
		DEPRF3(("it's null tree\n"));
		cl->GetClient()->changeturner();
	}
	else
		cl->GetClient()->getturner()->newtree(param[1]);
	return cl;
}

SReference get_chipher(int params, const SReference *param){
	DEPRF3(("get_chiper %s\n", param[0]->TextRepresentation().c_str()));
	ASSERT_PARAMC(6);
	SExpressionString *str=param[0].DynamicCastGetPtr<SExpressionString>();
	if(str==0)
		throw IntelibX("it's not a string in get_chipher");
	char chiphr[80];
	const char *str1=str->GetValue();
	for(int i=0; i<80; i++)
		chiphr[i]=str1[i];
	char out[300];
	char nonce[32];
	DEPRF3(("start decode\n"));
	CodeDecode *decode=new CodeDecode();
	int get=decode->base64_decode(out, chiphr, 80);
	decode->copystr(out);
	DEPRF3(("decoded %d\n", get));
	out[61]='\0';
	DEPRF3(("out is %s\n", out));
	char *find=strstr(out, "nonce");
	int index=7;
	while(find[index]<='9' && find[index]>='0'){
		nonce[index-7]=find[index];
		index++;
	}
	nonce[index-7]='\0';
	SExpressionString *str11=param[1].DynamicCastGetPtr<SExpressionString>();
	SExpressionString *str2=param[2].DynamicCastGetPtr<SExpressionString>();
	SExpressionString *str3=param[3].DynamicCastGetPtr<SExpressionString>();
	SExpressionString *str4=param[4].DynamicCastGetPtr<SExpressionString>();
	SExpressionString *str5=param[5].DynamicCastGetPtr<SExpressionString>();
	if(str11==0 || str2==0 || str3==0 || str4==0 || str5==0)
		throw IntelibX("it's not a string in get_chipher between params");
	decode->formtheanswer(str11->GetValue(), str2->GetValue(), nonce,
		str3->GetValue(), str4->GetValue(), str5->GetValue());
	char *res=decode->getcode();
	int f=0;
	//DEPRF3(("res %s\n", res));
	while(res[f]!='\0')f++;
	decode->base64_encode(out, res, f++);
	//DEPRF3(("returned code [%s]\n", out));
	char myans[1024];
	char frs[]="<response xmlns='urn:ietf:params:xml:ns:xmpp-sasl'>";
	char sec[]="</response>";
	int i=0;
	while(frs[i]!='\0'){
		myans[i]=frs[i];
		i++;
	}
	int k=0;
	while(out[k]!='\0'){	
		myans[i]=out[k];
		k++;
		i++;
	}
	k=0;
	while(sec[k]!='\0'){
		myans[i]=sec[k];
		k++;
		i++;
	}
	myans[i]='\0';
	DEPRF3(("our information [%s]\n", myans));
	return myans;
}

void CodeDecode::copystr(const char *str1){
	strcpy(out, str1);
}

unsigned int CodeDecode::base64_decode(char *out, char *data, unsigned int len) {
	/* три "декодированных" байта. и два индекса. почему два? смотри функцию base64_encode */
	char byte1,byte2,byte3,index2,index3;
	/* сколько декодировали.... пока ноль ) */
	unsigned int count = 0;
	for(;len--;data+=4) { /* шагаем по 4 байта*/
		count+=3;
		/*превращаем первый байт в старшие 6 бит настоящего первого байта*/
		byte1 = (index2byte(*data) << 2) & 0xFF;
		index2 = index2byte(*(data+1));
		/*и дополняем "натурала" двумя старшими битами следующего индекса*/
		byte1 |= index2 >> 4;
		if(*(data+2) != '=') { /*нода завершается знаком '='
			В данном случае "здесь" закодирован один байт*/
			byte2 = (index2 & 0xF) << 4;
			/* надоело печатать одно и тоже, дальше идет тоже, что и в предыдущем шаге и наоборот функции base64_encode*/
			index3 = index2byte(*(data+2));
			byte2 |= index3 >> 2;
			if(*(data+3) != '=') {
				byte3 = (index3 << 6) & 0xFF;
				byte3 |= index2byte(*(data+3));
			} else {count--;byte3=0;len=0;}
		} else {count-=2;byte2=byte3=0;len=0;}
		*out++ = byte1;
		*out++ = byte2;
		*out++ = byte3;
	}
	return count;
}

void CodeDecode::base64_encode(char *out, char *data, unsigned int len) {
	/*набор символов, входящих в base64*/
	char *base64_set = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	/*второй и третий индексы. первого и последнего нет, потому что для их получения достаточно одной операциии,
	а для этих - более одной*/
	unsigned char index2,index3;
	int i=0;
	for(; i<len; (i+=3,data+=3,out+=4)) { /*шагаем по три байта в данных и 4 байта в закодированных данных*/
		/*пишем на выход символ, соответствующий индексу из 6 старших бит первого байта*/
		*out = base64_set[*data >> 2];
		/*инициализируем второй индекс. старшие биты 00, далее 2 младших бита первого байта и ....[1]*/
		index2 = (*data << 4) & 0x30;
		if(i+2<len) {
			/* ....[1]... устанавливаем младшие 4 бита соответственно 4 старшим битам второго байта*/
			index2 |= *(data+1) >> 4;
			/* затем младшие 4 бита второго байта устанавливаем старшими (отступив 2, ведь число 6-битное :)
			в третьем индексе и ...[2]*/
			index3 = ((*(data+1) << 4) & 0xFF) >> 2;
			if(i+3<len) {
				/* ....[2]..... и дополняем его двумя старшими битами третьего числа в качестве младших*/
				index3 |= *(data+2) >> 6;
				/* оставшиеся 6 младших бит записываем сразу в поток */
				*(out + 3) = base64_set[*(data+2) & 0x3F];
			} else {
				/* ....[2]..... и оставляем его неизменным. А в четвертый байт base64-ноды ставим знак '=',
				что значит: "обработать эту ноду как два байта, остальное в игнор" */
				*(out + 3) = '=';
			}
			*(out + 2) = base64_set[index3];
		} else { /*...[1]... и все. строка закончилась. ставим последние два байта ==, что значит
				"в этой 4-х байтовой base64-ноде недостает 2 символов, обработать как один байт,
				остальное игнорировать"*/
			*(out + 2) = '=';
			*(out + 3) = '=';
		}
		*(out + 1) = base64_set[index2];
	}
	*out = 0;
}


char CodeDecode::index2byte(char index){
	if(index >= 0x41 && index <= 0x5A) return index-0x41;
	if(index >= 0x61 && index <= 0x7A) return index-0x61+26;
	if(index >= 0x30 && index <= 0x39) return index-0x30+52;
	if(index == 0x2B) return 62;
	if(index == 0x2F) return 63;
	return 0;
}

void CodeDecode::formtheanswer(const char *usern, const char *servn, const char *non,
		const char *cnon, const char *digest, const char *password){
	const char username[]="username=";
	const char realm[]=",realm=";
	const char nonce[]=",nonce=";
	const char cav[]="\"";
	const char cnonce[]=",cnonce=";
	const char nc[]=",nc=00000001,qop=auth,digest-uri=";
	const char responce[]=",charset=utf-8,response=";
	const char dvoet[]=":";
	const char autent[]="AUTHENTICATE:";
	char name_pas[256];
	char author[128];
	int pos=0;
	int pos1=0;
	int pos2=0;
	pos=copystrmy(code, username, pos);
	pos=copystrmy(code, cav, pos);
	pos=copystrmy(code, usern, pos);
	pos=copystrmy(code, cav, pos);
	pos=copystrmy(code, realm, pos);
	pos=copystrmy(code, cav, pos);
	pos=copystrmy(code, servn, pos);
	pos=copystrmy(code, cav, pos);
	pos=copystrmy(code, nonce, pos);
	pos=copystrmy(code, cav, pos);
	pos=copystrmy(code, non, pos);
	pos=copystrmy(code, cav, pos);
	pos=copystrmy(code, cnonce, pos);
	pos=copystrmy(code, cav, pos);
	pos=copystrmy(code, cnon, pos);
	pos=copystrmy(code, cav, pos);
	pos=copystrmy(code, nc, pos);
	pos=copystrmy(code, cav, pos);
	pos=copystrmy(code, digest, pos);
	pos=copystrmy(code, cav, pos);
	pos=copystrmy(code, responce, pos);
	code[pos]='\0';
	pos1=copystrmy(name_pas, usern, pos1);
	pos1=copystrmy(name_pas, dvoet, pos1);
	pos1=copystrmy(name_pas, servn, pos1);
	pos1=copystrmy(name_pas, dvoet, pos1);
	pos1=copystrmy(name_pas, password, pos1);
	name_pas[pos1]='\0';
	pos2=copystrmy(author, autent, pos2);
	pos2=copystrmy(author, digest, pos2);
	author[pos2]='\0';
	DEPRF3(("here is first part in forming answer %s\n", code));
	foranswer(name_pas, non, cnon, author, pos);
}


//this function is for copy one line to another
//where is line (may be) with some information
int CodeDecode::copystrmy(char *where, const char *from, int pos){
	int i=0;
	while(from[i]!='\0'){
		where[pos]=from[i];
		pos++;
		i++;
	}
	return pos;
}

void CodeDecode::foranswer(const char *str1, const char *str3, const char *str4, const char *str5, int i){
	//str1 name:serv:pass
	//str3 nonce
	//str4 cnonce
	//str5 AUTHENYICATE:dighest-uri
	MD5Context md5handler;
	unsigned char md5digest[MD5_DIGEST_SIZE];
	unsigned char digestha1[MD5_DIGEST_SIZE];
	unsigned char digestha2[MD5_DIGEST_SIZE];
	unsigned char digestresponse[MD5_DIGEST_SIZE];
	int j=0;
	DEPRF3(("str1 %s\n", str1));
	DEPRF3(("str3 %s\n", str3));
	DEPRF3(("str4 %s\n", str4));
	DEPRF3(("str5 %s\n", str5));
	char str6[]="auth";//qop
	char str8[]="00000001";//nc
	while(str1[j]!='\0') j++;
	MD5Init(&md5handler);
	MD5Update(&md5handler, str1, j);
	MD5Final(md5digest,&md5handler);
	char ch[2];
	j=0;
	while(str3[j]!='\0')j++;
	MD5Init(&md5handler);
	MD5Update(&md5handler, md5digest, 16);
	MD5Update(&md5handler, ":", 1);
	int j3=j;
	MD5Update(&md5handler, str3, j);
        MD5Update(&md5handler, ":", 1);
	j=0;
        while(str4[j]!='\0')j++;
	int j4=j;
        MD5Update(&md5handler, str4, j);
        MD5Final(digestha1, &md5handler);
	//here we finish to do has1
	j=0;
        while(str5[j]!='\0')j++;
        MD5Init(&md5handler);
        MD5Update(&md5handler, str5, j);
	MD5Final(digestha2, &md5handler);
	//here we end to do has2
	char has1[32];
	char has2[32];
	int index=0;
	for(j=0; j<16; j++){
                sprintf(ch, "%02x", digestha1[j]);
                has1[index]=ch[0];
		has1[index+1]=ch[1];
		sprintf(ch, "%02x", digestha2[j]);
		has2[index]=ch[0];
		has2[index+1]=ch[1];
		index+=2;
	}
	MD5Init(&md5handler);
	MD5Update(&md5handler, has1, 32);
	MD5Update(&md5handler, ":", 1);
	MD5Update(&md5handler, str3, j3);
	MD5Update(&md5handler, ":", 1);
	MD5Update(&md5handler, str8, 8);
	MD5Update(&md5handler, ":", 1);
	j=0;
        while(str6[j]!='\0')j++;
        MD5Update(&md5handler, str4, j4);
	MD5Update(&md5handler, ":", 1);
	MD5Update(&md5handler, str6, j);
	MD5Update(&md5handler, ":", 1);
        MD5Update(&md5handler, has2, 32);
	MD5Final(digestresponse, &md5handler);
	DEPRF3(("i don't now what is this\n"));
	for (int j=0;j<MD5_DIGEST_SIZE;j++) {
		DEPRF3(("%02x",digestresponse[j]));
	}
	DEPRF3(("\n"));
	for(int j=0; j<16; j++){
		sprintf(ch, "%02x", digestresponse[j]);
		code[i]=ch[0];
		i++;
		code[i]=ch[1];
		i++;
	}
	code[i]='\0';
	DEPRF3(("our code %s\n", code));
}

//this function will add information is client's list
SReference add_clients_information(int params, const SReference *param){
	ASSERT_PARAMC(3);
	SExpressionClientPart *cl=param[0].DynamicCastGetPtr<SExpressionClientPart>();
	if(cl==0)
		throw IntelibX("it's not client in add_clients_information");
	SExpressionString *str=param[1].DynamicCastGetPtr<SExpressionString>();
	if(str==0)
		throw IntelibX("it's not string in add_clients_information");

	cl->GetClient()->add_information(str->GetValue(), param[2]);	
	return true;
}

//this function will reset client's infromation
SReference reset_clients_information(int params, const SReference *param){
	ASSERT_PARAMC(2);
	SExpressionClientPart *cl=param[0].DynamicCastGetPtr<SExpressionClientPart>();
	if(cl==0)
		throw IntelibX("it's not client in reset_clients_information");
	cl->GetClient()->reset_information(param[1]);
	return true;
}

//this function will change clients information
SReference change_clients_information(int params, const SReference *param){
	ASSERT_PARAMC(3);
	SExpressionClientPart *cl=param[0].DynamicCastGetPtr<SExpressionClientPart>();
        if(cl==0)
                throw IntelibX("it's not client in change_clients_information");
        SExpressionString *str=param[1].DynamicCastGetPtr<SExpressionString>();
        if(str==0)
                throw IntelibX("it's not string in change_clients_information");

	cl->GetClient()->change_information(str->GetValue(), param[2]);
	return true;
}

//this function will return part of client information
SReference get_clients_information(int params, const SReference *param){
	ASSERT_PARAMC(2);
	SExpressionClientPart *cl=param[0].DynamicCastGetPtr<SExpressionClientPart>();
        if(cl==0)
                throw IntelibX("it's not client in change_clients_information");
	SExpressionString *str=param[1].DynamicCastGetPtr<SExpressionString>();
        if(str==0)
                throw IntelibX("it's not string in change_clients_information");
	return cl->GetClient()->get_information(str->GetValue());
}

void define_serv_functions(){
	MAKELISTEN->SetFunction(openlisten);
	CREATESERVSTRUCT->SetFunction(make_serv_sock);
	ISWORK->SetFunction(isworkcycle);
	PREPEARFORMS->SetFunction(readyformessege);
	USERHASTOSAY->SetFunction(havetosay);
	GETMESSAGES->SetFunction(getmesfrompol);
	HAVEAPPLICATIONS->SetFunction(mustaccept);
	ACCEPTUSERS->SetFunction(acceptusers);
	GETREADYFORNEXT->SetFunction(stopfalse);
	ARRAYOFUSERS->SetFunction(curuserslist);
	GETTURNER->SetFunction(maketurnforuser);
}

void define_client_functions(){
	MAKECLIENT->SetFunction(make_client);
	GETCONNECTION->SetFunction(get_connection);
	READMESSAGE->SetFunction(read_messages);
	MAKESELECT->SetFunction(make_select);
	SENDMESSAGE->SetFunction(send_message);
	GETDURTREE->SetFunction(get_tree);
	DELETETREE->SetFunction(delete_tree);
	NEWTREE->SetFunction(new_tree);
	GETCLCOND->SetFunction(get_sost_client);
	CHANGECLCOND->SetFunction(change_sost);
	GETCHIPHER->SetFunction(get_chipher);
	ADDCLINFORM->SetFunction(add_clients_information);
	CHANGECLINFORM->SetFunction(change_clients_information);
	GETCLINFORM->SetFunction(get_clients_information);
	RESETCLINFORM->SetFunction(reset_clients_information);
}
