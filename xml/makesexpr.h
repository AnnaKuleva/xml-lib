#ifndef _MAKESEXPRESSION_H_KURS3_
#define _MAKESEXPRESSION_H_KUSR3_

#include <intelib/sexpress/sexpress.hpp>
#include <intelib/sexpress/sstring.hpp>
#include <intelib/tools/sstream.hpp>
#include <intelib/lisp/lsymbol.hpp>
#include "/Users/mac/mylib/mylist.h"
#include "structures.h"
#include "lexan.h"
#include "servpart.h"
#include "../dtd/turndtdins.h"
#include "xmlsexpress.h"
#include "../dtd/dtdlib.h"

class MakeXmlSExpression{
	enum durlex{
                opentag,
                closetag,
                tagcontent,
                attrname,
                attrcontent,
                smthelse,
		prolog,
		doctype,
		end
        };
	durlex condition;
	lexem* wait;
	lexem* save;
	Automat avt;
	SReference tree;
	bool nonstop;
	SReference dtddoc;
	SReference turn();
	lexem *lexemanaliz();
	virtual char getsimbol()=0;
	void getprolog();
	void getdoctype();
	bool dtdvsxml(SReference xml);
	MakeXmlSExpression(MakeXmlSExpression &a):avt(false){}
public:
	MakeXmlSExpression(bool type);
	virtual ~MakeXmlSExpression();
	void insertnewlex(lexem* );
	void deletelex();
	void changecondition();
	SReference start(bool flag=false);
};

class XmlDocument:public MakeXmlSExpression{
	SExpressionStreamFile *myfile;
	virtual char getsimbol(){return myfile->Getc();}
public:
	XmlDocument(bool type, FILE *f):MakeXmlSExpression(type){
		myfile=new SExpressionStreamFile(f);
	}
	virtual ~XmlDocument(){myfile->Close();}
};

class XmlStream:public MakeXmlSExpression{
	ServPart serv;
	virtual char getsimbol();
public:
	XmlStream(bool type, char *s);
	int getport(){return serv.getport();}
	int getfsoc(){return serv.getfsoc();}
	virtual ~XmlStream(){};
};

class StreamWork{
	Automat avt;
	bool letterswork;
	streamcondition condition;
	struct letter *virtbuf;
	struct letter *save;
	SExpressionXmlAttr *saveattr;
	SReference tree;
	SReference savetag;
	void cleansave();
	bool isbuild(SReference r);
	SReference getlastunbuild(SReference search);
	void insertnewtag(struct lexem *get);
	void insertnewattr(struct lexem *get);
	void insertcontent(struct lexem *get);
	void closetag(struct lexem *get);
	void buildattr(struct lexem *get);
	void changebuild(SExpressionXmlTag *xml);
	void operationtype(struct lexem *get);
	void workprolog(struct lexem *get);
	void workwithprattr(struct lexem *get);
	void workwithprcon(struct lexem *get);
	void workend(struct lexem *get);
	void insertletter(char c);
	char getsimbol();
	lexem *lexemanaliz();
	void readyforbuilding(struct lexem *get);
public:
	StreamWork();
	void makequque(const char *s, int length);
	void work();
	void setnulltree();
	SReference gettree();
	void newtree(SReference t){tree=t;}
	void changecondition(streamcondition s);
	~StreamWork();
};	
#endif
