#ifndef _PARSTAG_H_KURS_3_
#define _PARSETAG_H_KURS_3_

#include "xmlsexpress.h"
#include <intelib/sexpress/sexpress.hpp>
#include <intelib/genlisp/lispform.hpp>
#include <intelib/lisp/lisp.hpp>
#include <intelib/lisp/lsymbol.hpp>

class ParsString{
	SString str;
	SReference tail;
	//this function will search for close part of tag
	SString getname(int i);
	//this function will search for close tag
	SReference littlestr(SString str1, SString str2);
	//when we find an open tag - we must find close and collect content
	SReference getcontent(SString find, SReference save);
	SString turnsave(SString str1, int k);
	SReference evetywithtail(SReference get, SExpressionXmlTag *xml);
	SReference noentity();
	SReference findevery(int i, SReference get, SExpressionXmlTag *xml);
	SReference didnfindcltag(int i);
	SReference didnfindclpart(int i);
	SReference findattr(SString line);
	SReference getattrs(SReference list);
	SExpressionXmlAttr *divfortwo(SString s);
public:
	ParsString(SExpressionString *s, SReference t);
	//workfunction
	SReference work();
};

#endif
