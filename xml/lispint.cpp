#include "lispint.h"
#include <intelib/sexpress/sstring.hpp>
#include <intelib/tools/sstream.hpp>
#include "xmlsexpress.h"
#include "structures.h"
#include <string.h>
#include "makesexpr.h"
#include "mistake.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include "declare_functions.h"

static LListConstructor L;

#define ASSERT_PARAMC(n) \
    INTELIB_ASSERT(params <= n, IntelibX_too_many_params(params));\
    INTELIB_ASSERT(params >= n, IntelibX_too_few_params(params));

//_______________________________________
//___________printf_functions____________|
//_______________________________________|

//this function prints xml-tag
//1 param - xml-tag
//2 param - depth
SReference print_beauty(int params, const SReference *param){
	ASSERT_PARAMC(2);
	SExpressionInt *d=param[1].DynamicCastGetPtr<SExpressionInt>();
	if(d==NULL)
		throw IntelibX("it's not an integer in printbeauty\n");
	int depth=d->GetValue();
	for(int i=0; i<depth; i++)
		printf("  ");
	SExpressionString *str=param[0].DynamicCastGetPtr<SExpressionString>();
	if(str!=NULL){
		printf("%s\n", str->GetValue());
		return depth;
	}
	SExpressionXmlTag *pr=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
	if(pr==NULL)
		throw IntelibX("it's not an xml-tag/string in printbeauty\n");
	SExpressionXmlTag *x;
	x=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
	if(x==NULL){
		if(!param[0].IsEmptyList()){
			printf("%s\n", param[0]->TextRepresentation().c_str());
		}
		return depth;
	}
	printf("name=%s\n", x->GetName());
	for(int i=0; i<depth+1; i++)
		printf("  ");
	printf("attributes=%s\n", x->GetAttr()->TextRepresentation().c_str());
	for(int i=0; i<depth+1; i++)
		printf("  ");
	printf("content=\n");
	return depth;
}


//this function prints SReference
//1 param - SReference which will be printed
SReference printsmth(int params, const SReference *param){
	ASSERT_PARAMC(1);
	printf("%s\n", param[0]->TextRepresentation().c_str());
	return param[0];
}

//this function prints string in user's file
//1 param - string which will be printed
//2 param - filestream
//3 param - depth on which string must be printed
SReference printstrinusersfile(int params, const SReference *param){
	DEPRF3(("in create_pr_str_file\n"));
	ASSERT_PARAMC(3);
	SExpressionInt *x=param[2].DynamicCastGetPtr<SExpressionInt>();
	if(x==0)
		throw IntelibX("it's not int in create_pr_str_file\n");
	SExpressionStreamFile *f=param[1].DynamicCastGetPtr<SExpressionStreamFile>();
	if(f==0)
		throw IntelibX("it's not a filestream in PrintStrInFile::printstrinfile\n");
	SExpressionString *s=param[0].DynamicCastGetPtr<SExpressionString>();
	if(s==0)
		throw IntelibX("it's not a string in PrintStrInFile::printstrinfile\n");
	int depth=x->GetValue();
	for(int i=0; i<depth; i++)
		f->Puts("  ");
	f->Puts(s->GetValue());
	f->Putc('\n');
	return *PTheEmptyList;
}

//this function prints in user's file tag's open-part
//1 param - xml tag
//2 param - filestream
//3 param - it is depth on which this tag must be printed
//return value - attributes for xml-tag
SReference printopenusersfile(int params, const SReference *param){
	DEPRF3(("printopenusersfile\n"));
	ASSERT_PARAMC(3);
        SExpressionStreamFile *f=param[1].DynamicCastGetPtr<SExpressionStreamFile>();
	if(f==0)
		throw IntelibX("it's not a filestream in printopenfile\n");
	SExpressionXmlTag *x=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
	if(x==0)
		throw IntelibX("it's not a xmltag in printopenfile\n");
	SExpressionInt *d=param[2].DynamicCastGetPtr<SExpressionInt>();
	int depth=d->GetValue();
	for(int i=0; i<depth; i++)
		f->Puts("  ");
	f->Putc('<');
	f->Puts(x->GetName());
	f->Putc(' ');
	SReference attr=x->GetAttr();
	return attr;
}

//this function prints in user's file xml-attribute
//1 param - attribute
//2 param - filestream
SReference printattrusersfile(int params, const SReference *param){
    DEPRF3(("printattrusersfile\n"));
	ASSERT_PARAMC(2);
	SExpressionStreamFile *f=param[1].DynamicCastGetPtr<SExpressionStreamFile>();
	if(f==0)
		throw IntelibX("it's not a filestream in printattrfile\n");
	if(param[0].IsEmptyList()){
		f->Puts(">\n");
		return *PTheEmptyList;
	}
	SExpressionXmlAttr *x=param[0].DynamicCastGetPtr<SExpressionXmlAttr>();
	if(x==0)
		throw IntelibX("it's not a xml attribute in printattrfile\n");
	f->Puts(x->GetName());
	f->Putc('=');
	f->Putc('"');
	f->Puts(x->GetContent());
	f->Putc('"');
	f->Putc(' ');
	return *PTheEmptyList;
}

//this function prints in file empty tag
//1 param - xml-tag
//2 param - file stream
//3 param - depth
SReference printemptytagfile(int params, const SReference *param){
        ASSERT_PARAMC(3);
	SExpressionStreamFile *f=param[1].DynamicCastGetPtr<SExpressionStreamFile>();
	if(f==0)
		throw IntelibX("it's not a filestream in printattrfile\n");
	SExpressionXmlTag *x=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
	if(x==0)
		throw IntelibX("it's not an xml tag in printempty\n");
	SExpressionInt *d=param[2].DynamicCastGetPtr<SExpressionInt>();
	if(d==NULL)
		throw IntelibX("it's not an integer in printempty\n");
	int depth=d->GetValue();
	for(int i=0; i<depth; i++)
        f->Puts("  ");
    f->Putc('<');
    f->Puts(x->GetName());
    SReference g=x->GetAttr();
    if(!g.IsEmptyList()){
            while(!g.IsEmptyList()){
                    f->Putc(' ');
                    SExpressionXmlAttr *attr=g.Car().DynamicCastGetPtr<SExpressionXmlAttr>();
                    g=g.Cdr();
                    f->Puts(attr->GetName());
                    f->Putc('=');
                    f->Putc('"');
                    f->Puts(attr->GetContent());
                    f->Putc('"');
            }
    }
    f->Putc('/');
    f->Putc('>');
    f->Putc('\n');
    return *PTheEmptyList;

}

//this function prints in file close tag
//1 param - xml-tag
//2 param - file stream
//3 param - depth
SReference printcloseusersfile(int params, const SReference *param){
    DEPRF3(("printcloseusersfile\n"));
	ASSERT_PARAMC(3);
	SExpressionStreamFile *f=param[1].DynamicCastGetPtr<SExpressionStreamFile>();
	if(f==0)
		throw IntelibX("it's not a filestream in PrintCloseTagFile::printclosefile\n");
	SExpressionXmlTag *x=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
	if(x==0)
		throw IntelibX("it's not a xmltag in PrintCloseTagFile::printclosefile\n");
	SExpressionInt *d=param[2].DynamicCastGetPtr<SExpressionInt>();
	int depth=d->GetValue();
	for(int i=0; i<depth; i++)
		f->Puts("  ");
	f->Puts("</");
	f->Puts(x->GetName());
	f->Puts(">\n");
	return *PTheEmptyList;
}

//_______________________________________
//__________xml-tag_functions____________|
//_______________________________________|

//this function creates new xml-tag
//1 param - string (it will be xml-tag's name)
SReference newxmltag(int params, const SReference *param){
    ASSERT_PARAMC(1);
    SExpressionString *s=param[0].DynamicCastGetPtr<SExpressionString>();
    if(s==0)
        throw IntelibX("it's not a string in newxmltag\n");
    SExpressionXmlTag *x=new SExpressionXmlTag(s->GetValue());
    DEPRF3(("|%s|\n", x->TextRepresentation().c_str()));
    return x;
}

//this function copies xml-tag, it returns new one
//1 param - xml-tag
SReference copy_xml_tag(int params, const SReference *param){
    ASSERT_PARAMC(1);
    SExpressionXmlTag *tag=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
    if(tag==0)
        throw IntelibX("it's not an xml-tag in copy_xml_tag\n");
    SExpressionXmlTag *res=new SExpressionXmlTag(tag);
    return res;
}

//this function insearts new element in xml-tag's content
//1 param - xml-tag
//2 param - insearted content
SReference insertcontent(int params, const SReference *param){
    ASSERT_PARAMC(2);
    SExpressionXmlTag *x=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
    if(x==0)
        throw IntelibX("it's not xml tag in insertcontent\n");
    if(param[1].DynamicCastGetPtr<SExpressionString>()!=0 ||
       param[1].DynamicCastGetPtr<SExpressionXmlTag>()!=0)
        x->InsertElem((L|param[1]));
    else
        x->InsertElem(param[1]);
    DEPRF3(("{%s}\n", x->TextRepresentation().c_str()));
    return x;
}

//this is predicate
//it returns true if parameter is xml-tag
//otherwise - returns true
SReference isxmlatom(int params, const SReference *param){
    ASSERT_PARAMC(1);
    SExpressionXmlTag *xml=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
    if(xml==0)
        return *PTheEmptyList;
    return true;
}

//this function returns xml-tag's name
//1 param - xml-tag
SReference get_xml_name(int params, const SReference *param){
    ASSERT_PARAMC(1);
    SExpressionXmlTag *xml=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
    if(xml==0)
        throw IntelibX("it's not an SExpressionXmlTag in getxmlname");
    const char *s=xml->GetName();
    SExpressionString *res=new SExpressionString(s);
    return res;
}

//this function get's xml-tag's content
//one param - xml-tag
SReference getcontent(int params, const SReference *param){
    ASSERT_PARAMC(1);
    SExpressionXmlTag *pr=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
    if(!pr){
        throw IntelibX("it is not SExpressionXmlTag get content");
    }
    SReference n=pr->GetContent();
    return n;
}

//this function inserts new content in xml-tag
//1 param - xml-tag
//2 param - SReference (inserted content)
SReference deletetagfrom(int params, const SReference *param){
    ASSERT_PARAMC(2);
    SExpressionXmlTag *x=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
    if(x==0)
        throw IntelibX("it's not xml tag in deletetagfrom\n");
    SReference r=param[1];
    if(r.DynamicCastGetPtr<SExpressionString>()!=0 ||
       r.DynamicCastGetPtr<SExpressionXmlTag>()!=0)
        x->NewContent((L|r));
    else
        x->NewContent(r);
    DEPRF3(("it's new content %s\n", r->TextRepresentation().c_str()));
    return x;
}

//________________________________________
//___________xml-attr_functions___________|
//________________________________________|

//this function creates new xml-attribute
//1 param - string (it will be name of attribute)
//2 param - string (it will be content of attribute)
SReference newxmlattr(int params, const SReference *param){
    ASSERT_PARAMC(2);
    SExpressionString *s=param[0].DynamicCastGetPtr<SExpressionString>();
    if(s==0)
        throw IntelibX("it's not a string in first parametr of mewxmlattr\n");
    SExpressionString *s1=param[1].DynamicCastGetPtr<SExpressionString>();
    if(s1==0)
        throw IntelibX("it's not a string in second parametr of mewxmlattr\n");
    SExpressionXmlAttr *x=new SExpressionXmlAttr(s->GetValue(), s1->GetValue());
    return x;
}

//this function creates new xml-attribute(copy function)
//1 param - xml-attr
SReference copy_xml_attr(int params, const SReference *param){
    ASSERT_PARAMC(1);
    SExpressionXmlAttr *attr=param[0].DynamicCastGetPtr<SExpressionXmlAttr>();
    if(attr==0)
        throw IntelibX("it's not am xml-attr in copy_xml_attr\n");
    SExpressionXmlAttr *a=new SExpressionXmlAttr(attr);
    return a;
}

//this function gets xml-tag's attributes
//1 param - xml-tag
SReference getxmltagattr(int params, const SReference *param){
    ASSERT_PARAMC(1);
    SExpressionXmlTag *xml=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
    if(xml==0)
        throw IntelibX("it's not an xml-tag in getxmltagattr\n");
    return xml->GetAttr();
}

//this function new inserts attributes in xml-tag
//1 param - xml-tag
//2 param - SReference (it's list of insearted attributes)
SReference newxmltagattr(int params, const SReference *param){
    ASSERT_PARAMC(2);
    SExpressionXmlTag *xml=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
    if(xml==0)
        throw IntelibX("it's not an xml-tag in newxmltagattr\n");
    xml->NewAttributes(param[1]);
    return xml;
}

//this function gets attribute's content
//1 param - xml-attribute
SReference getattrcont(int params, const SReference *param){
    ASSERT_PARAMC(1);
    SExpressionXmlAttr *attr=param[0].DynamicCastGetPtr<SExpressionXmlAttr>();
    if(attr==0)
        throw IntelibX("it's not an attr in getattrcont\n");
    SExpressionString *res=new SExpressionString(attr->GetContent());
    return res;
}

//this function inserts new content of xml-attribute
//1 param - xml-attribute
//2 param - string
SReference attrnewcont(int params, const SReference *param){
    ASSERT_PARAMC(2);
    SExpressionXmlAttr *attr=param[0].DynamicCastGetPtr<SExpressionXmlAttr>();
    if(attr==0)
        throw IntelibX("it's not an attr in attrnewcont\n");
    SExpressionString *str=param[1].DynamicCastGetPtr<SExpressionString>();
    if(str==0)
        throw IntelibX("it's not a string in attrnewcont\n");
    attr->NewCont(str->GetValue());
    return attr;
}

//this function returns xml-attribute's name
//1 param - xml-attribute
SReference get_attr_name(int params, const SReference *param){
    ASSERT_PARAMC(1);
    SExpressionXmlAttr *attr=param[0].DynamicCastGetPtr<SExpressionXmlAttr>();
    if(attr==0)
        throw IntelibX("is't not an SExpressionXmlAttr in get_attr_name");
    const char *s=attr->GetName();
    SExpressionString *res=new SExpressionString(s);
    return res;
}

//this function inserts attribute in xml tag
//1 param - xml-tag
//2 param - xmlattribute
SReference insertreadyattr(int params, const SReference *param){
    ASSERT_PARAMC(2);
    DEPRF3(("want to insert new attr\n"));
    SExpressionXmlTag *x=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
    if(x==0)
        throw IntelibX("it's not an xml-tag in insertreadyattr\n");
    SExpressionXmlAttr *y=param[1].DynamicCastGetPtr<SExpressionXmlAttr>();
    if(y==0)
        throw IntelibX("it's not an xml-attr in insertreadyattr\n");
    SReference r=SListConstructor();
    r=r||(L|y);
    DEPRF3(("insert attr\n"));
    x->InsertAttr(r);
    return x;
}

//________________________________________
//_____file-stream_&&_file_functions______|
//________________________________________|

//this funtion closes file's stream
//1 param - filestream
SReference close_file_stream(int params, const SReference *param){
    ASSERT_PARAMC(1);
    SExpressionStreamFile* file=param[0].DynamicCastGetPtr<SExpressionStreamFile>();
    if(file==NULL)
        throw IntelibX("Error!:it's not a file stream in close_file_stream");
    if(file->Close())
        return true;
    return *PTheEmptyList;
}

//this function creates new file stream
//1 param - string - name of file
//2 param - string - type of opening
//return value - created file stream
SReference getfilestream(int params, const SReference *param){
    ASSERT_PARAMC(2);
    SExpressionString *str=param[0].DynamicCastGetPtr<SExpressionString>();
    if(str==0){
        throw IntelibX("it's not a string in getfilestream()\n");
    }
    SExpressionString *type=param[1].DynamicCastGetPtr<SExpressionString>();
    FILE *myfile;
    if(0==strcmp(type->GetValue(), "w"))
        myfile=fopen(str->GetValue(), "w");
    else if (0==strcmp(type->GetValue(), "a"))
        myfile=fopen(str->GetValue(), "a");
    else if (0==strcmp(type->GetValue(), "r"))
        myfile=fopen(str->GetValue(), "r");
    else if (0==strcmp(type->GetValue(), "w+"))
        myfile=fopen(str->GetValue(), "w+");
    else if (0==strcmp(type->GetValue(), "a+"))
        myfile=fopen(str->GetValue(), "a+");
    else if (0==strcmp(type->GetValue(), "r+"))
        myfile=fopen(str->GetValue(), "r+");
    else
        return *PTheEmptyList;
    if(myfile == NULL){
        perror("fopen");
        return *PTheEmptyList;
    }
    SExpressionStreamFile *f=new SExpressionStreamFile(myfile);
    return f;
}

//this function returns list of files' names from directory
//names will be like this - name_dir/name_file
//1 param - name of directory
SReference getfilesdir(int params, const SReference *param){
    ASSERT_PARAMC(1);
    SExpressionString *str=param[0].DynamicCastGetPtr<SExpressionString>();
    if(str==0)
        throw IntelibX("it's not a string in getfilesdir\n");
    DIR *dir;
    struct dirent *entry;
    SString name=str;
    SReference res=SListConstructor();
    dir=opendir(str->GetValue());
    if(dir==NULL){
        perror("diropen");
        return res;
    }
    while((entry=readdir(dir))!=NULL){
        if(entry->d_name[0]!='.'){
            SString n1=name;
            n1+='/';
            n1+=entry->d_name;
            SExpressionString *n=new SExpressionString(n1.c_str());
            res=res||(L|n);
        }
    }
    DEPRF3(("list of files %s\n", res->TextRepresentation().c_str()));
    closedir(dir);
    return res;
}

//this function checks needed directory
//1 param - name of needed directory
//2 param - integer
//if 2 param == 1, function creates needed directory
//otherwise, function does nothing
//return - true if dir exists (or function creates it)
//otherwise it returns false
SReference exists_dir(int params, const SReference *param){
    ASSERT_PARAMC(2);
    SExpressionString *str=param[0].DynamicCastGetPtr<SExpressionString>();
    if(str==NULL)
        throw IntelibX("it's not a string in exists_dir\n");
    SExpressionInt *create=param[1].DynamicCastGetPtr<SExpressionInt>();
    if(create==NULL)
        throw IntelibX("it's not an integer in existr_dir\n");
    struct stat st;
    if(stat(str->GetValue(),&st) != 0)
        if(create->GetValue()!=1)
            return false;
        else
            mkdir(str->GetValue(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    return true;
}

//this function opens xml-file and returns value
//which will be formed from this file
//1 param - string - name of file
SReference openfilexml(int params, const SReference *param){
    ASSERT_PARAMC(1);
    SExpressionString *str=param[0].DynamicCastGetPtr<SExpressionString>();
    if(str==0){
        throw IntelibX("it's not a string in openfilexml()\n");
    }
    DEPRF3(("is's name of file [%s]\n", str->GetValue()));
    FILE *myfile;
    myfile=fopen(str->GetValue(), "r");
    SReference m=SListConstructor();
    if(myfile == NULL){
        perror("fopen");
        return m;
    }
    XmlDocument *cr=new XmlDocument(false, myfile);
    try{
        m=cr->start(true);
    }
    catch(TurnMistake &b){
        b.print();
    }
    catch(BadSimbol &b){
        b.print();
    }
    catch(ErrorInForming &b){
        b.print();
    }
    catch(EmptyListOfLex &b){
        b.print();
    }
    catch(Disbalance &b){
        b.print();
    }
    catch(IntelibX &x) {
        printf("\nCaught IntelibX: %s\n", x.Description() );
        if(x.Parameter().GetPtr()) {
            printf("%s\n", x.Parameter()->TextRepresentation().c_str());
        }
        if(x.Stack().GetPtr()) {
            printf("%s\n", x.Stack()->TextRepresentation().c_str());
        }
    }
    catch(const char *s){
        printf("%s\n", s);
    }
    
    catch(...){
        printf("some errors with converting xml-file into SReference\n");
    }
    delete cr;
    return m;
}

//_______________________________________
//____________print_tag_in_file__________|
//_______________________________________|

/*There are functions which provide correct print
 of xml-tag in user's file */

LSymbol PRINTINFILE("PRINTINFILE");
LSymbol STARTPRINTING("STARTPRINTING");
LSymbol PRINTATTRFILE("PRINTATTRFILE");
LSymbol PRINTFILE("PRINTFILE");
LSymbol XMLTAGINFILE("XMLTAGINFILE");

static LFunctionalSymbol<LFunctionDefun> DEFUN("DEFUN");
static LFunctionalSymbol<LFunctionCond> COND("COND");
static LFunctionalSymbol<LFunctionCar> CAR("CAR");
static LFunctionalSymbol<LFunctionCdr> CDR("CDR");
static LFunctionalSymbol<LFunctionPlus> PLUS("PLUS");
static LFunctionalSymbol<LFunctionNull> lNULL("lNULL");

void LispInit_PrintInFile(){
    static LSymbol XML("XML");
    static LSymbol FILENAME("FILENAME");
    static LSymbol TYPE("TYPE");
    (L|DEFUN, PRINTINFILE, (L|XML, FILENAME, TYPE),
     (L|STARTPRINTING, XML, (L|FILESTREAM, FILENAME, TYPE))
     ).Evaluate();
}

void LispInit_StartPrinting(){
    static LSymbol XML("XML");
    static LSymbol FILENAME("FILENAME");
    (L|DEFUN, STARTPRINTING, (L|XML, FILENAME),
     (L|PRINTFILE, XML, FILENAME, 0),
     (L|CLOSEFILESTREAM, FILENAME)
     ).Evaluate();
}

void LispInit_PrintAttrFile(){
    static LSymbol ATTR("ATTR");
    static LSymbol FILENAME("FILENAME");
    (L|DEFUN, PRINTATTRFILE, (L|ATTR, FILENAME),
     (L|COND,
      (L|(L|lNULL, ATTR), (L|INFILEATTR, ATTR, FILENAME)),
      (L|T, (L|INFILEATTR, (L|CAR, ATTR), FILENAME),
       (L|PRINTATTRFILE, (L|CDR, ATTR), FILENAME)
       )
      )
     ).Evaluate();
}

void LispInit_PrintFile(){
    static LSymbol XML("XML");
    static LSymbol FILENAME("FILENAME");
    static LSymbol DEPTH("DEPTH");
    (L|DEFUN, PRINTFILE, (L|XML, FILENAME, DEPTH),
     (L|COND,
      (L|(L|lNULL, XML), NIL),
      (L|(L|ISSTRING, XML), (L|INFILESTR, XML, FILENAME, DEPTH)),
      (L|(L|ISXMLTAG, XML), (L|XMLTAGINFILE, XML, FILENAME, DEPTH)),
      (L|T, (L|PRINTFILE, (L|CAR, XML), FILENAME, DEPTH), (L|PRINTFILE, (L|CDR, XML), FILENAME, DEPTH))
      )
     ).Evaluate();
}

void LispInit_XmlTagInFile(){
    static LSymbol XML("XML");
    static LSymbol FILENAME("FILENAME");
    static LSymbol DEPTH("DEPTH");
    (L|DEFUN, XMLTAGINFILE, (L|XML, FILENAME, DEPTH),
     (L|COND,
      (L|(L|lNULL, (L|GETCONTENT, XML)), (L|PRINTEMPTY, XML, FILENAME, DEPTH)),
      (L|T, (L|PRINTATTRFILE, (L|INFILETAG, XML, FILENAME, DEPTH), FILENAME),
       (L|PRINTFILE, (L|GETCONTENT, XML), FILENAME, (L|PLUS, DEPTH, 1)),
       (L|INFILECLOSETAG, XML, FILENAME, DEPTH)
       )
      )
     ).Evaluate();
}

//this function opens file; prints tag in file, closes file
//1 param - xml-tag
//2 param - string - name of file
SReference print_tag_in_file(int params, const SReference * param){
    ASSERT_PARAMC(3);
    //SExpressionXmlTag *xml=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
    //if(xml==NULL)
    //	throw IntelibX("Error!: it's not an xml-tag in print_tag_in_file\n");
    SExpressionString *filename=param[1].DynamicCastGetPtr<SExpressionString>();
    if(filename==NULL)
        throw IntelibX("Error!: it's not a string in print_tag_in_file (the first param)\n");
    SExpressionString *type=param[2].DynamicCastGetPtr<SExpressionString>();
    if(type==NULL)
        throw IntelibX("Error!: it's not a string in print_tag_in_file (the second param)\n");
    LispInit_PrintInFile();
    LispInit_StartPrinting();
    LispInit_PrintAttrFile();
    LispInit_PrintFile();
    LispInit_XmlTagInFile();
    define_print_functions();
    GETCONTENT->SetFunction(getcontent);
    (L|PRINTINFILE, ~param[0], filename, type).Evaluate();
    return param[0];
}

//________________________________________
//__________string_functions______________|
//________________________________________|

//this is help function. it gets substring from string
//between input positions
//it gets 3 parameters
//1 param - the first position of needed characters
//2 param - the second position of needed characters
//3 param - string
char *getthispart(int first, int last, SString str){
	int length=last-first+1;
	SString mystr(str.c_str());
	char *s=new char[length];
	int k=0;
	for(int i=first; i<last; i++){
		s[k]=mystr[i];
		k++;
	}
	s[k]='\0';
	DEPRF3(("this is new string [%s]\n", s));
	return s;
}

//this function returns true if given SExpression is string
//1 param - SReference which we must test
SReference isstring(int params, const SReference *param){
    ASSERT_PARAMC(1);
    SExpressionString *pr=param[0].DynamicCastGetPtr<SExpressionString>();
    if(pr==0){
        return *PTheEmptyList;
    }
    return true;
}

//this function parses string - it deletes all enters and inserts tag <br/>
//it is needed to make corret XHTML
//1 param - string
//return value - list of strings and tags
SReference stringinxhtml(int params, const SReference *param){
        ASSERT_PARAMC(1);
        SExpressionString *my=param[0].DynamicCastGetPtr<SExpressionString>();
        if(my==NULL)
                throw IntelibX("it's not a SExpressionString in stringinxhtml()\n");
	SReference tag=SListConstructor();
	int i=0;
	SString mystr(my);
	int last=0;
	DEPRF3(("string in stringinxhtml %s\n", my->GetValue()));
	SString str(mystr);
	do{
		i++;
		DEPRF3(("in cycle! searching for enter\n"));
		if(mystr[i]=='\n' || mystr[i]=='\0'){
			SReference m=SListConstructor();
			char *newone=getthispart(last, i, str);
			SExpressionString *str1=new SExpressionString(newone);
			delete [] newone;
			last=i+1;
			m=m||(L|str1);
			SExpressionXmlTag *empty=new SExpressionXmlTag("br");
			m=m||(L|empty);
			tag=tag||m;
		}
	}while(mystr[i]!='\0');
	return tag;
}

//this function parses inputed string
SReference parsestringintag(int params, const SReference *param){
        ASSERT_PARAMC(2);
	DEPRF3(("we are in parsestringintag %s\n", param[0]->TextRepresentation().c_str()));
        SExpressionString *l=param[0].DynamicCastGetPtr<SExpressionString>();
	if(l==0){
		SReference res=SListConstructor();
		res=res||(L|*PTheEmptyList);
		res=res||(L|param[0]);
		res=res||(L|param[1]);
		return res;
	}
	ParsString *xml1=new ParsString(l, param[1]);
	return xml1->work();
}

//this function changes substring on another one
//3 param - string in which function changes substrings (it can be one character)
//2 param - substing (it can be one character)
//1 param - string in which subtrings change
SReference changeentities(int params, const SReference *param){
    ASSERT_PARAMC(3);
    DEPRF3(("we are in changeentities %s\n", param[1]->TextRepresentation().c_str()));
	SExpressionString *l=param[0].DynamicCastGetPtr<SExpressionString>();
    SExpressionString *l1=param[1].DynamicCastGetPtr<SExpressionString>();
	SExpressionString *l2=param[2].DynamicCastGetPtr<SExpressionString>();
	if(l2==NULL)
                throw IntelibX("it's not a string in changeentities in tag\n");
	SString entity, change, str;
	str=l2;
	if(l1==NULL && l==NULL){
		SExpressionChar *l3=param[1].DynamicCastGetPtr<SExpressionChar>();
        if(l3==NULL)
            throw IntelibX("not correct second parametr in changeentities\n");
        change+=l3->GetValue();
        change+='\0';
        DEPRF3(("second parameter in changeentities %s\n", change.c_str()));
		SExpressionChar *l4=param[0].DynamicCastGetPtr<SExpressionChar>();
        if(l4==NULL)
                throw IntelibX("not correct first parametr in changeentities\n");
        entity+=l4->GetValue();
        entity+='\0';
        DEPRF3(("first parameter in changeentities %s\n", entity.c_str()));
	}
	else if(l1==NULL){
		SExpressionChar *l3=param[1].DynamicCastGetPtr<SExpressionChar>();
        if(l3==NULL)
			throw IntelibX("not correct second parametr in changeentities\n");
		change+=l3->GetValue();
		change+='\0';
		DEPRF3(("second parameter in changeentities %s\n", change.c_str()));
		entity=l;
	}
	else if(l==NULL){
		SExpressionChar *l3=param[0].DynamicCastGetPtr<SExpressionChar>();
        if(l3==NULL)
                throw IntelibX("not correct first parametr in changeentities\n");
        entity+=l3->GetValue();
        entity+='\0';
        DEPRF3(("first parameter in changeentities %s\n", entity.c_str()));
		change=l1;
	}
	else{
        entity=l;
		change=l1;
	}
	int i=0;
	int ent_length=entity.length();
	DEPRF3(("change entity\n"));
	SString save;
	while(str[i]!='\0'){
		int x=0;
		bool isent=true;
		for(int k=i; k<ent_length+i; k++){
			if(str[k]=='\0' || str[k]!=entity[x]){
				isent=false;
				break;
			}
			x++;
		}
		if(isent){
			save+=change;
			i+=ent_length;
		}
		else{
			save+=str[i];
			i++;
		}
	}
	SExpressionString *r=new SExpressionString(save.c_str());
	DEPRF3(("return from change ent %s\n", r->GetValue()));
	return r;
}

//this function deletes all needed simbols from given string
//1 param - character
//2 param - string
SReference deletesimbol(int params, const SReference *param){
	ASSERT_PARAMC(2);
	SExpressionString *str_expr=param[1].DynamicCastGetPtr<SExpressionString>();
	if(str_expr==0)
		throw "it's not a string in in deletesimbol\n";
	SExpressionChar *character=param[0].DynamicCastGetPtr<SExpressionChar>();
	if(character==0)
		throw "it's not a character in deletesimbol\n";
	const char c=character->GetValue();
	SString str=str_expr;
	SString res;
	int i=0;
	while(str[i]!='\0'){
		if(str[i]!=c)
			res+=str[i];
		i++;
	}
	res+='\0';
	return res;
}

//this function will look for entity
//1 param - it is string where we are looking for entity
//2 param - it is position in string from which we are searching for entity
//result - true - if it's entity
//false - otherwise
//there are two types of entities - which are made from numbers and from simbols
bool isentity(SString str, int pos){
	if(str[pos]!='&')
		return false;
	int i=pos+1;
	enum type_ent { NONE, SIMBOLS, NUMBERS };
	type_ent type=NONE;
	while(str[i]!='\0'){
		//between & and ; must be simbols
	        if(str[i]==';' && i>pos+1){
			return true;
        	}
        	else{
            		switch(type){
                	case NONE:
                    		if(str[i]<='z' && str[i]>='a')
                        		type=SIMBOLS;
                    		else if(str[i]=='#')
                        		type=NUMBERS;
                    		else
                        		return false;
				break;
                	case SIMBOLS:
                    		if(str[i]>'z' || str[i]<'a')
                	        	return false;
				break;
                	case NUMBERS:
                    		if(str[i]>'9' || str[i]<'0')
                        		return false;
				break;
            		}
        	}
		i++;	
	}
	return false;
}

//this function will change all '&' in string on &amp;
//1 param - string
//result - new string without '&'
SReference changeampsonentity(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionString *str=param[0].DynamicCastGetPtr<SExpressionString>();
	if(str==NULL)
		throw "it's not a string in changeampsonentity\n";
	SString change_str=str;
	SString res;
	int i=0;
	while(change_str[i]!='\0'){
		if(change_str[i]=='&' && !isentity(change_str, i))
			res+="&amp;";
		else
			res+=change_str[i];
		i++;
	}
	res+='\0';
	return res;
}

//this function gets number from string
//it returns nil - if there is not a number in string
//otherwise - it returns number
//1 param - string
SReference getnumbfromstr(int params, const SReference *param){
        ASSERT_PARAMC(1);
        SExpressionString *s=param[0].DynamicCastGetPtr<SExpressionString>();
        if(s==NULL)
		throw IntelibX("it's not a string in getnumbfromstr\n");
	int i=0, k=0;
	SString str=s;
	while(str[i]!='\0'){
		if(str[i]>='0' && str[i]<='9')
			k=k*10+str[i]-'0';
		else
			return *PTheEmptyList;
		i++;
	}
	SExpressionInt *x=new SExpressionInt(k);
	return x;
}

//this function makes string from number
//1 param - integer
SReference getstrfromnumb(int params, const SReference *param){
        ASSERT_PARAMC(1);
        SExpressionInt *n=param[0].DynamicCastGetPtr<SExpressionInt>();
        if(n==0)
		return *PTheEmptyList;
	int numb=n->GetValue();
	int i=0, k=numb;
	while(k!=0){
		k=k/10;
		i++;
	}
	char *str=new char[i+1];
	for(int n=i-1; n>=0; n--){
		str[n]=(numb%10)+'0';
		numb/=10;
	}
	str[i]='\0';
	SExpressionString *s=new SExpressionString(str);
	return s;
}

//this function concatenates two strings
//1 param - string
//2 param - string
SReference concatstr(int params, const SReference *param){
	ASSERT_PARAMC(2);
	SExpressionString *str1=param[0].DynamicCastGetPtr<SExpressionString>();
	SExpressionString *str2=param[1].DynamicCastGetPtr<SExpressionString>();
	if(str1==0 || str2==0)
		throw IntelibX("it's not a string in concat strings\n");
	SString s(str1);
	s+=str2;
	return s;
}

//here we will get path of bone (use this function in xslt)
//we will parse string according characters which are usually in paths
//result - is list of all entities which are in path
//1 param - string
SReference parse_path(int params, const SReference *param){
        ASSERT_PARAMC(1);
        SExpressionString *str=param[0].DynamicCastGetPtr<SExpressionString>();
	if(str==NULL)
		throw IntelibX("it's not a string in parse_path");
	SString parse(str->GetValue());
	DEPRF3(("\nbegin %s\n", parse->GetValue()));
	int length=parse.Length();
	SReference result=SListConstructor();
	SString s;
	for(int i=0; i<length; i++){
		DEPRF3(("in parse cycle\n"));
		if(i<length-1)
			if(parse[i]=='/' && parse[i+1]=='/'){
				if(s.length()!=0){
					result=result||(L|s);
					s=SString();
				}
				result=result||(L|new SExpressionString("//"));
				i++;
				continue;
			}
		//these amount of simbils isn't full
		if(parse[i]=='/' || parse[i]=='@' || parse[i]=='[' || parse[i]==']' || parse[i]=='*'){
			if(s.length()!=0){
                                result=result||(L|s);
                                s=SString();
                        }
			SString str;
			str+=parse[i];
			result=result||(L|new SExpressionString(str.c_str()));
			continue;
		}
		if(i<length-1)
			if(parse[i]=='.' && parse[i+1]=='.'){
                        	if(s.length()!=0){
                                	result=result||(L|s);
                                	s=SString();
                        	}
				result=result||(L|new SExpressionString(".."));
                        	i++;
                        	continue;
                	}
		if(parse[i]=='.'){
			if(s.length()!=0){
                                result=result||(L|s);
                                s=SString();
                        }
                        result=result||(L|new SExpressionString("."));
                        continue;
                }
		char *ins = new char[2];
		ins[0]=parse[i];
		ins[1]='\0';
		s+=SString(ins);
	}
	if(s.length()!=0){
		DEPRF3(("last string %s\n", s->GetValue()));
		result=result||(L|new SExpressionString(s->GetValue()));
       	}
	DEPRF3(("result in parse string %s\n", result->TextRepresentation().c_str()));
	return result;
}


//this function will return true if we have only name in string
//(it is not a path
//1 param - string
SReference onlyname(int params, const SReference *param){
        ASSERT_PARAMC(1);
	SReference s=param[0];
	while(!s.IsEmptyList()){
        	SExpressionString *str=s.Car().DynamicCastGetPtr<SExpressionString>();
        	if(str==NULL)
                	throw IntelibX("it's not a string in onlyname");
		if(strcmp(str->GetValue(), "/")==0)
                        return *PTheEmptyList;
		if(strcmp(str->GetValue(), "//")==0)
			return *PTheEmptyList;
		if(strcmp(str->GetValue(), "@")==0)
                        return *PTheEmptyList;
		if(strcmp(str->GetValue(), "..")==0)
                        return *PTheEmptyList;
		if(strcmp(str->GetValue(), ".")==0)
                        return *PTheEmptyList;
		s=s.Cdr();
	}
	return T;
}

//this function concatenates string and number
//1 param - string
//2 param - number
//result - concatination of string and number
SReference string_concat_number(int params, const SReference *param){
	ASSERT_PARAMC(2);
	SExpressionString *str=param[0].DynamicCastGetPtr<SExpressionString>();
	if(str==NULL)
		throw IntelibX("it's not a string in string_concat_number\n");
	SExpressionInt *numb=param[1].DynamicCastGetPtr<SExpressionInt>();
	if(numb==NULL)
		throw IntelibX("it's not an integer in strin_concat_number\n");
	int number=numb->GetValue();
	int i=0;
	int save=number;
	if(number==0){
		i++;
	}
	else{
		while(number!=0){
			number/=10;
			i++;
		}
	}
	const char *sstr=str->GetValue();
	int length=strlen(sstr)+i+2;
	int l=strlen(sstr);
	char *res=new char[length];
	for(int k=0; k<l; k++)
		res[k]=sstr[k];
	res[l]='_';
	res[l+i]='\0';
	i--;
	if(save==0){
		res[l+i]=save+'0';
	}
	else{
		while(save!=0){
			res[l+i]=save%10+'0';
			save/=10;
			i--;
		}
	}
	SExpressionString *result=new SExpressionString(res);
	delete [] res;
	return result;
}

//this function will convert string to number >=0
//it there will be sumbols which aren't numbers - we will return nil
//otherwise return value would be list of number (without first 0)
//1 param - string
//result - list of numbers
SReference number_from_string(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionInt *num=param[0].DynamicCastGetPtr<SExpressionInt>();
	if(num!=0)
	{
		SReference res=SListConstructor();
		res=res||(L|num);
		return res;
	}
	SExpressionString *str=param[0].DynamicCastGetPtr<SExpressionString>();
	if(str==NULL)
		return *PTheEmptyList;
	const char* sstr=str->GetValue();
	int length=strlen(sstr);
	int current_value=-1;
	SReference res=SListConstructor();
	for(int i=0; i<length; i++){
		if(sstr[i]<'0' || sstr[i]>'9')
			return *PTheEmptyList;
		if(sstr[i]==' ' || sstr[i]=='\n' || sstr[i]=='\t')
		{
			if(current_value>=0)
			{
				SExpressionInt *cur = new SExpressionInt(current_value);
				res=res||(L|cur);
				current_value=-1;
			}
			continue;
		}
		if(current_value<0)
			current_value=0;
		
		current_value=current_value*10+sstr[i]-'0';
	}
	if(current_value>=0)
	{
		SExpressionInt *cur = new SExpressionInt(current_value);
		res=res||(L|cur);
	}
	return res;
}

//this function will returns random numbers
//1 param - left part of board
//2 param - rigth part of board
SReference generate_random_numb(int params, const SReference *param){
	ASSERT_PARAMC(2);
	SExpressionInt *left=param[0].DynamicCastGetPtr<SExpressionInt>();
	if(left==0)
		throw IntelibX("it's not a number in generate_random_numb");
	SExpressionInt *right=param[1].DynamicCastGetPtr<SExpressionInt>();
	if(right==0)
		throw IntelibX("it's not a number in generate_random_numb");
	if(left->GetValue() > right->GetValue()){
		SExpressionInt *r=right;
		right=left;
		left=r;
	}
	printf("%d %d\n", left->GetValue(), right->GetValue());
	int res=random()%right->GetValue()+left->GetValue();
	return new SExpressionInt(res);
}

//this function divides number on characters
//1 param - integer;
//result - list of characters
SReference divide_numb(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionInt *num=param[0].DynamicCastGetPtr<SExpressionInt>();
	if(num==0)
		throw IntelibX("it's not a number in divede_numb");
	int n=num->GetValue();
	SReference res=SListConstructor();
	if(n<0)
		return *PTheEmptyList;
	if(n==0)
		return res=res||(L|0);
	while(n!=0){
		SExpressionInt *n_num=new SExpressionInt(n%10);
		n=n/10;
		res=res||(L|n_num);
	}
	return res;
}

//this function will divide string on number-characters
//function will return nill if there are in string another simbols
//1 param - it is string
SReference divide_string_on_num(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionString *str=param[0].DynamicCastGetPtr<SExpressionString>();
	if(str==0)
		return *PTheEmptyList;
	SReference res=SListConstructor();
	const char* sstr=str->GetValue();
	int length=strlen(sstr);
	for(int i=0; i<length; i++){
		if(sstr[i]<'0' || sstr[i]>'9')
			return *PTheEmptyList;
		SExpressionInt *n=new SExpressionInt(sstr[i]-'0');
		res=res||(L|n);
	}
	return res;
}

//this function will return divided string
//1 param - string which wil be didvided
//2 param - character
SReference divide_str_simbol(int params, const SReference *param){
	ASSERT_PARAMC(2);
	SExpressionString *str=param[0].DynamicCastGetPtr<SExpressionString>();
	if(str==NULL)
		throw IntelibX("Error!:it's not a string in divide_str_simbol\n");
	SExpressionChar *c=param[1].DynamicCastGetPtr<SExpressionChar>();
	if(c==NULL)
		throw IntelibX("Error!:it's not a character in divide_str_simbol\n");
	const char* sstr=str->GetValue();
	int length=strlen(sstr);
	int len=0;
	for(int i=0; i<length; i++){
		if(sstr[i]==c->GetValue())
			break;
		len++;
	}
	if(len==length)
	{
		SReference res=SListConstructor();
		res=res||(L|str);
		return res;
	}
	char* first=new char[len+1];
	for(int i=0; i<len; i++)
		first[i]=sstr[i];
	first[len]='\0';
	int k=0;
	char *second=new char[length-len];
	for(int i=len+1; i<length; i++)
	{
		second[k]=sstr[i];
		k++;
	}
	second[k]='\0';
	SReference res=SListConstructor();
	res=res||(L|first);
	res=res||(L|second);
	return res;
}

//this class incapsulates functions which converts xml-tag in string
class StrFromTag{
        struct partstr *makeonestr(SExpressionXmlTag *xml);
        struct partstr *workattr(SReference attr);
        struct partstr *insertattr(struct partstr *maint, struct partstr *attr);
        int countlength(struct partstr *q);
        char *makestring(struct partstr *q, int length);
        char *concatination(const char *str1, int l1, const char *str2, int l2);
public:
        char *strforming(SExpressionXmlTag *xml);
};


//this is start-function of transforming xml-tag in string
//1 param - xml-tag
char *StrFromTag::strforming(SExpressionXmlTag *xml){
        DEPRF3(("strforming\n"));
        struct partstr *p=makeonestr(xml);
        int leng=countlength(p);
        DEPRF3(("got length is %d\n", leng));
        return makestring(p, leng);
}

//this function gets list of string's parts
//it concatinates them in one string
//1 param - list of parts
//2 param - length of result string
char *StrFromTag::makestring(struct partstr *q, int length){
        DEPRF3(("makestring\n"));
        struct partstr *p=q;
        struct partstr *t;
        char *str=new char[length];
        int i=0;
        while(p!=NULL){
                for(int j=0; j<p->length; j++){
                        str[i]=p->part[j];
                        i++;
                }
                t=p;
                p=p->next;
                delete []t->part;
                delete t;
        }
        str[i]='\0';
        DEPRF3(("got string is [%s]\n", str));
        return str;
}

//this function counts the whole length of string
//1 param - list of string's parts
int StrFromTag::countlength(struct partstr *q){
        int i=0;
        struct partstr *p=q;
        while(p!=NULL){
                i+=p->length;
                p=p->next;
        }
        i++;
        return i;
}

//this function makes from xml-tag string
//1 param - xml-tag
struct partstr *StrFromTag::makeonestr(SExpressionXmlTag *xml){
        DEPRF3(("makeonestr\n"));
        SReference l=xml;
        struct partstr *tag;
        struct partstr *t;
        struct partstr *cltag;
	//convert open tag
        const char *name=xml->GetName();
        tag=new partstr;
        int j=strlen(name);
        char *middle1;
        middle1=concatination("<", 1, name, j);
        tag->part=concatination(middle1, j+1, " ", 1);
        delete middle1;
        DEPRF3(("tag->part is %s\n", tag->part));
        tag->length=j+2;
	//convert attributes
        struct partstr *last=insertattr(tag, workattr(xml->GetAttr()));
        cltag=new partstr;
        middle1=concatination(name, j, ">", 1);
	//convert close tag
        cltag->part=concatination("</", 2, middle1, j+1);
        delete []middle1;
        cltag->length=j+3;
        cltag->next=NULL;
	//convert tag-content
        SReference cont=xml->GetContent();
        struct partstr *q=NULL, *p=NULL;
        while(!cont.IsEmptyList()){
                p=q;
                SExpressionString *x=cont.Car().DynamicCastGetPtr<SExpressionString>();
                if(x!=0){
                        DEPRF3(("it's string\n"));
                        t=new struct partstr;
                        t->length=strlen(x->GetValue());
                        t->part=new char[t->length+1];
                        strcpy(t->part, x->GetValue());
                        t->next=NULL;
                }
                else
                        t=makeonestr(cont.Car().DynamicCastGetPtr<SExpressionXmlTag>());
                if(p!=NULL){
                        while(p->next!=NULL)p=p->next;
                        p->next=t;
                }
                else{
                        q=t;
                }
                cont=cont.Cdr();
        }
        DEPRF3(("last part %s\n", last->part));
        struct partstr *cl=new struct partstr;
        cl->part=new char[1];
        cl->part[0]='>';
        cl->length=1;
        if(q==NULL){
                last->next=cl;
                cl->next=cltag;
        }
        else{
                last->next=cl;
                cl->next=q;
                p=q;
                while(p->next!=NULL)p=p->next;
                p->next=cltag;
        }
        return tag;
}

//this function converts xml-attributes in string
//1 param - list of attributes
struct partstr *StrFromTag::workattr(SReference attr){
        DEPRF3(("workattr\n"));
        struct partstr *tag=NULL, *p=NULL, *t=NULL;
        char *middle1, *middle2;
        while(!attr.IsEmptyList()){
                p=new partstr;
                SExpressionXmlAttr *attrib=attr.Car().DynamicCastGetPtr<SExpressionXmlAttr>();
                int i=strlen(attrib->GetContent());
                middle1=concatination("'", 1, attrib->GetContent(), i);
                middle2=concatination(middle1, i+1, "' ", 3);
                delete []middle1;
                middle1=concatination("=", 1, middle2, i+3);
                delete []middle2;
                int j=strlen(attrib->GetName());
                middle2=concatination(attrib->GetName(), j, middle1, i+4);
                delete []middle1;
                p->part=middle2;
                DEPRF3(("this is attr %s\n", p->part));
                p->length=j+i+4;
                p->next=NULL;
                t=tag;
                if(t==NULL)
                        tag=p;
                else{
                        while(t->next!=NULL)t=t->next;
                        t->next=p;
                }
                attr=attr.Cdr();
        }
        return tag;
}

//this function insearts list of converted attributes in main list
//return value - pointer on last partstr
//1 param - head
//2 param - list of converted attributes
struct partstr *StrFromTag::insertattr(struct partstr *maint, struct partstr *attr){
        DEPRF3(("insertattr\n"));
        if(attr==NULL)
                return maint;
        struct partstr *p;
        maint->next=attr;
        p=attr;
        while(p->next!=NULL) p=p->next;
        return p;
}

//this function concatenates two stings
//1 param - the first string
//2 param - the first string's length
//3 param - the second string
//4 param - the second's strng
char *StrFromTag::concatination(const char *str1, int l1, const char *str2, int l2){
        char *res=new char[l1+l2+1];
        int i=0;
        while(str1[i]!='\0'){
                res[i]=str1[i];
                i++;
        }
        int j=0;
        while(str2[j]!='\0'){
                res[i]=str2[j];
                i++;
                j++;
        }
        res[i]='\0';
        return res;
}

//this function turns comming tag in string
//1 param - xml tag
SReference turntaginstr(int params, const SReference *param){
        ASSERT_PARAMC(1);
        SExpressionXmlTag *xml=param[0].DynamicCastGetPtr<SExpressionXmlTag>();
        if(xml==0)
                throw IntelibX("it's not a tag in turntaginstr");
        StrFromTag *turn=new StrFromTag;
        char *str=turn->strforming(xml);
        return str;
}

void define_print_functions(){
    PRINTBEAUTY->SetFunction(print_beauty);
    PRINTSMTH->SetFunction(printsmth);
    INFILESTR->SetFunction(printstrinusersfile);
    INFILETAG->SetFunction(printopenusersfile);
    INFILEATTR->SetFunction(printattrusersfile);
    INFILECLOSETAG->SetFunction(printcloseusersfile);
    PRINTEMPTY->SetFunction(printemptytagfile);
}

void define_xml_functions(){
	NEWTAG->SetFunction(newxmltag);
	INSCONT->SetFunction(insertcontent);
	ISXMLTAG->SetFunction(isxmlatom);
	XMLNAMEGET->SetFunction(get_xml_name);
	GETCONTENT->SetFunction(getcontent);
	NEWCONTENT->SetFunction(deletetagfrom);
	COPYTAG->SetFunction(copy_xml_tag);
}

void define_attr_functions(){
	NEWATTR->SetFunction(newxmlattr);
	XMLATTRGET->SetFunction(getxmltagattr);
	NEWATTRLIST->SetFunction(newxmltagattr);
	GETATTRCONTENT->SetFunction(getattrcont);
	INSATTRCONT->SetFunction(attrnewcont);
	GETATTRNAME->SetFunction(get_attr_name);
	INSREADYATTR->SetFunction(insertreadyattr);
	COPYATTR->SetFunction(copy_xml_attr);
}

void define_string_functions(){
	ISSTRING->SetFunction(isstring);
	TRANSFORMSTR->SetFunction(stringinxhtml);
	PARSTAGS->SetFunction(parsestringintag);
	CHANGEENT->SetFunction(changeentities);
	DELSIMB->SetFunction(deletesimbol);
	CHANGEAMP->SetFunction(changeampsonentity);
	NUMBFROMSTR->SetFunction(getnumbfromstr);
	STRFROMNUMB->SetFunction(getstrfromnumb);
	CONCATSTR->SetFunction(concatstr);
	PARSEXPATH->SetFunction(parse_path);
	ONLYNAME->SetFunction(onlyname);
	STRCONCATNUMB->SetFunction(string_concat_number);
	GETNUMBERSTR->SetFunction(number_from_string);
	NUMFROMSTR->SetFunction(divide_string_on_num);
	DIVIDESTRSIM->SetFunction(divide_str_simbol);
	TAGINSTR->SetFunction(turntaginstr);
}

void define_number_functions(){
	GENERATENUM->SetFunction(generate_random_numb);
	DIVIDENUM->SetFunction(divide_numb);
}

void define_file_functions(){
	CLOSEFILESTREAM->SetFunction(close_file_stream);
	FILESTREAM->SetFunction(getfilestream);
	DIRFILES->SetFunction(getfilesdir);
	EXISTSDIR->SetFunction(exists_dir);
	TURNOPENFILE->SetFunction(openfilexml);
	PRINTINFILEXML->SetFunction(print_tag_in_file);
}

