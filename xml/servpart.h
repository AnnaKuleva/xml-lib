#ifndef _SERVPART_H_KURS3_
#define _SERVPART_H_KURS3_

class ServPart{
	int nport;
	int fsoc;
	int mypol;
	bool stop;
	bool end;
	int get;
	int during;
	char buf[128];
	ServPart(ServPart &a){}
	int convert(const char *s);
	void getmessage();
public:
	ServPart(char *s);
	~ServPart();
	bool emptybuf();
	char getsimbol();
	void server();
	void work();
	int getport(){return mypol;}
	int getfsoc(){return fsoc;}
};

#endif
