#include "servpart.h"
#include "structures.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <unistd.h>
#include "mistake.h"

ServPart::ServPart(char *s){
	nport=convert(s);
	fsoc=0;
	mypol=-1;
	get=0;
	during=-1;
	stop=false;
	end=false;
	DEPRF3(("nport= %d\n", nport));
}

void ServPart::server(){
	struct sockaddr_in a;
	int opt=1;
	if((fsoc=socket(AF_INET, SOCK_STREAM, 0))==-1){
                throw ServPartErrors("socket");
        }
	a.sin_family=AF_INET;
        a.sin_port=htons(nport);
        a.sin_addr.s_addr=INADDR_ANY;
        setsockopt(fsoc, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
	if((bind(fsoc, (struct sockaddr *)&a, sizeof(a)))==-1){
		DEPRF3(("some errors\n"));
                throw ServPartErrors("bind");
        }
	if(-1==listen(fsoc, 5)){
                throw ServPartErrors("listen");
        }
	DEPRF3(("fsoc=%d\n", fsoc));
}

int ServPart::convert(const char *s){
	int i=0;
	int res=0;
	while(s[i]!='\0'){
		res=res*10+s[i]-'0';
		i++;
	}
	return res;
}

void ServPart::work(){
	int max_d;
	fd_set mas;
	char buf1[]="Sorry! Server is busy! Please, try again later\n";
	char buf2[]="Welcome! Let's start our work!\n";
	int mistake;
	while(!stop && !end){
		max_d=fsoc;
		FD_ZERO(&mas);
		FD_SET(fsoc, &mas);
		if(mypol>0){
			FD_SET(mypol, &mas);
			if(mypol>max_d)
				max_d=mypol;
		}
		mistake=select(max_d+1, &mas, NULL, NULL, NULL);
		DEPRF3(("mistake=%d\n", mistake));
		if(mistake<1){
			throw ServPartErrors("select");
		}
		if(mypol>0){
			DEPRF3(("cycle after select\n"));
			if(FD_ISSET(mypol, &mas)){
				DEPRF3(("my user has something for me\n"));
				getmessage();
			}
		}
		if(FD_ISSET(fsoc, &mas)){
			DEPRF3(("search for users\n"));
			int get;
			get=accept(fsoc, NULL, NULL);
			if(get==-1){
				throw ServPartErrors("accept");
			}
			if(mypol>0){
				write(get, buf1, sizeof(buf1));
				close(get);
			}
			else{
				stop=true;
				mypol=get;
				write(mypol, buf2, sizeof(buf2));
			}
		}
	}
	stop=false;
}

void ServPart::getmessage(){
	stop=true;
	get=read(mypol, buf, sizeof(buf)-1);
	if(get==0){
		DEPRF3(("we lost user\n"));
		shutdown(mypol, 2);
		close(mypol);
		end=true;
		buf[0]=EOF;
		get=1;
		during=-1;
		return;
	}
	buf[get]='\0';
	during=-1;
	DEPRF3(("get from buf %s\n", buf));
}

ServPart::~ServPart(){
	close(fsoc);
}

bool ServPart::emptybuf(){
	if(get==0) return false;
	return true;
}

char ServPart::getsimbol(){
	during++;
	if(during+1==get)
		get=0;
	return buf[during];
}

