#ifndef _DATE_LIB_KULEVA_ANNA_2015_
#define _DATE_LIB_KULEVA_ANNA_2015_

#include <intelib/sexpress/sexpress.hpp>

SReference create_date(int params, const SReference *param);
SReference greater_date(int params, const SReference *param);
SReference date_to_string(int params, const SReference *param);

void define_date_finctions();

#endif
