#include "sexpress_date.h"

#include <intelib/sexpress/iexcept.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ASSERT_TIME(fail) \
	INTELIB_ASSERT(fail==false, IntelibX("SExpressionDate: wrong formed date\n"));

IntelibTypeId SExpressionDate::TypeId(&SExpression::TypeId);

SExpressionDate::SExpressionDate(int year, int month, int day, int hour, int minute, int second)
	: SExpression(TypeId)
	, m_year(year)
	, m_month(month)
	, m_day(day)
	, m_hour(hour)
        , m_minute(minute)
	, m_second(second)
{
	m_fail=test_correct();
}

//date = YYYY-MM-DD HH:MM:SS
SExpressionDate::SExpressionDate(const char* date):SExpression(TypeId){
	if(strlen(date) != 19)
	{
		m_fail=true;
		m_year=0;
		m_month=m_day=1;
		m_hour=m_minute=m_second=0;
		return;
	}
	if(date[4]!='-' || date[7]!='-' || date[10]!=' '
				|| date[13]!=':' || date[16]!=':')
	{
		m_fail=true;
		m_year=0;
		m_month=m_day=1;
		m_hour=m_minute=m_second=0;
		return;
	}
	char year[5];
	char month[3];
	char day[3];
	char hour[3];
	char minute[3];
	char second[3];
	strncpy((char*)year, date, 4);
	year[4]='\0';
	m_year=atoi((char*)year);
	strncpy((char*)month, date+5, 2);
	month[2]='\0';
	m_month=atoi((char*)month);
	strncpy((char*)day, date+8, 2);
	day[2]='\0';
	m_day=atoi((char*)day);
	strncpy((char*)hour, date+11, 2);
	hour[2]='\0';
	m_hour=atoi((char*)hour);
	strncpy((char*)minute, date+14, 2);
	minute[2]='\0';
	m_minute=atoi((char*)minute);
	strncpy((char*)second, date+17, 2);
	second[2]='\0';
	m_second=atoi((char*)second);
	m_fail=test_correct();
}

#if INTELIB_TEXT_REPRESENTATIONS == 1
SString SExpressionDate::TextRepresentation() const{
	ASSERT_TIME(m_fail);
	SString res;
	char year[5];
	char month[3];
	char day[3];
	char hour[3];
	char minute[3];
	char second[3];
	sprintf(year, "%d", m_year);
	sprintf(month, "%d", m_month);
	sprintf(day, "%d", m_day);
	sprintf(hour, "%d", m_hour);
	sprintf(minute, "%d", m_minute);
	sprintf(second, "%d", m_second);
	res=SString(year) + "-" + month + "-" + day + " ";
	res+=SString(hour) + ":" + minute + ":" + second;
	return res;
}
#endif

bool SExpressionDate::test_correct() const{
	if(m_year<0)
		return true;
	if(m_month<1 || m_month>12)
		return true;
	if(m_day<1)
		return true;
	if(((m_month==1) || (m_month==3) || (m_month==5) || (m_month==7) ||
			(m_month==8) || (m_month==10) || (m_month==12)) && m_day>31)
		return true;
	//February
	if(m_month==2){
		if(m_year%4==0){
			if(m_year%100!=0 && m_day>29)
				return true;
		}
		else{
			if(m_day>28)
				return true;
		}
	}
	if(((m_month==4) || (m_month==6) || (m_month==9) || (m_month==11)) && m_day>30)
		return true;
	if(m_hour<0 || m_hour>23)
		return true;
	if(m_minute<0 || m_minute>59)
		return true;
	if(m_second<0 || m_second>59)
		return true;
	return false;
}

bool SExpressionDate::Greater(const SExpression* other) const{
	INTELIB_ASSERT(TermType() == other->TermType(), IntelibX_bug());
	ASSERT_TIME(m_fail);
	const SExpressionDate *date=static_cast<const SExpressionDate*>(other);
	ASSERT_TIME(date->Fail());
	if(m_year > date->GetYear())
		return true;
	if(m_year < date->GetYear())
		return false;
	if(m_month > date->GetMonth())
		return true;
	if(m_month < date->GetMonth())
		return false;
	if(m_day > date->GetDay())
		return true;
	if(m_day < date->GetDay())
		return false;
	if(m_hour > date->GetHour())
		return true;
	if(m_hour < date->GetHour())
		return false;
	if(m_minute > date->GetMinute())
		return true;
	if(m_minute < date->GetMinute())
		return false;
	if(m_second > date->GetSecond())
		return true;
	return false;
}

bool SExpressionDate::SpecificEql(const SExpression* other) const{
	INTELIB_ASSERT(TermType() == other->TermType(), IntelibX_bug());
	ASSERT_TIME(m_fail);
	const SExpressionDate *date=static_cast<const SExpressionDate*>(other);
	ASSERT_TIME(date->Fail());
	if(date->GetYear() != m_year)
		return false;
	if(date->GetMonth() != m_month)
		return false;
	if(date->GetDay() != m_day)
		return false;
	if(date->GetHour() != m_hour)
		return false;
	if(date->GetMinute() != m_minute)
		return false;
	if(date->GetSecond() != m_second)
		return false;
	return true;
}
