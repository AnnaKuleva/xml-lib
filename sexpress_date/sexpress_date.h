#ifndef _SEXPRESSION_DATE_KULEVA_2015_
#define _SEXPRESSION_DATE_KULEVA_2015_

#include <intelib/sexpress/sexpress.hpp>
#include <intelib/sexpress/sstring.hpp>

//Date s-expression
//this class represents s-expression of date
class SExpressionDate : public SExpression{
	int m_year;
	int m_month;
	int m_day;
	int m_hour;
	int m_minute;
	int m_second;
	bool m_fail;
	
	bool test_correct() const;
public:
	static IntelibTypeId TypeId;
	SExpressionDate(int year, int month, int day, int hour=0, int minute=0, int second=0);
	//date = YYYY-MM-DD HH:MM:SS
	SExpressionDate(const char* date);
#if INTELIB_TEXT_REPRESENTATIONS == 1
	virtual SString TextRepresentation() const;
#endif
	int GetYear() const { return m_year; }
	int GetMonth() const { return m_month; }
	int GetDay() const { return m_day; }
	int GetHour() const { return m_hour; }
	int GetMinute() const { return m_minute; }
	int GetSecond() const { return m_second; }
	bool Fail() const { return m_fail; }

	bool Greater(const SExpression* other) const;
protected:
	virtual ~SExpressionDate() {}	

	virtual bool SpecificEql(const SExpression* other) const;
};

#endif
