#include <intelib/sexpress/sexpress.hpp>
#include <intelib/sexpress/iexcept.hpp>
#include <intelib/sexpress/sstring.hpp>
#include <intelib/tools/sstream.hpp>
#include <intelib/lisp/lisp.hpp>
#include <intelib/lisp/lsymbol.hpp>
#include <intelib/lfun_std.hpp>

#include <stdio.h>

#include "../date_test.hxx"
#include "../date_lib.h"

int main(){
	LListConstructor L;
	try{
		LispInit_date_test();
		define_date_finctions();
		(L|TEST, "2014-12-16 12:43:54").Evaluate();
	}
	catch(IntelibX &x) {
		printf("\nCaught IntelibX: %s\n", x.Description());
		if(x.Parameter().GetPtr()){
			printf("%s\n", x.Parameter()->TextRepresentation().c_str());
		}
		if(x.Stack().GetPtr()){
			printf("%s\n", x.Stack()->TextRepresentation().c_str());
		}
	}
	catch(...){
		printf("something strange was caught\n");
	}
	return 0;
}
