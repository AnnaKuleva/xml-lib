(%%%
	(MODULE-NAME "date_test")
	(DECLARE-PUBLIC test)
	(DECLARE-USED-MODULE "date_symb.h")
	(DECLARE-EXTERNAL createdate)
	(DECLARE-EXTERNAL greaterdate)
)

(defun test (date)
	(print "test 0")
	(print (createdate date))
	(test1)
)

(defun test1 ()
	(print "test 1")
	(print (createdate 2014 12 31 23 59 59))
	(test2)
)

(defun test2 ()
	(print "test 2")
	(print (createdate 1234 12 23))
	(test3)
)

(defun test3 ()
	(print "test 3")
	(cond
		((eql (createdate 1234 12 31) (createdate 1234 12 31)) (print "test 3 passed"))
		(t (print "test 3 failed"))
	)
	(test4)
)

(defun test4 ()
	(print "test 4")
	(cond
		((greaterdate (createdate "2003-06-16 10:54:36")(createdate "2003-06-05 12:42:55")) (print "test 4 passed"))
		(t (print "test 4 failed"))
	)
	(test5)
)

(defun test5 ()
	(print "test 5")
	(cond
		((greaterdate (createdate 1234 12 31) (createdate 1234 12 30)) (print "test 5 passed"))
		(t (print "test 5 failed"))
	)
	(test6)
)

(defun test6 ()
	(print "test 6")
	(cond
		((eql (createdate 1234 12 23) (createdate 1200 12 31)) (print "test 6 failed"))
		(t (print "test 6 passed"))
	)
	(test7)
)

(defun test7 ()
	(print "test 7 - fail test")
	;(createdate 1234 23 12)
	;(createdate -1 2 2)
	;(createdate 1445 2 29)
	;(createdate 1234 12 1 25 0 0)
	;(print (createdate 1234 12 1 0 67 0))
	;(print (createdate 1234 12 1 23 59 60))
	(print (createdate "1111111111111111111"))
)
