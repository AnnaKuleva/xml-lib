#include "sexpress_date.h"
#include "declare_date_functions.h"

#include <intelib/sexpress/sstring.hpp>
#include <intelib/genlisp/lispform.hpp>
#include <intelib/sexpress/iexcept.hpp>
#include <intelib/lisp/lisp.hpp>
#include <intelib/lisp/lsymbol.hpp>
#include <intelib/lfun_std.hpp>

#include <stdio.h>

static LListConstructor L;

#define ASSERT_PARAMC(n) \
	INTELIB_ASSERT(params <= n, IntelibX_too_many_params(params));\
	INTELIB_ASSERT(params >= n, IntelibX_too_few_params(params));

//this function creates new date
//3 params - year, month, day
//6 params - year, month, day, hour, minute, second
//1 param - string YYYY-MM-DD HH:MM:SS
SReference create_date(int params, const SReference *param){
	if(params==1)
	{
		SExpressionString* str=param[0].DynamicCastGetPtr<SExpressionString>();
		if(str==NULL)
			throw IntelibX("it's not a string in creating date\n");
		SExpressionDate* res=new SExpressionDate(str->GetValue());
		if(res->Fail())
			throw IntelibX("Error!: uncorrect date\n");
		return res;
	}
	if(params==3)
	{
		SExpressionInt *year=param[0].DynamicCastGetPtr<SExpressionInt>();
		SExpressionInt *month=param[1].DynamicCastGetPtr<SExpressionInt>();
		SExpressionInt *day=param[2].DynamicCastGetPtr<SExpressionInt>();
		if(year==NULL || month==NULL || day==NULL)
			throw IntelibX("uncorrect parameters in creating date\n");
		SExpressionDate* res=new SExpressionDate(year->GetValue(), month->GetValue(), day->GetValue());
		if(res->Fail())
			throw IntelibX("Error!: uncorrect date\n");
		return res;
	}
	if(params==6)
	{
		SExpressionInt *year=param[0].DynamicCastGetPtr<SExpressionInt>();
		SExpressionInt *month=param[1].DynamicCastGetPtr<SExpressionInt>();
		SExpressionInt *day=param[2].DynamicCastGetPtr<SExpressionInt>();
		SExpressionInt *hour=param[3].DynamicCastGetPtr<SExpressionInt>();
		SExpressionInt *minute=param[4].DynamicCastGetPtr<SExpressionInt>();
		SExpressionInt *second=param[5].DynamicCastGetPtr<SExpressionInt>();
		if(year==NULL || month==NULL || day==NULL ||
			hour==NULL || minute==NULL || second==NULL)
			throw IntelibX("uncorrect parameters in creating date\n");
		SExpressionDate* res=new SExpressionDate(year->GetValue(), month->GetValue(),
				day->GetValue(), hour->GetValue(), minute->GetValue(), second->GetValue());
		if(res->Fail())
			throw IntelibX("Error!: uncorrect date\n");
		return res;
	}
	throw IntelibX("create date - wrong number of params\n");
}

//this function compare two dates
//it returns true if the first one is greater than the second one
SReference greater_date(int params, const SReference *param){
	ASSERT_PARAMC(2);
	SExpressionDate* date1=param[0].DynamicCastGetPtr<SExpressionDate>();
	SExpressionDate* date2=param[1].DynamicCastGetPtr<SExpressionDate>();
	if(date1==NULL || date2==NULL)
		throw IntelibX("uncorrect parameters in compearness dates\n");
	if(!date1->Greater(date2))
		return *PTheEmptyList;
	return true;
}

//this function returns string, which represents date
//(may be later here will be many formats)
//1 param - date
SReference date_to_string(int params, const SReference *param){
	ASSERT_PARAMC(1);
	SExpressionDate* date=param[0].DynamicCastGetPtr<SExpressionDate>();
	if(date==NULL)
		throw IntelibX("it's not a date in date_to_string\n");
	return date->TextRepresentation();
}

//this funcion defines functions for working with day && time
void define_date_finctions(){
	CREATEDATE->SetFunction(create_date);
	GREATERDATE->SetFunction(greater_date);
	DATETOSTRING->SetFunction(date_to_string);
}
