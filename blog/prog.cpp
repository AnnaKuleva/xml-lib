#include <intelib/sexpress/sexpress.hpp>
#include <intelib/sexpress/iexcept.hpp>
#include <intelib/sexpress/sstring.hpp>
#include <intelib/tools/sstream.hpp>
#include <intelib/lisp/lisp.hpp>
#include <intelib/lisp/lsymbol.hpp>
#include <intelib/lfun_std.hpp>

#include "../xml/lispint.h"
#include "transform_blog.hxx"
#include "../dtd/dtdlib.h"
#include "../sexpress_date/date_lib.h"

//#include "../xml/declare_functions.h"

int main(){
	LListConstructor L;
	try{
		printf("start program\n");
		LispInit_transform_blog();
		SStreamStdout lstdout;
		define_xml_functions();
		define_attr_functions();
		define_string_functions();
		define_file_functions();
		define_dtd_functions();
		define_date_finctions();
		//(L|FIRSTTRY, "croco/croco2", "croco's blog", "web-site/site",
		//		"\"../style.css\"", "web-site/right_part", "web-site/top-part.xml").Evaluate();
		(L|FIRSTTRY, "croco/ex", "annie's blog", "web-site1/site",
                              "\"../style.css\"", "web-site1/right_part", "web-site1/top-part.xml").Evaluate();
		printf("it's ended\n");
	}
	catch(IntelibX &x) {
        	printf("\nCaught IntelibX: %s\n", x.Description() );
        	if(x.Parameter().GetPtr()) {
            		printf("%s\n", x.Parameter()->TextRepresentation().c_str());
        	}
        	if(x.Stack().GetPtr()) {
            		printf("%s\n", x.Stack()->TextRepresentation().c_str());
        	}
	}
	catch(const char *s){
		printf("%s\n", s);
	}
	catch(...) {
		printf("Something strange caught\n");
	}
	return 0;
}
