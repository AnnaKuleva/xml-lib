(%%% 
	(MODULE-NAME "transform_blog")
	(DECLARE-PUBLIC firsttry)
	(DECLARE-USED-MODULE "../xml/symb.h")
	(DECLARE-USED-MODULE "../sexpress_date/date_symb.h")
	(DECLARE-EXTERNAL isstring)
	(DECLARE-EXTERNAL printbeauty)
	(DECLARE-EXTERNAL getcontent)
	(DECLARE-EXTERNAL turnopenfile)
	(DECLARE-EXTERNAL newtag)
	(DECLARE-EXTERNAL inscont)
	(DECLARE-EXTERNAL newattr)
	(DECLARE-EXTERNAL insreadyattr)
	(DECLARE-EXTERNAL printinfilexml)
	(DECLARE-EXTERNAL filestream)
	(DECLARE-EXTERNAL printempty)
	(DECLARE-EXTERNAL xmlnameget)
	(DECLARE-EXTERNAL newcontent)
	(DECLARE-EXTERNAL dividestrsim)
	(DECLARE-EXTERNAL parstags)
	(DECLARE-EXTERNAL newattrlist)
	(DECLARE-EXTERNAL xmlattrget)
	(DECLARE-EXTERNAL changeent)
	(DECLARE-EXTERNAL getattrcontent)
	(DECLARE-EXTERNAL getattrname)
	(DECLARE-EXTERNAL insattrcont)
	(DECLARE-EXTERNAL delsimb)
	(DECLARE-EXTERNAL changeamp)
	(DECLARE-EXTERNAL dirfiles)
	(DECLARE-EXTERNAL isxmltag)
	(DECLARE-EXTERNAL numbfromstr)
	(DECLARE-EXTERNAL strfromnumb)
	(DECLARE-EXTERNAL concatstr)
	(DECLARE-EXTERNAL transformstr)
	(DECLARE-EXTERNAL parsexpath)
	(DECLARE-EXTERNAL onlyname)
	(DECLARE-EXTERNAL existsdir)
	(DECLARE-EXTERNAL createdate)
	(DECLARE-EXTERNAL greaterdate)
	(DECLARE-EXTERNAL datetostring)
	(DECLARE-EXTERNAL strconcatnumb)
)

;start function. it gets parameters of blog
;name - name of directory where files of blog are saved
;title - title of every page
;dir - name of directory where new files will be saved
;css-table - name of css-table
;right_top - name of directory with xml-files which will be written on pages
;top - it's header which will be on every page
(defun firsttry (name title dir css-table right_top top)
	(let ((right_colomn (get_right_colomn (dirfiles right_top)))
		(top_part (turnopenfile top)))
		(existsdir dir 1)
		(start_form_main_page title dir
			(opendir name dir title css-table right_colomn)
		css-table right_colomn top_part)
	)
)

;this function which gets list ((name_1 . xml-tag) (name_2 . xml_tag) ...)
;if starts forming main-page
(defun start_form_main_page(title dir l css-table right_colomn top_part)
	(print_in_file (main_page title dir l css-table right_colomn top_part) top_part nil)
)

(defun print_in_file(l top_part prev)
	(cond
		((null l) t)
		(t (printinfilexml 
			(insert_top_part (insert_buttons (car (turnopenfile (print (car l)))) prev (cdr l)) top_part) (car l) "w") 
			(print_in_file (cdr l) top_part (car l))
		)
	)
)

;this function inserts top-part of page
(defun insert_top_part(xml top_part)
	(let ((body (cadr (getcontent xml))))
		(newcontent body (cons top_part (getcontent body)))
	xml
	)
)

;this function returns xml-tag which will be placed in right
;part of web-page
;right_top - list on file's names
;result - list of xml-tags
(defun get_right_colomn (right_top)
	(cond
		((null right_top) nil)
		(t (cons (form_right_colomn (car right_top)) (get_right_colomn (cdr right_top))))
	)
)

(defun form_right_colomn (right_part)
	(inscont (insreadyattr (newtag "dir") (newattr "class" "right_top"))
		(car (turnopenfile right_part))
	)
)

(defun opendir (name dir title css-table right_top)
	(foreveryfile (merge_files (dirfiles name)) dir title css-table right_top nil) 
)


;this function builds one file from post && comments
(defun merge_files (name)
	(create_pairs_post_com
		(create_correct_lists name "L" nil)
		(create_correct_lists name "C" nil)
		nil
	)
)

;function creates list of pairs (post . comments)
;post - list with post-files
;comments - list with comment-files
;res - resuls ((post1 . comment1) (post2 . comment2) ...)
(defun create_pairs_post_com (post comments res)
	(cond
		((null post) res)
		(t (create_pairs_post_com (cdr post) comments
			(cons (find_post_pair (cadr (dividestrsim (car post) #\-)) comments (car post)) res))
		)
	)
)

;function creates list of files with needed files
;l - list with files
;file_type - type of file (string)
;res - result list
;return value - list of files with needed files
(defun create_correct_lists (l file_type res)
	(cond
		((null l) res)
		((eql file_type (get_file_name (car l) #\- #\/))
			(create_correct_lists (cdr l) file_type (cons (car l) res))
		)
		(t (create_correct_lists (cdr l) file_type res))
	)
)

;function creates pair (name_post_file . name_comment_file)
;number_post - part after
;comment_list - list of comment's files names
;post - full post file's name
;returns dotted pair
(defun find_post_pair(number_post comment_list post)
	(cond
		((null comment_list) (cons post nil))
		((eql number_post (cadr (dividestrsim (car comment_list) #\-)))
			(cons post (car comment_list))
		)
		(t (find_post_pair number_post (cdr comment_list) post))
	)
)


;this function opens files && transform them
;it forms list of xml-tags which would become new xml-files
;result list = ( (name_1 . xml-tag) (name_2 . xml_tag) ... )
;name - list ((post1 . comments1) (post2 . comments2) ...)
(defun foreveryfile (l name title css-table right_top res)
	(cond
		((null l) res)
                (t (foreveryfile (cdr l) name title css-table right_top
			(cons (workwithfile (car l) name title css-table right_top) res))
		)
	)
)

;this function writes file in needed directory (new reformed file)
;name - name of file
;dir name of directory
;title - title of xml-documents
;css-table - name of css-table
;right_top - file with xml-content which will be written on pages
(defun workwithfile (name dir title css-table right_top)
	(cons (concatstr (concatstr (concatstr dir "/") (get_file_name (car name) #\. #\/)) ".html")
		(insert_body (begin_form_html_doc title css-table) (openfile name) right_top)
	)
)

;this function inserts "main-page" button && "up" button
;next - next page
;prev - previous page
(defun insert_buttons (tag prev next)
	(let ((form (inscont (insreadyattr (newtag "table") (newattr "class" "bottom_links"))
		(inscont(insreadyattr 
				(insreadyattr (newtag "td") (newattr "align" "left"))
			(newattr "width" "25%"))
			(inscont (insreadyattr (newtag "a") (newattr "href" "#")) "up")
		))))

	(cond
		((null prev) (form_with_next tag (inscont form (newtag "td")) next))
		(t (form_with_next tag 
				(inscont form
					(inscont (insreadyattr (insreadyattr (newtag "td") (newattr "width" "25%"))
						(newattr "align" "right"))
						(inscont
							(insreadyattr (newtag "a")
								(newattr "href" (get_file_name prev #\/ #\/)))
						"&#60;&#60; previous")
					)
				)
			next)
		)
	))
)

(defun form_with_next(tag form next)
	(let ((main (inscont (insreadyattr
			(insreadyattr (newtag "td") (newattr "align" "right"))
		(newattr "width" "25%"))
		(inscont (insreadyattr (newtag "a") (newattr "href" "main_page0.html")) "main page"))))
	(cond
		((null next) (inscont tag (inscont (inscont form (newtag "td")) main)))
		(t (inscont tag
			(inscont(inscont form
				(inscont (insreadyattr (insreadyattr (newtag "td") (newattr "width" "25%"))
					(newattr "align" "left"))
					(inscont
						(insreadyattr (newtag "a")
							(newattr "href" (get_file_name (car next) #\/ #\/)))
					"next &#62;&#62;")
				))
			main)
		))
	))
)

;this function gets name of needed file
;it cuts part of path && type of file
;name - name of file
;left - it's simbol from which type of file begins
;right - it'ssimbol which divides names of directories
;result - string
(defun get_file_name (name left right)
	(let ((name_list (dividestrsim name right)))
	(cond
		((null (cdr name_list))
			(car (dividestrsim (car name_list) left))
		)
		(t (get_file_name (cadr name_list) left right))
	))
)

;this function begins forming html-document
;title - it's content of needed title
;css-table - name of css-table
;return value - xml-tag <html> <head> <title> title </title> </head> <style> css-table</style> </html>
(defun begin_form_html_doc(title css-table)
	(inscont
		(insreadyattr (insreadyattr (insreadyattr (newtag "html")
			(newattr "xmlns" "http://www.w3.org/1999/xhtml"))
			(newattr "xml:lang" "ru")) (newattr "lang" "ru")
		) 
		(inscont (inscont (inscont (newtag "head")
			(insreadyattr (newtag "meta") (newattr "charset" "utf-8")))
			(inscont (newtag "title") title))
			(inscont (insreadyattr (newtag "style") (newattr "media" "all"))
				(concatstr (concatstr "@import " css-table) ";")
			)
		)
	)
)

;this function inserts body of html-document
;html - main tag of html-doc
;body - tag/list of tags which must be content of body tag
;right_top - file with xml-content which will be written on pages
;result - ready html-tree
(defun insert_body(html body right_top)
	(inscont html
		(inscont (insreadyattr (newtag "body") (newattr "class" "mainbody"))
			(inscont (newtag "table")
				(inscont (inscont (newtag "tr")
					(inscont (insreadyattr (newtag "td") (newattr "class" "left_colomn")) body))
					(inscont (insreadyattr (newtag "td") (newattr "class" "right_colomn"))
						right_top
					)
				)
			)
		)
	)
)

;this function creates main page on which will be saved references on
;blog's pages
;files - list of structures (date . (file_name user_name text xml)
;result - list of xml-tags which will be placed in tag body
(defun create_main_page(files)
	(cond
		((null files) nil)
		(t (cons
			(form_post_div (caar files) (cadar files) (caddar files) (cadddr (car files)))
		(create_main_page (cdr files))))
	)
)

;this function sorts dates
;dates - list of dates and other information
(defun sort_dates(dates)
	(cond
		((null dates) nil)
		(t (divide_dates dates))
	)
)

(defun get_middle(head tail)
	(cond
		((eql (length head) (length tail)) (cons (car tail) (append head (cdr tail))))
		((eql (length head) (- (length tail) 1)) (cons (car tail) (append head (cdr tail))))
		(t (get_middle (append head (cons (car tail) nil)) (cdr tail)))
	)
)

(defun divide_dates(dates)
	(let ((res (get_middle nil dates)))
		(get_parts (car res) (cdr res))
	)
)

(defun get_parts(middle dates)
	(let ((left (get_left_part (car middle) dates nil))
		(right (get_right_part (car middle) dates nil)))
	(cond
		((null right) (cons middle (sort_dates left)))
		(t (append (sort_dates right) (cons middle (sort_dates left))))
	))
)

(defun print_dates(dates)
	(cond
		((null dates) nil)
		(t (print (caar dates)) (print_dates (cdr dates)))
	)
) 

(defun get_right_part (middle dates res)
	(cond
		((null dates) res)
		((greaterdate (caar dates) middle)
			(get_right_part middle (cdr dates) (cons (car dates) res))
		)
		(t (get_right_part middle (cdr dates) res))
	)
)

(defun get_left_part (middle dates res)
	(cond
		((null dates) res)
		((or (greaterdate middle (caar dates)) (eql middle (caar dates)))
			(get_left_part middle (cdr dates) (cons (car dates) res))
		)
		(t (get_left_part middle (cdr dates) res))
        )
)

;this function gets information from files
;files - list of files from directory
;result - list of such elements (date . (file_name . user_name . text))
(defun get_dates(files)
	(cond
		((null files) nil)
		(t (get_dates_help files))
	)
)

;this is help function for function get_dates
;this function throw away all empty posts
;files - list of files from directory
;result - list of such elements (date . (file_name user_name text xml))
(defun get_dates_help(files)
	(let ((res (form_news_line (cadr (getcontent (cdar files))) (caar files) (cdar files))))
        (cond
                (res (cons res (get_dates (cdr files))))
                (t (get_dates (cdr files)))
        ))
)

;this function creates xml-tag <a href="file"> some text </a>
;file - file's name
;text - text of link
;result - xml-tag
(defun create_reference (text file)
	(let ((name (get_file_name file #\/ #\/)))	
	(inscont (insreadyattr (newtag "a") (newattr "href" name))
			text
	))
)

;this function starts creating main page of blog - content-page
;title - title of main page
;css-table - name of css-table
;name - list of xml-tags and file's names
;right_top - file with xml-content which will ritten on pages
;top_part - top part which will be on every page
;result - sorted_list
(defun main_page(title dir name css-table right_top top_part)
	(let ((sorted_list (sort_dates (get_dates name))))
	(form_news_pages (create_main_page sorted_list) 0 dir title css-table right_top top_part)
	(reverse (rebuild_list sorted_list nil)))
)

;this function forms news-pages
;l - list with news-posts
;page_num - page-number
(defun form_news_pages(l page_num dir title css-table right_top top_part)
	(cond
		((null l) l)
		(t (form_post_pages l 0 page_num nil dir title css-table right_top top_part))

	)
)

;this function will prints only 30 posts on one news-page
;l - list with news post
;numb - number of posts in current
(defun form_post_pages(l numb page_num res dir title css-table right_top top_part)
	(cond
		((eql numb 30) (printinfilexml
					(create_finaly_page (reverse res) page_num l title css-table right_top top_part)
					(concatstr (concatstr dir "/") (concatstr (strconcatnumb "main_page" page_num) ".html"))

					"w"
				)
				(form_news_pages l (+ 1 page_num) dir title css-table right_top top_part)
		)
		(t (form_post_pages (cdr l) (+ 1 numb) page_num (cons (car l) res) dir title css-table right_top top_part))
	)
)


;this function inserts buttons in our news-page
;l - list of news
;page_num - page number
;tail - "tail" posts
(defun create_finaly_page(l page_num tail title css-table right_top top_part)
	(let ((main (insert_top_part 
		(insert_body (begin_form_html_doc title css-table)
			(inscont (insreadyattr (newtag "div") (newattr "id" "wrapper"))l )
		right_top)
	top_part)))
	(cond
		((eql page_num 0) (insert_buttons main nil (cons (concatstr (strconcatnumb "main_page" (+ 1 page_num)) ".html") nil)))
		((null tail) (insert_buttons main (concatstr (strconcatnumb "main_page" (- page_num 1)) ".html") nil))
		(t (insert_buttons main (concatstr (strconcatnumb "main_page" (- page_num 1)) ".html")
			(cons (concatstr (strconcatnumb "main_page" (+ 1 page_num)) ".html") nil))
		)
	))
)	

;this function will rebuld sorted list - only file names in correct order
;l - current list
;res - return value
(defun rebuild_list(l res)
	(cond
		((null l) res)
		(t (rebuild_list (cdr l) (cons (cadar l) res)))
	)
)

;this function forms news-line
;xml - it's file's content
;file - name of file
(defun form_news_line (xml file xml_file)
	(let ((post (find_xml_with_attr xml "class" "post_table")))
	(cond
		((null post) nil)
		(t (test_correct_post (getcontent post) file xml_file))
	))
)

;test on correct formed post
(defun test_correct_post(post file xml)
	(cond
		((null post) nil)
		(t (form_news_post post file xml))
	)
)

;this function forms news-post
;post - content of xml-tag with post
;file - file name
;result - (date . (file_name user_name text xml))
(defun form_news_post (post file xml)
	(let ((user_name (car (getcontent (cadr (getcontent (car (getcontent (car post))))))))
		(date (createdate (cadr (getcontent (cadr (getcontent (car post)))))))
		(text (car (getcontent (cadr post)))))
	(printinfilexml xml file "w")
	(cons date (cons file (cons user_name (cons text nil)))))
)

;this function forms xml-tag for news-line
;user_name - name of user which had written this post
;date - date of post
;text - text of post
;file - name of file
;result - xml-tag
(defun form_post_div (date file user_name text)
	(inscont
		(insreadyattr (newtag "table") (newattr "class" "news_post"))
		(inscont (newtag "tbody")
		(inscont (newtag "tr")
		(inscont
			(inscont
				(inscont (newtag "td")
		(inscont (insreadyattr (newtag "h3") (newattr "class" "news_header"))
			(cons "User "
				(cons (insreadyattr (inscont (newtag "font") user_name)
								(newattr "class" "news_user"))
					(cons (concatstr " wrote on " (datetostring date)) nil)
				)
			)
		))
		(inscont(insreadyattr (newtag "p") (newattr "class" "news_text"))text))
		(inscont(insreadyattr (newtag "div")(newattr "class" "news_links"))
			(inscont (insreadyattr (newtag "ul") (newattr "class" "news_links_list"))
				(inscont (newtag "li")
					(create_reference "Read more..." file)
				)
			)
		))
		)))
)

;this function finds tag with needed attribute
;xml - it's xml tag/string/list of xml-tags/nil
;attr - name of attr which is needed
;attr_cont - needed content of attribute
;result - xml-tag/nil
(defun find_xml_with_attr(xml attr attr_cont)
	(cond
		((null xml) nil)
		((isstring xml) nil)
		((and (isxmltag xml) (hasneededattr (xmlattrget xml) attr attr_cont)) xml)
		((isxmltag xml) (find_xml_with_attr (getcontent xml) attr attr_cont))
		(t (find_xml_with_attr_help xml attr attr_cont))
	)
)

;help function for function find_xml_with_attr
;xml - it's list of xml tag
;attr - name of attr which is needed
;attr_cont - needed content of attribute
;result - xml-tag/nil
(defun find_xml_with_attr_help(xml attr attr_cont)
	(let  ((needed (find_xml_with_attr (car xml) attr attr_cont)))
	(cond
		((null needed) (find_xml_with_attr (cdr xml) attr attr_cont))
		(t needed)
	))
)

;xml - list of xml-attributes
(defun hasneededattr(xml attr attr_cont)
	(cond
		((null xml) nil)
		((and
			(eql (getattrname (car xml)) attr)
			(eql (getattrcontent (car xml)) attr_cont)) t
		)
		(t (hasneededattr (cdr xml) attr attr_cont))
	)
)

;this function will work with pair (post-file . comment-file)
;it finds the first comment && makes correct caption of page
(defun openfile (name)	
	(change_lj (delete_empty_tables (changeamponent(chattrcon (deletetag 
		(findhidetags (change_wrong_tag (reformxml (getfirststep (bodyback (workwithid (create_one_file name))))
		(insreadyattr (newtag "div") (newattr "id" "wrapper")))))
	"delete_tag")))))
)

;function changes lj tags
;and <img/> <br clear="all"/>
(defun change_lj(xml)
	(cond
		((null xml) nil)
		((isstring xml) xml)
		((and (isxmltag xml) (eql "lj" (xmlnameget xml)))
			(newcontent (insreadyattr (newtag "font") (newattr "class" "lj-nick"))
				(find_attr (xmlattrget xml) "user")
			)
		)
		((and (isxmltag xml) (eql "img" (xmlnameget xml)))
                        (cons xml
                                (insreadyattr (newtag "br") (newattr "clear" "all"))
                        )
                )
		((isxmltag xml)(newcontent xml (change_lj (getcontent xml))))
		(t (cons (change_lj (car xml)) (change_lj (cdr xml))))
	)
)

;function finds attr with needed name
(defun find_attr(attr name)
	(cond
		((null attr) name)
		((eql (getattrname (car attr)) name) (getattrcontent (car attr)))
		(t (find_attr (cdr attr) name))
	)
)

(defun create_one_file(name)
	(print name)
	(cond
		((null (cdr name))(inscont (newtag "great-tag")
			(rename_event (car (turnopenfile (car name)))))
		)
		(t (inscont (newtag "great-tag")
			(cons (rename_event (car (turnopenfile (car name)))) (turnopenfile (cdr name))))
		)
	)
)

;this function renames first event-tag
(defun rename_event(xml)
	(cond
		((and (isxmltag xml) (eql "event" (xmlnameget xml)))
			(inscont (newtag "main_event") (getcontent xml))
		)
		(t (print "Error!: no event-tag in post file"))
	)
)

(defun delete_empty_tables(xml)
	(cons (newcontent xml (get_not_empty (getcontent  xml))) nil)
)

(defun get_not_empty (xml)
	(cond
		((null xml) nil)
		((and (eql "div" (xmlnameget (car xml))) (null (cdr (getcontent (car (getcontent (car xml)))))))
			(get_not_empty (cdr xml))
		)
		(t (cons (car xml) (get_not_empty (cdr xml))))
	)
)

;stark work with id of posts
(defun workwithid (l)
	(beginchangepos
		l (correctdir (changepairs (analistd l nil) nil) l)
	)
)


(defun beginchangepos (l pair)
	(inshelppart (insertcomdep (startsort l pair) pair))
)

;we must change all '\n' to <br>
(defun insertbr (xml)
	(cond
		((null xml) nil)
		((isstring xml) (convertstr xml))
		((isxmltag xml) (newcontent xml (insertbr (getcontent xml))))
		(t (cons (insertbr (car xml)) (insertbr (cdr xml))))
	)
)

(defun convertstr (str)
	(transformstr str)
)

;here we will insert help tags
(defun inshelppart (l)
	(cond
                ((null l) nil)
                ((isstring l) l)
                ((isxmltag l)
                        (cond
                                ((eql (xmlnameget l) "comment") l )
                                (t (newcontent l (inshelppart (getcontent l))))
                        )
                )
                (t (cons (inshelppart (car l)) (inshelppart (cdr l))))
        )
)
	

;here we will start sort comments accord their ids
(defun startsort (l pairs)
	(cond
		((null l) nil)
		((isstring l) l)
		((isxmltag l) (startsost_help l pairs))
		(t (cons (startsort (car l) pairs)(startsort (cdr l) pairs)))
	)
)

(defun startsost_help (l pairs)
	(cond
		((eql (xmlnameget l) "main_event") l)
		((eql (xmlnameget l) "comments")
			(newcontent l (sort_comments (getcontent l) (cdr pairs) nil))
		)
		(t (newcontent l (startsort (getcontent l) pairs)))
	)
)

;here we have only "comment" - co we must get their ids and find same in pairs
(defun sort_comments (l pair res)
	(cond
		((null pair) (reverse res))
		(t (sort_comments l (cdr pair) (placecomment (cdar pair) l res)))
	)
)

(defun placecomment (num l res)
	(cond
		((null l) res)
		((eql (+ 1 (getid (car l))) num) (cons (car l) res))
		(t (placecomment num (cdr l) res))
	)
)

;here we will get id of comment
(defun getid (l)
	(cond
                ((null l) nil)
                ((isstring l) nil)
                ((isxmltag l)
                        (cond
                                ((eql (xmlnameget l) "id")
                                        (numbfromstr (car (getcontent l))))
                                (t (getid (getcontent l)))
                        )
                )
                (t (cond
			((null (getid (car l))) (getid (cdr l)))
			(t (getid(car l))))
		)
	)
)

(defun correctdir (l xml)
	(funcforstep (startinsert (withzeros l nil nil)))
)

(defun startinsert (l)
	(level (toparents (car l) (cdr l)))
)

(defun level (l)
	(cond
		((mustrepeat (reverse l)) (onelevel l nil))
		(t (samelevel (divlist l nil)))
	)
)

(defun samelevel (l)
	(startinsert (cons (turnlistpair (car l) nil) (cdr l)))
)

(defun divlist (l res1)
	(cond
		((null l) (cons res1 (cons nil nil))) 
		((listp (caar l)) (divlist (cdr l) (append res1 (car l))))
		(t (cons res1  l))
	)
)

(defun mustrepeat (l)
	(cond
		((listp (caar l)) t)
		(nil nil)
	)
)

(defun turnlistpair (l res)
	(cond
		((null l) (reverse res))
		(t (turnlistpair (cdr l) (cons (cons (car l) nil) res)))
	)
)

(defun onelevel (l res)
	(cond
		((null l) res)
		(t (onelevel (cdr l) (append res (onelevel1 (car l) nil))))
	)
)

(defun onelevel1 (l res)
	(cond
		((null l) res)
		(t (cons (car l) (onelevel1 (cdr l) res)))
	)
)

(defun toparents (l1 l2)
	(cond
		((null l2) l1)
		(t (toparents (instopar (car l2) l1 nil) (cdr l2)))
	)
)

(defun instopar (pair l res)
	(cond
		((null l) (append res (cons pair nil)))
		((atom (caar l)) (append (append res (cons pair nil)) l))
		((eql (car pair) (cdaar l)) 
			(cond
				((null res) (cons (append (car l) (cons pair nil)) (cdr l)))
				((null (cdr l)) (append res (cons (append (car l) (cons pair nil)) nil)))
				(t (append (append res (cons (append (car l) (cons pair nil)) nil))(cdr l)))
			)
		)
		(t (instopar pair (cdr l) (append res (cons (car l) nil))))
	)
)

;here we will make correct direction for ids and their parents
(defun withzeros (l res1 res2)
	(cond
		((null l) (cons (reverse res1) (reverse res2)))
		((eql (caar l) 0) (withzeros (cdr l) (append (cons (cons (car l) nil) nil) res1) res2))
		(t (withzeros (cdr l) res1 (cons (car l) res2)))
	)
)

;for every "comment" we must insert depth for step
(defun insertcomdep (xml l)
	(cond
		((null xml) nil)
		((isstring xml) xml)
		((isxmltag xml) (insertcomdep_help xml l))
		(t (cons (insertcomdep (car xml) l) (insertcomdep (cdr xml) l)))
	)
)

(defun insertcomdep_help (xml l)
	(cond
		((eql (xmlnameget xml) "main_event")
			(insreadyattr xml (newattr "style" (attrmargin  (caar l))))
		)
		((eql (xmlnameget xml) "comments")
			(newcontent xml (inputstep (getcontent xml) (cdr l) nil))
		)
		(t (newcontent xml (insertcomdep (getcontent xml) l)))
	)
)

(defun inputstep (xml l res)
	(cond
		((null l) res)
		(t (inputstep (cdr xml) (cdr l) (append res 
		(cons (insreadyattr (car xml) (newattr "style" (attrmargin (caar l)))) nil))))
	)
)

(defun attrmargin (numb)
	(concatstr "margin-left:" (concatstr (strfromnumb (* 10 numb)) "px"))
)

(defun funcforstep (l)
	(getstep l l)
)

;here we will get steps for ansvers on posts
(defun getstep (l save)
	(cond
		((null l) save)
		(t (getstep (cdr l) (givestep (car l) save)))
	)
)

(defun givestep (pair l)
	(cond
		((eql (car pair) 0) l)
		(t (replace (getdepth (car pair) l) (cdr pair) l nil))
	)
)

(defun replace (n1 n2 l res)
	(cond
		((null l) res)
		((eql n2 (cdar l)) (append res (cons (cons n1 n2) (cdr l))))
		(t (replace n1 n2 (cdr l) (append res (cons (car l) nil))))
	)
)

(defun getdepth (numb l)
	(cond
		((null l) 0)
		((eql numb (cdar l)) (+ 1 (caar l)))
		(t (getdepth numb (cdr l)))
	)
)

;here we will change pairs (strings -> integer)
(defun changepairs (l res)
	(cond
		((null l) res)
		(t (changepairs (cdr l)(cons (changepairs_help (car l)) res)))
	)
)

(defun changepairs_help (pair)
	(cond
		((and (null (car pair)) (null (cdr pair))) (cons 0 1))
		((null (car pair)) (cons 1 (+ 1 (numbfromstr (cdr pair)))))
		(t (cons (+ 1 (numbfromstr (car pair))) (+ 1 (numbfromstr (cdr pair)))))
	)
)

;here will look at "td" and look for empty parentid
(defun analistd (xml res)
	(cond
		((null xml) res)
		((isstring xml) res)
		((isxmltag xml)
			(cond
				((eql (xmlnameget xml) "comment")
					(cons (getpair (getcontent xml)) res)
				)
				((eql (xmlnameget xml) "main_event") 
					(cons (cons nil nil) res)
				)
				(t (analistd (getcontent xml) res))
			)
		)
		(t (analistd (cdr xml) (analistd(car xml) res)))
	)
)

;here we will get pair - parentid and id
(defun getpair (xml)
	(cond
		((null xml) nil)
		((isxmltag (car xml))
			(cond
				((eql (xmlnameget (car xml)) "parentid")
					(cons (car (getcontent (car xml))) (getpair (cdr xml)))
				)
				((eql (xmlnameget (car xml)) "id")
					(car (getcontent (car xml)))
				)
				(t (getpair (cdr xml)))
			)
		)
		(t (getpair (cdr xml)))
	)
)				

;here we will start to make changes with text - put all bodies back
;all text is in tables - so we will put body in separate cell
(defun bodyback (text)
	(cond
		((null text) nil)
		((isstring text)  text)
		((isxmltag text) (bodyback_help text))
		(t (body_back text))
	)
)

(defun bodyback_help (text)
	(let ((res (div2cells (bodyback (textback (getcontent text))))))
	(cond
		((null res) nil)
		(t (newcontent text res))
	))
)

(defun body_back (text)
	(let ((head (bodyback (car text))))
	(cond
		((null head) (bodyback (cdr text)))
		(t (cons head (bodyback (cdr text))))
	))
)

;here we will put all text back
(defun textback (text)
	(cond
		((null text) nil)
		((null (car text)) nil)
		((isstring (car text)) (cons (car text) (textback (cdr text))))
		((eql (xmlnameget (car text)) "body")
			(append (textback (cdr text)) (cons (car text) nil)))
		((eql (xmlnameget (car text)) "state")
                        (append (textback (cdr text)) (cons (car text) nil)))
		((eql (xmlnameget (car text)) "main_event")
			(cons (workwithevent (car text) (getcontent (car text))) (textback (cdr text)))
		)
		(t (cons (car text) (textback (cdr text))))
	)
)

(defun workwithevent(xml l)
	(let ((time (findtag l "eventtime")) (user (create_user_url (findtag l "url")))
		(post (findtag l "event")) (subject (findtag l "subject")))
	(newcontent xml 
		(cons (newcontent (insreadyattr (newtag "td") (newattr "class" "nick_cell"))
			 (cons user (cons time (cons subject nil))))
		(cons (inscont (insreadyattr (newtag "td") (newattr "class" "post_cell")) post) nil))
	))
)

(defun create_user_url (user)
	(cond
		((null user) nil)
		((eql (xmlnameget user) "url")
			(newcontent user
			(inscont
				(insreadyattr (newtag "a") (newattr "href" (car (getcontent user))))
				"croco"
			))
		)
		(t (print "Error!: no url in event-tag"))
	)
)
			
(defun findtag (xml name)
	(cond
		((null xml) nil)
		((isstring xml) nil)
		((and (isxmltag xml) (eql (xmlnameget xml) name)) xml)
		((isxmltag xml) (findtag (getcontent xml) name))
		(t (findtag_help xml name))
	)
)

(defun findtag_help(xml name)
	(let ((res (findtag (car xml) name)))
	(cond
		((null res) (findtag (cdr xml) name))
		(t res)
	))
)

(defun div2cells (xml)
	(separlast (reverse xml))
)

(defun putincells (p nick_cell)
	(let ((nick (inscont (insreadyattr (newtag "td") (newattr "class" "nick_cell")) nick_cell))
		(post (inscont (insreadyattr (newtag "td") (newattr "class" "post_cell")) p))
		(post_s (inscont (insreadyattr (newtag "td") (newattr "class" "post_cell")) "D")))
	(cond
		((null p) (cons nick (cons post_s nil)))
		(t (cons nick (cons post nil)))
	))
)

;get last elem from list
(defun separlast (xml)
	(cond
		((and (isxmltag (car xml)) (eql (xmlnameget (car xml)) "main_event"))
			(cons (car xml) (separlast (cdr xml)))
		)
		((isxmltag (car xml)) (separlast_help xml))
		(t (reverse xml))
	)
)

(defun separlast_help (xml)
	(cond
		((eql (xmlnameget (car xml)) "body")
			(putincells (car xml) (reverse (cddr xml)))
		)
		(t (reverse xml))
	)	
)

;must we separate them?

;betweenfunction - 1st step in transform
(defun getfirststep (xml)
	(transource
		(getcontent xml)(listattrs (newtag (xmlnameget xml))
		(xmlattrget xml)) nil
	)
)

(defun change_wrong_tag (xml)
	(cond
		((null xml) nil)
		((isstring xml)
			(changeent "&lt;/B&gt;" "&lt;/b&gt;" (changeent "&lt;/I&gt;" "&lt;/i&gt;"
			(changeent "&lt;/I&bt;" "&lt;/i&bt;" (changeent "&lt;FORM" "&lt;form"
			(changeent "lj-cut text\"" "lj-cut text=\"" (changeent "&lt;B&gt;" "&lt;b&gt;"
			(changeent "&quot;" "\""(changeent "&lt;Tr&gt;" "&lt;tr&gt;"
			(changeent "&lt;TD&gt;" "&lt;td&gt;" (changeent "&lt;TR&gt;" "&lt;tr&gt;"
				(changeent "0 //" "ex0" (changeent "\"mind" "text=\"mind" xml))))))))))))
		)
		((atom xml) (newcontent (turnattr xml (xmlattrget xml))
                                (change_wrong_tag (getcontent xml)))
                )
                (t (cons (change_wrong_tag (car xml)) (change_wrong_tag (cdr xml))))
	)
)

;here we will star function, which will find all hidden tags
(defun findhidetags (xml)
	(getnewstr (getcontent xml)
	(newattrlist (newtag (xmlnameget xml)) (xmlattrget xml)))
)

;here we will change a little source form
;all text must be transform
(defun transource (xml newone save)
	(cond
		((null xml) newone)
		((isstring xml) (inscont newone (modify xml save)))
		((atom xml) (inscont newone (transource (getcontent xml)
			(listattrs (newtag (xmlnameget xml)) (xmlattrget xml))
			(xmlnameget  xml))))
		(t (transource (cdr xml) (transource (car xml) newone save) save))
	)
)

;here we will get list of attrs
(defun chattrcon (xml)
	(cond
		((null xml) nil)
		((isstring xml)
			(changeent "&quot;" "&#34;"(changeent "&trade;" "&#8482;"
			(changeent "&nbsp;" "&#160;"(changeent "&mdash;" "&#151;"(changeent "&copy;" "&#169;"
			(changeent "&ndash;" "&#8211;"(changeent "&raquo;" "&#187;"
			(changeent "&laquo;" "&#171;" (changeent "&amp;" #\& xml))))))))))
		((atom xml) (newcontent (turnattr xml (xmlattrget xml))
				(chattrcon (getcontent xml)))
		)
		(t (cons (chattrcon (car xml)) (chattrcon (cdr xml))))
	)
)

;this function will change & on &amp;
(defun changeamponent(xml)
	(cond
		((null xml) nil)
		((isstring xml) (changeamp xml))
		((isxmltag xml) (newcontent xml (changeamponent (getcontent xml))))
		(t (cons (changeamponent (car xml)) (changeamponent (cdr xml))))
	)
)

;here we will start transform attrsnewattrlist
(defun turnattr (xml attrlist)
	(newattrlist xml (transattr attrlist nil))
)

;transform hidden " in content of attributes
(defun transattr (attrl res)
	(cond
		((null attrl) res)
		(t (transattr (cdr attrl) (convattribute (car attrl) res)))
	)
)

;change attribute content
(defun convattribute (attr res)
	(cons (insattrcont attr (delsimb #\" (changeent "&quot;" #\" (getattrcontent attr)))) res)
)

;here we will get name of xml tag and find out what to do with it's content
(defun modify (xml name)
	(cond
		((eql name "date") (getdata1  xml))
		(t xml)
	)
)

;here we get data
(defun getdata1 (cont)
	(getdata2 (dividestrsim cont #\T))
)

;here we get time
(defun getdata2 (cont)
	(cons (car cont) (cons (car (dividestrsim(cadr cont) #\Z)) nil))
)

;divide in two parts
(defun gethtmltag (name attr mean attr1 mean1 attr2 mean2)
	(insreadyattr
		(insreadyattr
			(insreadyattr (newtag name) (newattr attr mean))
		(newattr attr1 mean1))
	(newattr attr2 mean2))
)

(defun find_depth (xml maxd)
	(cond
		((null xml) (- maxd 1))
		((isstring xml) maxd)
		((atom xml)
			(find_depth (getcontent xml) (+ 1 maxd))
		)
		(t (maxfunc (find_depth (car xml) maxd) (find_depth (cdr xml) maxd)))
	)
)

(defun maxfunc (x y) 
	(cond 
		((< x y) y)
		(t x)
	)
)

(defun max_depth (xml) (find_depth xml 0))

;here will transform our document
(defun reformxml (xml newone)
	(cond
		((null xml) newone)
		((isstring xml) (inscont newone xml))
		((atom xml) (convtag xml newone))
		(t (reformxml (cdr xml) (reformxml (car xml) newone)))
	)
)

;convert tag
(defun convtag (xml newone)
	(ifnnullinsert xml (workwithname (xmlnameget xml)) newone)
)

;here we will insert attrs
;we need separate function, because we must control that all
;parts of list are really attributes
(defun listattrs (xml attrs)
	(cond
		((null attrs) xml)
		(t (listattrs (insreadyattr xml (car attrs)) (cdr attrs)))
	)
)

;if get tag isn't empty - i'll insert it in during
(defun ifnnullinsert (xml get newone)
	(cond
		((null get) (reformxml (getcontent xml) newone))
		((and (eql (xmlnameget get) "div")
			(and (isxmltag(car (getcontent get)))(eql (xmlnameget (car (getcontent get))) "table")))
			(listattrs get (xmlattrget xml))
			(reformxml (getcontent xml)(car (getcontent get)))
			(inscont newone get)
		)
		(t (inscont  newone (reformxml (getcontent xml) (listattrs get (xmlattrget xml)))))
	)
)

;if we have special name of tag - we will convert it in special way
(defun workwithname (name)
	(cond
		((eql "body" name) (newtag "basefont"))
		((eql "date" name)(insreadyattr 
					(inscont (newtag "div") "date: ")
				(newattr "class" "date"))
		)
		((eql "user" name) (inscont (newtag "div")
			(insreadyattr (inscont (newtag "font") "user: ") (newattr "class" "user")))
		)
		((eql "state" name) (newtag "delete_tag"))
		((eql "font" name) (newtag "font"))
		((eql "td" name) (newtag "td"))
		((eql "tr" name) (newtag "tr"))
		((eql "br" name) (newtag "br"))
		((eql "parentid" name) (newtag "delete_tag"))
		((eql "id" name) (newtag "delete_tag"))
	((eql "subject" name) (inscont
				(insreadyattr  (newtag "div") (newattr "class" "status_comment"))
				"theme: ")
		)
		((eql "comment" name)(inscont (newtag "div")
			(insreadyattr (newtag "table") (newattr "class" "post_table")))
		)
		((eql "comments" name) nil)
		((eql "great-tag" name) nil)
		((eql "main_event" name)(inscont (newtag "div")
			(insreadyattr (newtag "table") (newattr "class" "post_table")))
		)
		((eql "anum" name) (newtag "delete_tag"))
		((eql "event" name) (newtag "basefont"))
		((eql "eventtime" name) (insreadyattr
                                        (inscont (newtag "div") "date ")
                                (newattr "class" "date"))
		)
		((eql "props" name) (newtag "delete_tag"))
		((eql "url" name)(inscont (newtag "div")
                        (insreadyattr (inscont (newtag "font") "user: ") (newattr "class" "user")))
		)
		((eql "a" name) (newtag "a"))
		((eql "itemid" name) (newtag "delete_tag"))
		(t nil)
	)
)

;here we will delete tag
(defun deletetag (xml name)
	(cond
		((null xml) nil)
		((isstring xml)
			(cond
				((eql xml name) nil)
				(t xml)
			)
		)
		((atom xml)
			(cond
				((eql (xmlnameget xml) name) nil)
				(t (newcontent xml (deletetag (getcontent xml) name)))
			)
		)
		(t
			(cond
				((deletetag (car xml) name) (cons (car xml) (deletetag (cdr xml) name)))
				(t (deletetag (cdr xml) name))
			)
		)
	)
)

(defun getnewstr (xml newone)
	(cond
		((null xml) newone)
		((isstring xml) (changecont newone (parse (cons xml nil) -1)))
		((atom xml) (inscont newone (getnewstr (getcontent xml)
			(listattrs (newtag (xmlnameget xml))(xmlattrget xml))))
		)
		(t (getnewstr (cdr xml) (getnewstr (car xml) newone)))
	)
)
			
;in this function we will insert all new content
;we make separete function, beacuse we will get a list of tags and strings
(defun changecont (res l1)
	(cond
		((null l1) res)
		(t (changecont (inscont res (car l1)) (cdr l1)))
	)
)

;here we will parse strings
(defun parse (line depth)
	(func1 (append (divideline (car line) nil) (cdr line)) depth)
)

;it's middle function. we will stop our transformations when depth stops change
(defun func1 (line depth)
	(cond
		((eql depth (max_depth line)) line)
		(t (parse line (max_depth line)))
	)
)

(defun divideline (head tail)
	(cond
		((null head) tail)
		(t (func2 (parstags head tail)))
	)
)

;middle function in which we start next pars
(defun func2 (l)
	(divideline (car l)(cdr l))
)
