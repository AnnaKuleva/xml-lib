(%%%
	(MODULE-NAME "serverlisp")
	(DECLARE-PUBLIC begin)
	(DECLARE-USED-MODULE "../xml/symb.h")
	(DECLARE-EXTERNAL makelisten)
	(DECLARE-EXTERNAL createservstruct)
	(DECLARE-EXTERNAL iswork)
	(DECLARE-EXTERNAL prepearforms)
	(DECLARE-EXTERNAL userhastosay)
	(DECLARE-EXTERNAL getmessages)
	(DECLARE-EXTERNAL haveapplications)
	(DECLARE-EXTERNAL acceptusers)
	(DECLARE-EXTERNAL getreadyfornext)
	(DECLARE-EXTERNAL arrayofusers)
	(DECLARE-EXTERNAL getturner)
	(DECLARE-EXTERNAL xmlnameget)
	(DECLARE-EXTERNAL xmlattrget)
	(DECLARE-EXTERNAL getattrname)
)

;it's first function - here we create base for server
(defun begin (port numbus)
	(workcycle (makelisten (createservstruct port numbus)) nil)
)

;the main work cycle for server
;in which we will prepare server for work
;search for information
;and accept users if we could
(defun workcycle (serv list1)
	(print "workcycle")
	(print (arrayofusers serv))
	(cond
		((eql (iswork serv) 1) (secondstep (prepearforms serv) list1))
		(t (print "getreadyfornext")(workcycle (getreadyfornext serv) nil))
	)
)

;this is cycle, in which we learn - have users something to say
(defun readusers (list1 serv list2)
	(cond
		((null list1) (thirdstep (haveapplications serv) serv list2))
		(t(returntoread (cdr list1) (getcorrecturn serv (car list1) list2)))
	)
)

;here we must divide some parametrs
(defun returntoread (list1 res)
	(readusers list1 (car res) (cdr res))
)

;in this func we get message from user and now we must know
;ended this user conversation or not
(defun getcorrecturn (serv pair list1)
	(print "getcorrecturn")
        (print serv)
	(print pair)
	(changelist (car pair) (getmessages serv (car pair) (cdr pair)) list1)
)

;here we will analis ready tree of every user
(defun analistree (name xml tree)
	(print "analistree")
	(cond
		((eql name "stream") (analisfirst xml tree))
	)
)

;here we will analis tag stream
(defun analisfirst (xml tree)
	(print "analisfirst")
	(print xml)
	(print tree)
	(findnameattr (xmlattrget xml))
)

;here we will find attr where user declare his name
(defun findnameattr (list1)
	(cond
		((null list1) (prin1 "forget his name"))
		((eql (getmaneattr (car list1)) "name") (car list1))
		(t (findnameattr (cdr list1)))
	)
)

;this function will help us in finding needed tag
(defun correctname (list1 name)
	(cond
		((null list1) nil)
		((isstring (turnisstring list1)) nil)
		((atom list1)(cond
			((eql (xmlnameget  list1) name) list1)
			(t (correctname (getcontent (turningetcon list1)) name))
		))
		(t (findinfistpart (correctname (car list1) name) (cdr list1) name))
	)
)

;this function will be good for finding true answer 
;is it needed tag
(defun findinfirstpart (res list1 name)
	(cond
		(res res)
		(t (correctname list1 name))
	)
)

;if first part of list is -1 - users hase gone
;second is new turner
;third is changed serv
(defun changelist (disk res list1)
	(print "changelist")
	(print res)
	(cond
		((eql -1 (car res)) (cons (third res) list1))
		(t (cons (third res) (cons (cons disk (second res)) list1)))
	)
)

;we must search for information from users
;buf have we someboy?
(defun secondstep (serv list1)
	(print "in second")
	(print list1)
	(cond
		((eql (userhastosay serv) 1) (readusers list1 serv nil))
		(t (thirdstep (haveapplications serv) serv list1))
	)
)

;here we will learn about - need we accept somebody
(defun thirdstep (res serv list1)
	(prin1 "in third")
	(cond
		((eql res 1) (getit (acceptusers serv) list1))
		(t (workcycle serv list1))
	)
)

;we need new condition of our server so we get a pair answer and serv
(defun getit (res list1)
	(workcycle (cadr res) (haveaccepted (car res) list1))
)

;if we have smebody for accept, but can't do it because have no places servpart
;will give us -1
(defun haveaccepted (disk list1)
	(cond
		((eql -1 disk) list1)
		(t (insertuser disk list1))
	)
)

;here we will change list of users
(defun insertuser (disk list1)
	(cons (cons disk (getturner disk)) list1)
)

;here we will delete user that have gone
(defun deleteusers (disk list1 res)
	(cond
		((null list1) (prin1 "somth strange in deleteusers - no such user") res)
		((eql disk (caar list1)) (append (cdr list1) res))
		(t (deleteusers disk (cdr list1) (cons (car list1) res)))
	)
)


