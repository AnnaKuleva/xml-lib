#include <intelib/sexpress/sexpress.hpp>
#include <intelib/sexpress/iexcept.hpp>
#include <intelib/sexpress/sstring.hpp>
#include <intelib/tools/sstream.hpp>
#include <intelib/lisp/lisp.hpp>
#include <intelib/lisp/lsymbol.hpp>
#include <intelib/lfun_std.hpp>

//#include "../xml/declare_functions.h"
//#include "../xml/declare_serv_functions.h"
#include "../xml/servforlisp.h"
#include "../xml/lispint.h"
#include "serverlisp.hxx"
#include "../xml/mistake.h"

int main(){
	LListConstructor L;
	int port=9999;
	try{
		printf("here will start server's work\n");
		LispInit_serverlisp();
		define_serv_functions();
        	define_attr_functions();
        	define_xml_functions();	
		SStreamStdout lstdout;
		(L|BEGIN, "9999", "2").Evaluate();
		printf("work is ended\n");
	}
	catch(IntelibX &x) {
        	printf("\nCaught IntelibX: %s\n", x.Description() );
        	if(x.Parameter().GetPtr()) {
            		printf("%s\n", x.Parameter()->TextRepresentation().c_str());
        	}
        	if(x.Stack().GetPtr()) {
            		printf("%s\n", x.Stack()->TextRepresentation().c_str());
        	}
	}
	catch(DontEnd &b){
                b.print();
        }
        catch(TurnMistake &b){
                b.sendmes(port);
                b.print();
        }
        catch(BadSimbol &b){
                b.sendmes(port);
                b.print();
        }
        catch(ErrorInForming &b){
                b.sendmes(port);
                b.print();
        }
        catch(EmptyListOfLex &b){
                b.sendmes(port);
                b.print();
        }
        catch(Disbalance &b){
                b.sendmes(port);
                b.print();
        }
        catch(ServPartErrors &b){
                b.print();
                b.print();
        }
	catch(const char *s){
		printf("%s\n", s);
	}
	catch(...) {
		printf("Something strange caught\n");
	}
	return 0;
}

