(%%%
        (MODULE-NAME "serv_ex")
        (DECLARE-PUBLIC begin)
        (DECLARE-USED-MODULE "../xml/symb.h")
        (DECLARE-EXTERNAL makelisten)
        (DECLARE-EXTERNAL createservstruct)
        (DECLARE-EXTERNAL iswork)
        (DECLARE-EXTERNAL prepearforms)
        (DECLARE-EXTERNAL userhastosay)
        (DECLARE-EXTERNAL getmessages)
        (DECLARE-EXTERNAL haveapplications)
        (DECLARE-EXTERNAL acceptusers)
        (DECLARE-EXTERNAL getreadyfornext)
        (DECLARE-EXTERNAL arrayofusers)
        (DECLARE-EXTERNAL getturner)
        (DECLARE-EXTERNAL xmlnameget)
        (DECLARE-EXTERNAL xmlattrget)
        (DECLARE-EXTERNAL getattrname)
)

;это стартовая функция, которая на вход получает
;номер порта и количество пользователей, обслуживаемых сервером
;она создает экзмепляр класса SExpressionMakeSocket
;и запускает функцию, реализующую основной рабочий цикл
(defun begin (port numbus)
  (workcycle (makelisten (createservstruct port numbus)) nil)
)

;эта функция реализует основной цикл программы
;получет на вход два параметра:
;экзмепляр класса SExpressionMakeSocket и список пользователей
;если сервер в рабочем состоянии, то отправляется в функцию secondstep
;иначе - начинаем новую сессию
(defun workcycle (serv list1)
  (cond
    ((eql (iswork serv) 1) (secondstep (prepearforms serv) list1))
    (t (workcycle (getreadyfornext serv) nil))
  )
)

;эта функция принимает данные от пользователей, приславших сообщения
;на вход она получает:
;экзмепляр класса SExpressionMakeSocket и список существующий пользователей
;если приславших сообщения пользователей нет,
;то делается переход в функцию thirdstep
(defun secondstep (serv list1)
  (cond
    ((eql (userhastosay serv) 1) (readusers list1 serv nil))
    (t (thirdstep (haveapplications serv) serv list1))
  )
)

;функция просматривает всех пользователей на предмет полученных сообщений
;на вход она получает: список существующих пользователей
;экзмепляр класса SExpressionMakeSocket и пустой спикок
;третий параметр является нкапливающим - если какой-то из пользователей
;завершил свою работу, то на его нужно удалить из существующих пользователей
;после проверки всех, делается переход в функцию thirdstep
(defun readusers (list1 serv list2)
  (cond
    ((null list1) (thirdstep (haveapplications serv) serv list2))
    (t (returntoread (cdr list1) (getcorrecturn serv (car list1) list2)))
  )
)

;это вспомогательная функция для функции readusers
;она получает на вход:
;список из непросмотренных пользователей и
;точечную пару из экзмепляра класса SExpressionMakeSocket и
;нового списка пользователей
(defun returntoread (list1 res)
        (readusers list1 (car res) (cdr res))
)

;эта функция получает сообщение от пользователя
;и вызывает функцию изменяющую (если нужно) текущий список пользователей
;на вход функция получает:
;экзмепляр класса SExpressionMakeSocket,
;пару из дискриптора и экземпляра SExpressionStreanWork
;и список пользователей, который конструируется 
(defun getcorrecturn (serv pair list1)
  (changelist (car pair) (getmessages serv (car pair) (cdr pair)) list1)
)

;функция добавляет пользователя в новый констуируемы спискок,
;если первый элемент списка отличен от -1
;получает на вход:
;дискриптор пользователя, результат работы функции getmessages
;и новый формируемый список пользователей
(defun changelist (disk res list1)
  (cond
    ((eql -1 (car res)) (cons (third res) list1))
    (t (cons (third res) (cons (cons disk (second res)) list1)))
  )
)

;функция старается установить соединение с пользователем, если
;есть побобный запрос
;иначе - начинает всю работу заново
;функция получает на вход:
;результат работы функции haveapplications,
;экземпляра класса SExpressionStreanWork и текущий список пользователей
(defun thirdstep (res serv list1)
  (cond
    ((eql res 1) (getit (acceptusers serv) list1))
    (t (workcycle serv list1))
  )
)

;функция проводит работы, добавляющие нового пользователя во внутренние структуры
;на вход она получает:
;результат работы функции acceptusers и текущий список пользователей
(defun getit (res list1)
  (workcycle (cadr res) (haveaccepted (car res) list1))
)

;функция не будет ничего делать, если пользователь принят не был
;(например, все места уже заняты),
;а если все прошло успешно вызовет функцию insertuser
;принимает на вход дискриптор, ассоциированный с новым пользователем
;и текущий список пользователей
(defun haveaccepted (disk list1)
  (cond
    ((eql -1 disk) list1)
    (t (insertuser disk list1))
  )
)

;эта функция вставляет в текущий список нового пользователя
;она создает точечную пару из дискриптора и экземпляра класса SExpressionStreamWork
;и вставляет ее в текущий список пользователей
(defun insertuser (disk list1)
  (cons (cons disk (getturner disk)) list1)
)

