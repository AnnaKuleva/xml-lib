#include "xsltlib.h"
#include "xslt_symb.h"

#define ASSERT_PARAMC(n) \
	INTELIB_ASSERT(params <= n, IntelibX_too_many_params(params));\
	INTELIB_ASSERT(params >= n, IntelibX_too_few_params(params));

//this function gets three params
//the first - name of xml-file
//the second - name of xslt-file
//the third - name of result file
//this function makes xslt-transfromations and writes result in needed file
SReference xslt_transfomration_xml(int params, const SReference *param){
	ASSERT_PARAMC(3);
	SExpressionString *str = param[0].DynamicCastGetPtr<SExpressionString>();
	SExpressionString *str1 = param[1].DynamicCastGetPtr<SExpressionString>();
	SExpressionString *str2 = param[2].DynamicCastGetPtr<SExpressionString>();
	if(str==NULL || str1==NULL)
		throw IntelibX("Error!: in xslt_transformation. Not names of files\n");
	printf("xslttest start\n");
	static LListConstructor L;
	define_xml_functions();
        define_attr_functions();
        define_string_functions();
        define_number_functions();
        define_file_functions();
        define_print_functions();
	LispInit_xslttransform();
	SStreamStdout lstdout;
	(L|TRANSFORM_XML, str, str1, str2).Evaluate();
	printf("it's ended\n");
	return true;
}

void definde_xslt_function(){
	XSLT_TRANSFORMATION->SetFunction(xslt_transfomration_xml);
}
