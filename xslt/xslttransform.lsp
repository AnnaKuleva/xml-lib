(%%%
	(MODULE-NAME "xslttransform")
	(DECLARE-PUBLIC start_function)
	(DECLARE-PUBLIC transform_xml)
	(DECLARE-PUBLIC start)
        (DECLARE-USED-MODULE "../xml/symb.h")
	(DECLARE-EXTERNAL isstring)
	(DECLARE-EXTERNAL isxmltag)
	(DECLARE-EXTERNAL newtag)
	(DECLARE-EXTERNAL xmlnameget)
	(DECLARE-EXTERNAL inscont)
	(DECLARE-EXTERNAL getcontent)	
	(DECLARE-EXTERNAL xmlattrget)
	(DECLARE-EXTERNAL getattrname)
	(DECLARE-EXTERNAL getattrcontent)
	(DECLARE-EXTERNAL printinfilexml)
	(DECLARE-EXTERNAL turnopenfile)
	(DECLARE-EXTERNAL concatstr)
	(DECLARE-EXTERNAL insreadyattr)
	(DECLARE-EXTERNAL newattr)
	(DECLARE-EXTERNAL onlyname)
	(DECLARE-EXTERNAL parsexpath)
	(DECLARE-EXTERNAL newattrlist)
	(DECLARE-EXTERNAL xmlattrget)
	(DECLARE-EXTERNAL copytag)
	(DECLARE-EXTERNAL numbfromstr)
)
		
;_____________________________________________________
;____________________start&end_functions______________
;_____________________________________________________

;this function returns sexpression-tag, which is constructed from file
;file - it's correct file
;return value - xml-tree
(defun start (file)
	(turnopenfile file)
)

;this function gets three params
;the fisrt one - begin_file - name of file, where xml-tree is saved
;the second - xslt_file - name of file, where xslt-transformation is saved
;the third - result_file - name of file, where result will be saved
(defun transform_xml (begin_file xslt_file result_file)
	(start_function (start begin_file) (start xslt_file) result_file)
)

;this function gets xml-tree and xslt-tree
;is starts main work
;xml - it's xml-tree
;xslt - it's xslt-tree
;file_name - it's name of file, in which results of transformations would be written
;return value - t/nil
(defun start_function (xml xslt file_name)
	(insert_path (car xml))
	(let ((transform (start_transformation (car xslt) (car xml))))
	(cond
		(transform (printinfilexml transform file_name "w") t)
		(t nil)
	))
)

;this function starts main transform work
;xml - it's xml-tree
;xslt - it's xslt-tree
;return value - transformed tag/nil
(defun start_transformation (xslt xml)
	(let ((xslt_table (find_xml_name xslt "xsl:stylesheet")))
	(cond
		(xslt_table (find_start_temp (getcontent xslt_table) (xmlnameget xml) xml))
		(t (print "Error!: no xsl:stylesheet tag in xslt file") nil)
	))
)

;_____________________________________________________
;______________xslt_transforming_functions____________
;_____________________________________________________

;this function constructs new xml-tag
;xslt_temp - xslt-template
;xml - current xml-tag
;xslt - it is xslt-transformations table (we need it ti revoke transformations)
;return value - new xml tag
(defun construct_new_tag (xslt_temp xml xslt)
	(cond
		((null xslt_temp) nil)
		((isstring xslt_temp) xslt_temp)
		((isxmltag xslt_temp) (analis_xslt_temp xslt_temp xml xslt))
		(t (cons (construct_new_tag (car xslt_temp) xml xslt) (construct_new_tag (cdr xslt_temp) xml xslt)))
	)
)

;this function analisis xslt-template's (they can be lists, strings, tags)
;xslt_temp - that can be string, list, xml-tag
;xml - it's current xml
;xslt_table - it is xslt-transformations table (we need it to revoke transformations)
;this function analis template:
;if it is xslt-template - if finds correct function to it
;if it doesn't - constructs new tag
;result - new xml tag / list of new values / string
(defun analis_xslt_temp (xslt_temp xml xslt_table)
	(cond
		((null xslt_temp) nil)
		((isstring xslt_temp) xslt_temp)
		((isxmltag xslt_temp) (analis_xslt_tag xslt_temp xml xslt_table))
		(t (cons (analis_xslt_temp (car xslt_temp) xml xslt_table) (analis_xslt_temp (cdr xslt_temp) xml xslt_table)))
	)
)

;this function analisis xslt-template's tags
;xslt_temp - that must be tag
;xml - it's current xml
;xslt_table - it is xslt-transformations table (we need it to revoke transformations)
;this function analis template:
;if it is xslt-template - if finds correct function to it
;if it doesn't - constructs new tag
;result - new xml tag
(defun analis_xslt_tag(xslt_temp xml xslt_table)
	(let ((xslt (xmlnameget xslt_temp)))
	(cond
		((eql xslt "xsl:for-each") (for-each xslt_temp xml xslt_table))
		((eql xslt "xsl:value-of") (value_of xslt_temp xml))
		((eql xslt "xsl:copy-of") (copy_of xslt_temp xml))
		((eql xslt "xsl:apply-templates") (apply_template xslt_temp xml xslt_table))
		(t (inscont (newattrlist (newtag (xmlnameget xslt_temp))(xmlattrget xslt_temp))
			(construct_new_tag (getcontent xslt_temp) xml xslt_table))
		)
	))
)

;this function works with template anl list of xml-tags which must be transforemed
;xslt - it's xslt-template, and it must be tag
;xml - it's list of xml-tags (maybe strings)
;xslt_table - it is xslt-transformations table (we need it to revoke transformations)
;return value - list of transformed xml-tags
(defun analis_xslt_temp_list (xslt xml xslt_table)
	(cond
		((null xml) nil)
		(t (help_analis_list_temp xslt xml xslt_table))
	)
)

;this is help function
;xslt - it's xslt-template, and it must be tag
;xml - it's list of xml-tags (maybe strings)
;xslt_table - it is xslt-transformations table (we need it to revoke transformations)
;return value - list of transformed xml-tags
(defun help_analis_list_temp (xslt xml xslt_table)
	(let ((head (analis_xslt_temp xslt (car xml) xslt_table)) (tail (analis_xslt_temp_list xslt (cdr xml) xslt_table)))
	(cond
		(head (cons head tail))
		(t tail)
	))
)

;this function works with xsl:for-each template
;xslt - it's xsl:for-each template
;xml - it's current xml-tag
;xslt_table - it is xslt-transformations table (we need it to revoke transformations)
;return value - transformed xml-tag / nil
(defun for-each (xslt xml xslt_table)
	(let ((attr (find_attribute "select" (xmlattrget xslt))))
	(cond
		((null attr) (print "Error!:no attribute select in xslt-template xsl:for-each"))
		;this template always works with list of xml-tags - so we must call analis_xslt_temp_list
		(t (analis_xslt_temp_list (getcontent xslt) (xpath_start (getattrcontent attr) xml nil) xslt_table))
	))
)

;this funcion will work with xsl:value-of template
;xslt - it's xsl:value-of template
;xml - it's current xml-tag
;return value - string, if everything is ok
;nil - otherwise
(defun value_of (xslt xml)
	(let ((attr (find_attribute "select" (xmlattrget xslt))))
	(cond
		((null attr)(print "Error!:no attribute select in xslt-template xsl:value-of") nil)
		;((eql (getattrcontent attr) ".") (get_text xml ""))
		(t (get_text (xpath_start (getattrcontent attr) xml t) ""))
	))
)

;this function gets all text from xml-tag
;xml - it's xml tag
;str - it's constructed string
;return value - result string
(defun get_text (xml str)
	(cond
		((null xml) str)
		((isstring xml) (concatstr str xml))
		((isxmltag xml) (get_text (getcontent xml) str))
		(t (get_text (cdr xml) (get_text (car xml) str)))
	)
)

;this function copies needed xml-tag
;xslt - it's xsl:copy-of template
;xml - it's current xml-tag
;return value - xml-tag, if everything is ok
;otherwise will be error, because copytag needs xml-tag
(defun copy_of (xslt xml)
	(let ((attr (find_attribute "select" (xmlattrget xslt))))
	(cond
		((null attr)(print "Error!:no attribute select in xslt-template xsl:copy-of") nil)
		;((eql (getattrcontent attr) ".") (delete_attr (copytag xml) "path"))
		(t (delete_attr (copytag (xpath_start (getattrcontent attr) xml t)) "path"))
	))
)

;this function will work with xsl:apply-template
;the <xsl:apply-templates> element applies a template
;to the current element or to the current element's child nodes.
;xslt - it's xsl:copy-of template
;xml - it's current xml-tag
;xslt_table - it is xslt-transformations table (we need it to revoke transformations)
(defun apply_template (xslt xml xslt_table)
	(let ((attr (find_attribute "select" (xmlattrget xslt))))
	(cond
		;no select - all elements of current xml-node
		((null attr) (revoke_transformation (getcontent xml) xslt_table))
		(t (revoke_transformation (xpath_start (getattrcontent attr) xml nil) xslt_table))
	))
)

;____________________________________________________
;____________________xpath_functions_________________
;____________________________________________________

;this function starts searching all xml-tags which
;are needed according to path_expr
;path_expr - that is path expression
;xml - it's current xml-tag
;one - for some templates only one xml-tag is needed
;if such element is true - that means - only one
;return value - xml-tag / list of xml-tags / nil
(defun xpath_start (path_expr xml one)
	(cond
		((eql path_expr ".") xml)
		;later here will be function which finds in path predicates and ect.
		(one (find_xml_tag xml path_expr))
		(t (find_xml_list xml path_expr))
	)
)

;_____________________________________________________
;____________________search_functions_________________
;_____________________________________________________

;this function searches xml-tag with needed name (only the first one)
;xml - xml-tag
;name - name of needed xml-tag
;return value - xml-tag/nil
(defun find_xml_name(xml name)
	(cond
		((null xml) nil)
		((isstring xml) nil)
		((and (isxmltag xml) (eql name (xmlnameget xml))) xml)
		((isxmltag xml) (find_xml_name (getcontent xml) name))
		(t (help_find_xml_name xml name))
	)
)

;this is help function
;xml - list of xml tags/strings
;name - name of needed xml-tag
;return value - xml-tag/nil
(defun help_find_xml_name (xml name)
	(let ((head_res (find_xml_name (car xml) name)))
	(cond
		(head_res head_res)
		(t (find_xml_name (cdr xml) name))
	))
)

;this function searches xml-tags with needed name (all of them)
;xml - xml-tag
;name - name of needed xml-tag
;return value - list of xml-tag/nil
(defun find_xml_l_name(xml name)
	(cond
		((null xml) nil)
		((isstring xml) nil)
		((and (isxmltag xml) (eql name (xmlnameget xml)))
			(cons xml (find_xml_l_name (getcontent xml) name))
		)
		((isxmltag xml) (find_xml_l_name (getcontent xml) name))
		(t (help_find_l_xml_name xml name))
	)
)

;this is help function
;xml - list of xml tags/strings
;name - name of needed xml-tag
;return value - list of xml-tag/nil
(defun help_find_l_xml_name (xml name)
	(let ((head_res (find_xml_l_name (car xml) name)))
	(cond
		(head_res (append head_res (find_xml_l_name (cdr xml) name)))
		(t (find_xml_l_name (cdr xml) name))
	))
)

;this function searches xml-tag with needed path (only the first one)
;xml - xml-tag
;path - path of needed xml-tag
;return value - xml-tag/nil
(defun find_xml_path(xml path)
	(cond
		((null xml) nil)
		((isstring xml) nil)
		((and (isxmltag xml) (analis_attribute xml "path" path)) xml)
		((isxmltag xml) (find_xml_path (getcontent xml) path))
		(t (help_find_xml_path xml path))
	)
)

;this function searches xml-tag with needed path (all tags)
;xml - xml-tag
;path - path of needed xml-tag
;return value - list of xml-tags/nil
(defun find_xml_l_path(xml path)
	(cond
		((null xml) nil)
		((isstring xml) nil)
		((and (isxmltag xml) (analis_attribute xml "path" path))
			(cons xml (find_xml_l_path (getcontent xml) path))
		)
		((isxmltag xml) (find_xml_l_path (getcontent xml) path))
		(t (help_find_xml_l_path xml path))
	)
)

;this is help function
;xml - list of xml-tags/strings
;path - path of needed xml-tag
;return value - xml-tag/nil
(defun help_find_xml_path (xml path)
	(let ((head_res (find_xml_path (car xml) path)))
	(cond
		(head_res head_res)
		(t (find_xml_path (cdr xml) path))
	))
)


;this is help function
;xml - list of xml-tags/strings
;path - path of needed xml-tag
;return value - list of xml-tags/nil
(defun help_find_xml_l_path(xml path)
	(let ((head_res (find_xml_l_path (car xml) path)))
	(cond
		(head_res (append head_res (find_xml_l_path (cdr xml) path)))
		(t (find_xml_l_path (cdr xml) path))
	))
)

;this function searches xslt-template with needed path/name (match = path)
;xslt_temp - it's list of xslt-templates. They are elemnts of content of xsl:stylesheet
;return value - it's xslt-template from which we will start analis
;return value - nil, if there isn't xslt-template with needed path
(defun find_needed_temp(xslt_temp name)
	(cond
		((null xslt_temp) nil)
		((analis_attribute (car xslt_temp) "match" name) (car xslt_temp))
		(t (find_needed_temp (cdr xslt_temp) name))
	)
)

;this function finds start template
;xslt_temp - list of xslt-templates
;xml_name - name of root xml-tag
;xml - xml-tree
;return value - function returns new xml-tag, if everything is ok
;otherwise - returns nil
(defun find_start_temp(xslt_temp xml_name xml)
	(let ((path (find_needed_temp xslt_temp "/")) (name (find_needed_temp xslt_temp xml_name)))
	(print path)
	(print name)
	(cond
		(path (construct_new_tag (getcontent path) xml xslt_temp))
		(name (construct_new_tag (getcontent name) xml xslt_temp))
		(t (print "Error!:no start template") nil)
	))
)

;this function revokes xslt-transformations
;it can be called from xsl:apply-template
;xml - list of xml-tags / xml-tag
;xslt - xslt-transformation table
;return value - list of transforemed xml-tags / transforemed xml-tag
(defun revoke_transformation (xml xslt)
	(cond
		((null xml) nil)
		((isstring xml) nil)
		((isxmltag xml) (revoke_trans_help xml (xmlnameget xml) xslt))
		(t (cons (revoke_transformation (car xml) xslt) (revoke_transformation (cdr xml) xslt)))
	)
)

;this function is revoke_transformation's help-function
;xml - xml-tag (atom)
;xslt - xslt-transformation table
;return value - result of transformations, if everything is ok 
(defun revoke_trans_help (xml xml_name xslt)
	(let ((temp (find_needed_temp xslt xml_name)))
	(cond
		(temp (construct_new_tag (getcontent temp) xml xslt))
		(t (print "Error!:no needed template"))
	))
)

;this function finds needed xml-tag (only the first one)
;find_ent - it's path of needed xml-tag, or name 
;xml - xml-tree
;reutrn value - nil/needed xml-tag
(defun find_xml_tag(xml find_ent)
	(let ((is_path (onlyname (parsexpath find_ent))))
	(cond
		(is_path (find_xml_name xml find_ent))
		(t (find_xml_path xml find_ent))
	))
)

;this function finds needed xml-tag (all of them)
;find_ent - it's path of needed xml-tag, or name 
;xml - xml-tree
;reutrn value - nil/list of needed xml-tags
(defun find_xml_list(xml find_ent)
	(let ((is_path (onlyname (parsexpath find_ent))))
	(cond
		(is_path (find_xml_l_name xml find_ent))
		(t (find_xml_l_path xml find_ent))
	))
)
		
;_____________________________________________________
;_________functions_for_working_with_attributes_______
;_____________________________________________________

;function finds needed xml-attribure and tests it's value
;tag - it's tag, whose attribute we will analis
;name - name of needed attribute
;value - value of needed attribute
;return value - t, if we find needed tag and it has correct value
;return value - nil, otherwise
(defun analis_attribute (tag name value)
	(let ((attr (find_attribute name (xmlattrget tag))))
	(cond
		((null attr) nil)
		((eql (getattrcontent attr) value) t)
		(t nil) 
	))
)

;this function finds needed attribute
;name - name of needed attribute
;attr - list of attributes
;return value - function returns nil, if there isn't needed attribute
;otherwise - returns xml-attribute
(defun find_attribute (name attr)
	(cond
		((null attr) nil)
		((eql (getattrname (car attr)) name) (car attr))
		(t (find_attribute name (cdr attr)))
	)
)

;this function inserts path-attribute in every xml-tag
;xml - xml-tag
;cur_path - current path
(defun create_path(xml cur_path)
	(cond
		((null xml) nil)
		((isstring xml) xml)
		((isxmltag xml) (insreadyattr xml (newattr "path" (concatstr (concatstr cur_path "/") (xmlnameget xml))))
			(create_path (getcontent xml) (concatstr (concatstr cur_path "/") (xmlnameget xml)))
		)
		(t (create_path (car xml) cur_path) (create_path (cdr xml) cur_path))
	)
)

;this function inserts path in every tag. in root tag we insert - "/"
;XPath needs tags' paths
;xml - current xml tree
(defun insert_path(xml)
	(insreadyattr xml (newattr "path" "/"))
	(create_path (getcontent xml) (+ "/" (xmlnameget xml)))
)

;this function deletes needed-attr from every tag
;we need such function, for example in xsl:copy-of and after all work
;xml - xml-tree
;name - name of deleting attr
(defun delete_attr(xml name)
	(cond
		((null xml) nil)
		((isstring xml) xml)
		((isxmltag xml)
			(delete_attr (getcontent xml) name)(newattrlist xml (construct_without_attr (xmlattrget xml) name))
		)
		(t (delete_attr (car xml) name) (delete_attr (cdr xml) name))
	)
)

;this function constructs new attr-list without needed attr
;attr - attribute-list
;name - name of deleting attribute
(defun construct_without_attr (attr name)
	(cond
		((null attr) nil)
		((eql (getattrname (car attr)) name) (cdr attr))
		(t (cons (car attr) (construct_without_attr (cdr attr) name)))
	)
)
