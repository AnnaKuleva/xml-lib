#ifndef _XSLT_TRANSFORM_LIB_H_KURS5_
#define _XSLT_TRANSFORM_LIB_H_KURS5_

#include <intelib/sexpress/sexpress.hpp>
#include <intelib/sexpress/iexcept.hpp>
#include <intelib/sexpress/sstring.hpp>
#include <intelib/tools/sstream.hpp>
#include <intelib/lisp/lisp.hpp>
#include <intelib/lisp/lsymbol.hpp>
#include <intelib/lfun_std.hpp>
#include "../xml/xmlsexpress.h"
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>

#include "../xml/lispint.h"
#include "xslttransform.hxx"
#include "../dtd/dtdlib.h"

SReference xslt_transfomration_xml(int params, const SReference *param);

void definde_xslt_function();

#endif
