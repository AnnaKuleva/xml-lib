#include <intelib/sexpress/sexpress.hpp>
#include <intelib/sexpress/iexcept.hpp>
#include <intelib/sexpress/sstring.hpp>
#include <intelib/tools/sstream.hpp>
#include <intelib/lisp/lisp.hpp>
#include <intelib/lisp/lsymbol.hpp>
#include <intelib/lfun_std.hpp>

#include "../xml/lispint.h"
#include "../dtd/dtdlib.h"
#include "xslttransform.hxx"

int main(int argc, char* argv[]){
	LListConstructor L;
	if(argc<4 || argc>4){
		printf("uncorrect number of parameters\n");
		printf("xml_file xslt_file name_of_result_file\n");
		return 0;
	}
	try{
		printf("xslttest start\n");
		SStreamStdout lstdout;
		define_xml_functions();
		define_attr_functions();
		define_string_functions();
		define_file_functions();
		define_dtd_functions();

		LispInit_xslttransform();
		SReference xml=(L|START, argv[1]).Evaluate();
		SReference xslt=(L|START, argv[2]).Evaluate();
		(L|START_FUNCTION, ~xml, ~xslt, argv[3]).Evaluate();
		/*SReference xml=(L|START, "test/xslttest.xml").Evaluate();
                SReference xslt=(L|START, "test/xsltfile.txt").Evaluate();
                (L|XSLTSTART, ~xml, ~xslt, "test/resultxslt.xml").Evaluate();*/

		printf("it's ended\n");
	}
	catch(IntelibX &x){
		printf("\nCaught IntelibX: %s\n", x.Description());
		if(x.Parameter().GetPtr()){
				printf("%s\n", x.Parameter()->TextRepresentation().c_str());
		}
		if(x.Stack().GetPtr()){
			printf("%s\n", x.Stack()->TextRepresentation().c_str());
		}
	}
	catch(const char *s){
		printf("%s\n", s);
	}
	catch(...){
		printf("Something strange caught\n");
	}
	return 0;
}

